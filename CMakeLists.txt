cmake_minimum_required(VERSION 3.2)
project(eadlib)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a -pthread")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build/lib")
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build/release")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/build/bin")
set(CMAKE_EXTERNAL_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/external")

include(${CMAKE_ROOT}/Modules/ExternalProject.cmake)

#####################################
# Packaging for header-only release #
#####################################
set(EADLIB_VERSION_MAJOR "0")
set(EADLIB_VERSION_MINOR "1")
set(EADLIB_VERSION_PATCH "2")
set(EADLIB_VERSION_STATUS "a")
set(PROJECT_VERSION "${EADLIB_VERSION_MAJOR}.${EADLIB_VERSION_MINOR}.${EADLIB_VERSION_PATCH}${EADLIB_VERSION_STATUS}")
set(ARCHIVE_TARGET_DIRECTORY "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/${PROJECT_VERSION}")
file(MAKE_DIRECTORY ${ARCHIVE_TARGET_DIRECTORY})
set(ARCHIVE_TARGET "${ARCHIVE_TARGET_DIRECTORY}/${CMAKE_PROJECT_NAME}.tar.gz")

add_custom_target(Package-EADLib
        COMMENT ">> Packaging EADlib into '${ARCHIVE_TARGET}'"
        COMMAND tar "czf" ${ARCHIVE_TARGET} -C ${CMAKE_CURRENT_SOURCE_DIR}/src .
        COMMAND md5sum ${ARCHIVE_TARGET} > ${ARCHIVE_TARGET_DIRECTORY}/md5.txt)

if(EXISTS ${ARCHIVE_TARGET})
    message(">> Computing md5 hash for '${ARCHIVE_TARGET}'...")
    file(MD5 ${ARCHIVE_TARGET} MD5_HASH)
    message(">> MD5: ${MD5_HASH}")
endif ()

#####################
# SQLite Dependency #
#####################
set(SQLITE_RELEASE_VERSION "3190200")
set(SQLITE_RELEASE_SHA1 "ed35829ac78019528556809f41112b28a5d31e70")
set(SQLITE_RELEASE_URL "https://www.sqlite.org/2017/sqlite-amalgamation-${SQLITE_RELEASE_VERSION}.zip")
set(SQLITE_ZIP_FILENAME "sqlite-amalgamation-${SQLITE_RELEASE_VERSION}.zip")
set(SQLITE_DOWNLOAD_PATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/${SQLITE_ZIP_FILENAME}")
set(SQLITE_EXTRACTED_FILEPATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/sqlite3")

if(NOT EXISTS ${SQLITE_EXTRACTED_FILEPATH})
    message(">> SQLite dependency missing. Downloading...")

    file(DOWNLOAD "${SQLITE_RELEASE_URL}" "${SQLITE_DOWNLOAD_PATH}"
            STATUS status
            #SHOW_PROGRESS 1
            EXPECTED_HASH SHA1=${SQLITE_RELEASE_SHA1})

    list(GET status 0 status_code)
    if(NOT status_code EQUAL 0)
        message(FATAL_ERROR "Could not download SQlite dependency package.")
    else()
        message(">> SQLite package downloaded.")
        file(MAKE_DIRECTORY ${SQLITE_EXTRACTED_FILEPATH})
    endif()

    execute_process(
            COMMAND ${CMAKE_COMMAND} -E tar xzf ${SQLITE_DOWNLOAD_PATH} strip-components=1
            WORKING_DIRECTORY ${SQLITE_EXTRACTED_FILEPATH})

    set(SQLITE_UNZIPPED_VERSIONED_FOLDER "${SQLITE_EXTRACTED_FILEPATH}/sqlite-amalgamation-${SQLITE_RELEASE_VERSION}")
    file(GLOB SQLITE_SOURCE "${SQLITE_UNZIPPED_VERSIONED_FOLDER}/sqlite3.*")
    file(COPY ${SQLITE_SOURCE} DESTINATION ${SQLITE_EXTRACTED_FILEPATH})
    file(REMOVE_RECURSE ${SQLITE_UNZIPPED_VERSIONED_FOLDER})
    file(REMOVE ${SQLITE_DOWNLOAD_PATH})
else()
    message(">> Found SQLite dependency folder.")
endif()

set(SQLITE3_FILES
        external/sqlite3/sqlite3.c
        external/sqlite3/sqlite3.h)

add_library(sqlite3 STATIC ${SQLITE3_FILES})
SET_TARGET_PROPERTIES(sqlite3 PROPERTIES LINKER_LANGUAGE CXX)
TARGET_LINK_LIBRARIES(sqlite3 dl)

include_directories(external)

############################
# Generating documentation #
############################
option(BUILD_DOC "Build documentation" ON)
find_package(Doxygen)

if (DOXYGEN_FOUND)
    add_custom_target(Documentation ALL
            COMMENT ">> Building documentation..."
            COMMAND ${CMAKE_CURRENT_SOURCE_DIR}/make_docs.sh
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/
    )
else (DOXYGEN_FOUND)
    message(SEND_ERROR ">> Doxygen need to be installed to generate the documentation")
endif (DOXYGEN_FOUND)

##########
# EADlib #
##########
set(SOURCE_FILES
        src/cli/progress_bar.h
        src/datastructure/Bag.h
        src/datatype/Coordinate2D.h
        src/datatype/Coordinate3D.h
        src/datastructure/AVLTreeNode.h
        src/datastructure/BinaryTreeNode.h
        src/datastructure/GenericNodePair.h
        src/datastructure/LinkedListNode.h
        src/datastructure/AVLTree.h
        src/datastructure/BinaryTree.h
        src/datastructure/BST.h
        src/datastructure/Graph.h
        src/datastructure/Heap.h
        src/datastructure/LinearList.h
        src/datastructure/LinkedList.h
        src/datastructure/Matrix.h
        src/datastructure/MaxHeap.h
        src/datastructure/MinHeap.h
        src/exception/duplication.h
        src/exception/undefined.h
        src/exception/unsupported.h
        src/exception/resource_missing.h
        src/exception/corruption.h
        src/fileops/fileops_utils.h
        src/fileops/Settings.h
        src/testing/equalish.h
        src/testing/exec_time.h
        src/testing/threader.h
        src/chrono/chrono.h
        src/toolbox/coordinates/coordinates.cpp
        src/toolbox/coordinates/coordinates.h
        src/math/math.h
        src/math/shape/Circle.h
        src/math/Fraction.h
        src/math/geo/planar.h
        src/math/geo/spatial.h
        src/math/geo/trigonometry.h
        src/tool/Convert.h
        src/datatype/Coordinate.h
        src/algorithm/Tarjan.h
        src/datastructure/WeightedGraph.h
        src/cli/parser/Parser.h
        src/cli/parser/ParserOption.h
        src/cli/parser/ParserValue.h
        src/io/FileReader.h
        src/io/FileStats.h
        src/io/FileWriter.h
        src/cli/graphic/ProgressBar.h
        src/wrapper/SQLite/SQLite.h
        src/wrapper/SQLite/TableDB.h
        src/wrapper/SQLite/TableDBCell.h
        src/wrapper/SQLite/TableDBRow.h
        src/wrapper/SQLite/TableDBCol.h
        src/wrapper/SQLite/TableDBCursor.h
        src/math/geo/geo.h
        src/algorithm/sort.h
        src/algorithm/algorithm.h
        src/datastructure/GenericNode.h
        src/interface/IPrinter.h
        src/tool/Progress.h
        src/interface/IObserver.h
        src/exception/aborted_operation.h
        src/exception/bad_assignment.h
        src/datatype/enum.h
        src/datastructure/DMDGraph.h
        src/datastructure/GraphEdgeInfo.h
        src/datastructure/ObjectPool.h
        src/exception/resource_unavailable.h
        src/string/string.h
        src/cli/braille/braille.h
        src/cli/braille/graph/graph.h
        src/cli/braille/BrailleBitMapper.h
        src/cli/braille/BraillePlot.h
        src/cli/braille/graph/NumLabelProperty.h
        src/cli/braille/graph/MetaGraphData.h
        src/cli/braille/raster/raster.h
        src/cli/braille/graph/labelling/labelling.h
        src/datatype/Integer.h)

set(LOG_MODULE
        src/logger/Logger.h
        src/logger/log_configuration/OutputConfiguration.h
        src/logger/log_formatters/Formatter_Terminal.h
        src/logger/log_formatters/Formatter.h
        src/logger/log_configuration/TimeStamp.h
        src/logger/log_configuration/LogConfig.h
        src/logger/log_outputs/LogOutput.h
        src/logger/log_configuration/LogLevel_types.h
        src/logger/log_outputs/LogOutput_FileAppend.h
        src/logger/log_outputs/LogOutput_FileOverwrite.h
        src/logger/log_outputs/LogOutput_FileNew.h
        src/logger/log_formatters/Formatter_types.h
        src/logger/log_outputs/LogOutput_types.h
        src/logger/log_outputs/LogOutput_Terminal.h
        src/logger/log_formatters/Formatter_ColourTerminal.h)

#####################
# Google Unit-Tests #
#####################
find_package(GTest QUIET)

if( NOT GTEST_FOUND )
    message(">> GoogleTest package not found. Getting it from git repo...")

    set(GTEST_DOWNLOAD_PATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/googletest")

    ExternalProject_Add(googletest
            PREFIX ${GTEST_DOWNLOAD_PATH}
            GIT_REPOSITORY https://github.com/google/googletest.git
            CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${GTEST_DOWNLOAD_PATH})

    include_directories(${GTEST_DOWNLOAD_PATH}/include)
    link_directories(${GTEST_DOWNLOAD_PATH}/lib)
else()
    message(">> GoogleTest package found. Using local version.")
    include_directories(${GTEST_INCLUDE_DIRS})
endif()

set(TEST_FILES
        tests/datastructure/Bag_test.h
        tests/datatype/Coordinate2D_test.h
        tests/datatype/Coordinate3D_test.h
        tests/datastructure/AVLTree_test.h
        tests/datastructure/BinaryTree_test.h
        tests/datastructure/BST_test.h
        tests/datastructure/Graph_test.h
        tests/datastructure/Heap_test.h
        tests/datastructure/LinearList_test.h
        tests/datastructure/LinkedList_test.h
        tests/datastructure/Matrix_test.h
        tests/logger/Logger_test.h
        tests/toolbox/coordinates/coordinates_test.h
        tests/math/shape/Circle_test.h
        tests/math/Fraction_test.h
        tests/math/Matrix_test.h
        tests/math/geo/planar_test.h
        tests/math/geo/spatial_test.h
        tests/math/geo/trigonometry_test.h
        tests/main.cpp
        tests/threader.h
        tests/datatype/Coordinate_test.h
        tests/algorithm/Tarjan_test.h
        tests/datastructure/WeightedGraph_test.h
        tests/cli/Parser_test.h
        tests/math/math_test.h
        tests/tool/Convert_test.h
        tests/io/FileReader_test.h
        tests/io/FileWriter_test.h
        tests/wrapper/SQLite/TableDB_test.h
        tests/wrapper/SQLite/TableDBCell_test.h
        tests/algorithm/sort_test.h
        tests/tool/Progress_test.h
        tests/tool/enum_test.h
        tests/datastructure/DMDGraph_test.h
        tests/datastructure/ObjectPool_test.h
        tests/string/string_test.h
        tests/cli/braille/BrailleBitMapper_test.h
        tests/cli/braille/BraillePlot_test.h
        tests/cli/braille/graph/graph_test.h
        tests/cli/braille/braille_test.h
        tests/cli/braille/raster/raster_test.h
        tests/cli/braille/graph/labelling/labelling_test.h
        tests/datatype/Integer_test.h)

enable_testing()

#########
# Build #
#########
add_library(
        eadlib
        STATIC
        ${LOG_MODULE}
        ${SOURCE_FILES}
        ${SQLITE3_FILES}
)

add_executable(
        eadlib_tests
        ${TEST_FILES}
)

if( NOT GTEST_FOUND )
    add_dependencies(eadlib_tests googletest)
endif()

SET_TARGET_PROPERTIES(eadlib PROPERTIES LINKER_LANGUAGE CXX)

target_link_libraries(eadlib_tests gtest sqlite3)
add_test(eadlib_tests ${TEST_FILES})