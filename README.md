[TOC]


# Description

This is a personal modern (C++20) library comprising of various bits
and pieces found to have been useful enough during other personal project 
development and learning.

These various bits have been adapted into library components alongside other
features developed directly within the library. There is some very experimental
stuff subject to change/removal in there.

The code here is essentially for my personal use so interfaces will be subject 
to changes and code may not always be at production quality despite some effort
to make things work and tests them. In short: there be dragons so beware! 

Feel free to learn from my mistakes and/or use any of the more stable 
and tested components that may be of use to you. 

As all components have a list of dependencies you can just copy over the header 
files and use them as such.

I would ask just that you put a link to this repo + accreditation in your README 
file if you do though.

I'm happy to get bug reports. PRs; less so.

An7ar35


# The Library


___
## Algorithms

`eadlib::algo::`

#### sort.h

- Insertion sort
- Selection sort
- Bubble sort

#### Tarjan

`eadlib::algo::Tarjan<T>`

Implementation of the Tarjan algorithm for finding Strongly Connected Components
inside a Graph (`eadlib::Graph<T>`).


___
## Time Keeping

#### chrono.h

`eadlib::chrono::`

General time keeping functions are kept in this namespace.


___
## Command Line Interface (CLI)

#### Parser

`eadlib::cli::Parser`

CLI parser system to process program arguments from the command line.

The Parser uses regular expression to check argument values passed to it.
If any value failing its regular expression check during the parsing will 
trigger its associated error message and the parsing method with return false.

If all values check out then true is returned and anything parsed can be queried.

The Parser will raise an `std::regex_error` if a regular expression is malformed.


#### cli/braille/braille.h

`eadlib::cli::braille::`

Namespace for all generic braille related methods/classes.


#### cli/braille/graph.h

`eadlib::cli::braille::graph::`

Namespace for all generic braille graphing related methods.


#### Braille bit-mapper

`eadlib::cli::braille::BrailleBitMapper`

Uses unicode braille characters to display simple graphics in the terminal based on a 
monochrome bitmap. 

[Article available](https://an7ar35.bitbucket.io/pdf/articles/cs_bite/2019-01%20-%20Bitmap%20in%20the%20terminal%20using%20braille%20characters.pdf)

#### Braille Graph Plotter

`eadlib::cli::braille::BraillePlot`

Uses the unicode BrailleBitMapper to create CLI displayable XY plot charts.

[Article available](https://an7ar35.bitbucket.io/pdf/notes/2019-01%20-%20Creating%20a%20Plot%20Graph%20with%20Braille%20Characters.pdf)

#### ProgressBar

`eadlib::cli::ProgressBar`

Simple console progress bar implementing an observer interface.



___
## Data Structures

#### AVL Tree

`eadlib::AVLTree<K,V>`

Balanced binary tree where the heights of every node differ at most by +/- 1.

#### Bag

`eadlib::Bag<T>`

Randomised (`rand()`) access container. 

#### Binary Tree

`eadlib::BinaryTree<K,V>`

Binary tree data structure.

#### Binary Search Tree (BST)

`eadlib::BST<K,V>`

Binary search tree data structure.

#### DMDGraph

`eadlib::DMDGraph<K,V,R>`

Directed-multi-dimensional graph data structure able to hold multiple edge types
that are modelled in terms of dimensions.

__e.g.__: Nodes modelling people, edges modelling the relationship and the edge type
modelling the relationship type of the edge (friend, colleagues, parent/children, etc...)

#### Graph

`eadlib::Graph<T>`

Directed graph structure implemented using adjacency lists.
Keeps track of both parent and children of each nodes.

#### Multi-Graph

`eadlib::WeightedGraph<T>`

ADT Directed multi-graph (weighted) structure using adjacency lists.
Keeps track of both parents and children of each nodes.

#### Linear List

`eadlib::LinearList<T>`

A vector-like ADT linear list implementation with a growth/shrink factor of 2.
The list is implemented with a transparent interface as a circle with the 
elements set as a sliding window giving best possible O-time given placement. 
i.e. Good for deletion/insertion within a constant list size.

__e.g.__: inserting an element at the front of the list will not move all other 
items instead opting for shifting the virtual index for the beginning of the 
list down one.

#### Linked List

`eadlib::LinkedList<T>`

Double linked list implemented using a half/half raw and unique pointers. 

#### Matrix

`eadlib::Matrix<T>`

Simple 2-Dimensional matrix.

#### Heap

A linear list based Heap.

#### Max Heap

A linear list based Max-heap.

#### Min Heap

A linear list based Min-heap.

#### ObjectPool

`eadlib::ObjectPool<T>`

Simple generic object pool with pool size init and smart pointers.


___
## Data Types

#### Coordinate

`eadlib::Coordinate<TName,TValue>`

ADT coordinate class that can take in any number if named coordinates.

#### Coordinate2D

`eadlib::Coordinate2D<T>`

2-Dimensional coordinate container.

#### Coordinate3D

`eadlib::Coordinate3D`

3-Dimensional coordinate data type that include methods for calculating 
distances, differences and rotation.

#### Integer

`eadlib::Integer<N>`

Big-Endian, large signed Integer datatype class with base 2,10,16 I/O. 
Uses `std::bitset` as the underlining data structure for the 2's complement 
base 2 representation. 

___
## Exceptions

`eadlib::exception::aborted_operation` for when function had to be aborted.

`eadlib::exception::bad_assignement` 

`eadlib::exception::corruption` for when things get corrupted.

`eadlib::exception::duplication` for unexpected duplicate object/types.

`eadlib::exception::resource_missing` for absence of expected something.

`eadlib::exception::resource_unavailable` for unavailability of an of expected something.

`eadlib::exception::undefined` for undefined things.

`eadlib::exception::unsupported` for actions not (yet?) supported.



---
## Interfaces

`eadlib::interface::IObserver` for Observer pattern implementations.

`eadlib::interface::IPrinter` for passing list of arguments.

___
## I/O
#### File Reader

`eadlib::io::FileReader`

File reader wrapper to `std::ifstream` implemented to simplify the reading 
of files.

#### File Writer

`eadlib::io::FileWriter`

File writer wrapper to `std::ofstream` implemented to simplify the writing
of files.

#### File Stats \[Linux only]

`eadlib::io::FileStats`

Fetches file description from unix file systems \[POSIX].

__Notes:__ 

+ Uses Linux's implementation of the POSIX "`stat`" system command 
so Linux only.
+ BEWARE: Do not rely upon within race conditions.


___
## Logger

`eadlib::logger::Logger`

Customisable logging system with different formatting and output options.

Options are controlled through a configuration file (`log_config.cfg`) that 
is automatically generated on first run.

___
## Math

#### math/math.h

`eadlib::math::`

General math related methods are kept in this namespace.
 
#### Fraction

`eadlib::math::Fraction`

Fraction data type. Run-of-the-mill class to handle fractions.

### Geometry

#### math/geo/geo.h

`eadlib::math::geo::`

General geometry functions are kept in this namespace.

#### math/geo/planar.h

`eadlib::math::geo::planar::`

General planar geometry functions are kept in this namespace.

#### math/geo/spatial.h

`eadlib::math::geo::spatial::`

General spatial geometry functions are kept in this namespace.

#### math/geo/trigonometry.h

`eadlib::math::geo::trigonometry::`

General trigonometry functions are kept in this namespace.

### Shape

#### Circle

`eadlib::math::shape::Circle`

Circle object data type.


___
## String

#### string.h

`eadlib::string::`

General string related methods are kept in this namespace.


___
## Tools

#### Convert

`eadlib::tool::Convert`

Template data-type conversion tool. Change any type into another 
(within reasons). Uses the `std::stringstream` technique.

#### Progress

`eadlib::tool::Progress`

Progress tracker for processes. Takes in an `eadlib::interface::IObserver` 
interface to pass updates to. 

___
## Wrappers

#### SQLite

`eadlib::wrapper::SQLite`

Wrapper for the SQLite library enabling straight forward access to Database files.

The wrapper enables passing of string SQL queries to the database and retrieval
of data to a multi-type container.

The container to load database tables into is `eadlib::TableDB` . TableDB
can hold native data-types (string/boolean/integer/double) from the database 
tables thus avoiding the issue of having to cast or stream the data to get 
anything other than a string. 

__Note:__ BLOB data type is not yet supported.


# Using the Library

As it's a header-only library it only needs to have its path included in 
the `CMakeList.txt` file.

To create your own local packaged release run CMake with `Package-EADLib` 
as target. This will create a `eadlib.tar.gz` file inside a version tag
named folder (e.g.: `/0.0.1a/`) in `build/release`.

Otherwise a release package is kept on [GitHub](https://github.com/An7ar35/eadlib).
To use it in CMake you can use a script to download and extract the headers.
For example:

```cmake
cmake_minimum_required(VERSION 3.7)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++2a -pthread")
set(CMAKE_EXTERNAL_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/external")

#####################
# EADLIB dependency #
#####################
set(EADLIB_RELEASE_URL "https://github.com/An7ar35/eadlib/releases/download/0.1.1a/eadlib.tar.gz")
set(EADLIB_RELEASE_MD5 "f776512f7218c80085f4d82a86f9b06b")
set(EADLIB_TAR_FILENAME "eadlib.tar.gz")
set(EADLIB_DOWNLOAD_PATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/${EADLIB_TAR_FILENAME}")
set(EADLIB_EXTRACTED_FILEPATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/eadlib")

if(NOT EXISTS ${EADLIB_EXTRACTED_FILEPATH})
    message(">> EADlib dependency missing. Downloading...")

    file(DOWNLOAD "${EADLIB_RELEASE_URL}" "${EADLIB_DOWNLOAD_PATH}"
        STATUS status
        #SHOW_PROGRESS 1
        EXPECTED_MD5 ${EADLIB_RELEASE_MD5})

    list(GET status 0 status_code)
    if(NOT status_code EQUAL 0)
        message(FATAL_ERROR "Could not download EADlib dependency package.")
    else()
        message(">> EADlib package downloaded.")
        file(MAKE_DIRECTORY ${EADLIB_EXTRACTED_FILEPATH})
    endif()

    execute_process(
        COMMAND ${CMAKE_COMMAND} -E tar xzf ${EADLIB_DOWNLOAD_PATH}
        WORKING_DIRECTORY ${EADLIB_EXTRACTED_FILEPATH})

    file(REMOVE ${EADLIB_DOWNLOAD_PATH})
else()
    message("Found EADLIB dependency folder.")
endif()

include_directories(external)
```

You can then add dependency in your code by including the used header file.
For example: `#include <eadlib/logger/Logger.h>`

#### SQlite and dl dependency

SQlite is a required dependency for EADlib (the SQLite wrapper).
The path for the SQLite headers must included in the project and the
library linked.

For an over-the-top approach we can download the amalgamation file
archive from the SQLite site:

```cmake
######################################
# libdl dependency (requ. by SQLite) #
######################################
find_library(FOUND_DL dl REQUIRED)
if( FOUND_DL )
    message("Found DL.")
else()
    message( FATAL_ERROR, "DL library could not be found on the system. It is required as a dependency for SQlite3.")
endif()

##########
# SQLite #
##########
set(SQLITE_RELEASE_VERSION "3190200")
set(SQLITE_RELEASE_SHA1 "ed35829ac78019528556809f41112b28a5d31e70")
set(SQLITE_RELEASE_URL "https://www.sqlite.org/2017/sqlite-amalgamation-${SQLITE_RELEASE_VERSION}.zip")
set(SQLITE_ZIP_FILENAME "sqlite-amalgamation-${SQLITE_RELEASE_VERSION}.zip")
set(SQLITE_DOWNLOAD_PATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/${SQLITE_ZIP_FILENAME}")
set(SQLITE_EXTRACTED_FILEPATH "${CMAKE_EXTERNAL_OUTPUT_DIRECTORY}/sqlite3")

if(NOT EXISTS ${SQLITE_EXTRACTED_FILEPATH})
    message(">> SQLite dependency missing. Downloading...")

    file(DOWNLOAD "${SQLITE_RELEASE_URL}" "${SQLITE_DOWNLOAD_PATH}"
        STATUS status
        #SHOW_PROGRESS 1
        EXPECTED_HASH SHA1=${SQLITE_RELEASE_SHA1})

    list(GET status 0 status_code)
    if(NOT status_code EQUAL 0)
        message(FATAL_ERROR "Could not download SQlite dependency package.")
    else()
        message(">> SQLite package downloaded.")
        file(MAKE_DIRECTORY ${SQLITE_EXTRACTED_FILEPATH})
    endif()

    execute_process(
        COMMAND ${CMAKE_COMMAND} -E tar xzf ${SQLITE_DOWNLOAD_PATH} strip-components=1
        WORKING_DIRECTORY ${SQLITE_EXTRACTED_FILEPATH})

    set(SQLITE_UNZIPPED_VERSIONED_FOLDER "${SQLITE_EXTRACTED_FILEPATH}/sqlite-amalgamation-${SQLITE_RELEASE_VERSION}")
    file(GLOB SQLITE_SOURCE "${SQLITE_UNZIPPED_VERSIONED_FOLDER}/sqlite3.*")
    file(COPY ${SQLITE_SOURCE} DESTINATION ${SQLITE_EXTRACTED_FILEPATH})
    file(REMOVE_RECURSE ${SQLITE_UNZIPPED_VERSIONED_FOLDER})
    file(REMOVE ${SQLITE_DOWNLOAD_PATH})
else()
    message("Found SQLite dependency folder.")
endif()

set(SQLITE3_FILES
        external/sqlite3/sqlite3.c
        external/sqlite3/sqlite3.h)

add_library(sqlite3 ${SQLITE3_FILES})
SET_TARGET_PROPERTIES(sqlite3 PROPERTIES LINKER_LANGUAGE CXX)
TARGET_LINK_LIBRARIES(sqlite3 dl)
```
We do need to link up the resulting sqlite library to our project:
`target_link_libraries(MyProject sqlite3)`


# Compiling the Documentation

Full documentation is available under "````docs/doxygen/html/index.html````" after running the 
"````make_docs.sh````" script from the root directory of the project.

Requires:

+ Doxygen
+ Graphviz (dot)

# Platforms Supported

As this is mostly a header only library most components should be platform agnostic
save those components where specified.

# License

This software is released under the [__GNU General Public License 2__](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) license.

Please reference when used in project and/or research and/or papers and/or 
integrated in production code (i.e.: DBAD).

Copyright E. A. Davison 2015-19.
