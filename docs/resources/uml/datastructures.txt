@startuml

class eadlib::LinkedList<K> {
	-unsigned int number_of_elements
	-std::unique_ptr<Node> head
	-Node *tail
	__
	+LinkedList()
	+LinkedList( list : const std::initialize_list<K> )
	+LinkedList( list : const eadlib::LinkedList<K> & )
	+LinkedList( list : eadlib::LinkedList<K> && )
	+~LinkedList()
	..Operators..
	+operator()( index : const unsigned int ) : K
	+operator<<( out : std::ostream &, list : const LinkedList<K> & ) : std::ostream &
	..List Manipulations..
	+addToHead( const K &element );
	+addToTail( const K &element );
	+append( list : const LinkedList<K> & ) : LinkedList<K>
	+append( list : const std::initializer_list<K> ) : LinkedList<K>
	+append_Move( list : LinkedList<K> & );
	+append_Copy( list : const LinkedList<K> & );
	+appendToHead( list : const std::initializer_list<K> );
	+appendToTail( list : const std::initializer_list<K> );
	+insert( element : const K &, index : const int );
	+insert( list : const std::initializer_list<K> &, index : const int );
	+insert_Move( list : LinkedList<K> &, index : const int );
	+insert_Copy( list : const LinkedList<K> &, index : const int );
	+removeHead() : K
	+removeTail() : K
	+bool popHead() : bool
	+bool popTail() : bool
	+clear()
	..Access..
	+atHead() : K
	+atTail() : K
	..List Properties..
	+size() : unsigned int
	+isEmpty() : bool
	..List Print..
	+str() : std::string
	+forwardPrint() : std::string
	+reversePrint() : std::string
}

class eadlib::LinkedList::Node<K> {
	+K content
	+std::unique_ptr<Node> next;
	+Node * previous;
	+Node( element : const K & )
	+Node( element : const K &, p : Node * )
	+Node( element : const K &, n : std::unique_ptr<Node> &, p : Node * )
	+~Node()
}

eadlib::LinkedList "1" --|> "0..*" eadlib::LinkedList::Node
note on link #FFFFFF: Node is an inner subclass of LinkedList

class eadlib::LinearList<K> {
	-std::unique_ptr<K[]> array
	-unsigned int itsGrowthFactor
	-unsigned int itsShrinkFactor
	-unsigned int itsCapacity
	-unsigned int itsSize
	-int itsFirst
	-int itsLast
	--
	+LinearList( capacity : const unsigned int )
	+LinearList( list : const std::initialize_list<K> )
	+LinearList( list : const LinearList & )
	+LinearList( list : LinearList && )
	+~LinearList()
	..Operators..
	+operator<<( out : std::ostream &, list : const LinearList<K> & ) : std::ostream &
	+operator[]( i : int ) : K
	+operator[]( i : int ) : K&
	+operator=( list : const LinearList<K> & )
	..List Manipulations..
	+addToEnd( item : const K & )
	+addToFront( item : const K & )
	+insert( item : const K &, index : const unsigned int )
	+remove( index : const unsigned int )
	+clear()
	+reset()
	..List Properties..
	+size() : unsigned int
	+reserve( capacity : unsigned int )
	+capacity() : unsigned int
	+isEmpty() : bool
	..List Print..
	+str() : std::string
	+raw() : std::string
}

class eadlib::BinaryTree<K> {
	#std::unique_ptr<BTreeNode<K>> _root
	#int m_height
	#int m_count
	--
	+BinaryTree()
	+BinaryTree( content : const K & )
	+BinaryTree( content : const K &, left : eadlib::BinaryTree<K> &, right : eadlib::BinaryTree<K> & )
	+BinaryTree( levelOrder_list : std::initialize_list<K> )
	+BinaryTree( tree : const BinaryTree & )
	+BinaryTree( tree : BinaryTree && )
	+~BinaryTree()
	..Tree Properties..
	+empty() : bool
	+height() : int
	+count() : int
	+isComplete() : bool
	+isFull() : bool
	..Tree Transversal..
	+preOrder( void(*output_function)( content : K & ) )
	+inOrder( void(*output_function)( content : K & ) )
	+postOrder( void(*output_function)( content : K & ) )
	+levelOrder( void(*output_function)( content : K & ) )
	-preOrder( void(*output_function)( content : K & ), node : const std::unique_ptr<BTreeNode<K>> & )
	-inOrder( void(*output_function)( content : K & ), node : const std::unique_ptr<BTreeNode<K>> & )
	-postOrder( void(*output_function)( content : K & ), node : const std::unique_ptr<BTreeNode<K>> & )
	-levelOrder( void(*output_function)( content : K & ), node : const std::unique_ptr<BTreeNode<K>> & )
}

class eadlib::BTreeNode<K> {
	+K key
	+std::unique_ptr<BTreeNode<K>> left
	+std::unique_ptr<BTreeNode<K>> right
	+BTreeNode()
	+BTreeNode( content : const K & )
	+BTreeNode( content : const K &, pLeft : std::unique_ptr<BTreeNode<K>> &, pRight : std::unique_ptr<BTreeNode<K>> & )
	+BTreeNode( node :const BTreeNode<K> & )
	+BTreeNode( node : BTreeNode<K> && )
	+~BTreeNode()
	+height() : int
}

eadlib::BinaryTree --|> eadlib::BTreeNode

@enduml