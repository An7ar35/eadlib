#include "cli/graphics/BraillerBitMapper.h"

int main( int argc, char *argv[] ) {
    /* Locale and UTF-8 able terminal needed to display UTF-8 characters */
    std::setlocale( LC_ALL, "" );

    /* Creating a 30x30 bitmap */
    auto braille_mapper = eadlib::cli::BrailleBitMapper<30,30>( 0, 0 );

    /* Make a diagonal line */
    for( int i = 0; i < 30; i++ )
        for( int j = i; j < 30; j++ )
            braille_mapper.set( i, i );

    /* Create the braille map and print */
    auto map = braille_mapper.createBrailleMap();
    for( auto &row : map ) {
        for( auto &col : row )
            printf( "%lc", (wchar_t) col );
        std::cout << std::endl;
    }

    return 0;
}