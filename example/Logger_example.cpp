#include "logger/Logger.h"

int main( int argc, char *argv[] ) {
    LOG_FATAL( "Something that make the program state irrecoverable happened..." );
    LOG_ERROR( "Error #", 404, ": Website can't be reached." );
    LOG( "This is an informative message ", " of peace..." );
    for( int i = 0; i < 10; i++ ) {
        LOG_DEBUG( "Loop i = ", i , "/10" );
    }
    LOG_TRACE( "End of the program." );
    return 0;
}