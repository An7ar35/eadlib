int main( int argc, char *argv[] ) {
    try {
        //Setting up the parser options
        auto parser = eadlib::cli::Parser();
        parser.addTitleLine( "Program title" );
        parser.addDescriptionLine( "Description of the program..." );
        parser.option( "my_group", "-n", "-name", "Username", true,
                       { { std::regex( "[0-9a-zA-Z]+" ), "Wrong input", "DefaultUserName" } } );
        parser.addExampleLine( "To pass your user name to the program: " + std::string( argv[ 0 ] ) + " -n myUserName01" );

        if( parser.parse( argc, argv )) { //Parsing the cli arguments to the parser
            if( parser.getValueFlags( "-name" ).at( 0 )) { //Check if a value for 'name' was passed
                std::string user_name = parser.getValues( "-name" ).at( 0 ); //Getting the value
            } else {
                std::cout << "No user name was given.." << std::endl;
            }
        } else {
            std::cerr << "Wrong arguments given to the program." << std::endl;
        }
    } catch( std::regex_error &e ) {
        std::cerr << "Malformed regular expression detected in parser option." << std::endl;
    }
    return 0;
}
