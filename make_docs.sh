#!/usr/bin/env bash

#Checking required stuff to make the docs
type doxygen >/dev/null 2>&1 || { echo >&2 "\'Doxygen\' is required but is not found. Aborting..."; exit 1; }
type dot >/dev/null 2>&1 || { echo >&2 "\'Dot\' is required but is not found. Aborting..."; exit 1; }

#PLANTUML_PATH="$(type -a plantuml)"

mkdir -p docs/doxygen

#Making the docs...
doxygen Doxyfile


