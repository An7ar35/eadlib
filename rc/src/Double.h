/*!
    @class          eadlib::Double
    @brief          Templated precision floating data type

    For when the standard primitive type doesn't do it for you...

    @author         E. A. Davison
    @copyright      Copyright E. A. Davison 2016
    @license        GNUv2 Public License
*/

#ifndef EADLIB_DOUBLE_H
#define EADLIB_DOUBLE_H

#include <iostream>

namespace eadlib {
    template<size_t BitSize = 64> class Double {
      public:
        Double(); //TODO
        Double( bool value ); //TODO
        Double( int value ); //TODO
        Double( unsigned value ); //TODO
        Double( long value ); //TODO
        Double( unsigned long value ); //TODO
        Double( long long value ); //TODO
        Double( unsigned long long value ); //TODO
        Double( double value ); //TODO
        Double( const char *value ); //TODO
        Double( const std::string &value ); //TODO
        Double( const Double &value ); //TODO
        template<size_t BitSizeOfValue> Double( const Double<BitSizeOfValue> &value ); //TODO
        Double( Double &&value ) noexcept; //TODO
        ~Double();
        //Output
        friend std::ostream & operator <<( std::ostream &os, const Double<BitSize> &Double ); //TODO
        std::string base2() const; //TODO
        std::string base10() const; //TODO
        std::string base16() const; //TODO
        void printBase2( std::ostream &out ) const; //TODO
        void printBase10( std::ostream &out ) const; //TODO
        void printBase16( std::ostream &out ) const; //TODO
        //Input
        friend std::istream & operator >>( std::istream &input, Double<BitSize> &Double ); //TODO
        void inputBase2( std::istream &in ); //TODO
        void inputBase10( std::istream &in );  //TODO
        void inputBase16( std::istream &in );  //TODO
        //Assignment
        Double & operator =( const Double &rhs ); //TODO
        Double & operator =( Double &&rhs ) noexcept; //TODO
        void swap( Double &rhs ); //TODO
        //Evaluation Operators
        bool operator ==( const Double &rhs ) const; //TODO
        bool operator !=( const Double &rhs ) const; //TODO
        bool operator <( const Double &rhs ) const; //TODO
        bool operator >( const Double &rhs ) const; //TODO
        bool operator <=( const Double &rhs ) const; //TODO
        bool operator >=( const Double &rhs ) const; //TODO
        //Math Operators and methods
        const Double operator +( const Double &rhs ) const; //TODO
        const Double operator -( const Double &rhs ) const; //TODO
        const Double operator *( const Double &rhs ) const; //TODO
        const Double operator /( const Double &rhs ) const; //TODO
        const Double operator %( const Double &rhs ) const; //TODO
        const Double & operator +=( const Double &rhs ); //TODO
        const Double & operator -=( const Double &rhs ); //TODO
        const Double & operator *=( const Double &rhs ); //TODO
        const Double & operator /=( const Double &rhs ); //TODO
        const Double & operator ++(); //TODO
        const Double operator ++( int ); //TODO
        const Double & operator --(); //TODO
        const Double operator --( int ); //TODO
        const Double pow( const Double &pow ) const; //TODO
        const Double abs() const; //TODO
        const Double negate() const; //TODO
        const Double & flipSign(); //TODO
        //Info
        bool isNegative() const; //TODO
        size_t size() const; //TODO

      private:
        std::unique_ptr<std::bitset<BitSize>>  _significand;
    };

    template<size_t BitSize> Double<BitSize>::Double() :
        _significand( std::make_unique<std::bitset<BitSize>>() )
    {

    }

    template<size_t BitSize> Double<BitSize>::Double( bool value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( int value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( unsigned value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( long value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( unsigned long value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( long long value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( unsigned long long value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( double value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( const char *value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( const std::string &value ) {

    }

    template<size_t BitSize> Double<BitSize>::Double( const Double &value ) {

    }

    template<size_t BitSize> template<size_t BitSizeOfValue> Double<BitSize>::Double( const Double<BitSizeOfValue> &value ) {
        static_assert( BitSize >= BitSizeOfValue, "Attempt to copy-construct with eadlib::Double type of larger size detected!" );
    }

    template<size_t BitSize> Double<BitSize>::Double( Double &&value ) noexcept {

    }

    template<size_t BitSize> Double<BitSize>::~Double() {

    }

    template<size_t BitSize> std::string Double<BitSize>::base2() const {
        return std::__cxx11::string();
    }

    template<size_t BitSize> std::string Double<BitSize>::base10() const {
        return std::__cxx11::string();
    }

    template<size_t BitSize> std::string Double<BitSize>::base16() const {
        return std::__cxx11::string();
    }

    template<size_t BitSize> void Double<BitSize>::printBase2( std::ostream &out ) const {

    }

    template<size_t BitSize> void Double<BitSize>::printBase10( std::ostream &out ) const {

    }

    template<size_t BitSize> void Double<BitSize>::printBase16( std::ostream &out ) const {

    }

    template<size_t BitSize> void Double<BitSize>::inputBase2( std::istream &in ) {

    }

    template<size_t BitSize> void Double<BitSize>::inputBase10( std::istream &in ) {

    }

    template<size_t BitSize> void Double<BitSize>::inputBase16( std::istream &in ) {

    }

    template<size_t BitSize> Double<BitSize> & Double<BitSize>::operator =( const Double<BitSize> &rhs ) {
        return *this;
    }

    template<size_t BitSize> Double<BitSize> & Double<BitSize>::operator =( Double &&rhs ) noexcept {
        return *this;
    }

    template<size_t BitSize> void Double<BitSize>::swap( Double<BitSize> &rhs ) {

    }

    template<size_t BitSize> bool Double<BitSize>::operator ==( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> bool Double<BitSize>::operator !=( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> bool Double<BitSize>::operator <( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> bool Double<BitSize>::operator >( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> bool Double<BitSize>::operator <=( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> bool Double<BitSize>::operator >=( const Double<BitSize> &rhs ) const {
        return false;
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator +( const Double &rhs ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator -( const Double &rhs ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator *( const Double &rhs ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator /( const Double &rhs ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator %( const Double &rhs ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator +=( const Double &rhs ) {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator -=( const Double &rhs ) {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator *=( const Double &rhs ) {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator /=( const Double &rhs ) {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator ++() {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator ++( int ) {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::operator --() {
        return *this;
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::operator --( int ) {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::pow( const Double &pow ) const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::abs() const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> Double<BitSize>::negate() const {
        return Double();
    }

    template<size_t BitSize> const Double<BitSize> & Double<BitSize>::flipSign() {
        return *this;
    }

    template<size_t BitSize> bool Double<BitSize>::isNegative() const {
        return false;
    }

    template<size_t BitSize> size_t Double<BitSize>::size() const {
        return 0;
    }
}

#endif //EADLIB_DOUBLE_H
