/*!
    @class          eadlib::cli::braille::BrailleHistogram
    @brief          CLI Bar charts

    Creates braille based pixelated bar charts in the terminal.

    @note           Set locale for wide character support (UTF-8) before usage. LINUX ONLY!
    @dependencies   eadlib::cli::BrailleBitMapper, eadlib::Coordinate2D,
                    eadlib::string::,
                    eadlib::exception::unsupported, eadlib::exception::aborted_operation
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
*/
#ifndef EADLIB_CLI_BRAILLE_BRAILLEHISTOGRAM_H
#define EADLIB_CLI_BRAILLE_BRAILLEHISTOGRAM_H

#include "../../../../src/cli/braille/BrailleBitMapper.h"
#include "../../../../src/cli/braille/braille.h"
#include "../../../../src/cli/braille/graph/graph.h"
#include "../../../../src/cli/braille/graph/labelling/labelling.h"
#include "../../../../src/cli/braille/raster/raster.h"
#include "../graph/NumAxisProperty.h"
#include "../graph/CatAxisProperty.h"
#include "../../../../src/cli/braille/graph/NumLabelProperty.h"
#include "../graph/HistoPoint.h"
#include "../../../../src/datatype/Coordinate2D.h"
#include "../../../../src/exception/unsupported.h"
#include "../../../../src/exception/aborted_operation.h"
#include "../../../../src/string/string.h"

namespace eadlib::cli::braille {
    template<graph::Orientation O,
             size_t             W = 80, //hint for width
             size_t             H = 20  //hint for height
    > class BrailleHistogram {
      public:
        static const size_t MAX_NUM_LABEL_SIZE { 6 };
        static const size_t MAX_CAT_LABEL_SIZE { 12 };

        BrailleHistogram();
        ~BrailleHistogram() = default;
        //Axis options
        void setCatAxisOptions( std::string axis_label );
        void setNumAxisOptions( int tick_direction, float data_interval );
        void setNumAxisOptions( std::string axis_label, int tick_direction, float data_interval );
        //Labeling and decoration options
        void setDrawingStyle( graph::histogram::Style drawing_style );
        void setLabelPosition( graph::labelling::Position pos );
        void setMargins( size_t x, size_t y );
        //Data points
        void addPoint( const std::string &label, float value );
        void addPoint( const std::string &label, float range_low, float range_high );

        size_t count() const;
        void   clear();

        std::string getXAxisLabel() const;
        std::string getYAxisLabel() const;
        std::string getNumAxisLabel() const;
        std::string getCatAxisLabel() const;

        size_t pixelWidth() const;
        size_t pixelHeight() const;


        //TODO find max number of points based on label sizes

        std::unique_ptr<BrailleMap_t> getBrailleHistogram();
      private:
        template<typename T> using CoordinateList_t = std::vector<Coordinate2D<T>>;
        template<typename T> using TickList_t       = std::list<std::pair<Coordinate2D<size_t>, T>>;
        template<typename T> using HistoPoint_t     = graph::HistoPoint<std::string, T>;
        template<typename T> using HistoPointList_t = std::vector<graph::HistoPoint<std::string, T>>;
        using                      Offset_t         = std::pair<size_t, graph::labelling::Position>;

        //Variables
        graph::NumAxisProperty<float>       _num_axis_property;
        graph::CatAxisProperty<std::string> _cat_axis_property;

        graph::histogram::Style             _graph_style;
        Coordinate2D <size_t>               _canvas_margin { 1, 1 }; //error margin on the border for rounded scaled points
        std::vector<HistoPoint_t<float>>    _data_points;

        //Graph creation methods
        HistoPointList_t<size_t> scaleHistoPoints( const HistoPointList_t<float>       &data_points,
                                                   const graph::NumAxisProperty<float> &numAxisProperty,
                                                   const Coordinate2D<size_t>          &axis_center,
                                                   const size_t                        &tick_interval ) const;

        void addTickValues( const graph::NumAxisProperty<float>       &num_axis_property,
                            const graph::CatAxisProperty<std::string> &cat_axis_property,
                            const Coordinate2D <size_t>               &canvas_size,
                            const Coordinate2D <size_t>               &tick_interval,
                            const graph::histogram::Style             &draw_style,
                            const HistoPointList_t<size_t>            &histo_points,
                            graph::MetaGraphData<std::string, float>  &meta_graph_data ) const;

        void addTickValues( const graph::NumAxisProperty<float>       &num_axis_property,
                            const graph::CatAxisProperty<std::string> &cat_axis_property,
                            const Coordinate2D <size_t>               &canvas_size,
                            const Coordinate2D <size_t>               &tick_interval,
                            const graph::histogram::Style             &draw_style,
                            const HistoPointList_t<size_t>            &histo_points,
                            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const;

        void addGraphAxes( const Coordinate2D<size_t>        &axis_centre,
                           std::vector<Coordinate2D<size_t>> &bitmap_points ) const;

        void addTickPoints( const graph::NumAxisProperty<float>       &num_axis_property,
                            const graph::CatAxisProperty<std::string> &cat_axis_property,
                            graph::MetaGraphData<std::string, float>  &meta_graph_data ) const;

        void addTickPoints( const graph::NumAxisProperty<float>       &num_axis_property,
                            const graph::CatAxisProperty<std::string> &cat_axis_property,
                            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const;

        void addHistoPoints( const HistoPointList_t<size_t>           &histo_points,
                             graph::histogram::Style                  draw_style,
                             graph::MetaGraphData<std::string, float> &meta_graph_data ) const;

        void addHistoPoints( const HistoPointList_t<size_t>           &histo_points,
                             graph::histogram::Style                  draw_style,
                             graph::MetaGraphData<float, std::string> &meta_graph_data ) const;

        void addRasterPoints( const HistoPointList_t<Coordinate2D<size_t>> &points_coordinates,
                              const graph::histogram::Style                &draw_style,
                              CoordinateList_t<size_t>                     &bitmap_points ) const;

        void drawGraph( const graph::NumAxisProperty<float> &axis_property_x,
                        const graph::NumAxisProperty<float> &axis_property_y,
                        const CoordinateList_t<size_t>      &bitmap_points,
                        BrailleMap_t                        &canvas ) const;

        //Labelling methods
        std::unique_ptr<BrailleMap_t> createLabelledCanvas( const graph::CatAxisProperty<std::string> &axis_property_x,
                                                            const graph::NumAxisProperty<float>       &axis_property_y,
                                                            const Coordinate2D<size_t>                &tick_intervals,
                                                            graph::MetaGraphData<std::string, float>  &meta_graph_data ) const;

        std::unique_ptr<BrailleMap_t> createLabelledCanvas( const graph::NumAxisProperty<float>       &axis_property_x,
                                                            const graph::CatAxisProperty<std::string> &axis_property_y,
                                                            const Coordinate2D<size_t>                &tick_intervals,
                                                            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const;

        std::optional<Offset_t> addXNumLabels( const graph::NumAxisProperty<float>      &axis_property,
                                               const size_t                             &char_spacing,
                                               graph::MetaGraphData<float, std::string> &meta_graph_data,
                                               std::vector<size_t>                      &label_row,
                                               std::optional<Offset_t>                  y_label_offset) const;

        std::optional<Offset_t> addYNumLabels( const graph::NumAxisProperty<float>      &axis_property,
                                               graph::MetaGraphData<std::string, float> &meta_graph_data,
                                               BrailleMap_t                             &graph_canvas ) const;

        std::optional<Offset_t> addXCatLabels( const graph::CatAxisProperty<std::string> &axis_property,
                                               graph::MetaGraphData<std::string, float>  &meta_graph_data,
                                               BrailleMap_t                              &graph_canvas ) const;

        std::optional<Offset_t> addYCatLabels( const graph::CatAxisProperty<std::string> &axis_property,
                                               graph::MetaGraphData<float, std::string>  &meta_graph_data,
                                               BrailleMap_t                              &graph_canvas ) const;

    };

    //----------------------------------------------------------------------------------------------
    // BrailleHistogram class method implementations
    //----------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam W Number of characters horizontally to use for the braille graph
     * @tparam H Number of characters vertically to use for the braille graph
     * @param orientation Category vs Numbers orientation (default = XY).
     */
    template<graph::Orientation O, size_t W, size_t H>
        BrailleHistogram<O, W, H>::BrailleHistogram() :
            _graph_style( graph::histogram::Style::POINT )
    {
        static_assert(
            W >= 4 && H >= 2,
            "BrailleHistogram must be initialized with a character size > than (4,2)."
        );

        switch( O ) {
            case graph::Orientation::XY: {
                _cat_axis_property = graph::CatAxisProperty<std::string>( graph::labelling::Axis::X );
                _num_axis_property = graph::NumAxisProperty<float>( graph::labelling::Axis::Y );
            }
            break;

            case graph::Orientation::YX: {
                _num_axis_property = graph::NumAxisProperty<float>( graph::labelling::Axis::X );
                _cat_axis_property = graph::CatAxisProperty<std::string>( graph::labelling::Axis::Y );
            }
            break;
        }

        _num_axis_property.setLabellingPosition( graph::labelling::Position::SW );
    }

    /**
     * Sets the category axis properties
     * @param axis_label Axis label
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setCatAxisOptions(
            std::string axis_label )
    {
        _cat_axis_property._label = std::move( axis_label );
    }

    /**
     * Sets the number (quantities) axis properties
     * @param tick_direction Direction of the notch (-1: negative side, 0: cross, +1 positive side)
     * @param data_interval  Data interval hint for each notch (may be adjusted when the calculated
     *                       number of ticks is greater than the number of 2x4 character block on
     *                       the axis)
     * @throws eadlib::exception::unsupported when data interval's precision is too small for displaying in the label space
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setNumAxisOptions(
            int   tick_direction,
            float data_interval )
    {
        if( eadlib::string::length<float>( data_interval ) > MAX_NUM_LABEL_SIZE )
            throw eadlib::exception::unsupported(
                "Data interval precision is too big. Consider pre-scaling the points and intervals."
            );

        _num_axis_property._tick_direction = tick_direction;
        _num_axis_property._data_interval  = data_interval;
    }

    /**
     * Sets the number (quantities) axis properties
     * @param axis_label     Axis label
     * @param tick_direction Direction of the notch (-1: negative side, 0: cross, +1 positive side)
     * @param data_interval  Data interval hint for each notch (may be adjusted when the calculated
     *                       number of ticks is greater than the number of 2x4 character block on
     *                       the axis)
     * @throws eadlib::exception::unsupported when data interval's precision is too small for displaying in the label space
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setNumAxisOptions(
            std::string axis_label,
            int         tick_direction,
            float       data_interval )
    {
        _num_axis_property._label = std::move( axis_label );
        setNumAxisOptions( tick_direction, data_interval );
    }

    /**
     * Sets the graph drawing style property
     * @param drawing_style Drawing style
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setDrawingStyle( graph::histogram::Style drawing_style )
    {
        _graph_style = drawing_style;
    }

    /**
     * Sets the labelling position around the graph
     * @param pos Position
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setLabelPosition( graph::labelling::Position pos )
    {
        _num_axis_property.setLabellingPosition( pos );
        _cat_axis_property.setLabellingPosition( pos );
    }

    /**
     * Sets the margins for the display graph data
     * @param x X margin in pixels
     * @param y Y margin in pixels
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::setMargins( size_t x, size_t y )
    {
        _canvas_margin = { x + 1, y + 1 };
    }

    /**
     * Add a category point to the histogram
     * @param label Category label
     * @param value Numeric value
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addPoint(
            const std::string &label,
            float             value )
    {
        addPoint( label, 0.f, value );
    }

    /**
     * Adds a category point to the histogram
     * @param label      Category label
     * @param range_low  Lowest value on range
     * @param range_high Highest value on range
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addPoint(
            const std::string &label,
            float             range_low,
            float             range_high )
    {


        //TODO check against max char blocks on the label axis to make sure the added category can fit

        _data_points.emplace_back( HistoPoint_t<float>( label, range_low, range_high ) );

        if( label.length() > _cat_axis_property._max_label_size )
            _cat_axis_property._max_label_size = label.length();
        if( range_low < _num_axis_property._min )
            _num_axis_property._min = range_low;
        if( range_high > _num_axis_property._max )
            _num_axis_property._max = range_high;
    }

    /**
     * Gets the number of category points added
     * @return Data point count
     */
    template<graph::Orientation O, size_t W, size_t H>
        size_t BrailleHistogram<O, W, H>::count() const
    {
        return _data_points.size();
    }

    /**
     * Clears all points
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::clear()
    {
        _num_axis_property._min            = 0.f;
        _num_axis_property._max            = 0.f;
        _cat_axis_property._max_label_size = 0;
        _data_points.clear();
    }

    /**
     * Gets the X-Axis label
     * @return  X-Axis label
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::string BrailleHistogram<O, W, H>::getXAxisLabel() const
    {
        using graph::Orientation;

        switch( O ) {
            case Orientation::XY:
                return getCatAxisLabel();
            case Orientation::YX:
                return getNumAxisLabel();
        }
    }

    /**
     * Gets the Y-Axis label
     * @return  Y-Axis label
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::string BrailleHistogram<O, W, H>::getYAxisLabel() const
    {
        using graph::Orientation;

        switch( O ) {
            case Orientation::XY:
                return getNumAxisLabel();
            case Orientation::YX:
                return getCatAxisLabel();
        }
    }

    /**
     * Gets the numbered axis label
     * @return Numbered axis label string
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::string BrailleHistogram<O, W, H>::getNumAxisLabel() const
    {
        return _num_axis_property._label;
    }

    /**
     * Gets the category axis label
     * @return Category axis label string
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::string BrailleHistogram<O, W, H>::getCatAxisLabel() const
    {
        return _cat_axis_property._label;
    }

    /**
     * Gets the number of braille pixels available horizontally
     * @return Pixel width of the graph
     */
    template<graph::Orientation O, size_t W, size_t H>
        size_t BrailleHistogram<O, W, H>::pixelWidth() const
    {
        return W * 2;
    }

    /**
     * Gets the number of braille pixels available vertically
     * @return Pixel height of the graph
     */
    template<graph::Orientation O, size_t W, size_t H>
        size_t BrailleHistogram<O, W, H>::pixelHeight() const
    {
        return H * 4;
    }

    /**
     * Creates a unicode braille map of a histogram based on the data points given with addPoint(..)
     * @return Histogram as a BrailleMap_t
     * @throw eadlib::exception::aborted_operation when no data points were given prior or category labelling cannot fit axis size
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::unique_ptr<BrailleMap_t> BrailleHistogram<O, W, H>::getBrailleHistogram()
    {
        using eadlib::cli::braille::BrailleCharAxis;
        using eadlib::cli::braille::graph::MetaGraphData;
        using eadlib::cli::braille::graph::calculateXAxisPosition;
        using eadlib::cli::braille::graph::calculateYAxisPosition;
        using eadlib::cli::braille::graph::calculateTickInterval;
        using eadlib::cli::braille::graph::calculateCategoryInterval;

        if( W < 4 || H < 2 )
            throw eadlib::exception::aborted_operation( "Given width x height is too small. Must be at least 4x2" );
        if( _data_points.empty() )
            throw eadlib::exception::aborted_operation( "No data points were added to the graph." );

        auto canvas_size = Coordinate2D<size_t>(
            pixelWidth()  - _canvas_margin.x * 2,
            pixelHeight() - _canvas_margin.y * 2
        );

        switch( O ) {
            case graph::Orientation::XY: { //(x:categories vs y:numbers)
                auto  meta_graph_data = std::make_unique<MetaGraphData<std::string, float>>();
                auto  x_axis_property = _cat_axis_property;
                auto  y_axis_property = _num_axis_property;
                float y_axis_range    = y_axis_property._max + abs( y_axis_property._min );
                //calculate axis centre
                meta_graph_data->_axis_center = Coordinate2D<size_t>(
                    calculateYAxisPosition<float>( canvas_size.x, _canvas_margin.x, y_axis_range, y_axis_property._min ),
                    _canvas_margin.x
                );
                //calculate tick spacings
                size_t tick_spacing_x = calculateCategoryInterval<BrailleCharAxis::X>( canvas_size.x, _data_points.size() );
                auto   tick_spacing_y = calculateTickInterval<float>( y_axis_range, canvas_size.y, BRAILLE_PIXEL_HEIGHT, 1, y_axis_property._data_interval );
                auto   tick_interval  = Coordinate2D<size_t>( tick_spacing_x, tick_spacing_y.first );
                y_axis_property._data_interval = tick_spacing_y.second;
                //scale data
                auto   scaled_data    = scaleHistoPoints( _data_points, _num_axis_property, meta_graph_data->_axis_center, tick_spacing_y.first );
                //create tick location data
                addTickValues( y_axis_property, x_axis_property, canvas_size, tick_interval, _graph_style, scaled_data, *meta_graph_data );
                //add bitmap point data
                addGraphAxes( meta_graph_data->_axis_center, meta_graph_data->_bitmap_points );
                addTickPoints( y_axis_property, x_axis_property, *meta_graph_data );
                addHistoPoints( scaled_data, _graph_style, *meta_graph_data );
                //TODO tick interval on the X axis
                //TODO the rest

            }
            break;

            case graph::Orientation::YX: { //(x:numbers vs y:categories)
//                auto meta_graph_data = std::make_unique<MetaGraphData<float, std::string>>();
//                auto x_axis_property = _num_axis_property;
//                auto y_axis_property = _cat_axis_property;
//                auto x_axis_range    = x_axis_property._max + abs( x_axis_property._min );
//                //calculate axis centre
//                meta_graph_data->_axis_center = Coordinate2D<size_t>(
//                    calculateXAxisPosition<float>( canvas_size.y, _canvas_margin.y, x_axis_range, x_axis_property._max ),
//                    pixelHeight() - _canvas_margin.y
//                );
//                //calculate tick spacings
//                auto   tick_spacing_x = calculateTickInterval<float>( x_axis_range, canvas_size.x, BRAILLE_PIXEL_WIDTH, 2, x_axis_property._data_interval );
//                size_t tick_spacing_y = calculateCategoryInterval<BrailleCharAxis::Y>( canvas_size.y, _data_points.size() );
//                auto   tick_interval  = Coordinate2D<size_t>( tick_spacing_x.first, tick_spacing_y );
//                x_axis_property._data_interval = tick_spacing_x.second;
//                //scale data
//                auto   scaled_data    = scaleHistoPoints( _data_points, _num_axis_property, meta_graph_data->_axis_center, tick_spacing_x.first );
//                //create tick location data
//                addTickValues( x_axis_property, y_axis_property, canvas_size, tick_interval, _graph_style, scaled_data, *meta_graph_data );
//                //add bitmap point data
//                addGraphAxes( meta_graph_data->_axis_center, meta_graph_data->_bitmap_points );
//                addTickPoints( x_axis_property, y_axis_property, *meta_graph_data );
//                addHistoPoints( scaled_data, _graph_style, *meta_graph_data );
//                //TODO tick interval on the Y axis
//                //TODO the rest
            }
            break;
        }

//        auto labelled_canvas = createLabelledCanvas( x_axis_property, y_axis_property, tick_interval, *meta_graph_data );
//        drawGraph( x_axis_property, y_axis_property, *meta_graph_data, *labelled_canvas );
//        return std::move( labelled_canvas );
    }

    //----------------------------------------------------------------------------------------------
    // BrailleHistogram class private method implementations
    //----------------------------------------------------------------------------------------------
    /**
     * Scale the category ranges to the canvas
     * @param data_points     List of HistoPoint to scale
     * @param numAxisProperty Numbered axis properties
     * @param axis_center     Axis pixel centre
     * @param tick_interval   Num axis tick pixel interval
     * @return List of scaled HistoPoints
     */
    template<graph::Orientation O, size_t W, size_t H>
        typename BrailleHistogram<O, W, H>::template HistoPointList_t<size_t> BrailleHistogram<O, W, H>::scaleHistoPoints(
            const HistoPointList_t<float>       &data_points,
            const graph::NumAxisProperty<float> &numAxisProperty,
            const Coordinate2D<size_t>          &axis_center,
            const size_t                        &tick_interval ) const
    {
        using eadlib::cli::braille::pixel_cast;
        using eadlib::cli::braille::graph::scaleAmortised;

        auto scaled = HistoPointList_t<size_t>();

        for( const auto &p : data_points ) {
            auto low  = scaleAmortised<float>( p._range._low, numAxisProperty._data_interval, tick_interval );
            auto high = scaleAmortised<float>( p._range._high, numAxisProperty._data_interval, tick_interval );

            scaled.emplace_back(
                graph::HistoPoint<std::string, size_t>( p._label,
                                                        pixel_cast<float>( low ),
                                                        pixel_cast<float>( high ) )
            );
        }

        return scaled;
    }

    /**
     * Add tick locations and values to MetaGraphData container
     * @param num_axis_property Numbered Y-Axis properties
     * @param cat_axis_property Category X-Axis properties
     * @param canvas_size       Size of drawing canvas
     * @param tick_interval     Tick intervals
     * @param draw_style        Graph drawing style
     * @param histo_points      List of HistoPoint_t
     * @param meta_graph_data   MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addTickValues(
            const graph::NumAxisProperty<float>       &num_axis_property,
            const graph::CatAxisProperty<std::string> &cat_axis_property,
            const Coordinate2D<size_t>                &canvas_size,
            const Coordinate2D<size_t>                &tick_interval,
            const graph::histogram::Style             &draw_style,
            const HistoPointList_t<size_t>            &histo_points,
            graph::MetaGraphData<std::string, float>  &meta_graph_data ) const
    {
        /**
         * [LAMBDA] Adds a tick on the X-axis
         * @param x     Location of tick on X-axis
         * @param value Tick value
         */
        auto addTickX = [&]( const size_t &x, const std::string &value ) {
            meta_graph_data._tick_values_x.emplace_back(
                Coordinate2D<size_t>( x, meta_graph_data._axis_center.y ),
                value
            );
        };

        /**
         * [LAMBDA] Adds a tick on the Y-axis
         * @param y     Location of tick on Y-axis
         * @param value Tick value
         */
        auto addTickY = [&]( const size_t &y, const float &value ) {
            if( value >= 0 )
                meta_graph_data._tick_values_y.emplace_front(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x, y ),
                    value
                );
            else
                meta_graph_data._tick_values_y.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x, y ),
                    value
                );
        };


        { //CatAxisProperty (X-Axis)
            using eadlib::cli::braille::graph::histogram::Style;

            size_t tick_count = 1;
            size_t x          = meta_graph_data._axis_center.x;
            if( draw_style != Style::BEZIER && draw_style != Style::JOINED )
                x +=  + tick_interval.x;
            //Adding categories on positive X-Axis: right from axis centre
            for( const auto &cat : histo_points ) {
                addTickX( x, cat._label );
                tick_count++;
                x += tick_interval.x;
            }
        }

        { //NumAxisProperty (Y-Axis)
            constexpr size_t MIN_HEIGHT { BRAILLE_PIXEL_HEIGHT + 2 };

            size_t bottom_margin = canvas_size.y + _canvas_margin.y;
            size_t tick_count_n  = ( meta_graph_data._axis_center.y - _canvas_margin.y ) / tick_interval.y;
            size_t tick_count_s  = ( bottom_margin - meta_graph_data._axis_center.y ) / tick_interval.y;

            addTickY( meta_graph_data._axis_center.y, 0 ); //Tick for zero
            auto previous_y = meta_graph_data._axis_center.y;
            //Adding positive Y ticks: up from (0,0)
            for( size_t tick = 1, y = ( meta_graph_data._axis_center.y - tick_interval.y );
                 tick <= tick_count_n;
                 tick++, y -= tick_interval.y )
            {
                if( previous_y - y > MIN_HEIGHT ) {
                    addTickY( y, num_axis_property._data_interval * tick );
                    previous_y = y;
                }
            }

            previous_y = meta_graph_data._axis_center.y;
            //Adding negative Y ticks: down from (0,0)
            for( size_t tick = 1, y = ( meta_graph_data._axis_center.y + tick_interval.y );
                 tick <= tick_count_s;
                 tick++, y += tick_interval.y )
            {
                if( y - previous_y > MIN_HEIGHT ) {
                    addTickY( y, -( num_axis_property._data_interval * tick ) );
                    previous_y = y;
                }
            }
        }
    }

    /**
     * Add tick locations and values to MetaGraphData container
     * @param num_axis_property Numbered X-Axis properties
     * @param cat_axis_property Category Y-Axis properties
     * @param canvas_size       Size of drawing canvas
     * @param tick_interval     Tick intervals
     * @param draw_style        Graph drawing style
     * @param histo_points      List of HistoPoint_t
     * @param meta_graph_data   MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addTickValues(
            const graph::NumAxisProperty<float>       &num_axis_property,
            const graph::CatAxisProperty<std::string> &cat_axis_property,
            const Coordinate2D<size_t>                &canvas_size,
            const Coordinate2D<size_t>                &tick_interval,
            const graph::histogram::Style             &draw_style,
            const HistoPointList_t<size_t>            &histo_points,
            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const
    {
        /**
         * [LAMBDA] Adds a tick on the X-axis
         * @param x     Location of tick on X-axis
         * @param value Tick value
         */
        auto addTickX = [&]( const size_t &x, const float &value ) {
            if( value >= 0 )
                meta_graph_data._tick_values_y.emplace_front(
                    Coordinate2D<size_t>( x, meta_graph_data._axis_center.y ),
                    value
                );
            else
                meta_graph_data._tick_values_y.emplace_back(
                    Coordinate2D<size_t>( x, meta_graph_data._axis_center.y ),
                    value
                );
        };

        /**
         * [LAMBDA] Adds a tick on the Y-axis
         * @param y     Location of tick on Y-axis
         * @param value Tick value
         */
        auto addTickY = [&]( const size_t &y, const std::string &value ) {
            meta_graph_data._tick_values_x.emplace_back(
                Coordinate2D<size_t>( meta_graph_data._axis_center.x, y ),
                value
            );
        };


        { //NumAxisProperty (X-Axis)
            size_t right_margin = canvas_size.x + _canvas_margin.x;
            size_t tick_count_w = ( meta_graph_data._axis_center.x - _canvas_margin.x ) / tick_interval.x;
            size_t tick_count_e = ( right_margin - meta_graph_data._axis_center.x ) / tick_interval.x;

            addTickX( meta_graph_data._axis_center.x, 0 ); //Tick for zero
            auto previous_x = meta_graph_data._axis_center.x;
            //Adding positive X ticks: right from (0,0)
            for( size_t tick = 1, x = ( meta_graph_data._axis_center.x + tick_interval.x );
                 tick <= tick_count_e;
                 tick++, x += tick_interval.x )
            {
                if( x - previous_x > BRAILLE_PIXEL_WIDTH ) {
                    addTickX( x, num_axis_property._data_interval * tick );
                    previous_x = x;
                }
            }

            previous_x = meta_graph_data._axis_center.x;
            //Adding negative X ticks: left from (0,0)
            for( size_t tick = 1, x = ( meta_graph_data._axis_center.x - tick_interval.x );
                 tick <= tick_count_w;
                 tick++, x -= tick_interval.x )
            {
                if( previous_x - x > BRAILLE_PIXEL_WIDTH ) {
                    addTickX( x, -( num_axis_property._data_interval * tick ) );
                    previous_x = x;
                }
            }
        }

        { //CatAxisProperty (Y-Axis)
            using eadlib::cli::braille::graph::histogram::Style;

            size_t tick_count   = 1;
            size_t y            = meta_graph_data._axis_center.y;
            if( draw_style != Style::BEZIER && draw_style != Style::JOINED )
                y -= tick_interval.y;
            //Adding categories on positive X-Axis: up from axis centre
            for( const auto &cat : histo_points ) {
                addTickY( y, cat._label );
                tick_count++;
                y -= tick_interval.y;
            }
        }
    }

    /**
     * Adds the XY axes to the bitmap points
     * @param axis_centre   Axis centre
     * @param bitmap_points Bitmap points
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addGraphAxes(
            const Coordinate2D<size_t>        &axis_centre,
            std::vector<Coordinate2D<size_t>> &bitmap_points ) const
    {
        //Draw X axis line
        for( size_t x = 0; x < pixelWidth(); x++ )
            bitmap_points.emplace_back(
                Coordinate2D<size_t>( x, axis_centre.y )
            );
        //Draw Y axis line
        for( size_t y = 0; y < pixelHeight(); y++ )
            bitmap_points.emplace_back(
                Coordinate2D<size_t>( axis_centre.x, y )
            );
    }

    /**
     * Add tick points to bitmap point data
     * @param num_axis_property Numbered Y-Axis properties
     * @param cat_axis_property Category X-Axis properties
     * @param meta_graph_data   MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addTickPoints(
            const graph::NumAxisProperty<float>       &num_axis_property,
            const graph::CatAxisProperty<std::string> &cat_axis_property,
            graph::MetaGraphData<std::string, float>  &meta_graph_data ) const
    {
        //Cat axis (X-Axis)
        for( const auto &tick : meta_graph_data._tick_values_x ) {
            meta_graph_data._bitmap_points.emplace_back( //below axis
                Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y + 1 )
            );
            meta_graph_data._bitmap_points.emplace_back( //above axis
                Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y - 1 )
            );
        }
        //Num axis (Y-Axis)
        for( const auto &tick : meta_graph_data._tick_values_y ) {
            if( num_axis_property._tick_direction == 0 || num_axis_property._tick_direction > 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x + 1, tick.first.y )
                );
            }
            if( num_axis_property._tick_direction == 0 || num_axis_property._tick_direction < 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x - 1, tick.first.y )
                );
            }
        };
    }

    /**
     * Add tick points to bitmap point data
     * @param num_axis_property Numbered X-Axis properties
     * @param cat_axis_property Category Y-Axis properties
     * @param meta_graph_data   MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addTickPoints(
            const graph::NumAxisProperty<float>       &num_axis_property,
            const graph::CatAxisProperty<std::string> &cat_axis_property,
            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const
    {
        //Num axis (X-Axis)
        for( const auto &tick : meta_graph_data._tick_values_x ) {
            if( num_axis_property._tick_direction == 0 || num_axis_property._tick_direction > 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y + 1 )
                );
            }
            if( num_axis_property._tick_direction == 0 || num_axis_property._tick_direction < 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y - 1 )
                );
            }
        }
        //Cat axis (Y-Axis)
        for( const auto &tick : meta_graph_data._tick_values_y ) {
            meta_graph_data._bitmap_points.emplace_back( //right of axis
                Coordinate2D<size_t>( meta_graph_data._axis_center.x + 1, tick.first.y )
            );
            meta_graph_data._bitmap_points.emplace_back( //left of axis
                Coordinate2D<size_t>( meta_graph_data._axis_center.x - 1, tick.first.y )
            );
        };
    }

    /**
     * Adds the points that create the histogram point graphics on the bitmap
     * @param histo_points    List of all scaled histogram points
     * @param draw_style      Drawing style of the graph
     * @param meta_graph_data MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addHistoPoints(
            const BrailleHistogram::HistoPointList_t<size_t> &histo_points,
            graph::histogram::Style                          draw_style,
            graph::MetaGraphData<std::string, float>         &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::histogram::Style;

        auto histo_coordinates = HistoPointList_t<Coordinate2D<size_t>>();
        auto histo_point_it    = histo_points.cbegin();
        auto tick_it           = meta_graph_data._tick_values_x.cbegin();

        while( histo_point_it != histo_points.cend() && tick_it != meta_graph_data._tick_values_x.cend() ) {
            histo_coordinates.emplace_back(
                HistoPoint_t<Coordinate2D<size_t>>(
                    histo_point_it->_label,
                    Coordinate2D<size_t>( tick_it->first.x, histo_point_it->_range._low ),
                    Coordinate2D<size_t>( tick_it->first.x, histo_point_it->_range._high )
                )
            );
            ++histo_point_it;
            ++tick_it;
        }

        if( histo_coordinates.size() < 2 && ( draw_style == Style::JOINED || draw_style == Style::BEZIER ) )
            draw_style = Style::POINT; //can't do lines or curves with just 1 point!

        addRasterPoints( histo_coordinates, draw_style, meta_graph_data._bitmap_points );
    }

    /**
     * Adds the points that create the histogram point graphics on the bitmap
     * @param histo_points    List of all scaled histogram points
     * @param draw_style      Drawing style of the graph
     * @param meta_graph_data MetaGraphData container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addHistoPoints(
            const BrailleHistogram::HistoPointList_t<size_t> &histo_points,
            graph::histogram::Style                          draw_style,
            graph::MetaGraphData<float, std::string>         &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::histogram::Style;

        auto histo_coordinates = HistoPointList_t<Coordinate2D<size_t>>();
        auto histo_point_it    = histo_points.cbegin();
        auto tick_it           = meta_graph_data._tick_values_y.cbegin();

        while( histo_point_it != histo_points.cend() && tick_it != meta_graph_data._tick_values_y.cend() ) {
            histo_coordinates.emplace_back(
                HistoPoint_t<Coordinate2D<size_t>>(
                    histo_point_it->_label,
                    Coordinate2D<size_t>( histo_point_it->_range._low, tick_it->first.y ),
                    Coordinate2D<size_t>( histo_point_it->_range._high, tick_it->first.y )
                )
            );
            ++histo_point_it;
            ++tick_it;
        }

        if( histo_coordinates.size() < 2 && ( draw_style == Style::JOINED || draw_style == Style::BEZIER ) )
            draw_style = Style::POINT; //can't do lines or curves with just 1 point!

        addRasterPoints( histo_coordinates, draw_style, meta_graph_data._bitmap_points );
    }

    /**
     * Adds a point to the histogram bitmap and generate styling for it
     * @param points_coordinates List of point coordinates
     * @param draw_style         Drawing style of the graph
     * @param bitmap_points      Bitmap point container
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::addRasterPoints(
            const HistoPointList_t<Coordinate2D<size_t>> &points_coordinates,
            const graph::histogram::Style                &draw_style,
            CoordinateList_t<size_t>                     &bitmap_points ) const
    {
        using eadlib::cli::braille::graph::histogram::Style;
        using eadlib::cli::braille::raster::getLinePoints;

        switch( draw_style ) {
            case Style::POINT: {
                for( const auto & p :points_coordinates ) {
                    bitmap_points.emplace_back( p._range._low );
                    bitmap_points.emplace_back( p._range._high );
                }
            }
            break;

            case Style::JOINED: {
                auto from_it = points_coordinates.cbegin();
                auto to_it   = std::next( from_it );

                while( to_it != points_coordinates.cend() ) {

                    auto line1 = getLinePoints( from_it->_range._low, to_it->_range._low );
                    auto line2 = getLinePoints( from_it->_range._high, to_it->_range._high );

                    for( const auto &p : line1 )
                        bitmap_points.emplace_back( p );

                    for( const auto &p : line2 )
                        bitmap_points.emplace_back( p );

                    ++from_it;
                    ++to_it;
                }
            }
            break;

            case Style::BEZIER: {
                //TODO
            }
            break;

            case Style::PLATEAU: {
                //TODO
            }
            break;

            case Style::THIN_BAR: {
                for( const auto &p : points_coordinates ) {
                    auto bar = getLinePoints( p._range._low, p._range._high );

                    for( const auto &e : bar )
                        bitmap_points.emplace_back( e );
                }
            }
            break;

            case Style::THICK_BAR: {
                //TODO
            }
            break;
        }
    }

    /**
     * Draws the braille graph onto the BrailleMap_t canvas
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param bitmap_points   List of raster points to draw
     * @param canvas          Canvas to draw chart on
     */
    template<graph::Orientation O, size_t W, size_t H>
        void BrailleHistogram<O, W, H>::drawGraph(
            const graph::NumAxisProperty<float> &axis_property_x,
            const graph::NumAxisProperty<float> &axis_property_y,
            const CoordinateList_t<size_t> &bitmap_points,
            BrailleMap_t                   &canvas ) const
    {
        using eadlib::cli::braille::graph::labelling::Position;

        auto bit_mapper = BrailleBitMapper<W * 2, H * 4>();

        for( const auto & i : bitmap_points )
            bit_mapper.set( i.x, i.y );

        auto braille_graph = bit_mapper.createBrailleMap();

        //ERROR-CONTROL: BrailleBitMapper failed to create a bitmap
        if( braille_graph->empty() )
            throw std::out_of_range( "BrailleMap returned empty." );

        auto row_it = canvas.begin();
        auto braille_line = braille_graph->cbegin();

        if( axis_property_x._tick_label_pos == Position::NORTH )
            row_it = std::next( row_it );

        if( axis_property_y._tick_label_pos == Position::WEST ) {
            while( braille_line != braille_graph->cend() && row_it != canvas.end() ) {
                row_it->insert( row_it->end(), braille_line->cbegin(), braille_line->cend() );
                braille_line++;
                row_it++;
            }
        } else { //GraphPosition::E or GraphPosition::VOID
            while( braille_line != braille_graph->cend() && row_it != canvas.end() ) {
                row_it->insert( row_it->begin(), braille_line->cbegin(), braille_line->cend() );
                braille_line++;
                row_it++;
            }
        }
    }

    /**
     * Creates a blank canvas with labels (if any)
     * @param axis_property_x X-Axis category properties
     * @param axis_property_y Y-Axis numbered properties
     * @param tick_intervals  XY tick intervals in pixels
     * @param meta_graph_data MetaGraphData container (points and X/Y tick values if any)
     * @return Labelled BrailleMap_t canvas
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::unique_ptr<BrailleMap_t> BrailleHistogram<O, W, H>::createLabelledCanvas(
            const graph::CatAxisProperty<std::string> &axis_property_x,
            const graph::NumAxisProperty<float>       &axis_property_y,
            const Coordinate2D<size_t>                &tick_intervals,
            graph::MetaGraphData<std::string, float>  &meta_graph_data  ) const
    {
        using eadlib::cli::braille::graph::labelling::Position;
        using eadlib::cli::braille::BrailleCharAxis;
        using eadlib::cli::braille::smallestCharSpacing;

        auto labelled_canvas  = std::make_unique<BrailleMap_t>();

        meta_graph_data._tick_values_x.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.x < rhs.first.x; } );
        meta_graph_data._tick_values_y.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.y < rhs.first.y; } );

        auto char_spacing_x = smallestCharSpacing<BrailleCharAxis::X>( tick_intervals.x );//TODO cat labels

        if( axis_property_x._tick_label_pos == Position::NORTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        auto y_label_offset = addYNumLabels( axis_property_y, meta_graph_data, *labelled_canvas );

        if( axis_property_x._tick_label_pos == Position::SOUTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        if( axis_property_x._tick_label_pos == Position::NORTH )
            addXNumLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->front(), y_label_offset );//TODO cat labels

        if( axis_property_x._tick_label_pos == Position::SOUTH )
            addXNumLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->back(), y_label_offset );//TODO cat labels

        return std::move( labelled_canvas );
    }

    /**
     * Creates a blank canvas with labels (if any)
     * @param axis_property_x X-Axis numbered properties
     * @param axis_property_y Y-Axis category properties
     * @param tick_intervals  XY tick intervals in pixels
     * @param meta_graph_data MetaGraphData container (points and X/Y tick values if any)
     * @return Labelled BrailleMap_t canvas
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::unique_ptr<BrailleMap_t> BrailleHistogram<O, W, H>::createLabelledCanvas(
            const graph::NumAxisProperty<float>       &axis_property_x,
            const graph::CatAxisProperty<std::string> &axis_property_y,
            const Coordinate2D<size_t>                &tick_intervals,
            graph::MetaGraphData<float, std::string>  &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::labelling::Position;
        using eadlib::cli::braille::BrailleCharAxis;
        using eadlib::cli::braille::smallestCharSpacing;

        auto labelled_canvas  = std::make_unique<BrailleMap_t>();

        meta_graph_data._tick_values_x.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.x < rhs.first.x; } );
        meta_graph_data._tick_values_y.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.y < rhs.first.y; } );

        auto char_spacing_x = smallestCharSpacing<BrailleCharAxis::X>( tick_intervals.x );

        if( axis_property_x._tick_label_pos == Position::NORTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        auto y_label_offset = addYNumLabels( axis_property_y, meta_graph_data, *labelled_canvas );//TODO cat labels

        if( axis_property_x._tick_label_pos == Position::SOUTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        if( axis_property_x._tick_label_pos == Position::NORTH )
            addXNumLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->front(), y_label_offset );

        if( axis_property_x._tick_label_pos == Position::SOUTH )
            addXNumLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->back(), y_label_offset );

        return std::move( labelled_canvas );
    }

    /**
     * Adds X-Axis labels to a graph canvas
     * @param axis_property   X-Axis properties
     * @param char_spacing    Minimal number of character blocks to contain two consecutive ticks on the X axis
     * @param meta_graph_data Chart data (tick info/plot coordinates)
     * @param y_label_offset  Y-label size and direction if any
     * @param label_row       Iterator to the row where the X-axis labels are to be placed
     * @return Y-Axis offset used by the labelling
     * @throw std::invalid_argument when the labeling position in the axis property is E or W
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::optional<typename BrailleHistogram<O, W, H>::Offset_t> BrailleHistogram<O, W, H>::addXNumLabels(
            const graph::NumAxisProperty<float>      &axis_property,
            const size_t                             &char_spacing,
            graph::MetaGraphData<float, std::string> &meta_graph_data,
            std::vector<size_t>                      &label_row,
            std::optional<Offset_t>                  y_label_offset ) const
    {
        using eadlib::cli::braille::graph::removeOverlappingTicks;
        using eadlib::cli::braille::graph::NumLabelProperty;
        using eadlib::cli::braille::graph::labelling::Position;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID && meta_graph_data._tick_values_x.empty() )
            return std::optional<Offset_t>( { 0, axis_property._tick_label_pos } );

        //Calculating universal label size
        auto label_specs = NumLabelProperty( axis_property._data_interval,
                                        meta_graph_data._tick_values_x.back().second,
                                        meta_graph_data._tick_values_x.front().second,
                                        MAX_NUM_LABEL_SIZE );

        if( label_specs._size > MAX_NUM_LABEL_SIZE ) {
            std::cerr << "X-Axis label size was found to be too small. "
                      << "Truncating may have occurred as a result."
                      << std::endl;
            label_specs._size = MAX_NUM_LABEL_SIZE;
        }

        //Calculating left offset before graph
        auto left_offset = ( y_label_offset.has_value() && y_label_offset->second == Position::WEST )
                           ? y_label_offset.value().first
                           : 0;

        try {
            removeOverlappingTicks( label_specs._size, char_spacing, meta_graph_data._tick_values_x );
        } catch( std::invalid_argument &e ) {
            std::cerr << e.what() << std::endl;
        }

        //Labelling
        using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
        using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

        auto tick_it   = meta_graph_data._tick_values_x.begin();

        while( tick_it != meta_graph_data._tick_values_x.end() ) {
            writeNumericXLabel<float>( label_specs, MIDDLE_ALIGN, MAX_NUM_LABEL_SIZE, left_offset, *tick_it, label_row );
            tick_it++;
        }

        std::optional<Offset_t>( { 1, axis_property._tick_label_pos } );
    }

    /**
     * Adds Y-Axis labels a graph canvas
     * @param axis_property  Y-Axis properties
     * @param chart_data     Chart data (tick info/plot coordinates)
     * @param graph_canvas   Final graph canvas
     * @return X-axis offset used by the labelling
     * @throws std::invalid_argument when the labeling position in the axis property is N or S
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::optional<typename BrailleHistogram<O, W, H>::Offset_t> BrailleHistogram<O, W, H>::addYNumLabels(
            const graph::NumAxisProperty<float>      &axis_property,
            graph::MetaGraphData<std::string, float> &meta_graph_data,
            BrailleMap_t                             &graph_canvas ) const
    {
        using eadlib::cli::braille::graph::NumLabelProperty;
        using eadlib::cli::braille::graph::labelling::Position;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID || meta_graph_data._tick_values_y.empty() ) {
            while( graph_canvas.size() < H ) //add all required rows
                graph_canvas.emplace_back( std::vector<size_t>() );
            return std::optional<Offset_t>();
        }

        //Calculating universal label size
        auto label_specs = NumLabelProperty( axis_property._data_interval,
                                             meta_graph_data._tick_values_y.back().second,
                                             meta_graph_data._tick_values_y.front().second,
                                             MAX_NUM_LABEL_SIZE );

        if( label_specs._size > MAX_NUM_LABEL_SIZE ) {
            std::cerr << "Y-Axis label size was found to be too small. "
                      << "Truncating may have occurred as a result."
                      << std::endl;
            label_specs._size = MAX_NUM_LABEL_SIZE;
        }

        //Labelling
        using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
        using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

        auto offset        = graph_canvas.size();
        auto row_index     = 0L;
        auto tick_it       = meta_graph_data._tick_values_y.begin();

        while( tick_it != meta_graph_data._tick_values_y.end() ) {
            size_t characteristic = eadlib::string::length_int<float>( tick_it->second );
            auto   char_height    = eadlib::cli::braille::charHeight( tick_it->first.y + 1 );
            auto   write_index    = ( char_height > 0 ? char_height - 1 : 0 ) + offset;

            //Add any missing empty label rows
            while( graph_canvas.size() < write_index ) {
                graph_canvas.emplace_back( std::vector<size_t>( label_specs._size, 0x20 ) );
            }

            if( write_index <= row_index ) { // ignore & remove ticks that would overlap
                tick_it = meta_graph_data._tick_values_y.erase( tick_it );
            } else {
                auto label = std::vector<size_t>();

                writeNumericYLabel<float>( label_specs, RIGHT_ALIGN, *tick_it, label );

                graph_canvas.emplace_back( label );
                row_index = graph_canvas.size() - 1;
                tick_it++;
            }
        }

        while( graph_canvas.size() < H + offset ) //add blank labels to any remaining unlabelled rows
            graph_canvas.emplace_back( std::vector<size_t>( label_specs._size, 0x20 ) );

        return std::optional<Offset_t>( { label_specs._size, axis_property._tick_label_pos } );
    }

    /**
     * //TODO
     * @param axis_property
     * @param meta_graph_data
     * @param graph_canvas
     * @return
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::optional<typename BrailleHistogram<O, W, H>::Offset_t> BrailleHistogram<O, W, H>::addXCatLabels(
            const graph::CatAxisProperty<std::string> &axis_property,
            graph::MetaGraphData<std::string, float>  &meta_graph_data,
            BrailleMap_t                              &graph_canvas ) const
    {
        //TODO
        using eadlib::cli::braille::graph::removeOverlappingTicks;
        using eadlib::cli::braille::graph::NumLabelProperty;
        using eadlib::cli::braille::graph::labelling::Position;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID && meta_graph_data._tick_values_x.empty() )
            return std::optional<Offset_t>( { 0, axis_property._tick_label_pos } );

//        //Calculating universal label size
//        auto label_specs = NumLabelProperty( axis_property._data_interval,
//                                             meta_graph_data._tick_values_x.back().second,
//                                             meta_graph_data._tick_values_x.front().second,
//                                             MAX_NUM_LABEL_SIZE );
//
//        if( label_specs._size > MAX_NUM_LABEL_SIZE ) {
//            std::cerr << "X-Axis label size was found to be too small. "
//                      << "Truncating may have occurred as a result."
//                      << std::endl;
//            label_specs._size = MAX_NUM_LABEL_SIZE;
//        }
//
//        //Calculating left offset before graph
//        auto left_offset = ( y_label_offset.has_value() && y_label_offset->second == Position::WEST )
//                           ? y_label_offset.value().first
//                           : 0;
//
//        try {
//            removeOverlappingTicks( label_specs._size, char_spacing, meta_graph_data._tick_values_x );
//        } catch( std::invalid_argument &e ) {
//            std::cerr << e.what() << std::endl;
//        }
//
//        //Labelling
//        using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
//        using eadlib::cli::braille::graph::labelling::writeNumericXLabel;
//
//        auto tick_it   = meta_graph_data._tick_values_x.begin();
//
//        while( tick_it != meta_graph_data._tick_values_x.end() ) {
//            writeNumericXLabel<float>( label_specs, MIDDLE_ALIGN, MAX_NUM_LABEL_SIZE, left_offset, *tick_it, label_row );
//            tick_it++;
//        }

        std::optional<Offset_t>( { 1, axis_property._tick_label_pos } );
    }

    /**
     * //TODO
     * @param axis_property
     * @param meta_graph_data
     * @param graph_canvas
     * @return
     */
    template<graph::Orientation O, size_t W, size_t H>
        std::optional<typename BrailleHistogram<O, W, H>::Offset_t> BrailleHistogram<O, W, H>::addYCatLabels(
            const graph::CatAxisProperty<std::string> &axis_property,
            graph::MetaGraphData<float, std::string>  &meta_graph_data,
            BrailleMap_t                              &graph_canvas ) const
    {
        using eadlib::cli::braille::graph::labelling::Position;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID || meta_graph_data._tick_values_y.empty() ) {
            while( graph_canvas.size() < H ) //add all required rows
                graph_canvas.emplace_back( std::vector<size_t>() );
            return std::optional<Offset_t>();
        }

        bool truncate_labels = ( axis_property._max_label_size > MAX_NUM_LABEL_SIZE );

        if( truncate_labels ) {
            std::cerr << "Y-Axis label max size was found to be too small. "
                      << "Truncating may have occurred as a result."
                      << std::endl;
        }


        //TODO

        //Labelling
        using eadlib::cli::braille::graph::labelling::Axis;
        using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

        auto offset        = graph_canvas.size();
        auto row_index     = 0L;
        auto tick_it       = meta_graph_data._tick_values_y.begin();

//        while( tick_it != meta_graph_data._tick_values_y.end() ) {
//            size_t characteristic = eadlib::string::length_int<float>( tick_it->second );
//            auto   char_height    = eadlib::cli::braille::charHeight( tick_it->first.y + 1 );
//            auto   write_index    = ( char_height > 0 ? char_height - 1 : 0 ) + offset;
//
//            //Add any missing empty label rows
//            while( graph_canvas.size() < write_index ) {
//                graph_canvas.emplace_back( std::vector<size_t>( label_specs._size, 0x20 ) );
//            }
//
//            if( write_index <= row_index ) { // ignore & remove ticks that would overlap
//                tick_it = meta_graph_data._tick_values_y.erase( tick_it );
//            } else {
//                auto label = std::vector<size_t>();
//
//                writeNumericLabel<Axis::Y, float>( label_specs, 0, *tick_it, label );
//
//                graph_canvas.emplace_back( label );
//                row_index = graph_canvas.size() - 1;
//                tick_it++;
//            }
//        }

        while( graph_canvas.size() < H + offset ) //add blank labels to any remaining unlabelled rows
            graph_canvas.emplace_back( std::vector<size_t>( axis_property._max_label_size, 0x20 ) );

        return std::optional<Offset_t>( { axis_property._max_label_size, axis_property._tick_label_pos } );
    }
}


#endif //EADLIB_CLI_BRAILLE_BRAILLEHISTOGRAM_H
