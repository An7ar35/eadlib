/*!
    @class          eadlib::cli::braille::graph::CatAxisProperty
    @brief          Category axis property container
    @dependencies   eadlib::cli::braille::graph::labelling::
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
*/
#ifndef EADLIB_CATAXISPROPERTY_H
#define EADLIB_CATAXISPROPERTY_H

#include <string>
#include <iomanip>
#include <type_traits>

#include "../../../../../src/cli/braille/graph/labelling/labelling.h"

namespace eadlib::cli::braille::graph {
    template<typename T> struct CatAxisProperty {
        /**
         * Constructor (default on the X-axis)
         */
        CatAxisProperty() :
            CatAxisProperty( labelling::Axis::X )
        {}

        /**
         * Constructor
         * @param axis Axis the properties belong to
         */
        explicit CatAxisProperty( labelling::Axis axis ) :
            _axis( axis ),
            _label( "" ),
            _max_label_size( 0 ),
            _tick_label_pos( labelling::Position::VOID )
        {}

        /**
         * Sets a labelling position for the axis
         * @param pos Labelling position
         */
        void setLabellingPosition( labelling::Position pos ) {
            _tick_label_pos = graph::labelling::getAbsoluteLabelPosition( _axis, pos );
        }

        //Variables
        labelling::Axis     _axis;
        std::string         _label;
        size_t              _max_label_size;
        labelling::Position _tick_label_pos;
    };
}
#endif //EADLIB_CATAXISPROPERTY_H
