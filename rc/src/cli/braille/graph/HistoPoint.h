/*!
    @class          eadlib::cli::braille::graph::HistoPoint
    @brief          Histogram point container

    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
*/
#ifndef EADLIB_CLI_BRAILLE_GRAPH_HISTOPOINT_H
#define EADLIB_CLI_BRAILLE_GRAPH_HISTOPOINT_H

#include <type_traits>

namespace eadlib::cli::braille::graph {
    template<typename K, //Category key type
             typename V  //Value range type
    > struct HistoPoint {
        struct Range {
            Range( V low, V high ) :
                _low( low ),
                _high( high )
            {}

            V _low;
            V _high;
        };

        /**
         * Constructor
         * @param label Category label
         * @param range Value range
         */
        HistoPoint( const K &label, Range range ) :
            _label( label ),
            _range( range )
        {};

        /**
         * Constructor
         * @param label Category label
         * @param from  Value range begin
         * @param to    Value range end
         */
        HistoPoint( const K &label, V from, V to ) :
            _label( label ),
            _range( Range( from, to ) )
        {};

        K     _label;
        Range _range;
    };
}

#endif //EADLIB_CLI_BRAILLE_GRAPH_HISTOPOINT_H
