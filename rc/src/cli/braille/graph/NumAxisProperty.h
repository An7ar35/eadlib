/*!
    @class          eadlib::cli::braille::graph::NumAxisProperty
    @brief          Number axis property container
    @dependencies   eadlib::cli::braille::graph::labelling::
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
*/
#ifndef EADLIB_BRAILLE_GRAPH_NUMAXISPROPERTY_H
#define EADLIB_BRAILLE_GRAPH_NUMAXISPROPERTY_H

#include <string>
#include <iomanip>
#include <type_traits>

#include "../../../../../src/cli/braille/graph/labelling/labelling.h"

namespace eadlib::cli::braille::graph {
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    > struct NumAxisProperty {
        /**
         * Constructor (default on an Y-axis)
         */
        NumAxisProperty() :
            NumAxisProperty( labelling::Axis::X )
        {};

        /**
         * Constructor
         * @param axis Axis the properties belong to
         */
        explicit NumAxisProperty( labelling::Axis axis ) :
            _axis( axis ),
            _label( "" ),
            _draw_tick( false ),
            _tick_direction( 0 ),
            _data_interval( 1 ),
            _min( 0 ),
            _max( 0 ),
            _tick_label_pos( labelling::Position::VOID )
        {}

        /**
         * Sets a labelling position for the axis
         * @param pos Labelling position
         */
        void setLabellingPosition( labelling::Position pos ) {
            _tick_label_pos = graph::labelling::getAbsoluteLabelPosition( _axis, pos );
        }

        //Variables
        labelling::Axis     _axis;
        std::string         _label;
        bool                _draw_tick;
        int                 _tick_direction;
        T                   _data_interval;
        T                   _min;
        T                   _max;
        labelling::Position _tick_label_pos;
    };
}

#endif //EADLIB_BRAILLE_GRAPH_NUMAXISPROPERTY_H
