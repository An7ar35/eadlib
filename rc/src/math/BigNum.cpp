#include "BigNum.h"
#include <limits>
#include <regex>
#include <iomanip>

/**
 * Default Constructor
 */
eadlib::math::BigNum::BigNum() :
    itsSign( Sign::POSITIVE ),
    itsType( Type::INTEGER ),
    itsCharacteristic( 1, '0' )
{ };

/**
 * Constructor
 * @param value Value of 'int' type
 */
eadlib::math::BigNum::BigNum( int value ) {
    itsType = Type::INTEGER;
    processInteger<int>( processSign<int>( value, itsSign ), itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'long' type
 */
eadlib::math::BigNum::BigNum( long value ) {
    itsType = Type::INTEGER;
    processInteger<long>( processSign<long>( value, itsSign ), itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'long long' type
 */
eadlib::math::BigNum::BigNum( long long value ) {
    itsType = Type::INTEGER;
    processInteger<long long>( processSign<long long>( value, itsSign ), itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'unsigned int' type
 */
eadlib::math::BigNum::BigNum( unsigned int value ) {
    itsType = Type::INTEGER;
    itsSign = Sign::POSITIVE;
    processInteger<unsigned int>( value, itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'unsigned long' type
 */
eadlib::math::BigNum::BigNum( unsigned long value ) {
    itsType = Type::INTEGER;
    itsSign = Sign::POSITIVE;
    processInteger<unsigned long>( value, itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'unsigned long long' type
 */
eadlib::math::BigNum::BigNum( unsigned long long value ) {
    itsType = Type::INTEGER;
    itsSign = Sign::POSITIVE;
    processInteger<unsigned long long>( value, itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'double' type
 */
eadlib::math::BigNum::BigNum( double value ) {
    itsType = Type::DECIMAL;
    processDecimal<double>( processSign<double>( value, itsSign ), itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'float' type
 */
eadlib::math::BigNum::BigNum( float value ) {
    itsType = Type::DECIMAL;
    processDecimal<float>( processSign<float>( value, itsSign ), itsCharacteristic, itsMantissa );
}

/**
 * Constructor
 * @param value Value of 'std::string' type
 * @exception std::invalid_argument when string value is not a number
 */
eadlib::math::BigNum::BigNum( std::string value ) {
    if( isValid( value ) ) {
        processSign<std::string>( value, itsSign );
        itsType = processString( value, itsCharacteristic, itsMantissa );
    } else {
        LOG_ERROR( "[eadlib::math::BigNum( \'", value, "\' )] Argument is not recognised as a number." );
        throw std::invalid_argument( "BigNum( std::string ) : argument is not recognised as a number." );
    }
}

/**
 * Copy-Constructor
 * @param big_num 'BigNum'
 */
eadlib::math::BigNum::BigNum( const eadlib::math::BigNum &big_num ) :
    itsType( big_num.itsType ),
    itsSign( big_num.itsSign ),
    itsCharacteristic( big_num.itsCharacteristic ),
    itsMantissa(big_num.itsMantissa )
{}

/**
 * Prints the BigNum into a ostream
 * @param os Output Stream
 * @return os Output Stream
 */
std::ostream &eadlib::math::BigNum::print( std::ostream &os ) const {
    if( itsSign == Sign::NEGATIVE ) {
        os << "-";
    }
    for( std::vector<char>::const_reverse_iterator it = itsCharacteristic.rbegin(); it != itsCharacteristic.rend(); ++it ) {
        os << *it;
    }
    if( itsType == Type::DECIMAL ) {
        os << ".";
        for( std::vector<char>::const_iterator it = itsMantissa.begin(); it != itsMantissa.end(); ++it ) {
            os << *it;
        }
    }
    return os;
}

/**
 * Assignment operator
 * @param rhs 'BigNum' object to assign from
 * @return This 'BigNum' object
 */
eadlib::math::BigNum & eadlib::math::BigNum::operator =( const eadlib::math::BigNum &rhs ) {
    itsSign = rhs.itsSign;
    itsType = rhs.itsType;
    itsCharacteristic = rhs.itsCharacteristic;
    itsMantissa = rhs.itsMantissa;
    return *this;
}

/**
 * Converts the 'BigNum' to a 'std::string'
 * @return 'BigNum' as string
 */
std::string eadlib::math::BigNum::toString() const {
    std::ostringstream oss;
    oss << *this;
    return oss.str();
}

/**
 * Gets the sign of the 'BigNum'
 * @return Sign (+/-)
 */
std::string eadlib::math::BigNum::getSign() const {
    if( itsSign == Sign::NEGATIVE ) {
        return "-";
    } else {
        return "+";
    }
}

/**
 * Gets the digits of the Characteristic (left of the decimal point) part of the 'BigNum'
 * @return Characteristic of the number
 */
std::string eadlib::math::BigNum::getCharacteristic() const {
    std::string s { };
    for( std::vector<char>::const_reverse_iterator it = itsCharacteristic.rbegin(); it != itsCharacteristic.rend(); ++it ) {
        s += *it;
    }
    return s;
}

/**
 * Gets the digits of the Mantissa (right of the decimal point) part of the 'BigNum'
 * @return mantissa of the number
 */
std::string eadlib::math::BigNum::getMantissa() const {
    if( itsType == Type::DECIMAL ) {
        std::string s { };
        for( std::vector<char>::const_iterator it = itsMantissa.begin(); it != itsMantissa.end(); ++it ) {
            s += *it;
        }
        return s;
    } else {
        return "0";
    }
}

/**
 *
 */
int eadlib::math::BigNum::toInt() const {
    //TODO
    return 0;
}

long long eadlib::math::BigNum::toLongLong() const {
    //TODO
    return 0;
}

double eadlib::math::BigNum::toDouble() const {
    //TODO
    return 0;
}

/**
 * Boolean Smaller Operator (<)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator <( const eadlib::math::BigNum &rhs ) const {
    if( itsSign == Sign::NEGATIVE && rhs.itsSign == Sign::POSITIVE ) {
        return true;
    }
    if( itsSign == Sign::POSITIVE && rhs.itsSign == Sign::NEGATIVE ) {
        return false;
    }
    int l_comparaison = compareCharacteristic_abs( rhs );
    int r_comparaison = compareMantissa_abs( rhs );
    if( itsSign == Sign::POSITIVE && rhs.itsSign == Sign::POSITIVE ) {
        return l_comparaison < 0 || ( l_comparaison == 0 && r_comparaison < 0 );
    }
    if( itsSign == Sign::NEGATIVE && rhs.itsSign == Sign::NEGATIVE ) {
        return l_comparaison > 0 || ( l_comparaison == 0 && r_comparaison > 0 );
    }
}

/**
 * Boolean Bigger Operator (>)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator >( const eadlib::math::BigNum &rhs ) const {
    return !( operator <( rhs ) || operator ==( rhs ) );
}

/**
 * Boolean Smaller-Equal Operator (<=)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator <=( const eadlib::math::BigNum &rhs ) const {
    return !( operator >( rhs ) );
}

/**
 * Boolean Bigger-Equal Operator (>=)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator >=( const eadlib::math::BigNum &rhs ) const {
    return !( operator <( rhs ) );
}

/**
 * Boolean Not-Equivalent Operator (!=)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator !=( const eadlib::math::BigNum &rhs ) const {
    return !( operator ==( rhs ) );
}

/**
 * Boolean Equivalent Operator (==)
 * @param rhs 'BigNum' to compare to
 * @return Result of comparison
 */
bool eadlib::math::BigNum::operator ==( const eadlib::math::BigNum &rhs ) const {
     return ( itsCharacteristic == rhs.itsCharacteristic && itsMantissa == rhs.itsMantissa && itsSign == rhs.itsSign );
}

/**
 * Checks the 'BigNum' is equal to another within the precision specified
 * @param rhs          Other BigNum to compare to
 * @param error_margin Error margin tolerated
 * @return Equal status within the error margin specified
 */
bool eadlib::math::BigNum::nearEqual( const BigNum &rhs, BigNum error_margin ) {
    //TODO
    return false;
}

/**
 * Addition Operator (+)
 * @param rhs 'BigNum' to add with
 * @return Result
 */
const eadlib::math::BigNum eadlib::math::BigNum::operator +( const eadlib::math::BigNum &rhs ) const {
    const unsigned int lhs_lDigits { lDigits() };
    const unsigned int lhs_rDigits { rDigits() };
    const unsigned int rhs_lDigits { rhs.lDigits() };
    const unsigned int rhs_rDigits { rhs.rDigits() };
    //[01145]
    //[23---] padding needed
    //TODO
    BigNum result;
    int mantissa_carry { 0 };
    //mantissa addition
    if( lhs_rDigits > 0 || rhs_rDigits > 0 ) {
        result.itsType = Type::DECIMAL;
        std::list<char> temp { };
        /*
        if( itsSign == rhs.itsSign ) {
            for( unsigned int i = longest_digits; i > 0; i-- ) {
                if( i <= shortest_digits ) {
                    //calc
                } else {
                    //copy over
                }
            }
        } else { //Signs not the same so subtract

        }
         */

    }



    //characteristic addition
    lDigits() >= rhs.lDigits() ? result.itsCharacteristic.reserve( lDigits() ) : result.itsCharacteristic.reserve( rhs.lDigits() );

    return math::BigNum();
}

const eadlib::math::BigNum eadlib::math::BigNum::operator -( const eadlib::math::BigNum &rhs ) const {
    //TODO
    return math::BigNum();
}

const eadlib::math::BigNum eadlib::math::BigNum::operator *( const eadlib::math::BigNum &rhs ) const {
    //TODO
    return math::BigNum();
}

const eadlib::math::BigNum eadlib::math::BigNum::operator /( const eadlib::math::BigNum &rhs ) const {
    //TODO
    return math::BigNum();
}

const eadlib::math::BigNum & eadlib::math::BigNum::operator +=( const eadlib::math::BigNum &rhs ) {
    //TODO
    return *this;
}

const eadlib::math::BigNum & eadlib::math::BigNum::operator -=( const eadlib::math::BigNum &rhs ) {
    //TODO
    return *this;
}

const eadlib::math::BigNum & eadlib::math::BigNum::operator *=( const eadlib::math::BigNum &rhs ) {
    //TODO
    return *this;
}

const eadlib::math::BigNum & eadlib::math::BigNum::operator /=( const eadlib::math::BigNum &rhs ) {
    //TODO
    return *this;
}

/**
 * Gets the absolute value of the 'BigNum'
 * @return Absolute value as a 'BigNum'
 */
eadlib::math::BigNum eadlib::math::BigNum::abs() const {
    if( itsSign == Sign::NEGATIVE ) {
        return negate();
    } else {
        BigNum num( *this );
        return num;
    }
}

/**
 * Negates the 'BigNum'
 * @return Negated 'BigNum' as a 'BigNum'
 */
eadlib::math::BigNum eadlib::math::BigNum::negate() const {
    BigNum num( *this );
    num.itsSign == Sign::POSITIVE ? num.itsSign = Sign::NEGATIVE : num.itsSign = Sign::POSITIVE;
    return num;
}

/**
 * Gets the Characteristic of the 'BigNum'
 * @return Characteristic as a 'BigNum'
 */
eadlib::math::BigNum eadlib::math::BigNum::characteristic() const {
    if( itsType == Type::INTEGER ) {
        BigNum num( *this );
        return num;
    } else {
        BigNum num;
        num.itsSign = itsSign;
        num.itsCharacteristic = itsCharacteristic;
        return num;
    }
}

/**
 * Gets the Mantissa of the 'BigNum'
 * return Mantissa as a 'BigNum'
 */
eadlib::math::BigNum eadlib::math::BigNum::mantissa() const {
    BigNum num;
    num.itsSign = itsSign;
    num.itsType = Type::DECIMAL;
    num.itsMantissa = itsMantissa;
    return num;
}

/**
 * Gets the number of digits left of the decimal (Characteristic)
 * @return Number of digits
 */
unsigned int eadlib::math::BigNum::lDigits() const {
    unsigned int size = itsCharacteristic.size();
    return ( size == 1 && itsCharacteristic[ 0 ] == '0' ) ? 0 : size;
}

/**
 * Gets the number of digits right of the decimal (Mantissa)
 */
unsigned int eadlib::math::BigNum::rDigits() const {
    return itsMantissa.size();
}

/**
 * [PRIVATE] Compares the absolute value of the Characteristic with another BigNum's one
 * @param rhs 'BigNum' to compare with
 * @return (-1) if smaller, (0) if same, (1) if bigger
 */
int eadlib::math::BigNum::compareCharacteristic_abs( const eadlib::math::BigNum &rhs ) const {
    if( lDigits() < rhs.lDigits() ) return -1;
    if( lDigits() > rhs.lDigits() ) return 1;
    if( lDigits() == 0 && rDigits() == 0 ) return 0;
    //Characteristic lengths are the same:
    for( unsigned int i = 0; i < itsCharacteristic.size(); i++ ) {
        if( itsCharacteristic[ i ] < rhs.itsCharacteristic[ i ] ) return -1;
        if( itsCharacteristic[ i ] > rhs.itsCharacteristic[ i ] ) return 1;
    }
    return 0;
}

/**
 * [PRIVATE] Compares the absolute value of the Mantissa with another BigNum's one
 * @param rhs 'BigNum' to compare with
 * @return (-1) if smaller, (0) if same, (1) if bigger
 */
int eadlib::math::BigNum::compareMantissa_abs( const eadlib::math::BigNum &rhs ) const {
    //Mantissa's length is irrelevant
    if( rDigits() > 0 && rhs.rDigits() == 0 ) return 1;
    if( rDigits() == 0 && rhs.rDigits() > 0 ) return -1;
    if( rDigits() == 0 && rhs.rDigits() == 0 ) return 0;
    //Both Mantissas have digits and need to be compared
    if( rDigits() < rhs.rDigits() ) {
        for( unsigned int i = 0; i < rDigits(); i++ ) {
            if( itsMantissa[ i ] < rhs.itsMantissa[ i ] ) return -1;
            if( itsMantissa[ i ] > rhs.itsMantissa[ i ] ) return 1;
        }
        return -1;
    } else if( rhs.rDigits() < rDigits() ) {
        for( unsigned int i = 0; i < rhs.rDigits(); i++ ) {
            if( itsMantissa[ i ] < rhs.itsMantissa[ i ] ) return -1;
            if( itsMantissa[ i ] > rhs.itsMantissa[ i ] ) return 1;
        }
        return 1;
    } else { //( rDigits() == rhs.rDigits() )
        for( unsigned int i = 0; i < rDigits(); i++ ) {
            if( itsMantissa[ i ] < rhs.itsMantissa[ i ] ) return -1;
            if( itsMantissa[ i ] > rhs.itsMantissa[ i ] ) return 1;
        }
        return 0;
    }
}

/**
 * [PRIVATE] Checks if a string is a valid number
 * @param value String to check
 * @return Result of check
 */
bool eadlib::math::BigNum::isValid( std::string &value ) const {
    std::regex integer_format( "^[\\-]?[ \\d]+$" ); //integers
    std::regex decimal_format_a( "^[\\-]?[ \\d]+[\\.][ \\d]+$" ); //decimals with characteristic
    std::regex decimal_format_b( "^[\\-]?[\\.][ \\d]+$" ); //decimals with no characteristic
    return std::regex_match( value, integer_format ) || std::regex_match( value, decimal_format_a ) || std::regex_match( value, decimal_format_b );
}

/**
 * [PRIVATE] Processes digits of a string type value into char containers
 * @param value Value to be processed
 * @param left_container Container for the Characteristic's digits
 * @param right_container Container for the Mantissa's digits
 * @return Type of value (Integer/Decimal)
 * @exception std::invalid_argument when string value is not a number
 */
eadlib::math::BigNum::Type eadlib::math::BigNum::processString( std::string &value, std::vector<char> &left_container, std::vector<char> &right_container ) {
    value.erase( std::remove_if( value.begin(), value.end(), isspace ), value.end() ); //loose the whitespace
    removeUselessZeros( value ); //loose any leading and tailing '0'
    size_t delimiter = value.find( '.' );
    if( delimiter != std::string::npos ) { //decimal found
        std::string l_decimal = value.substr( 0, delimiter );
        unsigned int l_count = l_decimal.length();
        std::string r_decimal = value.substr( delimiter + 1 );
        unsigned int r_count = r_decimal.length();
        if( l_count < 1 && r_count < 1 ) {
            LOG_ERROR( "[eadlib::math::BigNum::processString( \'", value, "\', ... )] Not a number." );
            throw std::invalid_argument( "processString( std::string &, std::vector<char> &, std::vector<char> & ) : argument is not recognised as a number." );
        }
        if( l_count > 0 ) {
            left_container.reserve( l_count );
            for( std::string::reverse_iterator it = l_decimal.rbegin(); it != l_decimal.rend(); ++it ) {
                left_container.push_back( *it );
            }
        } else {
            left_container.assign( 1, '0' );
        }
        if( r_count > 0 ) {
            right_container.reserve( r_count );
            for( std::string::iterator it = r_decimal.begin(); it != r_decimal.end(); ++it ) {
                right_container.push_back( *it );
            }
            return Type::DECIMAL;
        } else {
            return Type::INTEGER;
        }
    } else { //no decimal found
        std::string l_decimal = value.substr( 0, delimiter );
        unsigned int l_count = l_decimal.length();
        if( l_count < 1 ) {
            left_container.assign( 1, '0' );
        } else {
            left_container.reserve( l_count );
            for( std::string::reverse_iterator it = l_decimal.rbegin(); it != l_decimal.rend(); ++it ) {
                left_container.push_back( *it );
            }
        }
        return Type::INTEGER;
    }
}

/**
 * [PRIVATE] Removes leading sign (-) in the value
 * @param value String to process
 */
bool eadlib::math::BigNum::removeSign( std::string &value ) {
    size_t delimiter = value.find( '-' );
    if( delimiter != std::string::npos ) {
        value = value.substr( delimiter + 1 );
        return true;
    } else {
        return false;
    }
}

/**
 * [PRIVATE] Removes any leading and tailing '0' of an unsigned string number
 * @param value String to process
 */
void eadlib::math::BigNum::removeUselessZeros( std::string &value ) {
    if( !value.empty() && value.at( 0 ) == '0' ) {
        bool flag { true };
        do {
            flag = removeLeadingZeros( value );
        } while( flag );
    }
    if( !value.empty() && value.at( value.length() - 1 ) == '0' ) {
        bool flag { true };
        do {
            flag = removeTailingZeros( value );
        } while( flag );
    };
    if( !value.empty() && value.at( value.length() - 1 ) == '.' ) {
        value = value.substr( 0, value.length() - 1 );
    }
}

/**
 * [PRIVATE] Removes leading zeros of a string by blocks of 100 max
 * @param s String to process
 * @return If there are still leading '0's
 */
bool eadlib::math::BigNum::removeLeadingZeros( std::string &value ) {
    unsigned int counter { 0 };
    while( counter != value.length() && value.at( counter ) == '0' && counter < 100 ) {
        counter++;
    }
    value = value.substr( counter );
    return ( !value.empty() && counter == 100 && value.at( 0 ) == '0' );
}

/**
 * [PRIVATE] Removes tailing zeros of a string by blocks of 100 max
 * @param s String to process
 * @return If there are still tailing '0's
 */
bool eadlib::math::BigNum::removeTailingZeros( std::string &value ) {
    unsigned int counter { 0 };
    unsigned long long position = { value.length() - 1 };
    while( counter != value.length() && value.at( position ) == '0' && counter < 100 ) {
        position--;
        counter++;
    }
    value = value.substr( 0, position + 1 );
    return ( !value.empty() && counter == 100 && value.at( value.length() - 1 ) == '0' );
}