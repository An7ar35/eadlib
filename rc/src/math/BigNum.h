#ifndef EADLIB_BIGNUM_H
#define EADLIB_BIGNUM_H

#include <string>
#include <vector>
#include <list>
#include "../src/logger/Logger.h"
#include "../src/tool/Convert.h"

//TODO Scientific notation support for String input and output
//would it be faster to convert to binary or a bunch of ints in an array as sections of the bignum?
//http://www.doc.ic.ac.uk/~eedwards/compsys/arithmetic/
//https://stackoverflow.com/questions/13630002/storing-binary-number-in-integer-variable
//std::bitset<size_t N> !!!!


namespace eadlib {
    namespace math{
        class BigNum {
          public:
            //Constructors
            BigNum();
            BigNum( int value );
            BigNum( long value );
            BigNum( long long value );
            BigNum( unsigned int value );
            BigNum( unsigned long value );
            BigNum( unsigned long long value );
            BigNum( double value );
            BigNum( float value );
            BigNum( std::string value );
            BigNum( const char *value ) : BigNum( std::string( value ) ) { };
            BigNum( const BigNum &big_num );
            //Destructor
            ~BigNum() { };
            //IO
            std::ostream & print( std::ostream &os ) const;
            //Assignment & Conversion
            BigNum & operator =( const BigNum &rhs );
            std::string toString() const;
            std::string getSign() const;
            std::string getCharacteristic() const;
            std::string getMantissa() const;
            int toInt() const;
            long long toLongLong() const;
            double toDouble() const;
            //Boolean Operators
            bool operator <( const BigNum &rhs ) const;
            bool operator >( const BigNum &rhs ) const;
            bool operator <=( const BigNum &rhs ) const;
            bool operator >=( const BigNum &rhs ) const;
            bool operator !=( const BigNum &rhs ) const;
            bool operator ==( const BigNum &rhs ) const;
            bool nearEqual( const BigNum &rhs, BigNum error_margin );
            //Arithmetic Operators
            const BigNum operator +( const BigNum &rhs ) const;
            const BigNum operator -( const BigNum &rhs ) const;
            const BigNum operator *( const BigNum &rhs ) const;
            const BigNum operator /( const BigNum &rhs ) const;
            const BigNum & operator +=( const BigNum &rhs );
            const BigNum & operator -=( const BigNum &rhs );
            const BigNum & operator *=( const BigNum &rhs );
            const BigNum & operator /=( const BigNum &rhs );
            //Functionalities
            BigNum abs() const;
            BigNum negate() const;
            BigNum characteristic() const;
            BigNum mantissa() const;
            unsigned int lDigits() const;
            unsigned int rDigits() const;
          private:
            //Private members
            enum class Sign { POSITIVE, NEGATIVE } itsSign;
            enum class Type { INTEGER, DECIMAL } itsType;
            std::vector<char> itsCharacteristic;
            std::vector<char> itsMantissa;
            //Private functions
            int compareCharacteristic_abs( const BigNum &rhs ) const;
            int compareMantissa_abs( const BigNum &rhs ) const;
            bool isValid( std::string &value ) const;
            template<typename T> static T & processSign( T &value, Sign &sign_flag );
            template<typename T> static void processInteger( T &value, std::vector<char> &left_container, std::vector<char> &right_container );
            template<typename T> static void processDecimal( T &value, std::vector<char> &left_container, std::vector<char> &right_container );
            static Type processString( std::string &value, std::vector<char> &left_container, std::vector<char> &right_container );
            static bool removeSign( std::string &value );
            static void removeUselessZeros( std::string &value );
            static bool removeLeadingZeros( std::string &value );
            static bool removeTailingZeros( std::string &value );
            //template<typename U> std::bitset bitsetThis( U value ); //??
            //bitset<size_t S>
        };

        /**
         * Input Stream Operator
         * @param in Input Stream
         * @param big_num 'BigNum' object
         * @return Input Stream
         */
        std::istream & operator >>( std::istream &in, eadlib::math::BigNum &big_num ) {
            std::string value;
            in >> value;
            big_num = eadlib::math::BigNum( value );
            return in;
        }

        /**
         * Output Stream Operator
         * @param out Output Stream
         * @param big_num 'BigNum' object
         * @return Output Stream
         */
        std::ostream & operator <<( std::ostream &out, const eadlib::math::BigNum &big_num ) {
            return big_num.print( out );
        }

        /**
         * [PRIVATE] Processes the sign of a value
         * @param value Value to be checked and processed
         * @param sign_flag Sign type (negative/positive)
         * @return The Abs of the value
         */
        template<typename T> T & eadlib::math::BigNum::processSign( T &value, eadlib::math::BigNum::Sign &sign_flag ) {
            if( value < 0 ) {
                sign_flag = Sign::NEGATIVE;
                value *= -1;
            } else {
                sign_flag = Sign::POSITIVE;
            }
            return value;
        }

        /**
         * [PRIVATE] Processes the sign of a string value (template specialization)
         * @param value Value to be checked and processed (removes '-' if found)
         * @param sign_flag Sign type (negative/positive)
         * @return The Abs of the value
         */
        template<> std::string & eadlib::math::BigNum::processSign( std::string &value, eadlib::math::BigNum::Sign &sign_flag ) {
            if( removeSign( value ) ) {
                sign_flag = Sign::NEGATIVE;
            } else {
                sign_flag = Sign::POSITIVE;
            }
        }

        /**
         * [PRIVATE] Processes digits of an integer type value (int/long/long long...) into a 'char' container
         * @param value Value to be processed
         * @param left_container Container for the Characteristic's digits
         * @param right_container Container for the Mantissa's digits
         */
        template<typename T> void eadlib::math::BigNum::processInteger( T &value, std::vector<char> &left_container, std::vector<char> &right_container ) {
            if( value == 0 ) {
                left_container.assign( 1, '0' );
            } else if( value > 0 ) {
                std::list<char> temp { };
                int size { 0 };
                while( value != 0 ) {
                    temp.push_back( ( value % 10 ) + 48 );
                    size++;
                    value /= 10;
                }
                left_container.reserve( size );
                for( const char &i : temp ) {
                    left_container.push_back( i );
                }
            } else { //value hit the 'K' numeric limit
                std::string s = eadlib::tool::Convert::to_string<T>( value );
                removeSign( s );
                processString( s, left_container, right_container );
            }
        }

        /**
         * [PRIVATE] Processes digits of a decimal type value (float, double, long double) into char containers
         * @param value Value to be processed
         * @param left_container Container for the Characteristic's digits
         * @param right_container Container for the Mantissa's digits
         */
        template<typename T> void eadlib::math::BigNum::processDecimal( T &value, std::vector<char> &left_container, std::vector<char> &right_container ) {
            std::string s = eadlib::tool::Convert::to_string<T>( value );
            processString( s, left_container, right_container );
        }
    }
}

#endif //EADLIB_BIGNUM_H
