#ifndef EADLIB_BRAILLEHISTOGRAM_TEST_H
#define EADLIB_BRAILLEHISTOGRAM_TEST_H

#include "gtest/gtest.h"
#include "../../../src/cli/braille/BrailleHistogram.h"

namespace unit_test::BrailleHistogram_Tests {
    void printGraph( std::vector<std::vector<size_t>> &graph ) {
        std::setlocale( LC_ALL, "" );
        std::cout << "START" << std::endl;
        for( auto &row : graph ) {
            for( auto &block : row )
                printf( "%lc", ( wchar_t ) block );
            std::cout << std::endl;
        }
        std::cout << "END" << std::endl;
    }

    void printCharValues( std::vector<std::vector<size_t>> &graph ) {
        std::cout << "{ ";
        for( auto row_it = graph.cbegin(); row_it != graph.cend(); ++row_it ) {
            std::cout << "{ ";
            for( auto col_it = row_it->cbegin(); col_it != row_it->cend(); ++col_it ) {
                std::cout << *col_it;
                if( col_it != std::prev( row_it->cend() ) )
                    std::cout << ", ";
            }
            if( row_it != std::prev( graph.cend() ) )
                std::cout << " }," << std::endl;
            else
                std::cout << " }" << std::endl;
        }
        std::cout << " }" << std::endl;
    }
}

TEST( BrailleHistogram_Tests, Constructor ) {
    using eadlib::cli::braille::graph::Orientation;

    auto histogram = eadlib::cli::braille::BrailleHistogram<Orientation::XY>();
    histogram.setNumAxisOptions( 0, 0.5f );

    histogram.addPoint( "Cat1", 0.8f );
    histogram.addPoint( "Cat2", 2.6f );
    histogram.addPoint( "Cat3", 1.2f );
    histogram.addPoint( "Cat4", 0.2f );

    auto g = histogram.getBrailleHistogram();
    unit_test::BrailleHistogram_Tests::printGraph( *g );
}


#endif //EADLIB_BRAILLEHISTOGRAM_TEST_H
