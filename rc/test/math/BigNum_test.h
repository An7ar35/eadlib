#ifndef EADLIB_TESTS_BIGNUM_TEST_H
#define EADLIB_TESTS_BIGNUM_TEST_H

#include "gtest/gtest.h"
#include "../../src/testing/equalish.h"
#include "../../rc/BigNum.h"
#include "../../rc/BigNum.cpp"

TEST( BigNum_Tests, Constructor ) {
    eadlib::math::BigNum num;
    EXPECT_TRUE( num.toString() == "0" );
}

TEST( BigNum_Tests, Integer_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<int>::min() );
    eadlib::math::BigNum c( std::numeric_limits<int>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<int>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<int>::max() ) );
}

TEST( BigNum_Tests, Long_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<long>::min() );
    eadlib::math::BigNum c( std::numeric_limits<long>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<long>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<long>::max() ) );
}

TEST( BigNum_Tests, LongLong_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<long long>::min() );
    eadlib::math::BigNum c( std::numeric_limits<long long>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<long long>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<long long>::max() ) );
}

TEST( BigNum_Tests, UnsignedInteger_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<unsigned int>::min() );
    eadlib::math::BigNum c( std::numeric_limits<unsigned int>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<unsigned int>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<unsigned int>::max() ) );
}

TEST( BigNum_Tests, UnsignedLong_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<unsigned long>::min() );
    eadlib::math::BigNum c( std::numeric_limits<unsigned long>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<unsigned long>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<unsigned long>::max() ) );
}

TEST( BigNum_Tests, UnsignedLongLong_Constructor ) {
    eadlib::math::BigNum a( 1 );
    eadlib::math::BigNum b( std::numeric_limits<unsigned long long>::min() );
    eadlib::math::BigNum c( std::numeric_limits<unsigned long long>::max() );
    EXPECT_TRUE( a.toString() == "1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<unsigned long long>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<unsigned long long>::max() ) );
}

TEST( BigNum_Tests, Double_Constructor ) {
    eadlib::math::BigNum a( 1.1 );
    eadlib::math::BigNum b( std::numeric_limits<double>::min() );
    eadlib::math::BigNum c( std::numeric_limits<double>::max() );
    EXPECT_TRUE( a.toString() == "1.1" );
    EXPECT_TRUE( eadlib::testing::numeric::equalish( b.toString(), std::to_string( std::numeric_limits<double>::min() ), 7 ) );
    EXPECT_TRUE( eadlib::testing::numeric::equalish( c.toString(), std::to_string( std::numeric_limits<double>::max() ), 7 ) );
}

TEST( BigNum_Tests, Float_Constructor ) {
    eadlib::math::BigNum a( 1.1 );
    eadlib::math::BigNum b( std::numeric_limits<float>::min() );
    eadlib::math::BigNum c( std::numeric_limits<float>::max() );
    EXPECT_TRUE( a.toString() == "1.1" );
    EXPECT_TRUE( b.toString() == std::to_string( std::numeric_limits<float>::min() ) );
    EXPECT_TRUE( c.toString() == std::to_string( std::numeric_limits<float>::max() ) );
}

TEST( BigNum_Tests, String_Constructor ) {
    eadlib::math::BigNum string1( "- 0 54 .  58 70 " );
    eadlib::math::BigNum string2( "- . 0 1 1 2 3 7 0 0 0 " );
    eadlib::math::BigNum string3( "000 . 00000 0" );
    eadlib::math::BigNum string4( "000 0 0 0 00 0" );
    eadlib::math::BigNum string5( ".0 0 0 0 0 0" );
    EXPECT_TRUE( string1.toString() == "-54.587" );
    EXPECT_TRUE( string2.toString() == "-0.011237" );
    EXPECT_TRUE( string3.toString() == "0" );
    EXPECT_TRUE( string4.toString() == "0" );
    EXPECT_TRUE( string5.toString() == "0" );

    std::string s { };
    for( int i = 0; i < 1000; i++ ) {
        s += '0';
    }
    s += '1';
    eadlib::math::BigNum string6( s );
    EXPECT_TRUE( string6.toString() == "1" );
}

TEST( BigNum_Tests, Copy_Constructor ) {
    eadlib::math::BigNum num( "123.456" );
    eadlib::math::BigNum copy( num );
    EXPECT_TRUE( copy.toString() == "123.456" );
}

TEST( BigNum_Tests, print ) {
}

TEST( BigNum_Tests, Assignment ) {
}

TEST( BigNum_Tests, toString ) {
}

TEST( BigNum_Tests, getSign ) {
}

TEST( BigNum_Tests, getCharacteristic ) {
}

TEST( BigNum_Tests, getMantissa ) {
}

TEST( BigNum_Tests, toInt ) {
}

TEST( BigNum_Tests, toLongLong ) {
}

TEST( BigNum_Tests, toDouble ) {
}

TEST( BigNum_Tests, Operator_Smaller ) {
    EXPECT_TRUE( eadlib::math::BigNum( 1 ) < eadlib::math::BigNum( 2 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0.1 ) < eadlib::math::BigNum( 0.2 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 1.111 ) < eadlib::math::BigNum( 1.112 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0.001 ) < eadlib::math::BigNum( 0.1 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0.001 ) < eadlib::math::BigNum( 1 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0.001 ) < eadlib::math::BigNum( 1.001 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 2 ) < eadlib::math::BigNum( 1 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 0.2 ) < eadlib::math::BigNum( 0.1 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 1.112 ) < eadlib::math::BigNum( 1.111 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 0.1 ) < eadlib::math::BigNum( 0.001 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 1 ) < eadlib::math::BigNum( 0.001 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 1.001 ) < eadlib::math::BigNum( 0.001 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 0 ) < eadlib::math::BigNum( 0 ) );
}

TEST( BigNum_Tests, Operator_Equivalent ) {
    EXPECT_TRUE( eadlib::math::BigNum( 123.456789 ) == eadlib::math::BigNum( 123.456789 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 123.456789 ) == eadlib::math::BigNum( 123.456779 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 123.456789 ) == eadlib::math::BigNum( 123 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 123.456789 ) == eadlib::math::BigNum( 0.456789 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 123 ) == eadlib::math::BigNum( 123 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 123 ) == eadlib::math::BigNum( 133 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 123 ) == eadlib::math::BigNum( 0.456789 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0.456789 ) == eadlib::math::BigNum( 0.456789 ) );
    EXPECT_FALSE( eadlib::math::BigNum( 0.456789 ) == eadlib::math::BigNum( 0.456779 ) );
    EXPECT_TRUE( eadlib::math::BigNum( -123.456 ) == eadlib::math::BigNum( -123.456 ) );
    EXPECT_FALSE( eadlib::math::BigNum( -123.456 ) == eadlib::math::BigNum( 123.456 ) );
    EXPECT_TRUE( eadlib::math::BigNum( 0 ) == eadlib::math::BigNum( 0 ) );
}

TEST( BigNum_Tests, nearEqual ) {
}

TEST( BigNum_Tests, Operator_Plus ) {
}

TEST( BigNum_Tests, Operator_Minus ) {
}

TEST( BigNum_Tests, Operator_Product ) {
}

TEST( BigNum_Tests, Operator_Divide ) {
}

TEST( BigNum_Tests, Operator_Plus_Equal ) {
}

TEST( BigNum_Tests, Operator_Minus_Equal ) {
}

TEST( BigNum_Tests, Operator_Product_Equal ) {
}

TEST( BigNum_Tests, Operator_Divide_Equal ) {
}

TEST( BigNum_Tests, absolute ) { //abs
}

TEST( BigNum_Tests, negate ) {
}

TEST( BigNum_Tests, characteristic ) {
}

TEST( BigNum_Tests, mantissa ) {
}

TEST( BigNum_Tests, lDigits ) {
    eadlib::math::BigNum bn1( 123456789 );
    eadlib::math::BigNum bn2( 0.123456789 );
    eadlib::math::BigNum bn3( 12345.123456789 );
    ASSERT_TRUE( bn1.lDigits() == 9 );
    ASSERT_TRUE( bn2.lDigits() == 0 );
    ASSERT_TRUE( bn3.lDigits() == 5 );
}

TEST( BigNum_Tests, rDigits ) {
    eadlib::math::BigNum bn1( 123456789 );
    eadlib::math::BigNum bn2( 0.123456789 );
    eadlib::math::BigNum bn3( 12345.123456789 );
    ASSERT_TRUE( bn1.rDigits() == 0 );
    ASSERT_TRUE( bn2.rDigits() == 9 );
    ASSERT_TRUE( bn3.rDigits() == 9 );
}

#endif //EADLIB_TESTS_BIGNUM_TEST_H
