/*!
    @class          eadlib::algo::Tarjan
    @brief          %Tarjan algorithm for finding Strongly Connected Components in graphs

                    The implementation uses maps to 'index' the vertices of the graph
                    instead of vectors allowing for Type agnostic SCC finding.

    @dependencies   eadlib::logger::Logger, eadlib::Graph
    @author         E. A. Davison
    @copyright      E. A. Davison 2016
    @license        GNUv2 Public License
*/

#ifndef EADLIB_TARJAN_H
#define EADLIB_TARJAN_H

#include <stack>
#include "../logger/Logger.h"
#include "../datastructure/Graph.h"

namespace eadlib {
    namespace algo {
        template<class T> class Tarjan {
          public:
            Tarjan( const eadlib::Graph<T> &graph );
            ~Tarjan();
            //Iterators
            typedef typename std::list<std::list<T>>::iterator iterator;
            typedef typename std::list<std::list<T>>::const_iterator const_iterator;
            iterator begin();
            iterator end();
            const_iterator cbegin() const;
            const_iterator cend() const;
            //Info
            size_t size() const;
            bool isEmpty() const;
            //Sorting
            template<class Comparator> void sort( Comparator comparator );

          private:
            struct Discovery {
                Discovery( const size_t &index, const size_t &lowest ) :
                    _index( index ),
                    _low_link( lowest )
                {}
                ~Discovery() {};
                size_t _index;
                size_t _low_link;
            };
            void findSCCs();
            void findSCCs( const T &vertex,
                           size_t &index,
                           std::unordered_map<T, Discovery> &discovery,
                           std::stack<T> &stack,
                           std::unordered_map<T, bool> &stackMember );
            const eadlib::Graph<T> &_graph;
            std::list<std::list<T>> _scc;
        };

        /**
         * Constructor
         * @param graph eadlib::Graph<T> to run the algorithm with
         */
        template<class T> Tarjan<T>::Tarjan( const eadlib::Graph<T> &graph ) :
            _graph( graph )
        {
            findSCCs();
        }


        /**
         * Destructor
         */
        template<class T> Tarjan<T>::~Tarjan() {}

        /**
         * iterator begin()
         * @return iterator
         */
        template<class T> typename Tarjan<T>::iterator Tarjan<T>::begin() {
            return _scc.begin();
        }

        /**
         * iterator cend()
         * @return iterator
         */
        template<class T> typename Tarjan<T>::iterator Tarjan<T>::end() {
            return _scc.end();
        }

        /**
         * const iterator begin()
         * @return iterator
         */
        template<class T> typename Tarjan<T>::const_iterator Tarjan<T>::cbegin() const {
            return _scc.cbegin();
        }

        /**
         * const iterator cend()
         * @return iterator
         */
        template<class T> typename Tarjan<T>::const_iterator Tarjan<T>::cend() const {
            return _scc.cend();
        }

        /**
         * Accessor wrapper for the list sort
         * @param comparator Comparison function object
         */
        template<class T> template<class Comparator> void Tarjan<T>::sort( Comparator comparator ) {
            _scc.sort( comparator );
        }

        /**
         * Gets the number of SCC lists generated
         * @return Size of the SCC list
         */
        template<class T> size_t Tarjan<T>::size() const {
            return _scc.size();
        }

        /**
         * Checks the empty state of the list of SCCs found
         * @return SCC list empty state
         */
        template<class T> bool Tarjan<T>::isEmpty() const {
            return _scc.empty();
        }

        /**
         * Finds Strongly Connected Components in the Graph
         */
        template<class T> void Tarjan<T>::findSCCs() {
            //Error control
            if( _graph.isEmpty() ) {
                LOG_ERROR( "[eadlib::algo::Tarjan<T>::getSCCs()] Graph is empty." );
                return;
            }
            //Setting up containers...
            std::unordered_map<T, Discovery> discovery;
            std::stack<T>                    stack;
            std::unordered_map<T, bool>      stackMember;
            //Populating containers...
            for( auto v : _graph ) {
                stackMember.insert( std::make_pair<>( v.first, false ) );
            }
            //Finding SCCs...
            size_t index { 0 };
            for( auto v : _graph ) {
                if( discovery.find( v.first ) == discovery.end() ) { //i.e. not discovered yet
                    findSCCs( v.first, index, discovery, stack, stackMember );
                }
            }
        }

        /**
         * Finds SCCs recursively using DFS
         * @param vertex Next node (vertex)
         * @param index Reach index counter
         * @param discovery Container of discovery index and low link
         * @param stack Stack to store the connected ancestor
         * @param stackMember Container holding the record of whether a node is a member of the stack or not
         */
        template<class T> void Tarjan<T>::findSCCs( const T &vertex,
                                                    size_t &index,
                                                    std::unordered_map<T, Discovery> &discovery,
                                                    std::stack<T> &stack,
                                                    std::unordered_map<T, bool> &stackMember ) {

            // Set the depth index for v to the smallest unused index
            auto d = discovery.find( vertex );
            if( d == discovery.end() ) { //never discovered
                d = discovery.insert( std::make_pair( vertex, Discovery( index, index ) ) ).first;
            } else { //update index
                d->second._index    = index;
                d->second._low_link = index;
            }
            index++;
            stack.push( vertex );
            stackMember.at( vertex ) = true;

            // Consider successors of v
            auto v = _graph.at( vertex );
            for( auto w = v.childrenList.begin(); w != v.childrenList.end(); ++w ) {
                if( discovery.find( *w ) == discovery.end() ) { //index is undefined
                    // Successor w has not yet been visited; recurse on it
                    findSCCs( *w, index, discovery, stack, stackMember );
                    d->second._low_link = std::min( d->second._low_link, discovery.at( *w )._low_link );
                } else if( stackMember.at( *w ) ) {
                    // Successor w is in stack and hence in the current SCC
                    d->second._low_link = std::min( d->second._low_link, discovery.at( *w )._index );
                }
            }

            // If v is a root node, pop the stack and generate an SCC
            if( d->second._low_link == d->second._index ) {
                //start a new strongly connected component
                std::list<T> scc;
                while( stack.top() != vertex ) {
                    //add w to current strongly connected component
                    scc.emplace_back( stack.top() );
                    stackMember.at( stack.top() ) = false;
                    stack.pop();
                }
                scc.emplace_back( stack.top() );
                stackMember.at( stack.top() ) = false;
                stack.pop();
                //add scc to list
                _scc.emplace_back( scc );
            }
        }
    }
}

#endif //EADLIB_TARJAN_H
