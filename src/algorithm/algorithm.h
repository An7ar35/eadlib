/**
    @defgroup       Algorithm
    @brief          General algorithms
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_ALGORITHM_H
#define EADLIB_ALGORITHM_H

#include "sort.h"

#endif //EADLIB_ALGORITHM_H
