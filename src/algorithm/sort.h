/**
    @ingroup        Algorithm
    @defgroup       Sorting
    @brief          Sorting algorithms
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_SORT_H
#define EADLIB_SORT_H

#include <functional>

namespace eadlib {
    namespace algo {
        /**
         * \ingroup Sorting
         * Insertion sort algorithm
         * > Worst case performance: O(n^2) compare/swap\n
         * > Best case performance: O(n) compare, O(1) swap\n
         * > Worst case space complexity: О(n) total
         * @tparam BiIt       Iterator (at least bi-directional)
         * @tparam Comparator Comparator type
         * @param first       Start iterator
         * @param last        End iterator
         * @param comp        Comparator function
         */
        template<class BiIt, class Comparator = std::less<>> void insertionSort( BiIt first,
                                                                                 BiIt last,
                                                                                 Comparator comp = Comparator {} ) {
            for( auto i = std::next( first, 1 ); i != last; ++i ) {
                auto j = i;
                while( j != first && comp( *j, *std::prev( j, 1 ) ) ) {
                    std::swap( *j, *std::prev( j, 1 ) );
                    j = std::prev( j, 1 );
                }
            }
        };

        /**
         * \ingroup Sorting
         * Selection sort algorithm
         * > Worst case performance: O(n^2) compare/swap\n
         * > Best case performance: O(n^2) compare, O(1) swap\n
         * > Worst case space complexity: О(n) total
         * @tparam BiIt       Iterator (at least bi-directional)
         * @tparam Comparator Comparator type
         * @param first       Start iterator
         * @param last        End iterator
         * @param comp        Comparator function
         */
        template<class BiIt, class Comparator = std::less<>> void selectionSort( BiIt first,
                                                                                 BiIt last,
                                                                                 Comparator comp = Comparator {} ) {
            for( auto i = first; i != last; ++i ) {
                auto i_min = i;
                for( auto j = std::next( i, 1 ); j != last; ++j ) {
                    if( comp( *j, *i_min ) ) {
                        i_min = j;
                    }
                }
                if( i_min != i ) {
                    std::swap( *i, *i_min );
                }
            }
        }

        /**
         * \ingroup Sorting
         * Bubble sort algorithm
         * > Worst case performance: O(n^2)\n
         * > Best case performance: O(n)
         * @tparam BiIt       Iterator (at least bi-directional)
         * @tparam Comparator Comparator type
         * @param first       Start iterator
         * @param last        End iterator
         * @param comp        Comparator function
         */
        template<class BiIt, class Comparator = std::less<>> void bubbleSort( BiIt first,
                                                                              BiIt last,
                                                                              Comparator comp = Comparator {} ) {
            while( last != first ) {
                auto new_last = first;
                for( auto cursor = std::next( first, 1 ); cursor != last ; ++cursor ) {
                    if( comp( *cursor, *std::prev( cursor, 1 ) ) ) {
                        std::swap( *cursor, *std::prev( cursor, 1 ) );
                        new_last = cursor;
                    }
                }
                last = new_last;
            }
        }
    }
}

#endif //EADLIB_SORT_H
