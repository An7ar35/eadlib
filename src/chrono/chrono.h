/**
    @defgroup       Chrono
    @brief          General timing functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_CHRONOS_H
#define EADLIB_CHRONOS_H

#include <sstream>
#include <ctime>
#include <locale>

namespace eadlib {
    namespace chrono {
        std::string getTime( tm *ltm );
        std::string getDate( tm *ltm );
        std::string getTimeStamp( tm *ltm );

        //-----------------------------------------------------------------------------------------------------------------
        // Chrono functions implementations
        //-----------------------------------------------------------------------------------------------------------------
        /**
         * @ingroup Chrono
         * Gets the time from a time struct pointer
         * @param ltm tm struct pointer
         * @return Time formatted as "hh:mm:ss"
         */
        inline std::string getTime( tm *ltm ) {
            /**
             * [Lambda] Pads int variable with a '0' if smaller than 10
             */
            auto pad = []( int variable ) {
                if( variable < 10 ) {
                    return "0" + std::to_string( variable );
                } else {
                    return std::to_string( variable );
                }
            };
            std::stringstream ss;
            ss << std::string( pad( ltm->tm_hour ) ) << ":" << std::string( pad( ltm->tm_min ) ) << ":"
               << std::string( pad( ltm->tm_sec ) );
            return ss.str();
        }

        /**
         * @ingroup Chrono
         * Gets the date from a time struct pointer
         * @param ltm tm struct pointer
         * @return Date formatted as "dd/mm/yyyy"
         */
        inline std::string getDate( tm *ltm ) {
            /**
             * [Lambda] Pads int variable with a '0' if smaller than 10
             */
            auto pad = []( int variable ) {
                if( variable < 10 ) {
                    return "0" + std::to_string( variable );
                } else {
                    return std::to_string( variable );
                }
            };
            std::stringstream ss;
            ss << std::string( pad( ltm->tm_mday ) ) << "/" << std::string( pad( 1 + ltm->tm_mon ) ) << "/"
               << ( 1900 + ltm->tm_year );
            return ss.str();
        }

        /**
        * @ingroup Chrono
        * Gets a full time stamp from a time struct pointer
        * @param ltm tm struct pointer
        * @return Time stamp formatted as "dd/mm/yyyy - hh:mm:ss"
        */
        inline std::string getTimeStamp( tm *ltm ) {
            std::stringstream ss;
            ss << chrono::getDate( ltm ) << " - " << chrono::getTime( ltm );
            return ss.str();
        }
    }
}

#endif //EADLIB_CHRONOS_H
