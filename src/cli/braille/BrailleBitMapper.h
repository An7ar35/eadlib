/*!
    @class          eadlib::cli::braille::BrailleBitMapper
    @brief          CLI parser system to process program arguments.

    Pixels graphics encoder using braille. Each blocks is a (6x2) rectangles.
    Inspired from the excellent <a href="https://github.com/asciimoo/drawille">Drawille</a> project.

    @note           Set locale for wide character support (UTF-8) before usage. LINUX ONLY!
    @dependencies
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License

    @example        BrailleBitMapper_example.cpp
                    Example creating simple lines.
*/
#ifndef EADLIB_BRAILLEBITMAPPER_H
#define EADLIB_BRAILLEBITMAPPER_H

#include <iostream>
#include <vector>
#include <string>
#include <memory>

namespace eadlib::cli::braille {
    typedef std::vector<std::vector<size_t>> BrailleMap_t;
    typedef std::vector<std::vector<bool>>   MonoBitmap_t;

    template<size_t X, size_t Y> class BrailleBitMapper {
      public:
        const size_t UTF8_BRAILLE_BLANK { 0x2800 };
        static constexpr size_t UTF8_BRAILLE_PIXMAP[4][2] = {
            { 0x01, 0x08 },
            { 0x02, 0x10 },
            { 0x04, 0x20 },
            { 0x40, 0x80 }
        };

        BrailleBitMapper();
        BrailleBitMapper( size_t block_spacing_x, size_t block_spacing_y );
        ~BrailleBitMapper() = default;

        void set( size_t x, size_t y );
        void unset( size_t x, size_t y );
        void flip( size_t x, size_t y );
        void flip();
        void reset();

        const MonoBitmap_t & getBitmap() const;
        std::unique_ptr<BrailleMap_t> createBrailleMap();

      private:
        std::vector<std::vector<bool>> _bitmap;
        size_t                         _block_spacing_x;
        size_t                         _block_spacing_y;

        void setPixel( size_t x, size_t y, BrailleMap_t &map ) const;
        size_t getHorizontalBlockCount( size_t block_spacing_x ) const;
        size_t getVerticalBlockCount( size_t block_spacing_y ) const;
    };

    /**
     * Constructor
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     */
    template<size_t X, size_t Y> BrailleBitMapper<X, Y>::BrailleBitMapper() :
        _block_spacing_x( 0 ),
        _block_spacing_y( 0 )
    {
        _bitmap = std::vector<std::vector<bool>>( Y );
        for( auto &row : _bitmap )
            row = std::vector( X, false );
    }

    /**
     * Constructor
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param block_spacing_x Horizontal space in pixels between the character blocks
     * @param block_spacing_y Vertical space in pixels between the character blocks
     */
    template<size_t X, size_t Y> BrailleBitMapper<X, Y>::BrailleBitMapper( size_t block_spacing_x,
                                                                           size_t block_spacing_y ) :
        _block_spacing_x( block_spacing_x ),
        _block_spacing_y( block_spacing_y )
    {
        _bitmap = std::vector<std::vector<bool>>( Y );
        for( auto &row : _bitmap )
            row = std::vector( X, false );
    }

    /**
     * Sets a pixel at coordinate
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param x X position on bitmap
     * @param y Y position on bitmap
     * @throws std::out_of_range when (x,y) coordinates are outside the bitmap
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::set( size_t x, size_t y ) {
        if( y < Y && x < X )
            _bitmap.at( y ).at( x ) = true;
        else
            throw std::out_of_range( "(" + std::to_string( x ) + ", " + std::to_string( y ) + ") is outside the bitmap." );
    }

    /**
     * Clears pixel at coordinate
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param x X position on bitmap
     * @param y Y position on bitmap
     * @throws std::out_of_range when (x,y) coordinates are outside the bitmap
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::unset( size_t x, size_t y ) {
        if( y < Y && x < X )
            _bitmap.at( y ).at( x ) = false;
        else
            throw std::out_of_range( "(" + std::to_string( x ) + ", " + std::to_string( y ) + ") is outside the bitmap." );
    }

    /**
     * Flips the pixel at coordinate
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param x X position on bitmap
     * @param y Y position on bitmap
     * @throws std::out_of_range when (x,y) coordinates are outside the bitmap
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::flip( size_t x, size_t y ) {
        if( y < Y && x < X )
            _bitmap.at( y ).at( x ) = !( _bitmap.at( y ).at( x ) );
        else
            throw std::out_of_range( "(" + std::to_string( x ) + ", " + std::to_string( y ) + ") is outside the bitmap." );
    }

    /**
     * Flips all the bits in the bitmap
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::flip() {
        for( auto &row : _bitmap )
            for( auto bit : row )
                bit.flip();
    }

    /**
     * Resets all the bits in the bitmap
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::reset() {
        for( auto &row : _bitmap )
            for( auto bit : row )
                bit = false;
    }

    /**
     * Gets access to the internal bitmap
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @return Internal bitmap
     */
    template<size_t X, size_t Y> const MonoBitmap_t & BrailleBitMapper<X, Y>::getBitmap() const {
        return _bitmap;
    }

    /**
     * Creates a braille map from the internal monochrome bitmap
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @return std::unique_ptr to the created BrailleMap_t
     */
    template<size_t X, size_t Y> std::unique_ptr<BrailleMap_t> BrailleBitMapper<X, Y>::createBrailleMap() {
        auto map = std::make_unique<BrailleMap_t>( getVerticalBlockCount( _block_spacing_y ) );
        for( auto &row : *map )
            row = std::vector<size_t>(
                getHorizontalBlockCount( _block_spacing_x ),
                UTF8_BRAILLE_BLANK
            );

        size_t x { 0 }, y { 0 };
        for( const auto &row : _bitmap ) {
            for( const auto &col : row ) {
                if( col )
                    setPixel( x, y, *map );
                x++;
            }
            x = 0;
            y++;
        }
        return std::move( map );
    }

    /**
     * Gets the number of required braille character blocks horizontally
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param block_spacing_x Horizontal spacing between the character block in pixels
     * @return Number of character blocks needed horizontally
     */
    template<size_t X, size_t Y> size_t BrailleBitMapper<X, Y>::getHorizontalBlockCount( size_t block_spacing_x ) const {
        size_t block_width  = 2 + block_spacing_x;
        size_t block_count  = X / block_width;
        size_t extra_pixels = X % block_width;
        return ( extra_pixels
                    ? ++block_count
                    : block_count
        );
    }

    /**
     * Gets the number of required braille character blocks vertically
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param block_spacing_y Vertical spacing between the character blocks in pixels
     * @return Number of character blocks needed vertically
     */
    template<size_t X, size_t Y> size_t BrailleBitMapper<X, Y>::getVerticalBlockCount( size_t block_spacing_y ) const {
        size_t block_height = 4 + block_spacing_y;
        size_t block_count  = Y / block_height;
        size_t extra_pixels = Y % block_height;
        return ( extra_pixels
                    ? ++block_count
                    : block_count
        );
    }

    /**
     * Sets a pixel on a BrailleMap
     * @tparam X Bitmap horizontal size in pixels
     * @tparam Y Bitmap vertical size in pixels
     * @param x Horizontal bitmap position of the pixel
     * @param y Vertical bitmap position of the pixel
     * @param map BrailleMap_t to apply pixel to
     * @throws std::out_of_range when the bitmap position (x,y) is outside the map's bound
     */
    template<size_t X, size_t Y> void BrailleBitMapper<X, Y>::setPixel( size_t x, size_t y, BrailleMap_t &map ) const {
        size_t block_width   = 2 + _block_spacing_x;
        size_t block_count_x = x / block_width;
        size_t overshoot_x   = x % block_width;
        size_t block_height  = 4 + _block_spacing_y;
        size_t block_count_y = y / block_height;
        size_t overshoot_y   = y % block_height;

        if( overshoot_x < block_width && overshoot_x < block_width - _block_spacing_x &&
            overshoot_y < block_height && overshoot_y < block_height - _block_spacing_y )
        { //i.e.: not in a margin
            size_t cell_value = UTF8_BRAILLE_PIXMAP[ overshoot_y ][ overshoot_x ];
            try {
                map.at( block_count_y ).at( block_count_x ) += cell_value;
            } catch( const std::out_of_range &e ) {
                throw std::out_of_range(
                    "setPixel( " + std::to_string( x ) + ", " + std::to_string( y ) + " ): " +
                    "map.at( " + std::to_string( block_count_y ) + " ).at( " + std::to_string( block_count_x ) + " ) failed range check."
                );
            }
        }
    }
}

#endif //EADLIB_BRAILLEBITMAPPER_H
