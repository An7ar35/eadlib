/*!
    @class          eadlib::cli::braille::BraillePlot
    @brief          CLI line charts

    Creates braille based pixelated line charts in the terminal.

    @note           Set locale for wide character support (UTF-8) before usage. LINUX ONLY!
    @dependencies   eadlib::cli::BrailleBitMapper, eadlib::cli::braille::,
                    eadlib::Coordinate2D, eadlib::string::,                    ,
                    eadlib::exception::unsupported, eadlib::exception::aborted_operation
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License
*/
#ifndef EADLIB_CLI_BRAILLE_BRAILLEPLOT_H
#define EADLIB_CLI_BRAILLE_BRAILLEPLOT_H

#include <memory>
#include <algorithm>
#include <optional>
#include <vector>
#include <list>

#include "braille.h"
#include "graph/graph.h"
#include "graph/labelling/labelling.h"
#include "raster/raster.h"
#include "graph/NumLabelProperty.h"
#include "graph/MetaGraphData.h"
#include "BrailleBitMapper.h"
#include "../../datatype/Coordinate2D.h"
#include "../../exception/unsupported.h"
#include "../../exception/aborted_operation.h"
#include "../../string/string.h"

namespace eadlib::cli::braille {
    template<size_t W = 80, //Block width of graph (w/o labels)
             size_t H = 20  //Block height of graph (w/o labels)
    > class BraillePlot {
      public:
        static const int MAX_LABEL_SIZE { 6 };

        BraillePlot();
        ~BraillePlot() = default;

        void setDrawingStyle( graph::plot::Style drawing_style );
        void setMargins( size_t x, size_t y );
        void keepAspectRatio( bool enable );
        void showDisplayAxis( graph::AxisSelection axis );
        void setAxisTicks( graph::AxisSelection axis, int direction, float data_interval );
        void setTickLabelPosition( graph::labelling::Position pos );
        void setAxisLabelX( std::string label );
        void setAxisLabelY( std::string label );
        void setAxisLabels( std::string x, std::string y );

        void   addPoint( Coordinate2D<float> data_point );
        void   clear();
        size_t count() const;

        std::string getAxisLabelX() const;
        std::string getAxisLabelY() const;

        size_t pixelWidth() const;
        size_t pixelHeight() const;

        std::unique_ptr<BrailleMap_t> getBraillePlot() const;
      private:
        template<typename T> using CoordinateList_t = std::vector<Coordinate2D<T>>;
        using                      Offset_t         = std::pair<size_t, graph::labelling::Position>;

        //Structs
        struct AxisProperty {
            std::string                _axis_label     { "" };
            bool                       _draw_tick      { false };
            int                        _direction      { 0 };
            float                      _data_interval  { 0 };
            graph::labelling::Position _tick_label_pos { graph::labelling::Position::VOID };
        };

        //Variables
        bool                             _aspect_ratio_lock;
        graph::plot::Style               _graph_style;
        graph::AxisSelection             _draw_axis;
        AxisProperty                     _x_axis_property;
        AxisProperty                     _y_axis_property;

        Coordinate2D<size_t>             _canvas_margin { 1, 1 }; //error margin on the border for rounded scaled points
        Coordinate2D<float>              _min;
        Coordinate2D<float>              _max;
        std::vector<Coordinate2D<float>> _data_points;

        //Helper Methods
        CoordinateList_t<size_t> pixel_cast( const CoordinateList_t<float> &values ) const;

        //Graph creation methods
        Coordinate2D<size_t> calculateTickInterval( const Coordinate2D<size_t> &canvas_size,
                                                    const Coordinate2D<float>  &graph_size,
                                                    AxisProperty               &axis_property_x,
                                                    AxisProperty               &axis_property_y,
                                                    bool fixed_aspect_ratio ) const;

        CoordinateList_t<float> scalePlotPoints( const CoordinateList_t<float> &plot_points,
                                                 const Coordinate2D<size_t>    &axis_center,
                                                 const Coordinate2D<float>     &scale_factor ) const;

        CoordinateList_t<size_t> scalePlotPoints( const CoordinateList_t<float> &plot_points,
                                                  const AxisProperty            &axis_property_x,
                                                  const AxisProperty            &axis_property_y,
                                                  const Coordinate2D<size_t>    &axis_center,
                                                  const Coordinate2D<size_t>    &tick_interval ) const;

        void addTickValues( const AxisProperty                 &axis_property_x,
                            const AxisProperty                 &axis_property_y,
                            const Coordinate2D<size_t>         &canvas_size,
                            const Coordinate2D<size_t>         &tick_interval,
                            graph::MetaGraphData<float, float> &meta_graph_data ) const;

        void addGraphAxes( const graph::AxisSelection         &draw_axis_opt,
                           graph::MetaGraphData<float, float> &meta_graph_data  ) const;

        void addTickPoints( const AxisProperty                 &axis_property_x,
                            const AxisProperty                 &axis_property_y,
                            graph::MetaGraphData<float, float> &meta_graph_data ) const;

        void addRasterPoints( const CoordinateList_t<size_t>     &graph_points,
                              const graph::plot::Style           &draw_style,
                              graph::MetaGraphData<float, float> &meta_graph_data ) const;

        void drawGraph( const AxisProperty                       &axis_property_x,
                        const AxisProperty                       &axis_property_y,
                        const graph::MetaGraphData<float, float> &meta_graph_data,
                        BrailleMap_t                             &canvas ) const;

        //Labelling methods
        std::unique_ptr<BrailleMap_t> createLabelledCanvas( const AxisProperty                 &axis_property_x,
                                                            const AxisProperty                 &axis_property_y,
                                                            const Coordinate2D<size_t>         &tick_intervals,
                                                            graph::MetaGraphData<float, float> &meta_graph_data ) const;

        std::optional<Offset_t> addXLabels( const AxisProperty                 &axis_property,
                                            const size_t                       &char_spacing,
                                            graph::MetaGraphData<float, float> &meta_graph_data,
                                            std::vector<size_t>                &label_row,
                                            std::optional<Offset_t>             y_label_offset ) const;

        std::optional<Offset_t> addYLabels( const AxisProperty                 &axis_property,
                                            graph::MetaGraphData<float, float> &meta_graph_data,
                                            BrailleMap_t                       &graph_canvas ) const;

    };

    //----------------------------------------------------------------------------------------------
    // BraillePlot class method implementations
    //----------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam W Number of characters horizontally to use for the braille graph
     * @tparam H Number of characters vertically to use for the braille graph
     */
    template<size_t W, size_t H> BraillePlot<W, H>::BraillePlot() :
        _graph_style( graph::plot::Style::POINT ),
        _aspect_ratio_lock( true ),
        _draw_axis( graph::AxisSelection::XY )
    {
        static_assert( W >= 4 && H >= 2, "BraillePlot must be initialized with a character size > than (4,2)." );
    }

    /**
     * Sets the graph drawing style property
     * @param drawing_style Drawing style
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setDrawingStyle( graph::plot::Style drawing_style ) {
        _graph_style = drawing_style;
    }

    /**
     * Sets the margins for the display graph data
     * @param x X margin in pixels
     * @param y Y margin in pixels
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setMargins( size_t x, size_t y ) {
        _canvas_margin = { x + 1, y + 1 };
    }

    /**
     * Sets the flag for keeping the aspect ratio of the data when drawn
     * @param enable Maintain ratio flag (enable/disable)
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::keepAspectRatio( bool enable ) {
        _aspect_ratio_lock = enable;
    }

    /**
     * Sets what axes to display on the drawn graph
     * @param axis Axis(es) to display
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::showDisplayAxis( graph::AxisSelection axis ) {
        _draw_axis = axis;
    }

    /**
     * Sets the axis tick properties
     * @param axis          Axis
     * @param direction     Direction of the notch (-1: negative side, 0: cross, +1 positive side)
     * @param data_interval Data interval hint for each notch (may be adjusted when the calculated
     *                      number of ticks is greater than the number of 2x4 character block on
     *                      the axis)
     * @throws eadlib::exception::unsupported when data interval's precision is too small for displaying in the label space
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setAxisTicks( graph::AxisSelection axis,
                                                                       int                  direction,
                                                                       float                data_interval)
    {
        if( eadlib::string::length<float>( data_interval ) > MAX_LABEL_SIZE )
            throw eadlib::exception::unsupported(
                "Data interval precision is too big. Consider pre-scaling the points and intervals."
            );

        using eadlib::cli::braille::graph::AxisSelection;

        switch( axis ) {
            case AxisSelection::XY:
                setAxisTicks( AxisSelection::X, direction, data_interval );
                setAxisTicks( AxisSelection::Y, direction, data_interval );
                break;
            case AxisSelection::X:
                _x_axis_property._draw_tick     = true;
                _x_axis_property._direction     = direction;
                _x_axis_property._data_interval = data_interval;
                break;
            case AxisSelection::Y:
                _y_axis_property._draw_tick     = true;
                _y_axis_property._direction     = direction;
                _y_axis_property._data_interval = data_interval;
                break;
            default: //a.k.a. AxisSelection::NONE
                _x_axis_property._draw_tick     = false;
                _y_axis_property._draw_tick     = false;
        }
    }

    /**
     * Sets the position(s) for the tick labels
     * @param pos Position in relation to the drawn graph
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setTickLabelPosition( graph::labelling::Position pos ) {
        using eadlib::cli::braille::graph::labelling::Axis;
        using eadlib::cli::braille::graph::labelling::Position;
        using eadlib::cli::braille::graph::labelling::getAbsoluteLabelPosition;

        _x_axis_property._tick_label_pos = getAbsoluteLabelPosition( Axis::X, pos );
        _y_axis_property._tick_label_pos = getAbsoluteLabelPosition( Axis::Y, pos );
    }

    /**
     * Sets the X-Axis label
     * @param label Label
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setAxisLabelX( std::string label ) {
        _x_axis_property._axis_label = std::move( label );
    }

    /**
     * Sets the Y-Axis label
     * @param label Label
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setAxisLabelY( std::string label ) {
        _y_axis_property._axis_label = std::move( label );
    }

    /**
     * Sets the axes labels
     * @param x_label Label for the X-Axis
     * @param y_label Label for the Y-Axis
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::setAxisLabels( std::string x_label,
                                                                        std::string y_label )
    {
        setAxisLabelX( std::move( x_label ) );
        setAxisLabelY( std::move( y_label ) );
    }

    /**
     * Add point to the graph
     * @param data_point Point (x,y)
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::addPoint( Coordinate2D<float> data_point ) {
        if( data_point.x < 0 && data_point.x < _min.x )
            _min.x = data_point.x;
        if( data_point.x > 0 && data_point.x > _max.x )
            _max.x = data_point.x;
        if( data_point.y < 0 && data_point.y < _min.y )
            _min.y = data_point.y;
        if( data_point.y > 0 && data_point.y > _max.y )
            _max.y = data_point.y;
        _data_points.emplace_back( data_point );
    }

    /**
     * Clears all points
     */
    template<size_t W, size_t H> void BraillePlot<W, H>::clear() {
        _data_points.clear();
        _min.reset();
        _max.reset();
    }

    /**
     * Gets the number of points added to the plot
     * @return Data point count
     */
    template<size_t W, size_t H> size_t BraillePlot<W, H>::count() const {
        return _data_points.size();
    }

    /**
     * Gets the X-Axis label
     * @return X-Axis label string
     */
    template<size_t W, size_t H> std::string BraillePlot<W, H>::getAxisLabelX() const {
        return _x_axis_property._axis_label;
    }

    /**
     * Gets the Y-Axis label
     * @return Y-Axis label string
     */
    template<size_t W, size_t H> std::string BraillePlot<W, H>::getAxisLabelY() const {
        return _y_axis_property._axis_label;
    }

    /**
     * Gets the number of braille pixels available horizontally
     * @return Pixel width of the graph
     */
    template<size_t W, size_t H> size_t BraillePlot<W, H>::pixelWidth() const {
        return W * BRAILLE_PIXEL_WIDTH;
    }

    /**
     * Gets the number of braille pixels available vertically
     * @return Pixel height of the graph
     */
    template<size_t W, size_t H> size_t BraillePlot<W, H>::pixelHeight() const {
        return H * BRAILLE_PIXEL_HEIGHT;
    }

    /**
     * Creates a unicode braille map of a graph based on the data points given with addPoint(..)
     * @return Graph as a BrailleMap_t
     * @throw eadlib::exception::aborted_operation when no data points were given prior
     */
    template<size_t W, size_t H> std::unique_ptr<BrailleMap_t> BraillePlot<W, H>::getBraillePlot() const {
        using eadlib::cli::braille::graph::calculateScale;
        using eadlib::cli::braille::graph::calculateAxisCenter;

        if( W < 4 || H < 2 )
            throw eadlib::exception::aborted_operation( "Given width x height is too small. Must be at least 4x2" );
        if( _data_points.empty() )
            throw eadlib::exception::aborted_operation( "No data points were added to the graph." );

        auto graph_size = Coordinate2D<float>(
            _max.x + abs( _min.x ),
            _max.y + abs( _min.y )
        );
        auto canvas_size = Coordinate2D<size_t>(
            pixelWidth()  - _canvas_margin.x * 2,
            pixelHeight() - _canvas_margin.y * 2
        );
        auto x_axis_property = _x_axis_property;
        auto y_axis_property = _y_axis_property;

        auto graph_data  = std::make_unique<graph::MetaGraphData<float, float>>();
        graph_data->_axis_center = calculateAxisCenter<float>( canvas_size, _canvas_margin, graph_size, _min.x, _max.y );

        if( x_axis_property._draw_tick || y_axis_property._draw_tick ) { //Amortised tick scaling
            auto tick_interval  = calculateTickInterval( canvas_size, graph_size, x_axis_property, y_axis_property, _aspect_ratio_lock );
            auto display_points = scalePlotPoints( _data_points, x_axis_property, y_axis_property, graph_data->_axis_center, tick_interval );
            addTickValues( x_axis_property, y_axis_property, canvas_size, tick_interval, *graph_data );
            addGraphAxes( _draw_axis, *graph_data );
            addRasterPoints( display_points, _graph_style, *graph_data );
            auto labelled_canvas = createLabelledCanvas( x_axis_property, y_axis_property, tick_interval, *graph_data );
            addTickPoints( x_axis_property, y_axis_property, *graph_data );
            drawGraph( x_axis_property, y_axis_property, *graph_data, *labelled_canvas );
            return std::move( labelled_canvas );
        } else { //Plain scaling w/o ticks
            auto scale          = calculateScale<float, size_t>( graph_size, canvas_size, _aspect_ratio_lock );
            auto scaled_points  = scalePlotPoints( _data_points, graph_data->_axis_center, scale );
            auto display_points = pixel_cast( scaled_points );
            addGraphAxes( _draw_axis, *graph_data );
            addRasterPoints( display_points, _graph_style, *graph_data );
            auto plain_canvas   = std::make_unique<BrailleMap_t>( H, std::vector<size_t>() );
            drawGraph( x_axis_property, y_axis_property, *graph_data, *plain_canvas );
            return std::move( plain_canvas );
        }
    }

    /**
     * Casts a collection of float Coordinates into size_t coordinates
     * @param values Collection of floating coordinates
     * @return Collection of size_t coordinates
     */
    template<size_t W, size_t H>
        std::vector<Coordinate2D<size_t>> BraillePlot<W, H>::pixel_cast(
            const std::vector<Coordinate2D<float>> &values ) const
    {
        using eadlib::cli::braille::pixel_cast;

        auto v = std::vector<Coordinate2D<size_t>>();

        for( const auto &i : values )
            v.emplace_back(
                Coordinate2D<size_t>( pixel_cast<float>( i.x ), pixel_cast<float>( i.y ) )
            );

        return v;
    }

    /**
     * Calculates the amortised tick spacing in pixels for the (X,Y) axes of the graph
     * @param canvas_size        Size for the graph to display
     * @param graph_size         Size of the graph data
     * @param axis_property_x    X-Axis properties
     * @param axis_property_y    Y-Axis properties
     * @param fixed_aspect_ratio Flag to enable fixed aspect ratio when scaling
     * @return Amortised tick intervals in pixels
     */
    template<size_t W, size_t H>
        Coordinate2D<size_t> BraillePlot<W, H>::calculateTickInterval(
            const Coordinate2D<size_t> &canvas_size,
            const Coordinate2D<float>  &graph_size,
            AxisProperty               &axis_property_x,
            AxisProperty               &axis_property_y,
            bool                        fixed_aspect_ratio ) const
    {
        using eadlib::cli::braille::graph::calculateTickInterval;

        //Adjust the X-axis tick interval value if #tick > character block / 2 on axis (i.e. max of 1 tick for every 2 blocks)
        auto spacing_x = calculateTickInterval<float>( graph_size.x, canvas_size.x, BRAILLE_PIXEL_WIDTH, 2, axis_property_x._data_interval );
        axis_property_x._data_interval = spacing_x.second;

        //Adjust the Y-axis tick interval value if #tick > character block on axis (i.e.: max of 1 tick per block)
        auto spacing_y = calculateTickInterval<float>( graph_size.y, canvas_size.y, BRAILLE_PIXEL_HEIGHT, 1, axis_property_y._data_interval );
        axis_property_y._data_interval = spacing_y.second;

        if( fixed_aspect_ratio )
            return Coordinate2D<size_t>(
                std::min( spacing_x.first, spacing_y.first ),
                std::min( spacing_x.first, spacing_y.first )
            );
        else
            return Coordinate2D<size_t>(
                spacing_x.first,
                spacing_y.first
            );
    }

    /**
     * Scale and shifts plot point coordinates to the display graph
     * @param plot_points  List of original plot points
     * @param axis_center  Axis center on the display graph
     * @param scale_factor Scale factor for X and Y graph coordinates
     * @return List of display-adapted points
     */
    template<size_t W, size_t H>
        std::vector<Coordinate2D<float>> BraillePlot<W, H>::scalePlotPoints(
            const std::vector<Coordinate2D<float>> &plot_points,
            const Coordinate2D<size_t>             &axis_center,
            const Coordinate2D<float>              &scale_factor ) const
    {
        using eadlib::cli::braille::graph::scale;

        auto scaled_points = std::vector<Coordinate2D<float>>();

        for( const auto &p : plot_points )
            scaled_points.emplace_back(
                scale<float>( p, scale_factor, axis_center )
            );

        return scaled_points;
    }

    /**
     * Amortised scaling with tick interval
     * @param plot_points     List of original graph points
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param axis_center     Axis center on the display graph
     * @param tick_interval   Tick interval pixel value
     * @return List of tick-amortised scaled points
     */
    template<size_t W, size_t H>
        std::vector<Coordinate2D<size_t>> BraillePlot<W, H>::scalePlotPoints(
            const std::vector<Coordinate2D<float>> &plot_points,
            const AxisProperty          &axis_property_x,
            const AxisProperty          &axis_property_y,
            const Coordinate2D<size_t>  &axis_center,
            const Coordinate2D<size_t>  &tick_interval ) const
    {
        using eadlib::cli::braille::graph::scaleAmortised;

        auto data_interval = Coordinate2D<float>( axis_property_x._data_interval, axis_property_y._data_interval );
        auto scaled_points = std::vector<Coordinate2D<size_t>>();

        for( const auto &p : plot_points ) {
            scaled_points.emplace_back(
                scaleAmortised<float>( p, axis_center, data_interval, tick_interval )
            );
        }

        return scaled_points;
    }

    /**
     * Adds the tick placements based on the interval provided into the MetaGraphData package
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param canvas_size     Size of the graph canvas
     * @param tick_interval   XY tick intervals in pixels
     * @param meta_graph_data MetaGraphData container
     */
    template<size_t W, size_t H>
        void BraillePlot<W, H>::addTickValues(
            const AxisProperty                 &axis_property_x,
            const AxisProperty                 &axis_property_y,
            const Coordinate2D<size_t>         &canvas_size,
            const Coordinate2D<size_t>         &tick_interval,
            graph::MetaGraphData<float, float> &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::AxisSelection;

        constexpr size_t MIN_HEIGHT { BRAILLE_PIXEL_HEIGHT + 2 };

        /**
         * [LAMBDA] Adds a tick on the X-axis
         * @param x     Location of tick on X-axis
         * @param value Tick value
         */
        auto addTickX = [&]( const size_t &x, const float &value ) {
            if( value >= 0 )
                meta_graph_data._tick_values_x.emplace_back(
                    Coordinate2D<size_t>( x, meta_graph_data._axis_center.y ),
                    value
                );
            else
                meta_graph_data._tick_values_x.emplace_front(
                    Coordinate2D<size_t>( x, meta_graph_data._axis_center.y ),
                    value
                );
        };

        /**
         * [LAMBDA] Adds a tick on the Y-axis
         * @param y     Location of tick on Y-axis
         * @param value Tick value
         */
        auto addTickY = [&]( const size_t &y, const float &value ) {
            if( value >= 0 )
                meta_graph_data._tick_values_y.emplace_front(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x, y ),
                    value
                );
            else
                meta_graph_data._tick_values_y.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x, y ),
                    value
                );
        };


        if( _draw_axis == AxisSelection::X || _draw_axis == AxisSelection::XY ) {
            if( axis_property_x._draw_tick ) {
                size_t right_margin = canvas_size.x + _canvas_margin.x;
                size_t tick_count_w = ( meta_graph_data._axis_center.x - _canvas_margin.x ) / tick_interval.x;
                size_t tick_count_e = ( right_margin - meta_graph_data._axis_center.x ) / tick_interval.x;

                addTickX( meta_graph_data._axis_center.x, 0 ); //Tick for zero
                auto previous_x = meta_graph_data._axis_center.x;
                //Adding positive X ticks: right from (0,0)
                for( size_t tick = 1, x = ( meta_graph_data._axis_center.x + tick_interval.x );
                     tick <= tick_count_e;
                     tick++, x += tick_interval.x )
                {
                    if( x - previous_x > BRAILLE_PIXEL_WIDTH ) {
                        addTickX( x, axis_property_x._data_interval * tick );
                        previous_x = x;
                    }
                }

                previous_x = meta_graph_data._axis_center.x;
                //Adding negative X ticks: left from (0,0)
                for( size_t tick = 1, x = ( meta_graph_data._axis_center.x - tick_interval.x );
                     tick <= tick_count_w;
                     tick++, x -= tick_interval.x )
                {
                    if( previous_x - x > BRAILLE_PIXEL_WIDTH ) {
                        addTickX( x, -( axis_property_x._data_interval * tick ) );
                        previous_x = x;
                    }
                }
            }
        }

        if( _draw_axis == AxisSelection::Y || _draw_axis == AxisSelection::XY ) {
            if( axis_property_y._draw_tick ) {
                size_t bottom_margin = canvas_size.y + _canvas_margin.y;
                size_t tick_count_n  = ( meta_graph_data._axis_center.y - _canvas_margin.y ) / tick_interval.y;
                size_t tick_count_s  = ( bottom_margin - meta_graph_data._axis_center.y ) / tick_interval.y;

                addTickY( meta_graph_data._axis_center.y, 0 ); //Tick for zero
                auto previous_y = meta_graph_data._axis_center.y;
                //Adding positive Y ticks: up from (0,0)
                for( size_t tick = 1, y = ( meta_graph_data._axis_center.y - tick_interval.y );
                     tick <= tick_count_n;
                     tick++, y -= tick_interval.y )
                {
                    if( previous_y - y > MIN_HEIGHT ) {
                        addTickY( y, axis_property_y._data_interval * tick );
                        previous_y = y;
                    }
                }

                previous_y = meta_graph_data._axis_center.y;
                //Adding negative Y ticks: down from (0,0)
                for( size_t tick = 1, y = ( meta_graph_data._axis_center.y + tick_interval.y );
                     tick <= tick_count_s;
                     tick++, y += tick_interval.y )
                {
                    if( y - previous_y > MIN_HEIGHT ) {
                        addTickY( y, -( axis_property_y._data_interval * tick ) );
                        previous_y = y;
                    }
                }
            }
        }
    }

    /**
     * Adds the XY axes to the plot points
     * @param draw_axis_opt   Draw option for the axes
     * @param meta_graph_data MetaGraphData container
     */
    template<size_t W, size_t H>
        void BraillePlot<W, H>::addGraphAxes(
            const graph::AxisSelection         &draw_axis_opt,
            graph::MetaGraphData<float, float> &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::AxisSelection;
        //Draw X axis line
        if( draw_axis_opt == AxisSelection::X || draw_axis_opt == AxisSelection::XY ) {
            for( size_t x = 0; x < pixelWidth(); x++ )
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( x, meta_graph_data._axis_center.y )
                );
        }
        //Draw Y axis line
        if( draw_axis_opt == AxisSelection::Y || draw_axis_opt == AxisSelection::XY ) {
            for( size_t y = 0; y < pixelHeight(); y++ )
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x, y )
                );
        }
    }

    /**
     * Adds the tick points to the chart data
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param meta_graph_data MetaGraphData container
     */
    template<size_t W, size_t H>
        void BraillePlot<W, H>::addTickPoints(
            const AxisProperty                 &axis_property_x,
            const AxisProperty                 &axis_property_y,
            graph::MetaGraphData<float, float> &meta_graph_data ) const
    {
        for( const auto &tick : meta_graph_data._tick_values_x ) {
            if( axis_property_x._direction == 0 || axis_property_x._direction > 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y + 1 )
                );
            }
            if( axis_property_x._direction == 0 || axis_property_x._direction < 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( tick.first.x, meta_graph_data._axis_center.y - 1 )
                );
            }
        }
        for( const auto &tick : meta_graph_data._tick_values_y ) {
            if( axis_property_y._direction == 0 || axis_property_y._direction > 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x + 1, tick.first.y )
                );
            }
            if( axis_property_y._direction == 0 || axis_property_y._direction < 0 ) {
                meta_graph_data._bitmap_points.emplace_back(
                    Coordinate2D<size_t>( meta_graph_data._axis_center.x - 1, tick.first.y )
                );
            }
        };
    }

    /**
     * Adds the points and any beautification to the chart's bitmap data
     * @param graph_points    Scaled graph points
     * @param draw_style      Graph drawing style
     * @param meta_graph_data MetaGraphData container
     */
    template<size_t W, size_t H>
        void BraillePlot<W, H>::addRasterPoints(
            const std::vector<Coordinate2D<size_t>> &graph_points,
            const graph::plot::Style                &draw_style,
            graph::MetaGraphData<float, float>      &meta_graph_data ) const
    {
        using eadlib::cli::braille::graph::plot::Style;

        switch( draw_style ) {
            case Style::POINT: {
                for( const auto &i : graph_points )
                    meta_graph_data._bitmap_points.emplace_back( i );
            }
            break;

            case Style::JOINED: {
                using eadlib::cli::braille::raster::getLinePoints;

                auto from_it = graph_points.cbegin();
                auto to_it   = std::next( from_it );

                while( to_it != graph_points.cend() ) {
                    auto line = getLinePoints( *from_it, *to_it );

                    for( const auto &p : line )
                        meta_graph_data._bitmap_points.emplace_back( p );

                    ++from_it;
                    ++to_it;
                }
            }
            break;

            case Style::BEZIER: {
                //TODO
            }
            break;

            case Style::V_BAR: {
                for( const auto &i : graph_points ) {
                    meta_graph_data._bitmap_points.emplace_back( i );

                    if( i.y > meta_graph_data._axis_center.y ) //Y negative
                        for( size_t y = meta_graph_data._axis_center.y; y < i.y; y++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( i.x, y )
                            );
                    else //Y positive
                        for( size_t y = i.y; y <= meta_graph_data._axis_center.y; y++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( i.x, y )
                            );
                }
            }
            break;

            case Style::H_BAR: {
                for( const auto &i : graph_points ) {
                    meta_graph_data._bitmap_points.emplace_back( i );

                    if( i.x > meta_graph_data._axis_center.x ) //X positive
                        for( size_t x = i.x - 1; x > meta_graph_data._axis_center.x; x-- )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( x, i.y )
                            );
                    else //X negative
                        for( size_t x = i.x + 1; x < meta_graph_data._axis_center.x; x++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( x, i.y )
                            );
                }
            }
            break;

            case Style::VH_BAR: {
                for( const auto &i : graph_points ) {
                    meta_graph_data._bitmap_points.emplace_back( i );

                    if( i.y > meta_graph_data._axis_center.y ) //Y negative
                        for( size_t y = meta_graph_data._axis_center.y; y < i.y; y++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( i.x, y )
                            );
                    else //Y positive
                        for( size_t y = i.y; y <= meta_graph_data._axis_center.y; y++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( i.x, y )
                            );

                    if( i.x > meta_graph_data._axis_center.x ) //X positive
                        for( size_t x = i.x - 1; x > meta_graph_data._axis_center.x; x-- )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( x, i.y )
                            );
                    else //X negative
                        for( size_t x = i.x + 1; x < meta_graph_data._axis_center.x; x++ )
                            meta_graph_data._bitmap_points.emplace_back(
                                Coordinate2D( x, i.y )
                            );
                }
            }
            break;
        }
    }

    /**
     * Draws the braille graph onto the BrailleMap_t canvas
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param meta_graph_data MetaGraphData container
     * @param canvas          Canvas to draw chart on
     */
    template<size_t W, size_t H>
        void BraillePlot<W, H>::drawGraph(
            const BraillePlot::AxisProperty          &axis_property_x,
            const BraillePlot::AxisProperty          &axis_property_y,
            const graph::MetaGraphData<float, float> &meta_graph_data,
            BrailleMap_t                             &canvas ) const
    {
        auto bit_mapper = BrailleBitMapper<W * 2, H * 4>();

        for( const auto &i : meta_graph_data._bitmap_points )
            bit_mapper.set( i.x, i.y );

        auto braille_graph = bit_mapper.createBrailleMap();

        //ERROR-CONTROL: BrailleBitMapper failed to create a bitmap
        if( braille_graph->empty() )
            throw std::out_of_range( "BrailleMap returned empty." );

        auto row_it = canvas.begin();
        auto braille_line = braille_graph->cbegin();

        if( axis_property_x._tick_label_pos == graph::labelling::Position::NORTH ) {
            row_it = std::next( row_it );
        }

        if( axis_property_y._tick_label_pos == graph::labelling::Position::WEST ) {
            while( braille_line != braille_graph->cend() && row_it != canvas.end() ) {
                row_it->insert( row_it->end(), braille_line->cbegin(), braille_line->cend() );
                braille_line++;
                row_it++;
            }
        } else { //graph::labelling::Position::E or graph::labelling::Position::VOID
            while( braille_line != braille_graph->cend() && row_it != canvas.end() ) {
                row_it->insert( row_it->begin(), braille_line->cbegin(), braille_line->cend() );
                braille_line++;
                row_it++;
            }
        }
    }

    /**
     * Creates a blank canvas with labels (if any)
     * @param axis_property_x X-Axis properties
     * @param axis_property_y Y-Axis properties
     * @param tick_intervals  XY tick intervals in pixels
     * @param meta_graph_data MetaGraphData container (points and X/Y tick values if any)
     * @return Labelled BrailleMap_t canvas
     */
    template<size_t W, size_t H>
        std::unique_ptr<BrailleMap_t> BraillePlot<W, H>::createLabelledCanvas(
            const AxisProperty                 &axis_property_x,
            const AxisProperty                 &axis_property_y,
            const Coordinate2D<size_t>         &tick_intervals,
            graph::MetaGraphData<float, float> &meta_graph_data ) const
    {
        using eadlib::cli::braille::BrailleCharAxis;
        using eadlib::cli::braille::smallestCharSpacing;

        auto labelled_canvas = std::make_unique<BrailleMap_t>();

        meta_graph_data._tick_values_x.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.x < rhs.first.x; } );
        meta_graph_data._tick_values_y.sort( []( const auto &lhs, const auto &rhs ) { return lhs.first.y < rhs.first.y; } );

        auto char_spacing_x = smallestCharSpacing<BrailleCharAxis::X>( tick_intervals.x );

        if( axis_property_x._tick_label_pos == graph::labelling::Position::NORTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        auto y_label_offset = addYLabels( axis_property_y, meta_graph_data, *labelled_canvas );

        if( axis_property_x._tick_label_pos == graph::labelling::Position::SOUTH )
            labelled_canvas->emplace_back( std::vector<size_t>() );

        if( axis_property_x._tick_label_pos == graph::labelling::Position::NORTH )
            addXLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->front(), y_label_offset );

        if( axis_property_x._tick_label_pos == graph::labelling::Position::SOUTH )
            addXLabels( axis_property_x, char_spacing_x, meta_graph_data, labelled_canvas->back(), y_label_offset );

        return std::move( labelled_canvas );
    }

    /**
     * Adds X-Axis labels to a graph canvas
     * @param axis_property   X-Axis properties
     * @param char_spacing    Minimal number of character blocks to contain two consecutive ticks on the X axis
     * @param meta_graph_data MetaGraphData container (tick info/plot coordinates)
     * @param y_label_offset  Y-label size and direction if any
     * @param label_row       Iterator to the row where the X-axis labels are to be placed
     * @return Y-Axis offset used by the labelling
     */
    template<size_t W, size_t H>
        std::optional<typename BraillePlot<W, H>::Offset_t> BraillePlot<W, H>::addXLabels(
            const BraillePlot::AxisProperty    &axis_property,
            const size_t                       &char_spacing,
            graph::MetaGraphData<float, float> &meta_graph_data,
            std::vector<size_t>                &label_row,
            std::optional<Offset_t>             y_label_offset ) const
    {
        using eadlib::cli::braille::graph::removeOverlappingTicks;
        using eadlib::cli::braille::graph::NumLabelProperty;
        using eadlib::cli::braille::graph::labelling::Position;
        using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID && meta_graph_data._tick_values_x.empty() )
            return std::optional<Offset_t>( { 0, axis_property._tick_label_pos } );

        //Calculating universal label size
        auto label_specs = NumLabelProperty( axis_property._data_interval,
                                             meta_graph_data._tick_values_x.back().second,
                                             meta_graph_data._tick_values_x.front().second,
                                             MAX_LABEL_SIZE );

        if( label_specs._size > MAX_LABEL_SIZE ) {
            std::cerr << "X-Axis label size was found to be too small. "
                      << "Truncating may have occurred as a result."
                      << std::endl;
            label_specs._size = MAX_LABEL_SIZE;
        }

        //Calculating left offset before graph
        auto left_offset = ( y_label_offset.has_value() && y_label_offset->second == Position::WEST )
                           ? y_label_offset.value().first
                           : 0;

        try {
            removeOverlappingTicks( label_specs._size, char_spacing, meta_graph_data._tick_values_x );
        } catch( std::invalid_argument &e ) {
            std::cerr << e.what() << std::endl;
        }

        //Labelling
        using eadlib::cli::braille::graph::labelling::Axis;
        using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

        auto tick_it   = meta_graph_data._tick_values_x.begin();

        while( tick_it != meta_graph_data._tick_values_x.end() ) {
            writeNumericXLabel<float>( label_specs, MIDDLE_ALIGN, MAX_LABEL_SIZE, left_offset, *tick_it, label_row );
            tick_it++;
        }

        std::optional<Offset_t>( { 1, axis_property._tick_label_pos } );
    }

    /**
     * Adds Y-Axis labels a graph canvas
     * @param axis_property   Y-Axis properties
     * @param meta_graph_data MetaGraphData container (tick info/plot coordinates)
     * @param graph_canvas    Final graph canvas
     * @return X-axis offset used by the labelling
     */
    template<size_t W, size_t H>
        std::optional<typename BraillePlot<W, H>::Offset_t> BraillePlot<W, H>::addYLabels(
            const AxisProperty                 &axis_property,
            graph::MetaGraphData<float, float> &meta_graph_data,
            BrailleMap_t                       &graph_canvas ) const
    {
        using eadlib::cli::braille::graph::NumLabelProperty;
        using eadlib::cli::braille::graph::labelling::Position;

        //EARLY-RETURN: No labelling
        if( axis_property._tick_label_pos == Position::VOID || meta_graph_data._tick_values_y.empty() ) {
            while( graph_canvas.size() < H ) //add all required rows
                graph_canvas.emplace_back( std::vector<size_t>() );
            return std::optional<Offset_t>();
        }

        //Calculating universal label size
        auto label_specs = NumLabelProperty( axis_property._data_interval,
                                             meta_graph_data._tick_values_y.back().second,
                                             meta_graph_data._tick_values_y.front().second,
                                             MAX_LABEL_SIZE );

        if( label_specs._size > MAX_LABEL_SIZE ) {
            std::cerr << "Y-Axis label size was found to be too small. "
                      << "Truncating may have occurred as a result."
                      << std::endl;
            label_specs._size = MAX_LABEL_SIZE;
        }

        //Labelling
        using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
        using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

        auto offset        = graph_canvas.size();
        auto row_index     = 0L;
        auto tick_it       = meta_graph_data._tick_values_y.begin();

        while( tick_it != meta_graph_data._tick_values_y.end() ) {
            size_t characteristic = eadlib::string::length_int<float>( tick_it->second );
            auto   char_height    = eadlib::cli::braille::charHeight( tick_it->first.y + 1 );
            auto   write_index    = ( char_height > 0 ? char_height - 1 : 0 ) + offset;

            //Add any missing empty label rows
            while( graph_canvas.size() < write_index ) {
                graph_canvas.emplace_back( std::vector<size_t>( label_specs._size, 0x20 ) );
            }

            if( write_index <= row_index ) { // ignore & remove ticks that would overlap
                tick_it = meta_graph_data._tick_values_y.erase( tick_it );
            } else {
                auto label = std::vector<size_t>();

                writeNumericYLabel<float>( label_specs, RIGHT_ALIGN, *tick_it, label );

                graph_canvas.emplace_back( label );
                row_index = graph_canvas.size() - 1;
                tick_it++;
            }
        }

        while( graph_canvas.size() < H + offset ) //add blank labels to any remaining unlabelled rows
            graph_canvas.emplace_back( std::vector<size_t>( label_specs._size, 0x20 ) );

        return std::optional<Offset_t>( { label_specs._size, axis_property._tick_label_pos } );
    }
}

#endif //EADLIB_CLI_BRAILLE_BRAILLEPLOT_H
