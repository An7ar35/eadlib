/**
    \defgroup       CLI Command Line Interface
    \ingroup        CLI
    \defgroup       Braille
    @brief          Braille bitmap character related functions
    @dependencies
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BRAILLE_H
#define EADLIB_BRAILLE_H

#include <iomanip>

namespace eadlib::cli::braille {
    static const size_t BRAILLE_PIXEL_WIDTH  { 2 };
    static const size_t BRAILLE_PIXEL_HEIGHT { 4 };

    /**
     * Enum of Braille character X/Y sizes in pixels
     */
    enum class BrailleCharAxis {
        X = BRAILLE_PIXEL_WIDTH,
        Y = BRAILLE_PIXEL_HEIGHT
    };

    /**
     * \ingroup Braille
     * Gets the number of character blocks needed for the number of horizontal pixels
     * @param pixels Horizontal pixel count
     * @return Minimal character count
     */
    constexpr size_t charWidth( size_t pixels ) {
        return pixels / BRAILLE_PIXEL_WIDTH + ( pixels % BRAILLE_PIXEL_WIDTH ? 1 : 0 );
    }

    /**
     * \ingroup Braille
     * Gets the number of character blocks needed for the number of vertical pixels
     * @param pixels Vertical pixel count
     * @return Minimal character count
     */
    constexpr size_t charHeight( size_t pixels ) {
        return pixels / BRAILLE_PIXEL_HEIGHT + ( pixels % BRAILLE_PIXEL_HEIGHT ? 1 : 0 );
    }

    /**
     * \ingroup Braille
     * Casts a float point to a rounded whole number (size_t)
     * @tparam T Floating numerical type
     * @param val value
     * @return Rounded size_t value
     */
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    >
    size_t pixel_cast( const T &val ) {
        return static_cast<size_t>( round( val ) );
    }

    /**
     * \ingroup Braille
     * Calculates the maximum number of character blocks needed to contain a pixel interval
     * @tparam CHAR_AXIS Braille character axis
     * @param interval   Interval size in pixels
     * @return Largest number of character blocks required to fit the interval
     */
    template<BrailleCharAxis CHAR_AXIS> size_t largestCharSpacing( size_t interval ) {
        /* In the event of odd tick intervals the block pattern is not uniform.
         * Thus we need to adjust the character interval count with this in mind in order to
         * find the largest width case.
         *
         * e.g.: Horizontally with a tick_interval = 3 from tick '0':
         *          -2  -1    0   1    2
         *          |10|01|00|10|01|00|10|
         *      (a)          |^-|-^|       -> width = 2 chars
         *      (b)             | ^|--|^ | -> width = 3 chars
         *      (c)    | ^|--|^ |          -> width = 3 chars
         *      (d) |^-|-^|                -> width = 2 chars
         */
        switch( CHAR_AXIS ) {
            case BrailleCharAxis::X: {
                return ( interval % BRAILLE_PIXEL_WIDTH > 0 )
                       ? charWidth( interval ) + 1
                       : charWidth( interval + 1 );
            }
            case BrailleCharAxis::Y: {
                return ( interval % BRAILLE_PIXEL_HEIGHT > 0 )
                       ? charHeight( interval ) + 1
                       : charHeight( interval + 1 );
            }
        }
    }

    /**
     * \ingroup Braille
     * Calculates the minimum number of character blocks needed to contain a pixel interval
     * @tparam CHAR_AXIS Braille character axis
     * @param interval   Interval size in pixels
     * @return Smallest number of character blocks required to fit the interval
     */
    template<BrailleCharAxis CHAR_AXIS> size_t smallestCharSpacing( size_t interval ) {
        switch( CHAR_AXIS ) {
            case BrailleCharAxis::X: {
                return charWidth( interval + 1 );
            }
            case BrailleCharAxis::Y: {
                return charHeight( interval + 1 );
            }
        }
    }
}

#endif //EADLIB_BRAILLE_H
