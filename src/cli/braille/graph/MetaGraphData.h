/*!
    @class          eadlib::cli::braille::graph::MetaGraphData
    @brief          Container for braille graphing metadata

    Contains bitmap coordinates, axis centre, tick coordinates and values.

    @dependencies   eadlib::Coordinate2D
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License
*/
#ifndef EADLIB_METAGRAPHDATA_H
#define EADLIB_METAGRAPHDATA_H

#include <iomanip>
#include <vector>
#include <list>

#include "../../../datatype/Coordinate2D.h"

namespace eadlib::cli::braille::graph {
    template<typename XType, typename YType> struct MetaGraphData {
        Coordinate2D<size_t>                              _axis_center;
        std::vector<Coordinate2D<size_t>>                 _bitmap_points;
        std::list<std::pair<Coordinate2D<size_t>, XType>> _tick_values_x;
        std::list<std::pair<Coordinate2D<size_t>, YType>> _tick_values_y;
    };
}

#endif //EADLIB_METAGRAPHDATA_H
