/*!
    @class          eadlib::cli::braille::graph::NumLabellingProperty
    @brief          Number label property structure

    Calculates and holds the properties of a number's structure

    @dependencies   eadlib::string::,
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License
*/
#ifndef EADLIB_NUMLABELLINGPROPERTY_H
#define EADLIB_NUMLABELLINGPROPERTY_H

#include <type_traits>
#include <iomanip>

#include "../../../string/string.h"

namespace eadlib::cli::braille::graph {
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    > struct NumLabelProperty {
        /**
         * Constructor
         * @tparam T Floating type for the number represented in the label
         * @param data_interval        Label's data interval
         * @param max_neg_value        Largest negative tick value
         * @param max_pos_value        Largest positive tick value
         * @param fractional_precision Mantissa rounding precision
         */
        NumLabelProperty( T   data_interval,
                          T   max_neg_value,
                          T   max_pos_value,
                          int fractional_precision )
        {
            using eadlib::string::length_int;
            using eadlib::string::length_frac;

            size_t max_neg_c = length_int<T>( max_neg_value );
            size_t max_pos_c = length_int<T>( max_pos_value );
            size_t interval_c = length_int<T>( data_interval );

            _mantissa = length_frac<T>( data_interval, fractional_precision );
            _characteristic = std::max( std::max( max_neg_c, max_pos_c ), interval_c );
            _size = _characteristic + ( _mantissa > 0 ? _mantissa + 1 : _mantissa );
        }

        size_t _mantissa;
        size_t _characteristic;
        size_t _size;
    };
}

#endif //EADLIB_NUMLABELLINGPROPERTY_H
