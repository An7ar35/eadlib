/**
    \ingroup        Braille
    \defgroup       BrailleGraph Braille graphing
    @brief          Braille graphing related functions
    @dependencies   eadlib::Coordinate2D
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BRAILLE_GRAPH_H
#define EADLIB_BRAILLE_GRAPH_H

#include <type_traits>
#include <iomanip>
#include <cmath>

#include "MetaGraphData.h"
#include "../braille.h"
#include "../../../datatype/Coordinate2D.h"
#include "../../../exception/aborted_operation.h"

namespace eadlib::cli::braille::graph {
    template<typename T> using TickList_t = typename std::list<std::pair<Coordinate2D<size_t>, T>>;

    enum class Orientation   { XY, YX };
    enum class AxisSelection { NONE, X, Y, XY };

    namespace plot {
        enum class Style { POINT, JOINED, BEZIER, V_BAR, H_BAR, VH_BAR };
    };

    namespace histogram {
        enum class Style { POINT, JOINED, BEZIER, PLATEAU, THIN_BAR, THICK_BAR };
    };

    /**
     * \ingroup BrailleGraph
     * Calculates point location on X-Axis based on a given Y-Axis offset
     * @tparam T          Numeric type of point
     * @param val         Value
     * @param axis_offset Axis center pixel location
     * @return Offset value
     * @throws std::underflow_error when value offset is > X-axis min
     */
    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic<T>::value>
    >
    T offsetValueX( const T      &val,
                    const size_t &axis_offset )
    {
        if( val < 0 && abs( val ) > axis_offset )
            throw std::underflow_error(
                "[eadlib::cli::braille::graph::offsetValueX(..)] Point value exceeds negative X-axis boundary."
            );

        return val >= 0
               ? axis_offset + val
               : axis_offset - abs( val );
    }

    /**
     * \ingroup BrailleGraph
     * Calculates point location on Y-Axis based on a given X-Axis offset
     * @tparam T          Numeric type of point
     * @param val         Value
     * @param axis_offset Axis center pixel location
     * @return Offset value
     * @throws std::underflow_error when value offset is > Y-axis max
     */
    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic<T>::value>
    >
    T offsetValueY( const T      &val,
                    const size_t &axis_offset )
    {
        if( val > 0 && val > axis_offset )
            throw std::underflow_error(
                "[eadlib::cli::braille::graph::offsetValueY(..)] Point value exceeds positive Y-axis boundary."
            );

        return val >= 0
               ? axis_offset - val
               : axis_offset + abs( val );
    }

    /**
     * \ingroup BrailleGraph
     * Scales a point
     * @tparam T          Numeric type of point and scale factor
     * @param point       (X,Y) coordinate of point
     * @param scale       Scale factor
     * @param axis_center Offset
     * @return Scaled point
     * @throws std::underflow_error when offsetting the scaled coordinate fails
     */
    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic<T>::value>
    >
    Coordinate2D<T> scale( const Coordinate2D<T>      &point,
                           const Coordinate2D<T>      &scale,
                           const Coordinate2D<size_t> &axis_center )
    {
        //scale
        auto p = point;
        p.x *= scale.x;
        p.y *= scale.y;
        //shift with offset
        p.x = offsetValueX<T>( p.x, axis_center.x );
        p.y = offsetValueY<T>( p.y, axis_center.y );
        return p;
    };

    /**
     * \ingroup BrailleGraph
     * Scale a value with interval amortisation
     * @tparam T            Numeric floating type
     * @param val           Value to scale
     * @param val_interval  Tick value interval
     * @param tick_interval Tick pixel interval
     * @return Scaled point
     */
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    >
    T scaleAmortised( const T      &val,
                      const T      &val_interval,
                      const size_t &tick_interval )
    {
        auto tick_count   = val / val_interval;
        auto whole_ticks  = ( tick_count > 0 ? floor( tick_count ) : ceil( tick_count ) );
        auto extra_pixels = ( tick_count - whole_ticks ) * tick_interval;
        return ( whole_ticks * tick_interval ) + extra_pixels;
    }

    /**
     * \ingroup BrailleGraph
     * Scales with amortised intervals a point
     * @tparam T            Numeric floating type
     * @param point         Point to scale
     * @param axis_centre   Pixel coordinates of the axis (0,0) centre
     * @param data_interval Tick interval data value
     * @param tick_interval Pixel interval value of tick
     * @return Scaled point
     */
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    >
    Coordinate2D<size_t> scaleAmortised( const Coordinate2D<T>      &point,
                                         const Coordinate2D<size_t> &axis_centre,
                                         const Coordinate2D<T>      &data_interval,
                                         const Coordinate2D<size_t> &tick_interval )
    {
        using eadlib::cli::braille::pixel_cast;

        auto scaled_x  = scaleAmortised<T>( point.x, data_interval.x, tick_interval.x );
        auto scaled_y  = scaleAmortised<T>( point.y, data_interval.y, tick_interval.y );
        auto shifted_x = offsetValueX<T>( scaled_x, axis_centre.x );
        auto shifted_y = offsetValueY<T>( scaled_y, axis_centre.y );

        return Coordinate2D<size_t>(
            pixel_cast<T>( shifted_x ),
            pixel_cast<T>( shifted_y )
        );
    };

    /**
     * \ingroup BrailleGraph
     * Calculate the scale factor between two 2-dimensional coordinates
     * @tparam T                 Numeric type for the sizes
     * @tparam U                 Numeric type for the target size
     * @param original_size      Original size
     * @param target_size        Target size
     * @param fixed_aspect_ratio Flag for retaining aspect ratio (default=true)
     * @return Scale factor
     * @throws std::invalid_argument when either original_size or target_size has a value == 0
     */
    template<typename T,
             typename U,
             typename = std::enable_if_t<std::is_arithmetic<T>::value &&
                                         std::is_arithmetic<U>::value>
    >
    Coordinate2D<T> calculateScale( const Coordinate2D<T> &original_size,
                                    const Coordinate2D<U> &target_size,
                                    bool fixed_aspect_ratio = true )
    {
        if( original_size.x <= 0 || original_size.y <= 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateScale(..)] Values <= 0 cannot be scaled." );
        if( target_size.x <= 0 || target_size.y <= 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateScale(..)] Cannot scale to a size <= 0." );

        auto width_ratio  = target_size.x / original_size.x;
        auto height_ratio = target_size.y / original_size.y;
        auto scale        = Coordinate2D<T>( 1, 1 );

        if( fixed_aspect_ratio ) {
            if( width_ratio >= 0 && height_ratio >= 0 ) { //upscale
                scale.x = std::min( width_ratio, height_ratio );
                scale.y = scale.x;
            } else if( width_ratio < 0 && height_ratio < 0 ) { //downscale
                scale.x = std::max( width_ratio, height_ratio );
                scale.y = scale.x;
            } else { //downscale
                scale.x = width_ratio < 0 ? width_ratio : height_ratio;
                scale.y = scale.x;
            }
        } else {
            scale.x = width_ratio;
            scale.y = height_ratio;
        }

        return scale;
    }

    /**
     * \ingroup BrailleGraph
     * Calculates the X-Axis position on the Y-Axis
     * @tparam T Numeric type for the original graph units
     * @param canvas_height    Total height in pixels of the drawing canvas (exc. margin)
     * @param canvas_margin_y  Left/right margin size in pixels of the drawing canvas
     * @param graph_height     Total height of the original graph
     * @param y_positive_range Original graph's positive Y-axis size
     * @return Y pixel coordinate where the X-axis crosses
     */
    template<typename T,
             typename = std::enable_if_t <std::is_arithmetic<T>::value>
    >
    size_t calculateXAxisPosition( const size_t &canvas_height,
                                   const size_t &canvas_margin_y,
                                   const T      &graph_height,
                                   const T      &y_positive_range )
    {
        auto position = ( y_positive_range / graph_height * ( canvas_height - 1 ) );
        return static_cast<size_t>( round( position + canvas_margin_y ) );
    }

    /**
     * \ingroup BrailleGraph
     * Calculates the Y-Axis position on the X axis
     * @tparam T Numeric type for the original graph units
     * @param canvas_width     Total width in pixels of the drawing canvas (exc. margin)
     * @param canvas_margin_x  Top/bottom margin size in pixels of the drawing canvas
     * @param graph_width      Total width of the original graph
     * @param x_negative_range Original graph's negative X-axis size
     * @return X pixel coordinate where the Y axis crosses
     */
    template<typename T,
             typename = std::enable_if_t <std::is_arithmetic<T>::value>
    >
    size_t calculateYAxisPosition( const size_t &canvas_width,
                                   const size_t &canvas_margin_x,
                                   const T      &graph_width,
                                   const T      &x_negative_range )
    {
        auto x_pos = ( abs( x_negative_range ) / graph_width * ( canvas_width - 1 ) );
        return static_cast<size_t>( round( x_pos + canvas_margin_x ) );
    }

    /**
     * \ingroup BrailleGraph
     * Calculates the center point of a display graph
     * @tparam T Numeric type for the original graph units
     * @param canvas_size   Size of the display graph in pixels
     * @param canvas_margin Margins on the canvas
     * @param graph_size    Size of the data graph
     * @param min_x         X-Coordinate for the left-most negative point on the graph
     * @param max_y         Y-Coordinate for the top-most positive point on the graph
     * @return Center point where the axes cross on the display
     */
    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic<T>::value>
    >
    Coordinate2D<size_t> calculateAxisCenter( const Coordinate2D<size_t> &canvas_size,
                                              const Coordinate2D<size_t> &canvas_margin,
                                              const Coordinate2D<T>      &graph_size,
                                              const T                    &min_x,
                                              const T                    &max_y )
    {
        auto x = calculateYAxisPosition( canvas_size.x, canvas_margin.x, graph_size.x, min_x );
        auto y = calculateXAxisPosition( canvas_size.y, canvas_margin.y, graph_size.y, max_y );
        return Coordinate2D<size_t>( x, y );
    }

    /**
     * \ingroup BrailleGraph
     * Calculates the number of ticks an axis can have
     * @tparam T Tick data numerical type
     * @param original_axis_size Axis size of the original graph
     * @param target_axis_size   Axis size of the target canvas in pixels
     * @param char_block_size    Size of the pixel block on the axis
     * @param tick_block_spread  Number of blocks allocated per ticks (e.g.: 2 = 1 tick every 2 blocks)
     * @param tick_data_interval Interval value between each ticks
     * @return Pair consisting of (1) tick interval in pixels and (2) adjusted data interval value based on options
     * @throws std::bad_cast         when T cannot be cast to size_t type
     * @throws std::invalid_argument when given variables are not valid
     */
    template<typename T,
             typename = std::enable_if_t<std::is_arithmetic<T>::value>
    >
    std::pair<size_t, T> calculateTickInterval( const T      &original_axis_size,
                                                const size_t &target_axis_size,
                                                const size_t &char_block_size,
                                                const size_t &tick_block_spread,
                                                const T      &tick_data_interval )
    {
        //ERROR-CONTROL: invalid arguments
        if( original_axis_size <= 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateTickInterval(..)] Original axis size must be > 0." );
        if( char_block_size < 1 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateTickInterval(..)] Target axis size must be > 0." );
        if( target_axis_size == 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateTickInterval(..)] Character block size must be > 0." );
        if( tick_block_spread == 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateTickInterval(..)] Tick block spread must be > 0." );
        if( tick_data_interval <= 0 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::calculateTickInterval(..)] Tick data interval must be > 0." );

        T    data_interval = tick_data_interval;
        auto tick_count    = original_axis_size / data_interval;

        while( tick_count > floor( target_axis_size / char_block_size ) / tick_block_spread ) {
            data_interval *= 2;
            tick_count = original_axis_size / data_interval;
        }

        return std::make_pair(
            static_cast<size_t>( floor( target_axis_size / tick_count ) ),
            data_interval
        );
    }

    /**
     * \ingroup BrailleGraph
     * Calculates the number of equal-spacing categories an axis can have
     * @tparam CHAR_AXIS Braille character axis
     * @param length     Length of space available on axis
     * @param cat_count  Number of categories
     * @return Size of each category intervals in pixels
     * throws std::invalid_argument                when the category count is < 1
     * throws eadlib::exception::aborted_operation when the number of categories exceed the available character space
     */
    template<BrailleCharAxis CHAR_AXIS> size_t calculateCategoryInterval( const size_t &length,
                                                                          const size_t &cat_count )
    {
        if( cat_count < 1 )
            throw std::invalid_argument(
                "[eadlib::cli::braille::graph::calculateCategoryInterval(..)] "
                "Category count must be > 0."
            );

        switch( CHAR_AXIS ) {
            case BrailleCharAxis::X: {
                if( cat_count > ( length / BRAILLE_PIXEL_WIDTH ) )
                    throw eadlib::exception::aborted_operation(
                        "[eadlib::cli::braille::graph::calculateCategoryInterval<BrailleCharAxis::X>(..)] "
                        "Not enough char blocks to fit all categories."
                    );
                else
                    return length / cat_count;
            }
            case BrailleCharAxis::Y: {
                if( cat_count > ( length / BRAILLE_PIXEL_HEIGHT ) )
                    throw eadlib::exception::aborted_operation(
                        "[eadlib::cli::braille::graph::calculateCategoryInterval<BrailleCharAxis::Y>(..)] "
                        "Not enough char blocks to fit all categories."
                    );
                else
                    return length / cat_count;
            }
        }
    }

    /**
     * \ingroup BrailleGraph
     * Remove X-Axis ticks evenly that have overlapping labels
     * @tparam XType       Numerical X-Axis type
     * @param label_size   Universal label size for the X-Axis
     * @param char_spacing Smallest character block spacing that can contain two consecutive ticks
     * @param tick_list    X-Axis tick list
     * @throws std::invalid_argument when bad arguments are passed to the method
     */
    template<typename XType,
             typename = std::enable_if_t<std::is_arithmetic<XType>::value>
    >
    void removeOverlappingTicks( size_t            label_size,
                                 size_t            char_spacing,
                                 TickList_t<XType> &tick_list )
    {
        using TickIterator_t = typename TickList_t<XType>::iterator;

        //ERROR-CONTROL: invalid arguments
        if( label_size < 1 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::removeOverlappingTicks(..)] Label size must be >= 1." );
        if( char_spacing < 2 )
            throw std::invalid_argument( "[eadlib::cli::braille::graph::removeOverlappingTicks(..)] Character spacing must be >= 2." );

        //EARLY-RETURN: empty tick list
        if( tick_list.empty() )
            return;

        //Working out how many ticks to skip based on overlapping labels
        size_t skip_count  = 0;
        size_t char_blocks = char_spacing;
        while( label_size > char_blocks - 2 ) {
            char_blocks *= 2;
            skip_count++;
        }

        //Removing ticks whose labels overlap
        auto neg_it    = tick_list.begin();
        auto neg_stack = std::stack<TickIterator_t>();
        while( neg_it != tick_list.end() && neg_it->second < 0 ) {
            neg_stack.push( neg_it );
            neg_it++;
        }

        auto pos_it        = neg_it; //should be at tick '0'
        auto erase_counter = 0;
        while( pos_it != tick_list.end() ) {
            if( skip_count > 0 && erase_counter < skip_count ) {
                pos_it = tick_list.erase( pos_it );
                erase_counter++;
            } else { //Keep the tick
                erase_counter = 0;
                pos_it++;
            }
        }

        erase_counter = 1; //+1 for the '0' tick
        while( !neg_stack.empty() ) {
            if( skip_count > 0 && erase_counter < skip_count ) {
                tick_list.erase( neg_stack.top() );
                erase_counter++;
            } else {
                erase_counter = 0;
            }
            neg_stack.pop();
        }
    }
}

#endif //EADLIB_BRAILLE_GRAPH_H
