/**
    \ingroup        BrailleGraph
    \defgroup       BrailleLabelling Braille graph labelling
    @brief          Braille graph labelling related functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
**/
#ifndef EADLIB_CLI_BRAILLE_GRAPH_LABELLING_H
#define EADLIB_CLI_BRAILLE_GRAPH_LABELLING_H

#include <type_traits>
#include <iomanip>
#include <optional>
#include <string>
#include <vector>
#include <cmath>

#include "../graph.h"
#include "../../../../string/string.h"
#include "../NumLabelProperty.h"

namespace eadlib::cli::braille::graph::labelling {
    template<typename T> using TickValue_t = std::pair<Coordinate2D<size_t>, T>;

    enum class Axis     { X, Y };
    enum class Position { VOID, NORTH, SOUTH, WEST, EAST, NW, NE, SW, SE };

    const int LEFT_ALIGN   { -1 };
    const int MIDDLE_ALIGN { 0 };
    const int RIGHT_ALIGN  { 1 };

    /**
     * Gets the given axis's absolute label position (N/S/E/W)
     * @param axis            Axis for label
     * @param label_positions Aggregate label positions
     * @return Absolute label position for given axis
     */
    constexpr Position getAbsoluteLabelPosition( Axis     axis,
                                                 Position label_positions )
    {
        switch( label_positions ) {
            case Position::VOID:
                return Position::VOID;
            case Position::NORTH:
                return ( axis == Axis::X ) ? Position::NORTH : Position::VOID;
            case Position::SOUTH:
                return ( axis == Axis::X ) ? Position::SOUTH : Position::VOID;
            case Position::WEST:
                return ( axis == Axis::Y ) ? Position::WEST : Position::VOID;
            case Position::EAST:
                return ( axis == Axis::Y ) ? Position::EAST : Position::VOID;
            case Position::NW:
                return ( axis == Axis::X ) ? Position::NORTH : Position::WEST;
            case Position::NE:
                return ( axis == Axis::X ) ? Position::NORTH : Position::EAST;
            case Position::SW:
                return ( axis == Axis::X ) ? Position::SOUTH : Position::WEST;
            case Position::SE:
                return ( axis == Axis::X ) ? Position::SOUTH : Position::EAST;
        }
    }

    /**
     * Writes a numeric X-Axis label to a given row
     * @tparam T Numeric label type
     * @param label_specs      NumLabelProperty container holding general specifications for all labels
     * @param text_align       Text alignment on the tick (-1: left, 0: centre, +1: right)
     * @param label_max_size   Maximum size for all labels
     * @param label_offset     Label offset count (in the case the x-label is appended when the y-labels have are on the left hand side)
     * @param tick_val         Tick value container (coordinate & numeric value) - assumes correct spacing
     * @param label_target_row Target row where the label is to be written to
     */
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    >
    void writeNumericXLabel( const NumLabelProperty<T> &label_specs,
                             int                       text_align,
                             unsigned                  label_max_size,
                             const size_t              &label_offset,
                             const TickValue_t<T>      &tick_val,
                             std::vector<size_t>       &label_target_row )
    {
        using eadlib::string::length_int;
        using eadlib::string::length_frac;

        if( !label_target_row.empty() )
            label_target_row.emplace_back( 0x20 ); //single white-space for minimum label margin

        auto   value                = std::to_string( tick_val.second );
        size_t mantissa             = std::min( length_frac<float>( tick_val.second, label_max_size ), label_specs._mantissa );
        size_t characteristic       = length_int<T>( tick_val.second );
        size_t str_size             = characteristic + ( mantissa > 0 ? mantissa + 1 : mantissa );

        size_t tick_char_index      = charWidth( tick_val.first.x + 1 ) + label_offset;
        size_t first_write_index    = label_target_row.size() + 1;
        size_t available_left_space = ( tick_char_index > first_write_index )
                                      ? tick_char_index - first_write_index
                                      : 0;
        size_t padding_left         = 0;

        if( text_align < 0 ) { //left align
            padding_left = ( available_left_space > str_size )
                           ? available_left_space - str_size
                           : 0;
        } else if( text_align > 0 ) { //right align
            padding_left = ( available_left_space > 0 )
                           ? available_left_space - 1
                           : 0;
        } else { //middle
            size_t str_offset = ( str_size > 1 )
                                ? str_size / 2
                                : 0;

            size_t write_index = ( str_offset >= available_left_space )
                                 ? 0
                                 : tick_char_index - str_offset;

            padding_left       = ( first_write_index < write_index )
                                 ? write_index - first_write_index
                                 : 0;
        }

        for( auto i = 0; i < padding_left; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space

        for( size_t i = 0; i < str_size; ++i )
            label_target_row.emplace_back( static_cast<size_t>( abs( value[ i ] ) ) );
    }

    /**
     * Writes a numeric Y-Axis label to a row
     * @tparam T Numeric label type
     * @param label_specs      NumLabelProperty container holding general specifications for all labels
     * @param text_align       Text alignment (-1: left, 0: centre, +1: right)
     * @param tick_val         Tick value container (coordinate & numeric value) - assumes correct spacing
     * @param label_target_row Target row where the label is to be written to
     */
    template<typename T,
             typename = std::enable_if_t<std::is_floating_point<T>::value>
    >
    void writeNumericYLabel( const NumLabelProperty<T> &label_specs,
                             int                       text_align,
                             const TickValue_t<T>      &tick_val,
                             std::vector<size_t>       &label_target_row )
    {
        using eadlib::string::length_int;
        using eadlib::string::length_frac;

        auto   value          = std::to_string( tick_val.second );
        size_t characteristic = length_int<T>( tick_val.second );
        size_t mantissa       = ( label_specs._mantissa > 0 )
                                ? label_specs._mantissa + 1
                                : label_specs._mantissa;
        size_t str_size       = characteristic + mantissa;

        size_t padding_left   = 0;
        size_t padding_right  = 0;
        size_t padding_size   = ( characteristic < label_specs._characteristic )
                                ? label_specs._characteristic - characteristic
                                : 0;

        if( text_align < 0 ) {
            padding_right = padding_size;
        } else if( text_align > 0 ) {
            padding_left  = padding_size;
        } else {
            padding_left  = padding_size / 2;
            padding_right = padding_size - padding_left;
        }

        for( auto i = 0; i < padding_left; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space

        for( size_t i = 0; i < str_size; ++i )
            label_target_row.emplace_back( static_cast<size_t>( abs( value[ i ] ) ) );

        for( auto i = 0; i < padding_right; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space
    }

    /**
     * Writes a numeric X-Axis label to a row
     * @tparam T String type only
     * @param label_max_size   Maximum label size
     * @param text_align       Text alignment on the tick (-1: left, 0: centre, +1: right)
     *                         (-1: as left as it can or end char on the tick, 0: centered on the tick, +1: first char on the tick)
     * @param label_offset     Label offset count (in the case the x-label is appended when the y-labels have are on the left hand side)
     * @param tick_val         Tick value container (coordinate & string) - assumes correct spacing
     * @param label_target_row Target row where the label is to be written to
     */
    template<typename T,
        typename = std::enable_if_t<std::is_same<std::string, T>::value>
    >
    void writeTextXLabel( unsigned                       label_max_size,
                          int                            text_align,
                          const size_t                   &label_offset,
                          const TickValue_t<std::string> &tick_val,
                          std::vector<size_t>            &label_target_row )
    {
        if( !label_target_row.empty() )
            label_target_row.emplace_back( 0x20 ); //single white-space for minimum label margin

        auto   value                = tick_val.second;
        auto   tick_char_index      = charWidth( tick_val.first.x ) + label_offset;
        auto   first_write_index    = label_target_row.size();
        size_t available_left_space = ( tick_char_index > first_write_index )
                                      ? tick_char_index - first_write_index
                                      : 0;
        size_t padding_left         = 0;

        if( value.length() > label_max_size )
            value = ( value.substr( 0, label_max_size - 1 ) += "." );

        if( text_align < 0 ) { //left align
            padding_left = ( available_left_space > value.size() )
                           ? available_left_space - value.size()
                           : 0;
        } else if( text_align > 0 ) { //right align
            padding_left = ( available_left_space > 0 )
                           ? available_left_space - 1
                           : 0;
        } else { //middle
            size_t str_offset  = ( value.size() > 1 )
                                 ? ( value.size() % 2 ? ( value.size() - 1 ) / 2 : value.size() / 2 )
                                 : 0;

            size_t write_index = ( str_offset >= available_left_space )
                                 ? 0 
                                 : tick_char_index - str_offset;

            padding_left       = ( first_write_index < write_index )
                                 ? write_index - first_write_index
                                 : 0;
        }

        for( auto i = 0; i < padding_left; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space

        for( char i : value )
            label_target_row.emplace_back( static_cast<size_t>( abs( i ) ) );
    }

    /**
     * Writes a string Y-Axis label to a given row
     * @tparam T String type only
     * @param label_max_size   Maximum label size
     * @param text_align       Text alignment (-1: left, 0: centre, +1: right)
     * @param tick_val         Tick value container (coordinate & string) - assumes correct spacing
     * @param label_target_row Target row where the label is to be written to
     */
    template<typename T,
             typename = std::enable_if_t<std::is_same<std::string, T>::value>
    >
    void writeTextYLabel( unsigned             label_max_size,
                          int                  text_align,
                          const TickValue_t<T> &tick_val,
                          std::vector<size_t>  &label_target_row )
    {
        auto   value         = tick_val.second;
        size_t padding_left  = 0;
        size_t padding_right = 0;
        size_t padding_size = ( value.size() < label_max_size )
                              ? label_max_size - value.size()
                              : 0;

        if( text_align < 0 ) {
            padding_right = padding_size;
        } else if( text_align > 0 ) {
            padding_left  = padding_size;
        } else {
            padding_left  = padding_size / 2;
            padding_right = padding_size - padding_left;
        }

        for( auto i = 0; i < padding_left; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space

        for( char i : value )
            label_target_row.emplace_back( static_cast<size_t>( abs( i ) ) );

        for( auto i = 0; i < padding_right; ++i )
            label_target_row.emplace_back( 0x20 ); //white-space
    }
}

#endif //EADLIB_CLI_BRAILLE_GRAPH_LABELLING_H
