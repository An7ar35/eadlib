/**
    \defgroup       Raster
    \ingroup        Braille
    @brief          Braille raster functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BRESENHAM_H
#define EADLIB_BRESENHAM_H

#include <type_traits>
#include <list>
#include <vector>
#include <cmath>
#include <functional>

#include "../../../datatype/Coordinate2D.h"

namespace eadlib::cli::braille::raster {
    /**
     * Gets a list of points making a straight line between and including 2 points on a raster
     * Note: uses and adapts Bresenham’s line algorithm
     * @tparam T Unsigned numeric type
     * @param a  Point A
     * @param b  Point B
     * @return List of points making up the line between A and B
     */
    template<typename T,
             typename = std::enable_if_t<std::is_unsigned<T>::value>
    >
    std::list<eadlib::Coordinate2D<T>> getLinePoints( eadlib::Coordinate2D<T> a,
                                                      eadlib::Coordinate2D<T> b )
    {
        using eadlib::Coordinate2D;

        auto list  = std::list<Coordinate2D<T>>();

        if( a == b )
            return list;

        if( a.x > b.x ) //set points left-to-right
            std::swap( a, b);

        auto diff        = Coordinate2D<long>( labs( b.x - a.x ), labs( b.y - a.y ) );
        auto diff_error  = Coordinate2D<long>( diff.x * 2, diff.y * 2 );
        bool steep_slope = ( diff.x < diff.y );
        int  sy          = ( a.y <= b.y ) ? 1 : -1; //y-axis increment step (up: 1, down: -1)
        auto curr_point  = a;
        auto error_total = 0;

        list.emplace_back( curr_point );
        if( steep_slope ) {
            while( curr_point != b ) {
                error_total += diff_error.x;
                if( error_total > diff.y ) {
                    curr_point.x += 1;
                    error_total  -= diff_error.y;
                }
                if( curr_point.y != b.y )
                    curr_point.y += sy;
                list.emplace_back( curr_point );
            }
        } else { //shallow slope
            while( curr_point != b ) {
                error_total += diff_error.y;
                if( error_total > diff.x ) {
                    curr_point.y += sy;
                    error_total  -= diff_error.x;
                }
                if( curr_point.x != b.x )
                    curr_point.x += 1;
                list.emplace_back( curr_point );
            }
        }

        return list;
    }

    /**
     * Gets a list of points making an area between and including 2 points
     * @tparam T   Unsigned numeric type
     * @param from Corner point A
     * @param to   Corner point B
     * @return List of raster points in area
     */
    template<typename T,
             typename = std::enable_if_t <std::is_unsigned<T>::value>
    >
    std::list<eadlib::Coordinate2D<T>> generateRasterAreaPoints( const eadlib::Coordinate2D<T> &from,
                                                                 const eadlib::Coordinate2D<T> &to )
    {
        using eadlib::Coordinate2D;

        auto list = std::list<Coordinate2D<T>>();
        auto min  = Coordinate2D<T>( std::min( from.x, to.x ), std::min( from.y, to.y ) );
        auto max  = Coordinate2D<T>( std::max( from.x, to.x ), std::max( from.y, to.y ) );

        for( auto x = min.x; x <= max.x; x++ )
            for( auto y = min.y; y <= max.y; y++ )
                list.emplace_back( Coordinate2D<T>( x, y ) );

        return list;
    }
}

#endif //EADLIB_BRESENHAM_H
