/**
   Component Name:  EADlib.cli.ProgressBar
   Language:        C++14

   License: GNUv2 Public License
   (c) Copyright E. A. Davison 2016

   Author: E. A. Davison

   Description: CLI progress bar
   Dependencies: eadlib::tool::Progress, eadlib::tool::IObserver
**/

#ifndef EADLIB_PROGRESSBAR_H
#define EADLIB_PROGRESSBAR_H

#include <iostream>
#include <sstream>
#include <iomanip>
#include <ios>

#include "../../interface/IObserver.h"

namespace eadlib {
    namespace cli {
        class ProgressBar : public interface::IObserver {
          public:
            ProgressBar( const size_t &width, const size_t &decimal_precision );
            ~ProgressBar();
            void update( const double &percentage ) override;
          private:
            //Variables
            std::string _progress_bar;
            size_t      _bar_width;
            size_t      _decimal_precision;
        };

        //--------------------------------------------------------------------------------------------------------------------------------------------
        // ProgressBar class public method implementations
        //--------------------------------------------------------------------------------------------------------------------------------------------
        /**
         * Constructor
         * @param width             Physical width of the progress bar
         * @param decimal_precision Decimal precision to display percentage progress with
         */
        inline ProgressBar::ProgressBar( const size_t &width, const size_t &decimal_precision ) :
            _bar_width( width > 0 ? width - 1 : width ),
            _progress_bar( width, ' ' ),
            _decimal_precision( decimal_precision )
        {}

        /**
         * Destructor
         */
        inline ProgressBar::~ProgressBar() {
        }

        inline void ProgressBar::update( const double &percentage ) {
            //Graphic progress
            if( _bar_width > 0 ) {
                auto position = (size_t) (percentage * _bar_width);
                std::stringstream ss;
                ss << "[";
                for( size_t i = 1; i < position; i++ ) {
                    ss << "=";
                }
                if( position < _bar_width ) {
                    ss << "|";
                    for( size_t i = position; i < _bar_width - 1; i++ ) {
                        ss << " ";
                    }
                }
                ss << "]";
                std::cout << "\r" << ss.str() << "";
            }
            //Numeric progress
            if( (unsigned)(percentage * 100) == 100 ) {
                std::cout << "100";
                if( _decimal_precision > 0 ) {
                    std::cout << ".";
                    for( size_t i = 0; i < _decimal_precision; i++ ) {
                        std::cout << "0";
                    }
                }
                std::cout << "\% \b" << std::endl;
            } else {
                if( percentage < 0.1 ) {
                    std::cout << " ";
                }
                if( percentage < 1 ) {
                    std::cout << " ";
                }
                std::cout << std::fixed;
                std::cout << std::setprecision( _decimal_precision ) << percentage * 100 << "\% \b";
            }
            std::cout << std::flush;
        }
    }
}

#endif //EADLIB_PROGRESSBAR_H
