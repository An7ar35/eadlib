#ifndef EADLIB_PROGRESS_BAR_H
#define EADLIB_PROGRESS_BAR_H

#include <iostream>

namespace eadlib {
    namespace cli {
        /**
         * Console Progress bar
         * @param current_step Current index
         * @param total_steps Number of indices to process
         * @param width Width of the bar
         */
        inline void progressBar_console( const int current_step, const int total_steps, const int width ) {
            int position { current_step * width / total_steps };
            float percent = current_step * 100 / total_steps;
            if( current_step == 1 ) {
                std::cout << "\r";
            }
            std::cout << "[";
            for( int i = 1; i < position; i++ ) {
                std::cout << "=";
            }
            if( (int) percent == 100 ) {
                std::cout << "] " << percent << "\%" << std::endl;
            } else {
                if( position < width ) {
                    std::cout << "|";
                }
                for( int i = position; i < width - 1; i++ ) {
                    std::cout << " ";
                }
                std::cout << "] " << percent << "\%\r";
            }
        }
    }
}
#endif //EADLIB_PROGRESS_BAR_H
