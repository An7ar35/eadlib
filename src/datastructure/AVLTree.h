/**
    @class          eadlib::AVLTree
    @brief          [ADT] AVL Tree

    Balanced binary tree where the heights of every node differ at most by +/- 1.\n
    i.e.: for every node; |height of left subtree - height of right subtree| <= 1

    @dependencies   eadlib::AVLTreeNode, eadlib::logger::Logger, eadlib::exception::undefined,
                    eadlib::LinearList, eadlib::LinkedList

    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_AVLTREE_H
#define EADLIB_AVLTREE_H

#include <cmath>
#include <string>
#include <list>
#include "../logger/Logger.h"
#include "AVLTreeNode.h"
#include "LinearList.h"
#include "LinkedList.h"
#include "../exception/undefined.h"
#include "../exception/duplication.h"

namespace eadlib {
    template<class K, class V> class AVLTree {
      public:
        AVLTree();
        AVLTree( const AVLTree<K,V> &tree );
        AVLTree( AVLTree &&tree ) noexcept;
        AVLTree( std::initializer_list<std::pair<K,V>> list );
        explicit AVLTree( LinearList<std::pair<K,V>> list );
        explicit AVLTree( LinkedList<std::pair<K,V>> list );
        ~AVLTree() = default;
        //Manipulation functions
        bool add( const K &key, const V &value );
        bool remove( const K &key );
        void clear();
        //Properties functions
        bool empty() const;
        size_t size() const;
        size_t height() const;
        bool isFull() const;
        bool isComplete() const;
        //Transversal & Search
        V value( const K &key ) const;
        K key( const V &value ) const;
        void preOrder( void(*output_function)( K &key, V &value ) ) const;
        void inOrder( void(*output_function)( K &key, V &value ) ) const;
        void postOrder( void(*output_function)( K &key, V &value ) ) const;
        void levelOrder( void(*output_function)( K &key, V &value ) ) const;
        std::string str();
      protected:
        enum class Branch { ROOT, LEFT, RIGHT };
        //Transversal of the tree
        void preOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<AVLTreeNode<K,V>> &node ) const;
        void inOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<AVLTreeNode<K,V>> &node ) const;
        void postOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<AVLTreeNode<K,V>> &node ) const;
        void levelOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<AVLTreeNode<K,V>> &node ) const;
      private:
        std::unique_ptr<AVLTreeNode<K,V>> _root;
        size_t                            _count;
        //Private functions
        V findKey( const K &key, AVLTreeNode<K, V> *node ) const;
        K findValue( const V &value, AVLTreeNode<K, V> *node ) const;
        bool remove( const K &key, AVLTreeNode<K,V> *parent, Branch branch, AVLTreeNode<K,V> *node );
        AVLTreeNode<K,V> * replaceWithSmallest( AVLTreeNode<K, V> *node, AVLTreeNode<K, V> *replacement );
        AVLTreeNode<K,V> * replaceWithLargest( AVLTreeNode<K, V> *node, AVLTreeNode<K, V> *replacement );
        void balance( AVLTreeNode<K,V> *node );
        //Rotation
        AVLTreeNode<K,V> * rotateRR( AVLTreeNode<K,V> *parent, Branch branch );
        AVLTreeNode<K,V> * rotateLL( AVLTreeNode<K,V> *parent, Branch branch );
        AVLTreeNode<K,V> * rotateLR( AVLTreeNode<K,V> *parent, Branch branch );
        AVLTreeNode<K,V> * rotateRL( AVLTreeNode<K,V> *parent, Branch branch );
        std::unique_ptr<AVLTreeNode<K,V>> detach( AVLTreeNode<K,V> *parent, Branch branch );
        AVLTreeNode<K,V> * attach( AVLTreeNode<K,V> *parent, Branch branch, std::unique_ptr<AVLTreeNode<K,V>> &orphan_branch );
        AVLTreeNode<K,V> * getChild( AVLTreeNode<K,V> *parent, Branch branch );
        Branch getBranch( AVLTreeNode<K,V> *node );
        int heightDifference( AVLTreeNode<K,V> *node ) const;
        bool isRoot( AVLTreeNode<K,V> *node ) const;

      public:
        /**
         * @class const_preorder_iterator
         * @brief Pre-Order const iterator
         */
        class const_preorder_iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef V                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef const AVLTreeNode<K,V>*   pointer; //Pointer to constant AVLTreeNode
            typedef const AVLTreeNode<K,V>&   reference;
            friend class AVLTree<K,V>;
            //Constructors
            const_preorder_iterator();
            const_preorder_iterator( const const_preorder_iterator &it );
            //Operators
            const_preorder_iterator & operator =( const const_preorder_iterator &rhs );
            const_preorder_iterator & operator ++();
            const const_preorder_iterator operator ++( int );
            reference operator *() const; //dereference e.g.: (*it).first
            pointer operator ->() const;  //dereference e.g.: it->first
            bool operator ==( const const_preorder_iterator &rhs ) const;
            bool operator !=( const const_preorder_iterator &rhs ) const;
          private:
            //Private variables
            AVLTreeNode<K,V> *_pos;
            std::list<AVLTreeNode<K,V> *> _list;
            //Private methods
            void addAll( std::list<AVLTreeNode<K,V> *> &container, AVLTreeNode<K,V> *node );
        };

        /**
         * @class const_inorder_iterator
         * @brief In-Order const iterator
         */
        class const_inorder_iterator { //in order
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef V                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef const AVLTreeNode<K,V>*   pointer; //Pointer to constant AVLTreeNode
            typedef const AVLTreeNode<K,V>&   reference;
            friend class AVLTree<K,V>;
            //Constructors
            const_inorder_iterator();
            const_inorder_iterator( const const_inorder_iterator &it );
            //Operators
            const_inorder_iterator & operator =( const const_inorder_iterator &rhs );
            const_inorder_iterator & operator ++();
            const const_inorder_iterator operator ++( int );
            reference operator *() const; //dereference e.g.: (*it).first
            pointer operator ->() const;  //dereference e.g.: it->first
            bool operator ==( const const_inorder_iterator &rhs ) const;
            bool operator !=( const const_inorder_iterator &rhs ) const;
          private:
            //Private variables
            AVLTreeNode<K,V> *_pos;
            std::list<AVLTreeNode<K,V> *> list;
            //Private methods
            void addAll( std::list<AVLTreeNode<K,V> *> &container, AVLTreeNode<K, V> *node );
        };

        /**
         * @class const_postorder_iterator
         * @brief Post-Order const iterator
         */
        class const_postorder_iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef V                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef const AVLTreeNode<K,V>*   pointer; //Pointer to constant AVLTreeNode
            typedef const AVLTreeNode<K,V>&   reference;
            friend class AVLTree<K,V>;
            //Constructors
            const_postorder_iterator();
            const_postorder_iterator( const const_postorder_iterator &it );
            //Operators
            const_postorder_iterator & operator =( const const_postorder_iterator &rhs );
            const_postorder_iterator & operator ++();
            const const_postorder_iterator operator ++( int );
            reference operator *() const; //dereference e.g.: (*it).first
            pointer operator ->() const;  //dereference e.g.: it->first
            bool operator ==( const const_postorder_iterator &rhs ) const;
            bool operator !=( const const_postorder_iterator &rhs ) const;
          private:
            //Private variables
            AVLTreeNode<K,V> *_pos;
            std::list<AVLTreeNode<K,V> *> _list;
            //Private methods
            void addAll( std::list<AVLTreeNode<K,V> *> &container, AVLTreeNode<K,V> *node );
        };

        /**
         * @class const_levelorder_iterator
         * @brief Level-Order const iterator
         */
        class const_levelorder_iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef V                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef const AVLTreeNode<K,V>*   pointer; //Pointer to constant AVLTreeNode
            typedef const AVLTreeNode<K,V>&   reference;
            friend class AVLTree<K,V>;
            //Constructors
            const_levelorder_iterator();
            const_levelorder_iterator( const const_levelorder_iterator &it );
            //Operators
            const_levelorder_iterator & operator =( const const_levelorder_iterator &rhs );
            const_levelorder_iterator & operator ++();
            const const_levelorder_iterator operator ++( int );
            reference operator *() const; //dereference e.g.: (*it).first
            pointer operator ->() const;  //dereference e.g.: it->first
            bool operator ==( const const_levelorder_iterator &rhs ) const;
            bool operator !=( const const_levelorder_iterator &rhs ) const;
          private:
            //Private variables
            AVLTreeNode<K,V> *_pos;
            std::list<AVLTreeNode<K,V> *> _list;
            //Private methods
            void addAll( std::list<AVLTreeNode<K,V> *> &container, AVLTreeNode<K,V> *node );
        };

        const_preorder_iterator   cbegin_preorder() const noexcept;
        const_preorder_iterator   cend_preorder() const noexcept;
        const_inorder_iterator    cbegin_inorder() const noexcept;
        const_inorder_iterator    cend_inorder() const noexcept;
        const_postorder_iterator  cbegin_postorder() const noexcept;
        const_postorder_iterator  cend_postorder() const noexcept;
        const_levelorder_iterator cbegin_levelorder() const noexcept;
        const_levelorder_iterator cend_levelorder() const noexcept;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // AVL Tree class public method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor (default)
     */
    template<class K, class V> AVLTree<K,V>::AVLTree() :
        _count( 0 ),
        _root( std::make_unique<AVLTreeNode<K,V>>() )
    {};

    /**
     * Copy-Constructor
     * @param tree AVLTree to copy
     */
    template<class K, class V> AVLTree<K,V>::AVLTree( const AVLTree<K, V> &tree ) :
        _count( 0 ),
        _root( std::make_unique<AVLTreeNode<K,V>>() )
    {
        LOG_DEBUG( "[eadlib::AVLTree<K,V>::AVLTree(..)] Copy constructor called. Copied ", tree._count, " nodes." );
        for( auto it = tree.cbegin_inorder(); it != tree.cend_inorder(); ++it ) {
            add( it->key(), it->value() );
        }
    }

    /**
     * Move-Constructor
     * @param tree AVLTree to move over
     */
    template<class K, class V> AVLTree<K,V>::AVLTree( AVLTree &&tree ) noexcept :
        _count( tree._count ),
        _root( std::move( tree._root ) )
    {
        LOG_DEBUG( "[eadlib::AVLTree<K,V>::AVLTree(..)] Move constructor called. Moved ", tree._count, " nodes." );
        tree._count = 0;
    }

    /**
     * Constructor
     * @param list Initializer list
     * @throws eadlib::exception::duplication when adding a duplicate key from the list
     */
    template<class K, class V> AVLTree<K,V>::AVLTree( const std::initializer_list<std::pair<K,V>> list ) :
        _count( 0 ),
        _root( std::make_unique<AVLTreeNode<K,V>>() )
    {
        for( typename std::initializer_list<std::pair<K,V>>::iterator it = list.begin(); it != list.end(); ++it ) {
            add( it->first, it->second );
        }
    }

    /**
     * Constructor
     * @param list LinearList<K>
     */
    template<class K, class V> AVLTree<K,V>::AVLTree( const LinearList<std::pair<K,V>> list ) :
        _count( 0 ),
        _root( std::make_unique<AVLTreeNode<K,V>>() )
    {
        for( auto e : list ) {
            add( e.first, e.second );
        }
    }

    /**
     * Constructor
     * @param list LinkedList<K>
     */
    template<class K, class V> AVLTree<K,V>::AVLTree( const LinkedList<std::pair<K,V>> list ) :
        _count( 0 ),
        _root( std::make_unique<AVLTreeNode<K,V>>() )
    {
        for( auto e : list ) {
            add( e.first, e.second );
        }
    }

    /**
     * Adds a key to the tree
     * @param key   Key of node
     * @param value Value to add
     * @return Success
     */
    template<class K, class V> bool AVLTree<K,V>::add( const K &key, const V &value ) {
        LOG_DEBUG( "[AVLTree<K,V>::add( ", key, ", ", value, " )] Adding node." );
        if( _count < 1 ) {
            if( !_root ) {
                _root = std::make_unique<AVLTreeNode<K,V>>();
            }
            _root->_key = key;
            _root->_value = value;
            _count++;
        } else {
            auto ptr = _root.get();
            while( true ) {
                if( key < ptr->key() ) {
                    if( ptr->_left ) {
                        ptr = ptr->_left.get();
                    } else {
                        ptr->_left = std::make_unique<AVLTreeNode<K,V>>( ptr, key, value );
                        this->_count++;
                        balance( ptr );
                        break;
                    }
                } else if( key > ptr->key() ) {
                    if( ptr->_right ) {
                        ptr = ptr->_right.get();
                    } else {
                        ptr->_right = std::make_unique<AVLTreeNode<K,V>>( ptr, key, value );
                        this->_count++;
                        balance( ptr );
                        break;
                    }
                } else {
                    LOG_ERROR("[AVLTree<K,V>::add( ", key, ", ", value, " )] Key already exists in tree.");
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Removes key in tree matching given key
     * @param key Key to look for and remove
     * @return Success
     */
    template<class K, class V> bool AVLTree<K,V>::remove( const K &key ) {
        if( size() < 1 ) {
            return false;
        } else {
            return remove( key, nullptr, Branch::ROOT, _root.get() );
        }
    }

    /**
     * Clears the tree
     */
    template<class K, class V> void AVLTree<K,V>::clear() {
        this->_root.reset( nullptr );
        _count = 0;
    }

    /**
     * Checks if the Tree is empty
     * @return Empty status
     */
    template<class K, class V> bool AVLTree<K,V>::empty() const {
        return _count < 1;
    }

    /**
     * Get the number of nodes in the tree
     * @return Size of the tree
     */
    template<class K, class V> size_t AVLTree<K,V>::size() const {
        return _count;
    }

    /**
     * Gets the height of the tree
     * @return Height of tree
     */
    template<class K, class V> size_t AVLTree<K,V>::height() const {
        if( _count > 0 && _root )
            return static_cast<size_t>( _root->height() );
        else
            return 0;
    }

    /**
     * Checks if the tree is full
     * @return 'Full tree' status
     */
    template<class K, class V> bool AVLTree<K,V>::isFull() const {
        if( _count == 0 ) return true;
        bool flag { false };
        eadlib::LinkedList<AVLTreeNode<K,V> *> queue;
        queue.addToHead( *_root.get() );
        while( !queue.empty() ) {
            AVLTreeNode<K,V> *current = queue.removeTail();
            if( current->_left ) {
                if( flag ) return false;
                queue.addToHead( *current->_left.get() );
            } else {
                flag = true;
            }
            if( current->_right ) {
                if( flag ) return false;
                queue.addToHead( *current->_right.get() );
            } else {
                flag = true;
            }
        }
        return true;
    }

    /**
     * Checks if the tree is complete
     * @return 'Complete tree' status
     */
    template<class K, class V> bool AVLTree<K,V>::isComplete() const {
        return ( pow( 2, height() ) -1 ) == _count;
    }

    /**
     * Checks if key given exists in tree
     * @param key Key to look for
     * @return Found status
     * @throws std::out_of_range when Value does not exist in tree
     */
    template<class K, class V> V AVLTree<K,V>::value( const K &key ) const {
        try {
            if( size() < 1 ) {
                throw std::out_of_range( "[eadlib::AVLTree<K,V>::value(..)] Tree is empty." );
            } else {
                return findKey( key, _root.get() );
            }
        } catch( std::out_of_range e ) {
            LOG_DEBUG( "[eadlib::AVLTree<K,V>::value( ", key, " )] Key does not exist in the tree." );
            throw std::out_of_range( e );
        }
    }

    /**
     * Looks for a value in the tree
     * @tparam K Key type
     * @tparam V Value type
     * @param value Value to find
     * @return Key of the value
     * @throws std::out_of_range when Key does not exist in tree
     */
    template<class K, class V> K AVLTree<K,V>::key( const V &value ) const {
        try {
            return findValue( value, _root.get() );
        } catch( std::out_of_range &e ) {
            LOG_DEBUG( "[eadlib::AVLTree<K,V>::key( ", value, " )] Value does not exist in the tree." );
            throw std::out_of_range( e );
        }
    }

    /**
     * Pre-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void AVLTree<K,V>::preOrder( void(*output_function)( K &key, V &value ) ) const {
        preOrder( output_function, _root );
    }

    /**
     * In-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void AVLTree<K,V>::inOrder( void(*output_function)( K &key, V &value ) ) const {
        inOrder( output_function, _root );
    }

    /**
     * Post-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void AVLTree<K,V>::postOrder( void(*output_function)( K &key, V &value ) ) const {
        postOrder( output_function, _root );
    }

    /**
     * Levelled BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void AVLTree<K,V>::levelOrder( void(*output_function)( K &key, V &value ) ) const {
        levelOrder( output_function, _root );
    }

    /**
     * To String as leveled order
     * @return String with the level ordered tree
     */
    template<class K, class V> std::string AVLTree<K,V>::str() {
        std::ostringstream oss;
        if( _root.get() == nullptr ) return oss.str();
        eadlib::LinkedList<AVLTreeNode<K,V> *> queue;
        queue.addToHead( _root.get() );
        while( !queue.empty() ) {
            AVLTreeNode<K,V> *current = queue.removeTail();
            if( current->_left ) oss << (current->_left).get()->_key << "<-";
            else oss << " <-";
            oss << current->_key;
            if( current->_right ) oss << "->" << current->_right->_key << " , ";
            else oss << "-> , ";
            if( current->_left ) queue.addToHead( current->_left.get() );
            if( current->_right ) queue.addToHead( current->_right.get() );
        }
        return oss.str();
    }

    //-----------------------------------------------------------------------------------------------------------------
    // AVL Tree class protected method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Pre-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node AVLTreeNode to process
     */
    template<class K, class V> void AVLTree<K,V>::preOrder( void(*output_function)( K &key, V &value ),
                                                            const std::unique_ptr<AVLTreeNode<K,V>> &node ) const
    {
        if( node ) {
            output_function( node->_key, node->_value );
            preOrder( output_function, node->_left );
            preOrder( output_function, node->_right );
        }
    }

    /**
     * In-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node AVLTreeNode to process
     */
    template<class K, class V> void AVLTree<K,V>::inOrder( void(*output_function)( K &key, V &value ),
                                                           const std::unique_ptr<AVLTreeNode<K,V>> &node ) const
    {
        if( node ) {
            inOrder( output_function, node->_left );
            output_function( node->_key, node->_value );
            inOrder( output_function, node->_right );
        }
    }

    /**
     * Post-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node AVLTreeNode to process
     */
    template<class K, class V> void AVLTree<K,V>::postOrder( void(*output_function)( K &key, V &value ),
                                                             const std::unique_ptr<AVLTreeNode<K,V>> &node ) const
    {
        if( node ) {
            postOrder( output_function, node->_left );
            postOrder( output_function, node->_right );
            output_function( node->_key, node->_value );
        }
    }

    /**
     * Level-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node AVLTreeNode to process
     */
    template<class K, class V> void AVLTree<K,V>::levelOrder( void(*output_function)( K &key, V &value ),
                                                              const std::unique_ptr<AVLTreeNode<K,V>> &node ) const
    {
        if( node.get() == nullptr ) return;
        eadlib::LinkedList<AVLTreeNode<K,V> *> queue;
        queue.addToHead( node.get() );
        while( !queue.empty() ) {
            AVLTreeNode<K,V> *current = queue.removeTail();
            output_function( current->_key, current->_value );
            if( current->_left ) queue.addToHead( current->_left.get() );
            if( current->_right ) queue.addToHead( current->_right.get() );
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // AVL Tree class private method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Balances the nodes in the tree in reverse (leaf to root)
     * @param node Root of the balance
     */
    template<class K, class V> void AVLTree<K,V>::balance( AVLTreeNode<K,V> *node ) {
        if( node ) {
            AVLTreeNode<K,V> * parent = node->_parent;
            int factor = node->getBalanceFactor();
            if( factor > 1 ) { //right rotation
                if( node->_left->getBalanceFactor() >= 0 )
                    parent = rotateRR( parent, getBranch( node ) );
                else
                    parent = rotateLR( parent, getBranch( node ) );
            } else if( factor < -1 ) { //left rotation
                if( node->_right->getBalanceFactor() <= 0 )
                    parent = rotateLL( parent, getBranch( node ) );
                else
                    parent = rotateRL( parent, getBranch( node ) );
            }
            balance( parent );
        }
    }

    /**
     * Right rotation
     * Note: where c<-b<-a becomes c<-b->a
     * @param parent Pointer to the parent of the rotation
     * @param branch Branch of the parent
     * @return Parent node
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::rotateRR( AVLTreeNode<K,V> *parent,
                                                                          Branch branch )
    {
        auto child = getChild( parent, branch );
        if( child ) {
            //     a              b
            //    /     =>       / \
            //   b              c   a
            //  / \                /
            // c   ?              ?
            std::unique_ptr<AVLTreeNode<K,V>> b = detach( child, Branch::LEFT ); // c<-b<-
            std::unique_ptr<AVLTreeNode<K,V>> a = detach( parent, branch ); // ->a
            if( b->_right ) { // ->?
                std::unique_ptr<AVLTreeNode<K, V>> b_rchild = detach( b.get(), Branch::RIGHT );
                attach( a.get(), Branch::LEFT, b_rchild );
            }
            attach( b.get(), Branch::RIGHT, a );
            attach( parent, branch, b );
        }
        return parent;
    }

    /**
     * Left Rotation
     * Note: where a->b->c becomes a<-b->c
     * @param parent Pointer to the parent of the rotation
     * @param branch Branch of the parent
     * @return Parent node
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::rotateLL( AVLTreeNode<K,V> *parent,
                                                                          Branch branch )
    {
        auto child = getChild( parent, branch );
        if( child ) {
            //  a             b
            //   \    =>     / \
            //    b         a   c
            //   / \         \
            //  ?   c         ?
            std::unique_ptr<AVLTreeNode<K,V>> b = detach( child, Branch::RIGHT ); // ->b->c
            std::unique_ptr<AVLTreeNode<K,V>> a = detach( parent, branch ); // ->a
            if( b->_left.get() ) { // ->?
                std::unique_ptr<AVLTreeNode<K, V>> b_lchild = detach( b.get(), Branch::LEFT );
                attach( a.get(), Branch::RIGHT, b_lchild );
            }
            attach( b.get(), Branch::LEFT, a );
            attach( parent, branch, b );
        }
        return parent;
    }

    /**
     * Left-Right rotation
     * @param parent Pointer to the parent of the rotation
     * @return Parent node
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::rotateLR( AVLTreeNode<K,V> *parent,
                                                                          Branch branch )
    {
        AVLTreeNode<K,V> * b = rotateLL( getChild( parent, branch ), Branch::LEFT );
        return rotateRR( b->_parent, getBranch( b ) );
    }

    /**
     * Right-Left rotation
     * @param parent Pointer to the parent of the rotation
     * @return Parent node
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::rotateRL( AVLTreeNode<K,V> *parent,
                                                                          Branch branch )
    {
        AVLTreeNode<K,V> * b = rotateRR( getChild( parent, branch ), Branch::RIGHT );
        return rotateLL( b->_parent, getBranch( b ) );
    }

    /**
     * Searches for existence of key in a node and below
     * @param key  Item to look for
     * @param node Node to evaluate and search from
     * @return Success of search
     * @throws std::out_of_range when key does not exist in the tree
     */
    template<class K, class V> V AVLTree<K,V>::findKey( const K &key, AVLTreeNode<K, V> *node ) const {
        if( node->_key == key ) {
            return node->value();
        } else {
            if( key < node->_key && node->_left ) {
                return findKey( key, node->_left.get() );
            }
            if( key > node->_key && node->_right ) {
                return findKey( key, node->_right.get() );
            }
            throw std::out_of_range( "[eadlib::AVLTree<K,V>::getKey(..)] Key does not exist in tree." );
        }
    }

    /**
     * Searches for a value in the tree
     * @tparam K    Key type
     * @tparam V    Value type
     * @param value Value to find
     * @param node  Starting node of the tree
     * @return Value
     * @throws std::out_of_range when value does not exist in the tree
     */
    template<class K, class V> K AVLTree<K,V>::findValue( const V &value, AVLTreeNode<K, V> *node ) const {
        /**
         * [LAMBDA] Adds all the node keys to the visited map
         * @param node_list All the nodes to check
         * @param node      Starting node
         */
        std::function<void(std::list<AVLTreeNode<K,V> *> &, AVLTreeNode<K,V> *)>
            addAll = [&]( std::list<AVLTreeNode<K,V> *> &node_list, AVLTreeNode<K,V> *node )
        {
            if( node ) {
                node_list.emplace_back( node );
                addAll( node_list, node->_left.get() );
                addAll( node_list, node->_right.get() );
            }
        };
        //findValue logic
        std::list<AVLTreeNode<K,V> *> node_list;
        addAll( node_list, node );

        for( auto e : node_list ) {
            if( e->value() == value ) {
                return node->key();
            }
        }
        throw std::out_of_range( "[eadlib::AVLTree<K,V>::findValue(..)] Value does not exist in tree." );
    }

    /**
     * Removes a node matching the key
     * @param key    Key to look for
     * @param parent Pointer to parent of the current node
     * @param branch Branch used from parent to current node
     * @param node   current node in the recursion
     * @return Success of deletion
     */
    template<class K, class V> bool AVLTree<K,V>::remove( const K &key,
                                                          AVLTreeNode<K,V> *parent,
                                                          AVLTree::Branch branch,
                                                          AVLTreeNode<K,V> *node )
    {
        if( node->_key == key ) {
            if( !node->_left ^ !node->_right ) { // 0..1 child (XOR on pointers to check)
                if( node->_left ) {
                    auto replacement = detach( node, Branch::LEFT );
                    balance( attach( parent, branch, replacement ) );
                } else { // node->_right
                    auto replacement = detach( node, Branch::RIGHT );
                    balance( attach( parent, branch, replacement ) );
                }
            } else if( node->_right && node ->_left ) { // 2 children
                    balance( replaceWithLargest( node, node->_left.get() ) );
            } else { // Leaf node
                switch( branch ) {
                    case Branch::LEFT:
                        parent->_left.reset( nullptr );
                        break;
                    case Branch::RIGHT:
                        parent->_right.reset( nullptr );
                        break;
                    case Branch::ROOT:
                        this->_root.reset( nullptr );
                        break;
                }
                balance( parent );
            }
            _count--;
            LOG_DEBUG( "[eadlib::AVLTree<K,V>::remove( ", key, ", .. )] Removed '", key, "'" );
            return true;
        } else { // recurse until key is found
            if( key < node->_key && node->_left ) {
                return remove( key, node, Branch::LEFT, node->_left.get() );
            }
            if( key > node->_key && node->_right ) {
                return remove( key, node, Branch::RIGHT, node->_right.get() );
            }
            return false;
        }
    }

    /**
     * Replaces a node with the the smallest valued node in the sub tree
     * @param node        Node to replace
     * @param replacement Replacement node candidate (recursive search)
     * @return Pointer to the replacement node's old parents
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::replaceWithSmallest( AVLTreeNode<K, V> *node,
                                                                                     AVLTreeNode<K, V> *replacement )
    {
        if( replacement->_left ) {
            return replaceWithSmallest( node, replacement->_left.get() );
        } else {
            auto replacement_parent = replacement->_parent;
            auto replacement_branch = ( replacement_parent == node ? getBranch( replacement ) : Branch::LEFT );
            auto replacement_node   = detach( replacement_parent, replacement_branch );
            if( node->_left ) {
                auto left_tree = detach( node, Branch::LEFT );
                attach( replacement_node.get(), Branch::LEFT, left_tree );
            }
            if( node->_right ) {
                auto right_tree = detach( node, Branch::RIGHT );
                attach( replacement_node.get(), Branch::RIGHT, right_tree );
            }
            attach( node->_parent, getBranch(node), replacement_node );
            return replacement_parent == node
                   ? replacement
                   :replacement_parent;
        }
    }

    /**
     * Replaces a node with the the largest valued node in the sub tree
     * @param node        Node to replace
     * @param replacement Replacement node candidate (recursive search)
     * @return Pointer to the replacement node's old parents
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::replaceWithLargest( AVLTreeNode<K, V> *node,
                                                                                    AVLTreeNode<K, V> *replacement )
    {
        if( replacement->_right ) {
            return replaceWithLargest( node, replacement->_right.get() );
        } else {
            auto replacement_parent = replacement->_parent;
            auto replacement_branch = ( replacement_parent == node ? getBranch( replacement ) : Branch::RIGHT );
            auto replacement_node   = detach( replacement_parent, replacement_branch );
            if( node->_left ) {
                auto left_tree = detach( node, Branch::LEFT );
                attach( replacement_node.get(), Branch::LEFT, left_tree );
            }
            if( node->_right ) {
                auto right_tree = detach( node, Branch::RIGHT );
                attach( replacement_node.get(), Branch::RIGHT, right_tree );
            }
            attach( node->_parent, getBranch(node), replacement_node );
            return replacement_parent == node
                   ? replacement
                   : replacement_parent;
        }
    }

    /**
     * Detaches a branch from specified node
     * @param parent Parent from which to detach branch
     * @param branch Branch to detach
     * @return Unique pointer to the detached branch
     * @exception eadlib::undefined When the parent pointer is null and branch is not ROOT
     */
    template<class K, class V> std::unique_ptr<AVLTreeNode<K,V>> AVLTree<K,V>::detach( AVLTreeNode<K,V> *parent,
                                                                                       AVLTree::Branch branch )
    {
        if( parent == nullptr && branch != Branch::ROOT ) {
            LOG_ERROR( "[eadlib::AVLTree<K,V>::detach( AVLTreeNode<K,V> *parent, AVLTree::Branch )] Parent pointer is undefined (null)." );
            throw eadlib::exception::undefined(
                "[eadlib::AVLTree<K,V>::detach( AVLTreeNode<K,V> *parent, AVLTree::Branch )] "
                "Parent pointer is undefined (null)."
            );
        }
        switch( branch ) {
            case Branch::LEFT:
                if( parent->_left.get() == nullptr ) return std::unique_ptr<AVLTreeNode<K,V>>();
                return std::move( parent->_left );
            case Branch::RIGHT:
                if( parent->_right.get() == nullptr ) return std::unique_ptr<AVLTreeNode<K,V>>();
                return std::move( parent->_right );
            case Branch::ROOT:
                return std::move( this->_root );
        }
    }

    /**
     * Attaches an orphan branch to a adoptive parent node
     * @param parent Adoptive parent node
     * @param branch Branch to attach to
     * @param orphan_branch Orphan branch to adopt
     * @return Pointer to root of attached orphan branch
     * @exception eadlib::undefined When the parent pointer is null and branch is not ROOT
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::attach( AVLTreeNode<K,V> *parent,
                                                                        AVLTree::Branch branch,
                                                                        std::unique_ptr<AVLTreeNode<K,V>> &orphan_branch )
    {
        if( parent == nullptr && branch != Branch::ROOT ) {
            LOG_ERROR( "[eadlib::AVLTree<K,V>::attach( AVLTreeNode<K,V> *parent, AVLTree::Branch branch, std::unique_ptr<AVLTreeNode<K,V>> &orphan_branch )] Parent pointer is undefined (null)." );
            throw eadlib::exception::undefined(
                "[eadlib::AVLTree<K,V>::attach( AVLTreeNode<K,V> *parent, AVLTree::Branch branch, std::unique_ptr<AVLTreeNode<K,V>> &orphan_branch )] "
                "Parent pointer is undefined (null)."
            );
        }
        AVLTreeNode<K,V> * ptr = orphan_branch.get();
        switch( branch ) {
                case Branch::LEFT:
                    parent->_left.reset( orphan_branch.release() );
                    parent->_left->_parent = parent;
                    return parent->_left.get();
                case Branch::RIGHT:
                    parent->_right.reset( orphan_branch.release() );
                    parent->_right->_parent = parent;
                    return parent->_right.get();
                case Branch::ROOT:
                    _root.reset( orphan_branch.release() );
                    _root->_parent = nullptr;
                    return _root.get();
        }
    }

    /**
     * Gets a pointer to the child in the branch specified
     * @param parent Parent node
     * @param branch Branch the child needed is attached to
     * @return Pointer to the child
     * @exception eadlib::undefined When the parent pointer is null and branch is not ROOT
     */
    template<class K, class V> AVLTreeNode<K,V> * AVLTree<K,V>::getChild( AVLTreeNode<K,V> *parent,
                                                                          Branch branch )
    {
        if( parent == nullptr && branch != Branch::ROOT ) {
            LOG_ERROR( "[eadlib::AVLTree<K,V>::getChild( AVLTreeNode<K,V> *parent, AVLTree::Branch branch )] Parent pointer is undefined (null)." );
            throw eadlib::exception::undefined(
                "[eadlib::AVLTree<K,V>::getChild( AVLTreeNode<K,V> *parent, AVLTree::Branch branch )] "
                "Parent pointer is undefined (null)."
            );
        }
        switch( branch ) {
            case Branch::LEFT:
                if( parent->_left ) return parent->_left.get();
                break;
            case Branch::RIGHT:
                if( parent->_right ) return parent->_right.get();
                break;
            case Branch::ROOT:
                if( this->_root ) return this->_root.get();
                break;
        }
        LOG_ERROR( "[eadlib::AVLTree<K,V>::getChild( AVLTreeNode<K,V> *parent, Branch branch )] Child doesn't exist." );
        return nullptr;
    }

    /**
     * Gets the branch from which the node spawns from
     * @param node Node to search the parent's branch of
     * @return Spawning branch
     * @exception eadlib::undefined when the node points to a parent that isn't his.
     */
    template<class K, class V> typename AVLTree<K,V>::Branch AVLTree<K,V>::getBranch( AVLTreeNode<K,V> *node ) {
        if( node->_parent == nullptr ) return Branch::ROOT;
        if( node->_parent->_left.get() == node ) return Branch::LEFT;
        if( node->_parent->_right.get() == node ) return Branch::RIGHT;
        LOG_ERROR(
            "eadlib::AVLTree<K,V>::getBranch( AVLTreeNode<K,V> * )] Node [",
            node->key(), "] points to the wrong parent [", node->_parent->key(),
            "]. Must have attachment issues or be confused."
        );
        throw eadlib::exception::undefined(
            "eadlib::AVLTree<K,V>::getBranch( AVLTreeNode<K,V> * )] "
            "Node points to the wrong parent. Must have attachment issues or be confused."
        );
    }

    /**
     * Finds the balance factor of a node
     * @param node AVLTreeNode to check
     * @return Balance factor
     */
    template<class K, class V> int AVLTree<K,V>::heightDifference( AVLTreeNode<K,V> *node ) const {
        return node->_left->height() - node->_right->height();
    }

    /**
     * Checks if node pointer is root
     * @param node Node to check
     * @return root node state (not null)
     */
    template<class K, class V> bool AVLTree<K,V>::isRoot( AVLTreeNode<K,V> *node ) const {
        return _root && node == _root.get();
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Pre-Order Const Iterator class method implementations for the AVLTree
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     */
    template<class K, class V> AVLTree<K,V>::const_preorder_iterator::const_preorder_iterator() :
        _pos( 0 )
    {}

    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @param it Iterator to copy over
     */
    template<class K, class V> AVLTree<K,V>::const_preorder_iterator::const_preorder_iterator( const AVLTree::const_preorder_iterator &it ) :
        _pos( it._pos )
    {}

    /**
     * Assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to assign with
     * @return Assigned iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator & AVLTree<K,V>::const_preorder_iterator::operator =(
            const AVLTree::const_preorder_iterator &rhs )
    {
        _pos = rhs._pos;
        return *this;
    }

    /**
     * Pre-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Incremented iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator & AVLTree<K,V>::const_preorder_iterator::operator ++()
    {
        if( !_list.empty() ) {
            _pos = _list.front();
            _list.pop_front();
        } else {
            _pos = 0;
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Current iterator
     */
    template<class K, class V>
        const typename AVLTree<K,V>::const_preorder_iterator AVLTree<K,V>::const_preorder_iterator::operator ++( int )
    {
        const_preorder_iterator tmp( *this );
        operator++();
        return tmp;
    }

    /**
     * Dereference operator
     * e.g.: (*it).first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator::reference & AVLTree<K,V>::const_preorder_iterator::operator *() const
    {
        return *_pos;
    }

    /**
     * Dereference operator
     * e.g.: it->first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator::pointer AVLTree<K,V>::const_preorder_iterator::operator ->() const
    {
        return _pos;
    }

    /**
     * Equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return Equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_preorder_iterator::operator ==(
            const AVLTree<K,V>::const_preorder_iterator &rhs ) const
    {
        return _pos == rhs._pos;
    }

    /**
     * Negative equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return !equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_preorder_iterator::operator !=(
            const AVLTree<K,V>::const_preorder_iterator &rhs ) const
    {
        return _pos != rhs._pos;
    }

    /**
     * Adds all the nodes using pre-order to the vector
     * @tparam K Key type
     * @tparam V Value type
     * @param container Node pointer container
     * @param node      Starting node
     */
    template<class K, class V>
        void AVLTree<K,V>::const_preorder_iterator::addAll(
            std::list<AVLTreeNode<K,V> *> &container,
            AVLTreeNode<K, V> *node )
    {
        if( node ) {
            container.emplace_back( node );
            addAll( container, node->_left.get() );
            addAll( container, node->_right.get() );
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // In-Order Const Iterator class method implementations for the AVLTree
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     */
    template<class K, class V> AVLTree<K,V>::const_inorder_iterator::const_inorder_iterator() :
        _pos( 0 )
    {}

    /**
     * Constructor
     * @param it Iterator to copy over
     */
    template<class K, class V> AVLTree<K,V>::const_inorder_iterator::const_inorder_iterator( const AVLTree::const_inorder_iterator &it ) :
        _pos( it._pos )
    {}

    /**
     * Assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to assign with
     * @return Assigned iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator & AVLTree<K,V>::const_inorder_iterator::operator =(
            const AVLTree::const_inorder_iterator &rhs )
    {
        _pos = rhs._pos;
        return *this;
    }

    /**
     * Pre-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Incremented iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator & AVLTree<K,V>::const_inorder_iterator::operator ++()
    {
        if( !list.empty() ) {
            _pos = list.front();
            list.pop_front();
        } else {
            _pos = 0;
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Current iterator
     */
    template<class K, class V>
        const typename AVLTree<K,V>::const_inorder_iterator AVLTree<K,V>::const_inorder_iterator::operator ++( int )
    {
        const_inorder_iterator tmp( *this );
        operator++();
        return tmp;
    }

    /**
     * Dereference operator
     * e.g.: (*it).first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator::reference AVLTree<K,V>::const_inorder_iterator::operator *() const
    {
        return *_pos;
    }

    /**
     * Dereference operator
     * e.g.: it->first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator::pointer AVLTree<K,V>::const_inorder_iterator::operator ->() const
    {
        return _pos;
    }

    /**
     * Equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return Equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_inorder_iterator::operator ==( const AVLTree<K,V>::const_inorder_iterator &rhs ) const
    {
        return _pos == rhs._pos;
    }

    /**
     * Negative equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return !equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_inorder_iterator::operator !=( const AVLTree<K,V>::const_inorder_iterator &rhs ) const
    {
        return _pos != rhs._pos;
    }

    /**
     * Adds all left nodes to the stack
     * @tparam K Key type
     * @tparam V Value type
     * @param node Starting node
     */
    template<class K, class V>
        void AVLTree<K,V>::const_inorder_iterator::addAll(
            std::list<AVLTreeNode<K,V> *> &container,
            AVLTreeNode<K, V>             *node )
    {
        if( node ) {
            addAll( container, node->_left.get() );
            container.emplace_back( node );
            addAll( container, node->_right.get() );
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Post-Order Const Iterator class method implementations for the AVLTree
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     */
    template<class K, class V> AVLTree<K,V>::const_postorder_iterator::const_postorder_iterator() :
        _pos( 0 )
    {}

    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @param it Iterator to copy over
     */
    template<class K, class V> AVLTree<K,V>::const_postorder_iterator::const_postorder_iterator( const AVLTree::const_postorder_iterator &it ) :
        _pos( it._pos )
    {}

    /**
     * Assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to assign with
     * @return Assigned iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_postorder_iterator & AVLTree<K,V>::const_postorder_iterator::operator =(
            const AVLTree<K,V>::const_postorder_iterator &rhs )
    {
        _pos = rhs._pos;
        return *this;
    }

    /**
     * Pre-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Incremented iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_postorder_iterator & AVLTree<K,V>::const_postorder_iterator::operator ++()
    {
        if( !_list.empty() ) {
            _pos = _list.front();
            _list.pop_front();
        } else {
            _pos = 0;
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Current iterator
     */
    template<class K, class V>
        const typename AVLTree<K,V>::const_postorder_iterator AVLTree<K,V>::const_postorder_iterator::operator ++( int )
    {
        const_postorder_iterator tmp( *this );
        operator++();
        return tmp;
    }

    /**
     * Dereference operator
     * e.g.: (*it).first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K, V>::const_postorder_iterator::reference AVLTree<K,V>::const_postorder_iterator::operator *() const
    {
        return *_pos;
    }

    /**
     * Dereference operator
     * e.g.: it->first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_postorder_iterator::pointer AVLTree<K,V>::const_postorder_iterator::operator ->() const
    {
        return _pos;
    }

    /**
     * Equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return Equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_postorder_iterator::operator ==(
            const AVLTree<K,V>::const_postorder_iterator &rhs ) const
    {
        return _pos == rhs._pos;
    }

    /**
     * Negative equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return !equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_postorder_iterator::operator !=(
            const AVLTree<K,V>::const_postorder_iterator &rhs ) const
    {
        return _pos != rhs._pos;
    }

    /**
     * Adds all the nodes using pre-order to the vector
     * @tparam K Key type
     * @tparam V Value type
     * @param container Node pointer container
     * @param node      Starting node
     */
    template<class K, class V>
        void AVLTree<K,V>::const_postorder_iterator::addAll(
            std::list<AVLTreeNode<K, V> *> &container,
            AVLTreeNode<K, V>              *node )
    {
        if( node ) {
            addAll( container, node->_left.get() );
            addAll( container, node->_right.get() );
            container.emplace_back( node );
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Level-Order Const Iterator class method implementations for the AVLTree
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     */
    template<class K, class V> AVLTree<K,V>::const_levelorder_iterator::const_levelorder_iterator() :
        _pos( 0 )
    {};

    /**
     * Constructor
     * @tparam K Key type
     * @tparam V Value type
     * @param it Iterator to copy over
     */
    template<class K, class V> AVLTree<K,V>::const_levelorder_iterator::const_levelorder_iterator( const AVLTree::const_levelorder_iterator &it ) :
        _pos( it._pos )
    {}

    /**
     * Assignment operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to assign with
     * @return Assigned iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator & AVLTree<K,V>::const_levelorder_iterator::operator =(
            const AVLTree::const_levelorder_iterator &rhs )
    {
        _pos = rhs._pos;
        return *this;
    }

    /**
     * Pre-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Incremented iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator & AVLTree<K,V>::const_levelorder_iterator::operator ++()
    {
        if( !_list.empty() ) {
            _pos = _list.front();
            _list.pop_front();
        } else {
            _pos = 0;
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @tparam K Key type
     * @tparam V Value type
     * @return Current iterator
     */
    template<class K, class V>
        const typename AVLTree<K,V>::const_levelorder_iterator AVLTree<K,V>::const_levelorder_iterator::operator ++( int )
    {
        const_levelorder_iterator tmp( *this );
        operator++();
        return tmp;
    }

    /**
     * Dereference operator
     * e.g.: (*it).first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator::reference & AVLTree<K,V>::const_levelorder_iterator::operator *() const
    {
        return *_pos;
    }

    /**
     * Dereference operator
     * e.g.: it->first
     * @tparam K Key type
     * @tparam V Value type
     * @return Iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator::pointer AVLTree<K,V>::const_levelorder_iterator::operator ->() const
    {
        return _pos;
    }

    /**
     * Equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return Equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_levelorder_iterator::operator ==( const AVLTree<K,V>::const_levelorder_iterator &rhs ) const
    {
        return _pos == rhs._pos;
    }

    /**
     * Negative equivalence operator
     * @tparam K Key type
     * @tparam V Value type
     * @param rhs Iterator to compare with
     * @return !equivalence state
     */
    template<class K, class V>
        bool AVLTree<K,V>::const_levelorder_iterator::operator !=( const AVLTree<K,V>::const_levelorder_iterator &rhs ) const
    {
        return _pos != rhs._pos;
    }

    /**
     * Adds all the nodes using pre-order to the vector
     * @tparam K Key type
     * @tparam V Value type
     * @param container Node pointer container
     * @param node      Starting node
     */
    template<class K, class V>
        void AVLTree<K,V>::const_levelorder_iterator::addAll(
            std::list<AVLTreeNode<K, V> *> &container,
            AVLTreeNode<K, V>              *node )
    {
        if( node ) {
            eadlib::LinkedList<AVLTreeNode<K,V> *> queue;
            queue.addToHead( node );
            while( !queue.empty() ) {
                AVLTreeNode<K, V> *current = queue.removeTail();
                container.emplace_back( current );
                if( current->_left ) queue.addToHead( current->_left.get() );
                if( current->_right ) queue.addToHead( current->_right.get() );
            }
        }
    }

    //-----------------------------------------------------------------------------------------------------------------
    // AVLTree Iterator methods implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * cbegin() for in-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return In-Order const begin iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator AVLTree<K,V>::cbegin_inorder() const noexcept
    {
        const_inorder_iterator it;
        it.addAll( it.list, _root.get() );
        if( !it.list.empty() ) {
            it._pos = it.list.front();
            it.list.pop_front();
        }
        return it;
    }

    /**
     * cend() for in-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return In-Order const end iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_inorder_iterator AVLTree<K,V>::cend_inorder() const noexcept
    {
        return const_inorder_iterator();
    }

    /**
     * cbegin() for pre-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Pre-Order const begin iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator AVLTree<K,V>::cbegin_preorder() const noexcept
    {
        const_preorder_iterator it;
        it.addAll( it._list, _root.get() );
        if( !it._list.empty() ) {
            it._pos = it._list.front();
            it._list.pop_front();
        }
        return it;
    }

    /**
     * cend() for pre-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Pre-Order const end iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_preorder_iterator AVLTree<K,V>::cend_preorder() const noexcept
    {
        return const_preorder_iterator();
    }

    /**
     * cbegin() for post-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Post-Order const begin iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_postorder_iterator AVLTree<K,V>::cbegin_postorder() const noexcept
    {
        const_postorder_iterator it;
        it.addAll( it._list, _root.get() );
        if( !it._list.empty() ) {
            it._pos = it._list.front();
            it._list.pop_front();
        }
        return it;
    }

    /**
     * cend() for post-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Post-Order const end iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_postorder_iterator AVLTree<K,V>::cend_postorder() const noexcept
    {
        return const_postorder_iterator();
    }

    /**
     * cbegin() for level-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Level-Order const begin iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator AVLTree<K,V>::cbegin_levelorder() const noexcept
    {
        const_levelorder_iterator it;
        it.addAll( it._list, _root.get() );
        if( !it._list.empty() ) {
            it._pos = it._list.front();
            it._list.pop_front();
        }
        return it;
    }

    /**
     * cend() for level-order iterator
     * @tparam K Key type
     * @tparam V Value type
     * @return Level-Order const end iterator
     */
    template<class K, class V>
        typename AVLTree<K,V>::const_levelorder_iterator AVLTree<K,V>::cend_levelorder() const noexcept
    {
        return const_levelorder_iterator();
    }
};

#endif //EADLIB_AVLTREE_H
