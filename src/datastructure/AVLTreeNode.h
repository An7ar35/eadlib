/**
    @class          eadlib::AVLTreeNode
    @brief          [ADT] AVL Tree node

    Node structure for the Balanced binary tree.

    @dependencies   eadlib::GenericNode, eadlib::logger::Logger

    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_AVLTREENODE_H
#define EADLIB_AVLTREENODE_H

#include "../logger/Logger.h"
#include "GenericNodePair.h"
#include <memory>

namespace eadlib {
    template<class K, class V> struct AVLTreeNode : public GenericNodePair<K,V> {
        //Constructors/Destructor
        explicit AVLTreeNode( AVLTreeNode<K,V> *parent = nullptr );
        AVLTreeNode( AVLTreeNode<K,V> *parent, const K &key, const V &value );
        AVLTreeNode( AVLTreeNode<K,V> *parent, const K &key, const V &value,
                     std::unique_ptr<AVLTreeNode<K,V>> &pLeft,
                     std::unique_ptr<AVLTreeNode<K,V>> &pRight );
        AVLTreeNode( const AVLTreeNode<K,V> &node ) = delete;
        AVLTreeNode( AVLTreeNode<K,V> &&node ) noexcept;
        virtual ~AVLTreeNode() = default;
        //Methods
        int height() const;
        bool balanced();
        int getBalanceFactor();
        //Variables
        int _balance_factor;
        AVLTreeNode<K,V> *_parent;
        std::unique_ptr<AVLTreeNode<K,V>> _left;
        std::unique_ptr<AVLTreeNode<K,V>> _right;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // AVLTreeNode class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Default Constructor
     * @param parent Pointer to the parent of the node (default=nullptr)
     */
    template<class K, class V> AVLTreeNode<K,V>::AVLTreeNode( AVLTreeNode<K,V> *parent ) :
        _parent( parent ),
        _balance_factor( 0 )
    {}

    /**
     * Constructor
     * @param parent Parent
     * @param key    Key of node
     * @param value  Value of node
     */
    template<class K, class V> AVLTreeNode<K,V>::AVLTreeNode( AVLTreeNode<K,V> *parent, const K &key, const V &value ) :
        _parent( parent ),
        _balance_factor( 0 )
    {
        this->_key = key;
        this->_value = value;
    }

    /**
     * Constructor
     * @param parent Parent
     * @param key    Key of node
     * @param value  Value of node
     * @param pLeft  Pointer of a node to place on the left
     * @param pRight Pointer of a node to place on the right
     */
    template<class K, class V> AVLTreeNode<K,V>::AVLTreeNode( AVLTreeNode<K,V> *parent,
                                                              const K &key,
                                                              const V &value,
                                                              std::unique_ptr<AVLTreeNode<K,V>> &pLeft,
                                                              std::unique_ptr<AVLTreeNode<K,V>> &pRight ) :
        _parent( parent ),
        _balance_factor( 0 ),
        _left( pLeft.release() ),
        _right( pRight.release() )
    {
        this->_key = key;
        this->_value = value;
    }

    /**
     * Move-Constructor
     * @param node Node to move over
     */
    template<class K, class V> AVLTreeNode<K,V>::AVLTreeNode( eadlib::AVLTreeNode<K,V> &&node ) noexcept :
        _parent( node._parent ),
        _balance_factor( 0 ),
        _left( std::make_unique<AVLTreeNode<K,V>>( node._left.release() ) ),
        _right( std::make_unique<AVLTreeNode<K,V>>( node._right.release() ) )
    {
        this->_key = node._key ;
        this->_value = node._value;
    }

    /**
     * Gets the height of the node
     * @return Height of node
     */
    template<class K, class V> int AVLTreeNode<K,V>::height() const {
        int l_height { 0 };
        int r_height { 0 };
        if( _left )
            l_height = _left->height();
        if( _right )
            r_height = _right->height();
        return std::max( l_height, r_height ) + 1;
    }

    /**
     * Refreshes and gets the balance status the balance factor
     * @return Balanced status
     */
    template<class K, class V> bool AVLTreeNode<K,V>::balanced() {
        getBalanceFactor();
        LOG_TRACE( "[eadlib::AVLTreeNode<K>::balanced()] Node '", this->_key, "' balance factor: ", _balance_factor, "." );
        return _balance_factor < 2 && _balance_factor > -2;
    }

    /**
     * Gets the balance factor at the node
     * @return Balance Factor
     */
    template<class K, class V> int AVLTreeNode<K,V>::getBalanceFactor() {
        int l_height { 0 };
        int r_height { 0 };
        if( _left )
            l_height = _left->height();
        if( _right )
            r_height = _right->height();
        _balance_factor = l_height - r_height;
        return _balance_factor;
    }
}

#endif //EADLIB_AVLTREENODE_H
