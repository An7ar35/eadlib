/**
    @class          eadlib::BST
    @brief          [ADT] Binary Search Tree
    @dependencies   eadlib::BinaryTree, eadlib::BinaryTreeNode, eadlib::LinearList, eadlib::LinkedList
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BST_H
#define EADLIB_BST_H

#include "BinaryTree.h"
#include "BinaryTreeNode.h"
#include "LinearList.h"
#include "LinkedList.h"

namespace eadlib {
    template<class K, class V> class BST : public BinaryTree<K,V> {
      public:
        BST() = default;
        BST( std::initializer_list<std::pair<K,V>> list );
        explicit BST( const eadlib::LinearList<std::pair<K,V>> &list );
        explicit BST( const eadlib::LinkedList<std::pair<K,V>> &list );
        ~BST() = default;
        //Operations
        void add( const K &key, const V &value );
        bool remove( const K &key );
        bool search( const K &key ) const;
        V getValue( const K & key ) const;
        K getKey( const V & value ) const;
      private:
        enum class Branch { ROOT, LEFT, RIGHT };
        bool search( const K &key, BTreeNode<K,V> *node ) const;
        bool remove( const K &key, BTreeNode<K, V> *parent, Branch branch, BTreeNode<K, V> *node );
        BTreeNode <K, V> * getNode( const K & key, BTreeNode <K, V> * node ) const;
        BTreeNode <K, V> * getNode( const V & value, BTreeNode <K, V> * node ) const;
        std::pair<K,V> removeSmallest( BTreeNode<K, V> *parent, BTreeNode<K, V> *node );
        std::pair<K,V> removeLargest( BTreeNode<K, V> *parent, BTreeNode<K, V> *node );
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Binary Search Tree class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param list Initializer list
     */
    template<class K, class V> BST<K,V>::BST( const std::initializer_list<std::pair<K,V>> list ) :
        BinaryTree<K,V>()
    {
        for( typename std::initializer_list<std::pair<K,V>>::iterator it = list.begin(); it != list.end(); ++it ) {
            add( it->first, it->second );
        }
    }

    /**
     * Constructor
     * @param list LinearList
     */
    template<class K, class V> BST<K,V>::BST( const eadlib::LinearList<std::pair<K,V>> &list ) :
        BinaryTree<K,V>()
    {
        for( auto e : list ) {
            add( e.first, e.second );
        }
    }

    /**
     * Constructor
     * @param list LinkedList<K,V>
     */
    template<class K, class V> BST<K,V>::BST( const eadlib::LinkedList<std::pair<K,V>> &list ) :
        BinaryTree<K,V>()
    {
        for( auto e : list ) {
            add( e.first, e.second );
        }
    }

    /**
     * Adds an key to the BST
     * @param key   Key of node to add
     * @param value Value of node to add
     */
    template<class K, class V> void BST<K,V>::add( const K &key, const V &value ) {
        if( this->count() < 1 ) {
            this->_root.reset( new BTreeNode<K,V>( key, value ) );
            this->_count++;
        } else {
            BTreeNode<K,V> *ptr = this->_root.get();
            while( true ) {
                if( key < ptr->key() ) {
                    if( ptr->_left ) {
                        ptr = ptr->_left.get();
                    } else {
                        ptr->_left = std::make_unique<BTreeNode<K,V>>( key, value );
                        this->_count++;
                        break;
                    }
                } else {
                    if( ptr->_right ) {
                        ptr = ptr->_right.get();
                    } else {
                        ptr->_right = std::make_unique<BTreeNode<K,V>>( key, value );
                        this->_count++;
                        break;
                    }
                }
            }
        }
    }

    /**
     * Removes key that matches
     * @param key Item to look for and remove
     * @return Success
     */
    template<class K, class V> bool BST<K,V>::remove( const K &key ) {
        if( this->count() < 1 ) {
            return false;
        } else {
            return remove( key, nullptr, Branch::ROOT, this->_root.get() );
        }
    }

    /**
     * Searches for existence of key
     * @param key Item to look for
     * @return Success of search
     */
    template<class K, class V> bool BST<K,V>::search( const K &key ) const {
        if( this->count() < 1 ) {
            return false;
        } else {
            return search( key, this->_root.get() );
        }
    }

    /**
     * Finds the value of a key
     * @param key Key to find
     * @return Value of key
     * @throws std::out_of_range when key does not exists within the BST
     */
    template<class K, class V> V BST<K, V>::getValue( const K &key ) const {
        if( this->count() > 0 ) {
            auto node = getNode( key, this->_root.get() );
            if( node )
                return node->value();
        }
        throw std::out_of_range( "Key does not exist in Tree." );
    }

    /**
     * Finds the lowest order key of a value (using DFS)
     * @param value Value to find
     * @return Lowest key of value
     * @throws std::out_of_range when value does not exists within the BST
     */
    template<class K, class V> K BST<K, V>::getKey( const V &value ) const {
        if( this->count() > 0 ) {
            auto node = getNode( value, this->_root.get() );
            if( node )
                return node->key();
        }
        throw std::out_of_range( "Value does not exist in Tree." );
    }

    /**
     * [PRIVATE] Searches for existence of key in a node and below
     * @param key Item to look for
     * @param node Node to evaluate and search from
     * @return Success of search
     */
    template<class K, class V> bool BST<K,V>::search( const K &key, BTreeNode<K,V> *node ) const {
        if( node->_key == key ) {
            return true;
        }
        else {
            if( key < node->_key && node->_left ) {
                return search( key, node->_left.get() );
            }
            if( key > node->_key && node->_right ) {
                return search( key, node->_right.get() );
            }
            return false;
        }
    }

    /**
     * [PRIVATE] Removes node with matching key value
     * @param key Key to look for
     * @param branch Link to the next node to look in
     * @param Success
     */
    template<class K, class V> bool BST<K,V>::remove( const K          &key,
                                                      BTreeNode <K, V> *parent,
                                                      Branch            branch,
                                                      BTreeNode <K, V> *node )
    {
        if( node->_key == key ) {
            if( node->_right ) {
                auto replacement_key_value = removeSmallest( node, node->_right.get() );
                node->_key = replacement_key_value.first;
                node->_value = replacement_key_value.second;
            } else if( node->_left ) {
                auto replacement_key_value = removeLargest( node, node->_left.get() );
                node->_key = replacement_key_value.first;
                node->_value = replacement_key_value.second;
            } else { //It's a leaf node
                switch( branch ) {
                    case Branch::LEFT:
                        parent->_left.reset( nullptr );
                        break;
                    case Branch::RIGHT:
                        parent->_right.reset( nullptr );
                        break;
                    case Branch::ROOT:
                        this->_root.reset( nullptr );
                        break;
                }
            }
            this->_count--;
            LOG_DEBUG( "[eadlib::BST<K,V>::remove( ", key, ", NodeInfo(..) )] Removed '", key, "'" );
            return true;
        } else {
            if( key < node->_key && node->_left ) {
                return remove( key, node, Branch::LEFT, node->_left.get() );
            }
            if( key > node->_key && node->_right ) {
                return remove( key, node, Branch::RIGHT, node->_right.get() );
            }
            return false;
        }
    }

    /**
     * Gets node of a key
     * @param key  Key
     * @param node Root node to search from
     * @return Node pointer
     */
    template<class K, class V> BTreeNode<K, V> * BST<K, V>::getNode( const K &key, BTreeNode<K, V> *node ) const {
        if( node->_key == key ) {
            return node;
        }
        else {
            if( key < node->_key && node->_left ) {
                return getNode( key, node->_left.get() );
            }
            if( key > node->_key && node->_right ) {
                return getNode( key, node->_right.get() );
            }
            return nullptr;
        }
    }

    /**
     * Gets node of a value
     * @param value Value
     * @param node  Root node to search from
     * @return Node pointer
     */
    template<class K, class V> BTreeNode<K, V> * BST<K, V>::getNode( const V &value, BTreeNode<K, V> *node ) const {
        if( node->_value == value ) {
            return node;
        } else {
            if( node->_left ) {
                auto ptr = getNode( value, node->_left.get() );
                if( ptr != nullptr )
                    return ptr;
            }
            if( node->_right ) {
                auto ptr = getNode( value, node->_right.get() );
                if( ptr != nullptr )
                    return ptr;
            }
            return nullptr;
        }
    }

    /**
     * [PRIVATE] Removes the smallest valued node in the tree
     * @param node Link to the next node (with its m_parent)
     * @return Key/Value pair removed
     */
    template<class K, class V> std::pair<K,V> BST<K,V>::removeSmallest( BTreeNode<K, V> *parent,
                                                                        BTreeNode<K, V> *node )
    {
        if( node->_left ) {
            return removeSmallest( node, node->_left.get() );
        } else {
            auto node_details = std::make_pair( node->_key, node->_value );
            if( parent->_left.get() == node )
                parent->_left.reset( nullptr );
            else //_parent->right.get() == node
                parent->_right.reset( node->_right.release() );
            return node_details;
        }
    }

    /**
     * [PRIVATE] Removes the largest valued node in the tree
     * @param node Link to the next node (with its m_parent)
     * @return Key/Value pair removed
     */
    template<class K, class V> std::pair<K,V> BST<K,V>::removeLargest( BTreeNode <K, V> *parent,
                                                                       BTreeNode <K, V> *node )
    {
        if( node->_right ) {
            return removeLargest( node, node->_right.get() );
        } else {
            auto node_details = std::make_pair( node->_key, node->_value );
            if( parent->_right.get() == node )
                parent->_right.reset( nullptr );
            else //_parent->left.get() == node
                parent->_left.reset( node->_left.release() );
            return node_details;
        }
    }
}

#endif //EADLIB_BST_H
