/**
    @class          eadlib::Bag
    @brief          [ADT] %Bag

    Randomised access container of elements

    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BAG_H
#define EADLIB_BAG_H

#include <ctime>
#include <vector>
#include <stdexcept>

namespace eadlib {
    template<class T> class Bag {
          public:
            /**
             * Constructor (infinite bag size)
             */
            Bag() {
                srand( time(NULL) );
                this->n = 0;
                this->max_size = -1;
            }
            /**
             * Constructor
             * @param size Size of the bag
             */
            Bag( int size ) {
                srand( time(NULL) );
                this->n = 0;
                size > 0 ? this->max_size = size : this->max_size = -1;
            }
            /**
             * Destructor
             */
            ~Bag() {};
            /**
             * Places an item in the bag
             * @param item Item to place in bag
             * @return Success
             */
            bool put( T item ) {
                if( this->bag.size() < this->max_size ) {
                    this->bag.push_back( item );
                    this->n++;
                    return true;
                } else {
                    return false;
                }
            }
            /**
             * Gets an item from the bag
             * @return item
             * @exception std::out_of_range when bag is empty
             */
            T grab() {
                if( this->bag.empty() ) {
                    throw std::out_of_range("Bag<>::grab(): empty bag");
                } else {
                    unsigned int index = this->random();
                    T item = this->bag.at( index );
                    this->bag.erase( this->bag.begin() + index );
                    n--;
                    return item;
                }
            }
            /**
             * Gets the number of items inside the bag
             * @return Number of items
             */
            unsigned int getNumberOfItems() {
                return this->n;
            }
            /**
             * Checks if bag is full
             * @return full sate of bag
             */
            bool isFull() {
                if( this->max_size == -1 ) {
                    return false;
                } else {
                    return this->bag.size() == this->max_size;
                }
            }
            /**
             * Checks if bag is empty
             * @return empty state of bag
             */
            bool isEmpty() {
                return this->bag.empty();
            }
            /**
             * Gets the capacity of the bag
             * return Size capacity (-1 if undefined)
             */
            int getCapacity() {
                return this->max_size;
            }
          private:
            unsigned int n;
            int max_size;
            std::vector<T> bag { };
            /**
             * Produces a random number in the range of the size of the bag
             */
            unsigned int random() {
                return ( rand() % n );
            }
        };
}

#endif //EADLIB_BAG_H
