/**
    @class          eadlib::BinaryTree
    @brief          [ADT] Binary Tree

    Implemented using std::unique_ptr instead of raw pointers

    @dependencies   eadlib::BinaryTreeNode, eadlib::logger::Logger, eadlib::LinkedList

    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BINARYTREE_H
#define EADLIB_BINARYTREE_H

#include <iostream>
#include <memory>
#include <cmath>
#include "../logger/Logger.h"
#include "BinaryTreeNode.h"
#include "LinkedList.h"

namespace eadlib {
    template<class K, class V> class BinaryTree {
      public:
        BinaryTree();
        BinaryTree( const K &key, const V &value );
        BinaryTree( const K &key, const V &value, eadlib::BinaryTree<K,V> &left, eadlib::BinaryTree<K,V> &right );
        BinaryTree( std::initializer_list<std::pair<K,V>> levelOrder_list ); //TODO look into this levelOrder thing...not sure about this
        BinaryTree( const BinaryTree &tree ) = delete;
        BinaryTree( BinaryTree &&tree ) noexcept;
        virtual ~BinaryTree() = default;
        //Properties functions
        bool isEmpty() const;
        long height() const;
        long count() const;
        bool isComplete() const;
        bool isFull() const;
        //Transversal
        void preOrder( void(*output_function)( K &key, V &value ) ) const;
        void inOrder( void(*output_function)( K &key, V &value ) ) const;
        void postOrder( void(*output_function)( K &key, V &value ) ) const;
        void levelOrder( void(*output_function)( K &key, V &value ) ) const;

      protected:
        //Transversal of the tree
        void preOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<BTreeNode<K,V>> &node ) const;
        void inOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<BTreeNode<K,V>> &node  ) const;
        void postOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<BTreeNode<K,V>> &node ) const;
        void levelOrder( void(*output_function)( K &key, V &value ), const std::unique_ptr<BTreeNode<K,V>> &node ) const;
        //Properties
        std::unique_ptr<BTreeNode<K,V>> _root;
        long _height;
        long _count;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // BinaryTree class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor (default)
     */
    template<class K, class V> BinaryTree<K,V>::BinaryTree() :
        _count( 0 ),
        _height( 0 ),
        _root( std::make_unique<BTreeNode<K,V>>() )
    {};

    /**
     * Constructor
     * @param key   Key of root
     * @param value Value of root
     */
    template<class K, class V> BinaryTree<K,V>::BinaryTree( const K &key, const V &value ) :
        _count( 1 ),
        _height( 1 ),
        _root( std::make_unique<BTreeNode<K,V>>( key, value ) )
    {};

    /**
     * Constructor
     * @param key   Key of root
     * @param value Value of root
     * @param left  BinaryTree on left of node
     * @param right BinaryTree on right of node
     */
    template<class K, class V> BinaryTree<K,V>::BinaryTree( const K &key, const V &value,
                                                            eadlib::BinaryTree<K,V> &left,
                                                            eadlib::BinaryTree<K,V> &right ) :
        _root( std::make_unique<BTreeNode<K,V>>( key, value ) )
    {
        _root->_left.reset( left._root.release() );
        _root->_right.reset( right._root.release() );
        _count = 1 + left.count() + right.count();
        _height = 1 + ( left._height > right._height ? left._height : right._height );
    }

    /**
     * Move-Constructor
     * @param tree BinaryTree to move
     */
    template<class K, class V> BinaryTree<K,V>::BinaryTree( BinaryTree &&tree ) noexcept :
        _root( std::make_unique<BTreeNode<K,V>>( tree._root.release() ) ),
        _count( tree._count ),
        _height( tree._height )
    {};

    /**
     * Constructor with InitializerList
     * @param levelOrder_list Initializer list represented in level-order
     */
    template<class K, class V> BinaryTree<K,V>::BinaryTree( std::initializer_list<std::pair<K,V>> levelOrder_list ) {
        _count = 0;
        _height = 0;
        if( levelOrder_list.size() < 1 ) {
            _root = std::make_unique<BTreeNode<K,V>>();
        } else {
            //Filling up the work-queue
            eadlib::LinkedList<std::pair<K,V>> work_queue;
            for( typename std::initializer_list<std::pair<K,V>>::iterator it = levelOrder_list.begin(); it != levelOrder_list.end(); ++it ) {
                work_queue.addToHead( { it->first, it->second } );
            }
            //Calculating height of tree
            _count = levelOrder_list.size();
            for( double n = levelOrder_list.size(); n > 1; n /= 2  ) {
                _height++;
            }
            //Building the tree
            eadlib::LinkedList<BTreeNode<K,V> *> build_queue;
            auto work_item = work_queue.removeTail();
            _root = std::make_unique<BTreeNode<K,V>>( work_item.first, work_item.second );
            build_queue.addToHead( _root.get() );
            while( !work_queue.empty() ) {
                BTreeNode<K,V> *current = build_queue.removeTail();
                work_item = work_queue.removeTail();
                current->_left = std::make_unique<BTreeNode<K,V>>( work_item.first, work_item.second );
                build_queue.addToHead( current->_left.get() );
                if( work_queue.empty() ) return;
                work_item = work_queue.removeTail();
                current->_right = std::make_unique<BTreeNode<K,V>>( work_item.first, work_item.second );
                build_queue.addToHead( current->_right.get() );
            }
        }
    }

    /**
     * Gets empty status of the tree
     * @return Whether the tree is empty or not
     */
    template<class K, class V> bool BinaryTree<K,V>::isEmpty() const {
        return _count == 0;
    }

    /**
     * Gets the number of levels in the tree
     * @return Levels in Tree
     */
    template<class K, class V> long BinaryTree<K,V>::height() const {
        return _height;
    }

    /**
     * Gets the amount of content within the tree
     * @return Content count
     */
    template<class K, class V> long BinaryTree<K,V>::count() const {
        return _count;
    }

    /**
     * Checks if the tree is complete or not
     * @return Complete tree status
     */
    template<class K, class V> bool BinaryTree<K,V>::isComplete() const {
        if( _count == 0 ) return true;
        bool flag { false };
        eadlib::LinkedList<BTreeNode<K,V> *> queue;
        queue.addToHead( _root.get() );
        while( !queue.empty() ) {
            BTreeNode<K,V> *current = queue.removeTail();
            if( current->_left ) {
                if( flag ) return false;
                queue.addToHead( current->_left.get() );
            } else {
                flag = true;
            }
            if( current->_right ) {
                if( flag ) return false;
                queue.addToHead( current->_right.get() );
            } else {
                flag = true;
            }
        }
        return true;
    }

    /**
     * Checks if the tree is full or not
     * @return Full tree status
     */
    template<class K, class V> bool BinaryTree<K,V>::isFull() const {
        return ( pow( 2, height() ) -1 ) == _count;
    }

    /**
     * Pre-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void BinaryTree<K,V>::preOrder( void(*output_function)( K &key, V &value ) ) const {
        preOrder( output_function, _root );
    }

    /**
     * In-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void BinaryTree<K,V>::inOrder( void(*output_function)( K &key, V &value ) ) const {
        inOrder( output_function, _root );
    }

    /**
     * Post-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void BinaryTree<K,V>::postOrder( void(*output_function)( K &key, V &value ) ) const {
        postOrder( output_function, _root );
    }

    /**
     * Levelled BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     */
    template<class K, class V> void BinaryTree<K,V>::levelOrder( void(*output_function)( K &key, V &value ) ) const {
        levelOrder( output_function, _root );
    }

    /**
     * [PROTECTED] Pre-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node BTreeNode to process
     */
    template<class K, class V> void BinaryTree<K,V>::preOrder( void(*output_function)( K &key, V &value ),
                                                               const std::unique_ptr<BTreeNode<K,V>> &node ) const {
        if( node ) {
            output_function( node->_key, node->_value );
            preOrder( output_function, node->_left );
            preOrder( output_function, node->_right );
        }
    }

    /**
     * [PROTECTED] In-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node BTreeNode to process
     */
    template<class K, class V> void BinaryTree<K,V>::inOrder( void(*output_function)( K &key, V &value ),
                                                              const std::unique_ptr<BTreeNode<K,V>> &node ) const {
        if( node ) {
            inOrder( output_function, node->_left );
            output_function( node->_key, node->_value );
            inOrder( output_function, node->_right );
        }
    }

    /**
     * [PROTECTED] Post-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node BTreeNode to process
     */
    template<class K, class V> void BinaryTree<K,V>::postOrder( void(*output_function)( K &key, V &value ),
                                                                const std::unique_ptr<BTreeNode<K,V>> &node ) const {
        if( node ) {
            postOrder( output_function, node->_left );
            postOrder( output_function, node->_right );
            output_function( node->_key, node->_value );
        }
    }

    /**
     * [PROTECTED] Level-Order BinaryTree transversal
     * @param output_function Function to process output of the content of the node
     * @param node BTreeNode to process
     */
    template<class K, class V> void BinaryTree<K,V>::levelOrder( void(*output_function)( K &key, V &value ),
                                                                 const std::unique_ptr<BTreeNode<K,V>> &node ) const {
        if( node.get() == nullptr ) return;
        eadlib::LinkedList<BTreeNode<K,V> *> queue;
        queue.addToHead( node.get() );
        while( !queue.empty() ) {
            BTreeNode<K,V> *current = queue.removeTail();
            output_function( current->_key, current->_value );
            if( current->_left ) queue.addToHead( current->_left.get() );
            if( current->_right ) queue.addToHead( current->_right.get() );
        }
    }
}

#endif //EADLIB_BINARYTREE_H
