/**
    @class          eadlib::BTreeNode
    @brief          [ADT] Binary Tree node
    @dependencies   eadlib::GenericNode
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_BINARYTREENODE_H
#define EADLIB_BINARYTREENODE_H

#include <memory>
#include "GenericNodePair.h"

namespace eadlib {
    template<class K, class V> struct BTreeNode : public GenericNodePair<K,V> {
        BTreeNode() { };
        BTreeNode( const K &key, const V &value );
        BTreeNode( const K &key, const V &value,
                   std::unique_ptr<BTreeNode> &pLeft,
                   std::unique_ptr<BTreeNode> &pRight );
        BTreeNode( const BTreeNode<K,V> &node ) = delete;
        BTreeNode( BTreeNode<K,V> &&node );
        virtual ~BTreeNode() { };
        int height() const;
        std::unique_ptr<BTreeNode<K,V>> _left;
        std::unique_ptr<BTreeNode<K,V>> _right;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Binary Tree BTreeNode class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param key Key of node
     * @param value Value of the node
     */
    template<class K, class V> BTreeNode<K,V>::BTreeNode( const K &key, const V &value ) {
        this->_key = key;
        this->_value = value;
    }

    /**
     * Constructor
     * @param key Key of node
     * @param value Value of node
     * @param pLeft Pointer of a node to place on the left
     * @param pRight Pointer of a node to place on the right
     */
    template<class K, class V> BTreeNode<K,V>::BTreeNode( const K &key, const V &value,
                                                          std::unique_ptr<BTreeNode> &pLeft,
                                                          std::unique_ptr<BTreeNode> &pRight ) {
        this->_key = key;
        this->_value = value;
        _left.reset( pLeft.release());
        _right.reset( pRight.release());
    }

    /**
     * Move-Constructor
     * @param node BTreeNode to move over
     */
    template<class K, class V> BTreeNode<K,V>::BTreeNode( BTreeNode<K,V> &&node ) :
        _left( std::make_unique<BTreeNode<K,V>>( node._left.release() ) ),
        _right( std::make_unique<BTreeNode<K,V>>( node._right.release() ) )
    {
        this->_key = node._key;
        this->_value = node._value;
    }

    /**
     * Gets the height of the node
     * @return Height of node
     */
    template<class K, class V> int BTreeNode<K,V>::height() const {
        int l_height { 0 };
        int r_height { 0 };
        if( _left )
            l_height = _left->height();
        if( _right )
            r_height = _right->height();
        return std::max( l_height, r_height ) + 1;
    }
}

#endif //EADLIB_BINARYTREENODE_H
