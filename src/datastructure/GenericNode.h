/**
    @class          eadlib::GenericSimpleNode
    @brief          [ADT] Generic node super class for Tree/Graph structures requiring only a Key
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_GENERICNODE_H
#define EADLIB_GENERICNODE_H

#include <ostream>

namespace eadlib {
    template<class K> class GenericNode {
      public:
        GenericNode() = default;
        explicit GenericNode( const K &key );
        GenericNode( const GenericNode<K> &node );
        GenericNode( GenericNode<K> &&node ) noexcept;
        virtual ~GenericNode() = default;
        template<typename U, typename S> friend std::ostream &operator <<( std::ostream &output, eadlib::GenericNode<U> node );
        bool operator ==( const GenericNode<K> &rhs ) const;
        bool operator !=( const GenericNode<K> &rhs ) const;
        bool operator <( const GenericNode<K> &rhs ) const;
        bool operator >( const GenericNode<K> &rhs ) const;
        bool operator <=( const GenericNode<K> &rhs ) const;
        bool operator >=( const GenericNode<K> &rhs ) const;
        const K & key() const;
        //Variable
        K _key;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // GenericNode class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param key Content of node
     */
    template<class K> GenericNode<K>::GenericNode( const K &key ) :
        _key( key )
    {}

    /**
     * Copy-Constructor
     * @param node GenericSimpleNode
     */
    template<class K> GenericNode<K>::GenericNode( const GenericNode<K> &node ) :
        _key( node._key )
    {}

    /**
     * Move-Constructor
     * @param node GenericSimpleNode
     */
    template<class K> GenericNode<K>::GenericNode( GenericNode<K> &&node ) noexcept :
        _key( node._key )
    {}

    /**
     * Output stream operator
     * @param output Output stream
     * @param node Node to output
     * @return Output stream of node
     */
    template<class U> std::ostream &operator <<( std::ostream &output, const eadlib::GenericNode<U> node ) {
        output << "[" << node._key << "]";
        return output;
    }

    /**
     * Equivalence operator (==)
     * @param rhs Node to compare to
     * @return Equivalence state
     */
    template<class K> bool GenericNode<K>::operator ==( const GenericNode<K> &rhs ) const {
        return _key == rhs._key;
    }

    /**
     * Negative equivalence operator (!=)
     * @param rhs Node to compare to
     * @return Negative equivalence state
     */
    template<class K> bool GenericNode<K>::operator !=( const GenericNode<K> &rhs ) const {
        return !( rhs == *this );
    }

    /**
     * Smaller than operator
     * @param rhs Node to compare to
     * @return Smaller state
     */
    template<class K> bool GenericNode<K>::operator <( const GenericNode<K> &rhs ) const {
        return _key < rhs._key;
    }

    /**
     * Bigger than operator
     * @param rhs Node to compare to
     * @return Bigger state
     */
    template<class K> bool GenericNode<K>::operator >( const GenericNode<K> &rhs ) const {
        return rhs < *this;
    }

    /**
     * Smaller or equal operator
     * @param rhs Node to compare to
     * @return Smaller or equal state
     */
    template<class K> bool GenericNode<K>::operator <=( const GenericNode<K> &rhs ) const {
        return !( rhs < *this );
    }

    /**
     * Bigger or equal operator
     * @param rhs Node to compare to
     * @return Bigger or equal state
     */
    template<class K> bool GenericNode<K>::operator >=( const GenericNode<K> &rhs ) const {
        return !( *this < rhs );
    }

    /**
     * Gets the key
     * @tparam K Key type
     * @return Key
     */
    template<class K> const K & GenericNode<K>::key() const {
        return _key;
    }
}

#endif //EADLIB_GENERICNODE_H
