/**
    @class          eadlib::GenericNode
    @brief          [ADT] Generic Key/Value node super class for Tree/Graph structures
    @dependencies   eadlib::GenericNode
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_GENERICNODEPAIR_H
#define EADLIB_GENERICNODEPAIR_H

#include "GenericNode.h"

namespace eadlib {
    template<class K, class V> class GenericNodePair : public GenericNode<K> {
      public:
        GenericNodePair();
        GenericNodePair( const K &key, const V &value );
        GenericNodePair( const GenericNodePair<K,V> &node );
        GenericNodePair( GenericNodePair<K,V> &&node ) noexcept;
        virtual ~GenericNodePair() = default;
        template<typename U, typename S> friend std::ostream & operator <<( std::ostream &output, eadlib::GenericNodePair<U,S> node );
        const V & value() const;
        //Variable
        V _value;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // GenericNodePair class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor (default)
     */
    template<class K, class V> GenericNodePair<K,V>::GenericNodePair() {}

    /**
     * Constructor
     * @param key   Key of node
     * @param value Value of node
     */
    template<class K, class V> GenericNodePair<K,V>::GenericNodePair( const K &key, const V &value ) :
        _value( value )
    {
        this->_key = key;
    }

    /**
     * Copy-Constructor
     * @param node GenericNode
     */
    template<class K, class V> GenericNodePair<K,V>::GenericNodePair( const GenericNodePair<K,V> &node ) :
        _value( node._value )
    {
        this->_key = node._key;
    }

    /**
     * Move-Constructor
     * @param node GenericNode
     */
    template<class K, class V> GenericNodePair<K,V>::GenericNodePair( GenericNodePair<K,V> &&node ) noexcept :
        _value( node._value )
    {
        this->_key = node._key;
    }

    /**
     * Output stream operator
     * @param output Output stream
     * @param node Node to output
     * @return Output stream of node
     */
    template<class U, class S> std::ostream & operator <<( std::ostream &output, const eadlib::GenericNodePair<U,S> node ) {
        output << "[" << node._key << "]=" << node._value;
        return output;
    }

    /**
     * Gets the value
     * @tparam K Key type
     * @tparam V Value type
     * @return Value
     */
    template<class K, class V> const V & GenericNodePair<K,V>::value() const {
        return _value;
    }
}

#endif //EADLIB_GENERICNODEPAIR_H
