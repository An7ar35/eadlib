#ifndef EADLIB_GRAPHEDGEINFO_H
#define EADLIB_GRAPHEDGEINFO_H

#include <ostream>

namespace eadlib {
    template<class K, class R> struct GraphEdgeInfo {
        enum class Direction { NONE, CHILD, PARENT };

        GraphEdgeInfo( const K &from, const K& to, const R &relationship, Direction direction ) :
            _origin( from ),
            _destination( to ),
            _relationship( relationship ),
            _direction( direction )
        {}

        GraphEdgeInfo( const GraphEdgeInfo &edge_info ) :
            _origin( edge_info._origin ),
            _destination( edge_info._destination ),
            _relationship( edge_info._relationship ),
            _direction( edge_info._direction )
        {}

        GraphEdgeInfo( GraphEdgeInfo &&edge_info ) noexcept :
            _origin( edge_info._origin ),
            _destination( edge_info._destination ),
            _relationship( std::move( edge_info._relationship ) ),
            _direction( edge_info._direction )
        {}

        GraphEdgeInfo & operator =( const GraphEdgeInfo &edge_info ) {
            _origin       = edge_info._origin;
            _destination  = edge_info._destination;
            _relationship = edge_info._relationship;
            _direction    = edge_info._direction;
            return *this;
        }

        GraphEdgeInfo & operator =( GraphEdgeInfo &&edge_info ) {
            _origin       = edge_info._origin;
            _destination  = edge_info._destination;
            _relationship = std::move( edge_info._relationship );
            _direction    = edge_info._direction;
            return *this;
        }

        friend std::ostream &operator <<( std::ostream &output, eadlib::GraphEdgeInfo<K,R> const &edge_info ) {
            output << edge_info._origin;
            switch( edge_info._direction ) {
                case GraphEdgeInfo::Direction::CHILD:
                    output << "->";
                    break;
                case GraphEdgeInfo::Direction::PARENT:
                    output << "<-";
                    break;
                case GraphEdgeInfo::Direction::NONE:
                    output << "--";
            }
            output << edge_info._destination
                   << ": "
                   << edge_info._relationship;
            return output;
        }

        K         _origin;
        K         _destination;
        R         _relationship;
        Direction _direction;
    };
}

#endif //EADLIB_GRAPHEDGEINFO_H
