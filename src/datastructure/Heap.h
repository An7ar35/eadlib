/**
    @class          eadlib::Heap
    @brief          [ADT] %Heap

    Uses a %LinearList

    @dependencies   eadlib::LinearList, eadlib::LinkedList, eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2016
    @license        GNUv2 Public License
**/
#ifndef EADLIB_HEAP_H
#define EADLIB_HEAP_H

#include "LinearList.h"
#include "LinkedList.h"
#include "../logger/Logger.h"

namespace eadlib {
    template<class T> class Heap {
      public:
        Heap();
        Heap( std::initializer_list<T> list );
        explicit Heap( const LinearList<T> &list );
        explicit Heap( LinkedList<T> &list );
        virtual ~Heap() = default;
        template<class U> friend std::ostream & operator <<( std::ostream &out, const Heap<U> &heap );
        T top() const;
        void clear();
        size_t size() const;
        bool isEmpty() const;

      protected:
        LinearList<T> m_content;
        void buildMaxHeap();
        void maxHeapify( const size_t &i, const size_t &heap_size );
        //Indexing
        size_t parent( const size_t &node_index ) const;
        size_t leftChild( const size_t &node_index ) const;
        size_t rightChild( const size_t &node_index ) const;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Heap public class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     */
    template<class T> Heap<T>::Heap() {
        m_content = eadlib::LinearList<T>();
    }

    /**
     * Constructor with Initializer list
     * @param list Initializer list
     */
    template<class T> Heap<T>::Heap( const std::initializer_list<T> list ) {
        m_content = eadlib::LinearList<T>( list.size() );
        for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
            m_content.addToEnd( *it );
        }
    }

    /**
     * Constructor with LinearList<T>
     * @param list LinearList<T>
     */
    template<class T> Heap<T>::Heap( const LinearList<T> &list ) {
        for( int i = 0; i < list.size(); i++ ) {
            m_content.addToEnd( list[ i ] );
        }
    }

    /**
     * Constructor with LinkedList<T>
     * @param list LinkedList<T>
     */
    template<class T> Heap<T>::Heap( LinkedList<T> &list ) {
        for( int i = 0; i < list.size(); i++ ) {
            m_content.addToEnd( list.removeHead() );
        }
    }

    /**
     * Output stream operator
     * @param out  Output stream
     * @param heap Heap instance
     * @return Output stream
     */
    template<class T> std::ostream & operator <<( std::ostream &out, const eadlib::Heap<T> &heap ) {
        for( int i = 0; i < heap.m_content.size(); i++ ) {
            out << heap.m_content[ i ] << " ";
        }
        return out;
    }

    /**
     * Gets the top item in the heap
     * @return Copy of the top item
     * @exception std::out_of_range when Heap is empty
     */
    template<class T> T Heap<T>::top() const {
        if( this->m_content.size() < 1 ) {
            LOG_ERROR( "[eadlib::Heap<T>::top()] Heap is empty." );
            throw std::out_of_range( "[eadlib::Heap<T>::top()] Heap is empty." );
        } else {
            return this->m_content[ 0 ];
        }
    }

    /**
     * Clears the heap
     */
    template<class T> void Heap<T>::clear() {
        m_content.clear();
    }

    /**
     * Gets the current size of the heap
     * @return Number of elements in the heap
     */
    template<class T>
        size_t Heap<T>::size() const {
        return m_content.size();
    }

    /**
     * Checks if the heap is empty
     * @return Emtpy state
     */
    template<class T> bool Heap<T>::isEmpty() const {
        return m_content.empty();
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Heap protected class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Build the max heap
     */
    template<class T> void Heap<T>::buildMaxHeap() {
        for( auto i = size() - 1; i >= 0; i-- ) { //for( int i = size() / 2; i > 0; i-- ) {
            std::cout << "Looking at node " << i << std::endl;
            maxHeapify( i, this->size() );
            std::cout << "After maxHeapify: " << *this << std::endl;
        }
    }

    /**
     * Max-Heapify
     * @param i Index of node
     * @param heap_size Size of the Heap
     */
    template<class T> void Heap<T>::maxHeapify( const size_t &i, const size_t &heap_size ) {
        size_t largest { 0 };
        size_t l = leftChild( i );
        size_t r = rightChild( i );
        if( l < heap_size && m_content[ l ] > m_content[ i ] ) {
            largest = l;
        } else {
            largest = i;
        }
        if( r < heap_size && m_content[ r ] > m_content[ largest ] ) {
            largest = r;
        }
        if( largest != i ) {
            std::swap( m_content[ i ], m_content[ largest ] );
            maxHeapify( largest, heap_size );
        }
    }

    /**
     * Gets the index of a node's m_parent
     * @return Index of m_parent
     */
    template<class T> size_t Heap<T>::parent( const size_t &node_index ) const {
        return node_index / 2;
    }

    /**
     * Gets the index of a node's left
     * @return Index of left
     */
    template<class T> size_t Heap<T>::leftChild( const size_t &node_index ) const {
        return node_index * 2 + 1;
    }

    /**
     * Gets the index of a node's right
     * @return Index of right
     */
    template<class T> size_t Heap<T>::rightChild( const size_t &node_index ) const {
        return node_index * 2 + 2;
    }
}

#endif //EADLIB_HEAP_H
