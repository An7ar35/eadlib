/**
    @class          eadlib::LinearList
    @brief          [ADT] Linear List (Formula based) dynamic vector

    Implemented as a circle with the elements set as a window on it with best possible time given placement
    e.g.: inserting an element at the front of the list will not move all other items instead opting for
    shifting the virtual index for the beginning of the list down one.
    Like a vector, the list dynamically grows/shrinks based on a set of growth/shrink factors (Default=2x).

    @dependencies   eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_LINEARLIST_H
#define EADLIB_LINEARLIST_H

#include "../logger/Logger.h"
#include <memory>
#include <list>

//TODO add const_iterator functionality

namespace eadlib {
    template<class T> class LinearList {
      public:
        explicit LinearList( const size_t &capacity = 2 );
        LinearList( std::initializer_list<T> list );
        LinearList( const LinearList &list );
        LinearList( LinearList &&list ) noexcept;
        ~LinearList();
        //Operators
        template<typename U> friend std::ostream & operator <<( std::ostream &out, const LinearList<U> &list );
        T operator []( size_t i ) const;                            //O(1)
        T & operator []( size_t i );                                //O(1)
        LinearList<T> & operator=( const LinearList<T> &list );
        //Element adding/removing function
        void addToEnd( const T &item );                             //O(n)
        void addToFront( const T &item );                           //O(n)
        void insert( const T &item, size_t index );                 //O(n/2)
        void remove( unsigned int index );                          //O(n/2)
        void clear();
        void reset();
        //Properties function
        size_t size() const;                                        //O(1)
        void reserve( size_t new_capacity );                        //O(n)
        size_t capacity() const;                                    //O(1)
        bool empty() const;                                         //O(1)
        //to string
        std::string str();                                          //O(n)
        std::string raw();                                          //O(n)
      private:
        size_t getRealIndex( size_t v_position, size_t capacity, const long &v_first ) const;
        bool shrink( unsigned int factor );
        bool grow( unsigned int factor );
        bool adaptArray( std::unique_ptr<T[]> &array,
                         size_t new_capacity,
                         size_t capacity,
                         size_t size,
                         size_t &r_first,
                         size_t &r_last ) const;
        //members
        std::unique_ptr<T[]> _array;
        unsigned int _growthFactor;
        unsigned int _shrinkFactor;
        size_t _capacity;
        size_t _size;
        size_t _first;
        size_t _last;

      public:
        /**
         * @class iterator
         * @brief LinearList<T> iterator
         */
        class iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef T                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef T*                        pointer;
            typedef T&                        reference;
            friend class LinearList<T>;
            //Constructors
            iterator();
            iterator( const iterator &it );
            //Operators
            iterator & operator =( const iterator &rhs );
            iterator & operator ++();
            const iterator operator ++( int );
            reference operator *() const;
            pointer operator ->() const;
            bool operator ==( const iterator &rhs ) const;
            bool operator !=( const iterator &rhs ) const;
          private:
            T * _pos;
            std::list<T*> _list;
        };

        iterator begin();
        iterator end();
    };

    //-----------------------------------------------------------------------------------------------------------------
    // LinearList class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param capacity Starting capacity of the LinearList (default=2)
     */
    template<class T> LinearList<T>::LinearList( const size_t &capacity ) :
        _array( new T[ capacity ] ),
        _size( 0 ),
        _capacity( capacity ),
        _first( 0 ),
        _last( 0 ),
        _growthFactor( 2 ),
        _shrinkFactor( 2 )
    {};

    /**
     * Constructor
     * @param list Initializer_list
     */
    template<class T> LinearList<T>::LinearList( const std::initializer_list<T> list ) :
        _array( new T[ list.size() ] ),
        _size( 0 ),
        _capacity( list.size() ),
        _first( 0 ),
        _last( 0 ),
        _growthFactor( 2 ),
        _shrinkFactor( 2 )
    {
        for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
            addToEnd( *it );
        }
    }

    /**
     * Copy-Constructor
     * @param list LinearList to copy
     */
    template<class T> LinearList<T>::LinearList( const eadlib::LinearList<T> &list ) :
        _array( new T[ list.capacity() ] ),
        _size( list.size() ),
        _capacity( list.capacity() ),
        _first( list._first ),
        _last( list._last ),
        _growthFactor( list._growthFactor ),
        _shrinkFactor( list._shrinkFactor )
    {
        for( int i = 0; i < _capacity; i++ ) {
            _array[ i ] = list._array[ i ];
        }
    }

    /**
     * Move-Constructor
     * @param list LinearList to move
     */
    template<class T> LinearList<T>::LinearList( eadlib::LinearList<T> &&list ) noexcept :
        _array( new T[ list.capacity() ] ),
        _size( list.size() ),
        _capacity( list.capacity() ),
        _first( 0 ),
        _last( 0 ),
        _growthFactor( 2 ),
        _shrinkFactor( 2 )
    {
        for( int i = 0; i < list.size(); i++ ) {
            addToEnd( list[ i ] );
        }
        list.clear();
        list._array.reset();
    }

    /**
     * Destructor
     */
    template<class T> LinearList<T>::~LinearList() {
        _array.reset();
    }

    /**
     * Output stream operator overloading
     * @param out Output stream
     * @param list LinearList
     * @return Output stream
     */
    template<class T> std::ostream & operator<<( std::ostream &out, const LinearList<T> &list ) {
        for( size_t i = 0; i < list.size(); i++ ) {
            out << list[ i ] << " ";
        }
        return out;
    }

    /**
     * Operator []
     * @param i Index
     * @return Element at index
     * @exception std::out_of_range when index 'i' isn't within size of the list
     */
    template<class T> T LinearList<T>::operator []( size_t i ) const {
        if( i >= 0, i < _size ) {
            return _array[ getRealIndex( i, capacity(), _first ) ];
        } else {
            LOG_ERROR( "[eadlib::LinearList<T>::operator[]( ", i, " ) const] Index is out of range." );
            throw std::out_of_range( "[eadlib::LinearList<T>::operator[]( int ) const] Index is out of range." );
        }
    }

    /**
     * Operator []
     * @param i Index
     * @return Reference to element at index
     * @exception std::out_of_range when index 'i' isn't within size of the list
     */
    template<class T> T & LinearList<T>::operator []( size_t i ) {
        if( i >= 0, i < _size ) {
            return _array[ getRealIndex( i, capacity(), _first ) ];
        } else {
            LOG_ERROR( "[eadlib::LinearList<T>::operator[]( ", i, " )] Index is out of range." );
            throw std::out_of_range( "[eadlib::LinearList<T>::operator[]( int )] Index is out of range." );
        }
    }

    /**
     * Operator '=' (copy-assignment)
     * @param list List to copy over
     * @return This list
     */
    template<class T> LinearList<T> & LinearList<T>::operator =( const eadlib::LinearList<T> &rhs ) {
        if( this != &rhs ) {
            _growthFactor = rhs._growthFactor;
            _shrinkFactor = rhs._shrinkFactor;
            _capacity = rhs._capacity;
            _size = rhs._size;
            _first = rhs._first;
            _last = rhs._last;
            _array.reset( new T[_capacity] );
            for( int i = 0; i < _capacity; i++ ) {
                _array[ i ] = rhs._array[ i ];
            }
        }
        return *this;
    }

    /**
     * Adds an element at the end of the list
     * @param item Element to add
     */
    template<class T> void LinearList<T>::addToEnd( const T &item ) {
        if( _size >= _capacity ) {
            grow( _growthFactor );
        }
        if( _size < 1 ) {
            _first = 0;
            _last = 0;
            _array[ _last ] = item;
        } else {
            size_t after_last = getRealIndex( size(), capacity(), _first );
            _array[ after_last ] = item;
            _last = after_last;
        }
        _size++;
    }

    /**
     * Adds an element at the front of the list
     * @param item Element to add
     */
    template<class T> void LinearList<T>::addToFront( const T &item ) {
        if( _size >= _capacity ) {
            grow( _growthFactor );
        }
        if( _size < 1 ) {
            _first = 0;
            _last = 0;
            _array[ _first ] = item;
        } else {
            if( _first == 0 ) {
                _array[ getRealIndex( capacity() - 1, capacity(), _first ) ] = item;
                _first = _capacity - 1;

            } else {
                _first--;
                _array[ _first ] = item;
            }
        }
        _size++;
    }

    /**
     * Inserts and element after a specified index
     * @param item Element to add
     * @param index Index after which to insert
     * @exception std::out_of_range When index is not within the bounds of the current list size
     */
    template<class T> void LinearList<T>::insert( const T &item, const size_t index ) {
        if( index < 0 || index >= size() ) {
            LOG_ERROR( "[eadlib::LinearList<T>::insert( ", item, ", ", index, " )] Index is out of range of the list." );
            throw std::out_of_range( "[eadlib::LinearList<T>:insert( const T &, const unsigned int )] Index is out of range of the list." );
        }
        if( _size >= _capacity ) {
            grow( _growthFactor );
        }
        if( index < ( size() - 1 ) / 2 ) { //move left side of index
            if( getRealIndex( 0, capacity(), _first ) == 0 ) {
                _first = capacity() - 1;
            } else {
                _first--;
            }
            for( unsigned int i = 0; i <= index; i++ ) {
                _array[ getRealIndex( i, capacity(), _first ) ] = _array[ getRealIndex( i + 1, capacity(), _first ) ];
            }
            _array[ getRealIndex( index + 1, capacity(), _first ) ] = item;
        } else { //move right side of index
            if( getRealIndex( size() - 1, capacity(), _first ) == capacity() - 1 ) {
                _last = 0;
            } else {
                _last++;
            }
            for( size_t i = size() - 1; i > index; i-- ) {
                _array[ getRealIndex( i + 1, capacity(), _first ) ] = _array[ getRealIndex( i, capacity(), _first ) ];
            }
            _array[ getRealIndex( index + 1, capacity(), _first ) ] = item;
        }
        _size++;
    }

    /**
     * Removes an element at specified index
     * @param index Index at which to remove the element
     */
    template<class T> void LinearList<T>::remove( const unsigned int index ) {
        if( index < 0 || index >= size() ) {
            LOG_ERROR( "[eadlib::LinearList<T>::remove( ", index, " )] Index is out of range of the list." );
            throw std::out_of_range( "[eadlib::LinearList<T>:remove( const unsigned int )] Index is out of range of the list." );
        }
        size_t real_index = getRealIndex( index, capacity(), _first );
        if( size() == 1 && index == 0 ) { //Last element present in array
            _first = 0;
            _last  = 0;
            _size  = 0;
        } else if( real_index == _first ) { //Element @beginning
            _first = getRealIndex( index + 1, capacity(), _first );
            _size--;
        } else if( real_index == _last ) { //Element @end
            _last = getRealIndex( index - 1, capacity(), _first );
            _size--;
        } else {
            if( index < ( size() - 1 ) / 2 ) { //move left side of index
                for( unsigned int i = index; i > 0; i-- ) {
                    _array[ getRealIndex( i, capacity(), _first ) ] = _array[ getRealIndex( i - 1, capacity(), _first ) ];
                }
                _first = getRealIndex( 1, capacity(), _first );
            } else { //move right side of index
                for( unsigned int i = index; i < size(); i++ ) {
                    _array[ getRealIndex( i, capacity(), _first ) ] = _array[ getRealIndex( i + 1, capacity(), _first ) ];
                }
                _last = getRealIndex( size() - 2, capacity(), _first );
            }
            _size--;
        }
        if( _size <= capacity() / _shrinkFactor ) {
            shrink( _shrinkFactor );
        }
    }

    /**
     * Clears the list of all content
     */
    template<class T> void LinearList<T>::clear() {
        _size  = 0;
        _first = 0;
        _last  = 0;
    }

    /**
     * Resets the list back to its inception properties
     */
    template<class T> void LinearList<T>::reset() {
        //TODO check and TEST!
        clear();
        _array.reset( std::make_unique<T[ 2 ]>() );
        _capacity = 2;
    }

    /**
     * Gets the number of elements in the list
     * @return Number of elements in the list
     */
    template<class T> size_t LinearList<T>::size() const {
        return _size;
    }

    /**
     * Increases the capacity of the list
     * @param new_capacity New capacity (if <= current capacity, nothing is done)
     */
    template<class T> void LinearList<T>::reserve( size_t new_capacity ) {
        if( adaptArray( _array, new_capacity, capacity(), size(), _first, _last ) ) {
            _capacity = new_capacity;
            LOG_DEBUG( "[eadlib::LinearList::<T>reserve( ", new_capacity, " )] Capacity changed successfully." );
        } else {
            LOG_ERROR( "[eadlib::LinearList<T>::reserve( ", new_capacity, " )] Failed to change the capacity." );
        }
    }

    /**
     * Gets the current capacity of the list
     * @return Current capacity
     */
    template<class T> size_t LinearList<T>::capacity() const {
        return _capacity;
    }

    /**
     * Checks if the list is empty
     * @return Empty state
     */
    template<class T> bool LinearList<T>::empty() const {
        return _size == 0;
    }

    /**
     * Gets a virtual representation of the linear list as a string
     * @return String made up of stream operators of each elements separated with '|'
     */
    template<class T> std::string LinearList<T>::str() {
        std::stringstream ss;
        if( size() > 0 ) {
            ss << "| ";
            for( size_t i = 0; i < size(); i++ ) {
                ss << _array[ getRealIndex( i, capacity(), _first ) ] << " | ";
            }
        }
        return ss.str();
    }

    /**
     * [DEBUG] Get the physical linear list (real array) as a string
     * @return String made up of stream operators of each elements separated with '|'
     */
    template<class T> std::string LinearList<T>::raw() {
        std::stringstream ss;
        ss << "| ";
        for( int i = 0; i < capacity(); i++ ) {
            ss << _array[ i ] << " | ";
        }
        return ss.str();
    }

    /**
     * Translates a position into the real index inside the circular array
     * @param v_position Virtual position in the array
     * @param capacity   Current capacity of the array
     * @param v_first    Virtual index of the first element in array
     * @return Real index position in the array
     */
    template<class T> size_t LinearList<T>::getRealIndex( const size_t v_position,
                                                          const size_t capacity,
                                                          const long &v_first ) const {
        size_t n_to_end = ( capacity - v_first );
        size_t real_index { 0 };
        if( v_position < n_to_end ) {
            real_index = v_first + v_position;
        } else {
            real_index = v_position - n_to_end;
        }
        return real_index;
    }

    /**
     * Shrinks the list capacity
     * @param factor Reduction factor
     * @return Success
     */
    template<class T> bool LinearList<T>::shrink( unsigned int factor ) {
        size_t new_capacity { capacity() / factor };
        if( size() <= new_capacity ) {
            LOG_DEBUG( "[eadlib::LinearList::shrink()] Shrinking capacity to ", new_capacity, "." );
            if( new_capacity < 2 ) new_capacity = 2;
            if( adaptArray( _array, new_capacity, capacity(), size(), _first, _last ) ) {
                _capacity = new_capacity;
                return true;
            } else {
                LOG_ERROR( "[eadlib::LinearList<T>::grow( ", factor, " )] Failed to adapt the array." );
            }
        } else {
            LOG_ERROR( "[eadlib::LinearList<T>::shrink( ", factor, " )] Size (", size(), ") is larger than new capacity (", new_capacity, "). ->Nothing done." );
        }
        return false;
    }

    /**
     * Grows the list capacity
     * @param factor Growth factor
     * @return Success
     */
    template<class T> bool LinearList<T>::grow( unsigned int factor ) {
        size_t new_capacity { capacity() * factor };
        if( size() <= new_capacity ) {
            LOG_DEBUG( "[eadlib::LinearList::grow()] Growing capacity to ", new_capacity, "." );
            if( adaptArray( _array, new_capacity, capacity(), size(), _first, _last ) ) {
                _capacity = new_capacity;
                return true;
            } else {
                LOG_ERROR( "[eadlib::LinearList<T>::grow( ", factor, " )] Failed to adapt the array." );
            }
        } else {
            LOG_ERROR( "[eadlib::LinearList<T>::grow( ", factor, " )] Size (", size(), ") is larger than new capacity (", new_capacity, "). ->Nothing done." );
        }
        return false;
    }

    /**
     * Adapts the array to the new specifications
     * @param array Dynamic array to work with
     * @param new_capacity New capacity of the array
     * @param capacity Old capacity of the array
     * @param size Current number of elements stored in array
     * @param r_first Real index of the first element in the array
     * @param r_last Real index of the last element in the array
     * @return Success of operation
     */
    template<class T> bool LinearList<T>::adaptArray( std::unique_ptr<T[]> &array,
                                                      const size_t new_capacity,
                                                      const size_t capacity,
                                                      const size_t size,
                                                      size_t &r_first,
                                                      size_t &r_last ) const {
        if( size > new_capacity ) {
            LOG_ERROR( "[eadlib::LinearList<T>::adaptArray( .., ", new_capacity, ", ", capacity, ", ", size, ", ", r_first, ", ", r_last, " )] Size (", size, ") is larger than capacity (", capacity, ")." );
            return false;
        }
        std::unique_ptr<T[]> new_array( new T[ new_capacity ] );
        for( size_t i = 0; i < size; i++ ) {
            new_array[ i ] = array[ getRealIndex( i, capacity, r_first ) ];
        }
        array.reset( new_array.release() );
        r_first = 0;
        r_last = size - 1;
        return true;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LinearList Iterator class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    template<class T> LinearList<T>::iterator::iterator() :
        _pos( 0 )
    {}

    template<class T> LinearList<T>::iterator::iterator( const LinearList::iterator &it ) :
        _pos( it._pos ),
        _list( it._list )
    {}

    template<class T> typename LinearList<T>::iterator &LinearList<T>::iterator::operator =( const LinearList::iterator &rhs ) {
        _pos  = rhs._pos;
        _list = rhs._list;
        return *this;
    }

    template<class T> typename LinearList<T>::iterator &LinearList<T>::iterator::operator ++() {
        if( !_list.empty() ) {
            _pos = _list.front();
            _list.pop_front();
        } else {
            _pos = 0;
        }
        return *this;
    }

    template<class T> const typename LinearList<T>::iterator LinearList<T>::iterator::operator ++( int ) {
        iterator tmp( *this );
        operator++();
        return tmp;
    }

    template<class T> typename LinearList<T>::iterator::reference LinearList<T>::iterator::operator *() const {
        return *_pos;
    }

    template<class T> typename LinearList<T>::iterator::pointer LinearList<T>::iterator::operator ->() const {
        return _pos;
    }

    template<class T> bool LinearList<T>::iterator::operator ==( const LinearList<T>::iterator &rhs ) const {
        return _pos == rhs._pos;
    }

    template<class T> bool LinearList<T>::iterator::operator !=( const LinearList<T>::iterator &rhs ) const {
        return _pos != rhs._pos;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LinearList Iterator methods implementations
    //-----------------------------------------------------------------------------------------------------------------
    template<class T> typename LinearList<T>::iterator LinearList<T>::begin() {
        iterator it;
        if( !empty() ) {
            for( int i = 0; i < size(); i++ ) {
                it._list.emplace_back( &operator []( i ) );
            }
            it._pos = it._list.front();
            it._list.pop_front();
        }
        return it;
    }

    template<class T> typename LinearList<T>::iterator LinearList<T>::end() {
        return iterator();
    }
}

#endif //EADLIB_LINEARLIST_H