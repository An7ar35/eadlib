/**
    @class          eadlib::LinkedList
    @brief          [ADT] %Double Linked List

    Implemented using std::unique_ptr and raw pointers (half/half technique)

    @dependencies   eadlib::LinkedListNode, eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/

/**
    @class          eadlib::LinkedList::iterator
    @brief          %LinkedList %iterator
 */
#ifndef EADLIB_LINKEDLIST_H
#define EADLIB_LINKEDLIST_H

#include <exception>
#include "../logger/Logger.h"
#include "LinkedListNode.h"
#include "LinearList.h"

namespace eadlib {
    template<typename T> class LinkedList {
      public:
        LinkedList() = default;
        LinkedList( std::initializer_list<T> list );
        LinkedList( const eadlib::LinkedList<T> &list );
        LinkedList( eadlib::LinkedList<T> &&list ) noexcept;
        ~LinkedList() = default;
        //Operators
        T operator()( unsigned int index ) const;
        template<typename U> friend std::ostream & operator <<( std::ostream &out, const LinkedList<U> &list );
        //List Manipulations
        void addToHead( const T &element );
        void addToTail( const T &element );
        LinkedList<T> append( const LinkedList<T> &list ) const;
        LinkedList<T> append( std::initializer_list<T> list ) const;
        void append_Move( LinkedList<T> &list );
        void append_Copy( const LinkedList<T> &list );
        void appendToHead( std::initializer_list<T> list );
        void appendToTail( std::initializer_list<T> list );
        void insert( const T &element, int index );
        void insert( const std::initializer_list<T> &list, int index );
        void insert_Move( LinkedList<T> &list, int index );
        void insert_Copy( const LinkedList<T> &list, int index );
        T removeHead();
        T removeTail();
        bool popHead();
        bool popTail();
        void clear();
        //Access
        T head() const;
        T tail() const;
        //List Properties
        unsigned int size() const;
        bool empty() const;
        //List Print
        std::string str() const;
        std::string forwardPrint() const;
        std::string reversePrint() const;
      private:
        unsigned int _numberOfElements { 0 };
        std::unique_ptr<LinkedListNode<T>> _head;
        LinkedListNode<T> *_tail { nullptr };

      public:
        /**
         * @class iterator
         * @brief LinkedList<T> iterator
         */
        class iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef T                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef T*                        pointer;
            typedef T&                        reference;
            friend class LinkedList<T>;
            //Constructors
            iterator();
            iterator( const iterator &it );
            //Operators
            iterator & operator =( const iterator &rhs );
            iterator & operator ++();
            const iterator operator ++( int );
            reference operator *() const;
            pointer operator ->() const;
            bool operator ==( const iterator &rhs ) const;
            bool operator !=( const iterator &rhs ) const;
          private:
            LinkedListNode<T> *_pos;
        };

        /**
         * @class const_iterator
         * @brief LinkedList<T> const iterator
         */
        class const_iterator {
          public:
            typedef std::forward_iterator_tag iterator_category;
            typedef T                         value_type;
            typedef ptrdiff_t                 difference_type;
            typedef const T*                  pointer;
            typedef const T&                  reference;
            friend class LinkedList<T>;
            //Constructors
            const_iterator();
            const_iterator( const const_iterator &it );
            //Operators
            const_iterator & operator =( const const_iterator &rhs );
            const_iterator & operator ++();
            const const_iterator operator ++( int );
            reference operator *() const;
            pointer operator ->() const;
            bool operator ==( const const_iterator &rhs ) const;
            bool operator !=( const const_iterator &rhs ) const;
          private:
            LinkedListNode<T> *_pos;
        };

        iterator begin();
        iterator end();
        const_iterator cbegin();
        const_iterator cend();
    };

    //-----------------------------------------------------------------------------------------------------------------
    // LinkedList class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor with Initializer List
     * @param list Initializer list
     */
    template<class T> LinkedList<T>::LinkedList( const std::initializer_list<T> list ) {
        for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
            addToTail( *it );
        }
    }

    /**
     * Copy-Constructor
     * @param list LinkedList<T> to copy
     */
    template<class T> LinkedList<T>::LinkedList( const eadlib::LinkedList<T> &list ) {
        append_Copy( list );
    }

    /**
     * Move-Constructor
     * @param list LinkedList<T> to move
     */
    template<class T> LinkedList<T>::LinkedList( eadlib::LinkedList<T> &&list ) noexcept {
        _head = list._head.release();
        _tail = list._tail;
        _numberOfElements = list.size();
    }

    /**
     * Access operator ()
     * @param index Index from which to get content from
     * @return Content of node at index
     * @exception std::out_of_range When index is not within the bounds of the list
     */
    template<class T> T LinkedList<T>::operator()( const unsigned int index ) const {
        if(( _numberOfElements > 0 && index >= _numberOfElements ) || index < 0 || _numberOfElements == 0 ) {
            LOG_ERROR( "[eadlib::LinkedList<T>::operator()( ", index, " )] Index given is out of bounds of the list." );
            throw std::out_of_range( "[eadlib::LinkedList<T>::operator()( unsigned int )] Index given is out of bounds of the list." );
        }
        LinkedListNode<T> *ptr = _head.get();
        for( int i = 0; i < index; i++ ) {
            ptr = ptr->_next.get();
        }
        return ptr->_key;
    }

    /**
     * Output stream operator
     * @param out Output stream
     * @param list LinearList
     * @return Output stream
     */
    template<class U> std::ostream & operator <<( std::ostream &out, const LinkedList<U> &list ) {
        LinkedListNode<U> *ptr = list._head.get();
        while( ptr ) {
            out << ptr->_key << " ";
            ptr = ptr->_next.get();
        }
        return out;
    }

    /**
     * Creates a list concatenating this list and another
     * @param list LinkedList<T> to append to this
     * @return LinkedList<T> of both lists combined
     */
    template<class T> LinkedList<T> LinkedList<T>::append( const eadlib::LinkedList<T> &list ) const {
        LinkedList<T> new_list( *this );
        new_list.append_Copy( list );
        return new_list;
    }

    /**
     * Creates a list concatenating this list and another
     * @param list Initializer list to append to this
     * @return LinkedList<T> of both lists combined
     */
    template<class T> LinkedList<T> LinkedList<T>::append( const std::initializer_list<T> list ) const {
        LinkedList<T> new_list( *this );
        new_list.appendToTail( list );
        return new_list;
    }

    /**
     * Adds to the beginning of the list
     * @param element Element to add
     */
    template<class T> void LinkedList<T>::addToHead( const T &element ) {
        if( _head ) {
            _head = std::make_unique<LinkedListNode<T>>( element, _head, nullptr );
            _head->_next->_previous = _head.get();
        } else {
            _head = std::make_unique<LinkedListNode<T>>( element, _head, nullptr );
            _tail = _head.get();
        }
        _numberOfElements++;
    }

    /**
     * Adds to the end of the list
     * @param element Element to add
     */
    template<class T> void LinkedList<T>::addToTail( const T &element ) {
        if( _tail ) {
            _tail->_next = std::make_unique<LinkedListNode<T>>( element, _tail );
            _tail = _tail->_next.get();
            _numberOfElements++;
        } else { //list is empty
            addToHead( element );
        }
    }

    /*
     * Appends another LinkedList to the end
     * @param list LinkedList<T> to move & append
     */
    template<class T> void LinkedList<T>::append_Move( eadlib::LinkedList<T> &list ) {
        if( !list.empty() ) {
            _tail->_next.reset( list._head.release());
            _tail->_previous = list._tail;
            list._tail = nullptr;
            _numberOfElements += list.size();
        }
    }

    /**
     * Appends another LinkedList to the end
     * @param list LinkedList<T> to copy & append
     */
    template<class T> void LinkedList<T>::append_Copy( const eadlib::LinkedList<T> &list ) {
        if( !list.empty() ) {
            LinkedListNode<T> *ptr = list._head.get();
            for( int i = 0; i < list.size(); i++ ) {
                addToTail( ptr->key() );
                ptr = ptr->_next.get();
            }
        }
    }

    /**
     * Appends Initializer list to the begining in order
     * @param list Initializer list
     */
    template<class T> void LinkedList<T>::appendToHead( std::initializer_list<T> list ) {
        LinkedList<T> temp;
        for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
            temp.addToTail( *it );
        }
        append_Move( temp );
    }

    /**
     * Appends Initializer list to the end in order
     * @param list Initializer list
     */
    template<class T> void LinkedList<T>::appendToTail( const std::initializer_list<T> list ) {
        for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
            addToTail( *it );
        }
    }

    /**
     * Inserts in the list
     * @param element Element to add
     * @param index Position after which to insert
     * @exception std::out_of_range when index is not within the range of the list
     */
    template<class T> void LinkedList<T>::insert( const T &element, const int index ) {
        if( _numberOfElements < 1 && index == 0 ) {
            addToHead( element );
        } else if( index >= _numberOfElements || index < 0 ) {
            LOG_ERROR( "[eadlib::LinkedList<T>::insert( \'", element, "\', ", index, " )] Index given is out of bounds of the list." );
            throw std::out_of_range( "[eadlib::LinkedList<T>::insert( T &, int )] Index given is out of bounds of the list." );
        } else if( index == _numberOfElements - 1 ) {
            addToTail( element );
        } else {
            LinkedListNode<T> *ptr = _head.get();
            for( int i = 0; i < index; i++ ) {
                ptr = ptr->_next.get();
            }
            ptr->_next.reset( new LinkedListNode<T>( element, ptr->_next, ptr ) );
            ptr->_next->_next->_previous = ptr->_next.get();
            _numberOfElements++;
        }
    }

    /**
     * Inserts an Initializer list
     * @param list Initializer list
     * @param index Index from which to insert
     * @exception std::out_of_range when index is not within the bounds of the list
     */
    template<class T> void LinkedList<T>::insert( const std::initializer_list<T> &list, const int index ) {
        if( index < 0 || index >= _numberOfElements ) {
            LOG_ERROR( "[LinkedList<T>::insert( const std::initializer_list<T> &, ", index, " )] Index given is out of bounds of the list." );
            throw std::out_of_range( "[LinkedList<T>::insert( const std::initializer_list<T> &, int )] Index given is out of bounds of the list." );
        }
        if( index == _numberOfElements - 1 ) { //appending at the end
            for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
                addToTail( *it );
            }
        } else {
            LinkedList<T> temp;
            for( typename std::initializer_list<T>::iterator it = list.begin(); it != list.end(); ++it ) {
                temp.addToTail( *it );
            }
            try {
                insert_Move( temp, index );
            } catch( std::out_of_range & ) {
                throw;
            }
        }
    }

    /**
     * Inserts (move) a list into the list
     * @param list LinkedList<T>
     * @param index Index from which to insert
     * @exception std::out_of_range when index is not within the bounds of the list
     */
    template<class T> void LinkedList<T>::insert_Move( eadlib::LinkedList<T> &list, const int index ) {
        if( !list.empty() ) {
            if( index < 0 || index >= _numberOfElements ) {
                LOG_ERROR( "[LinkedList<T>::insert_Move( eadlib::LinkedList<T> &, ", index, " )] Index given is out of bounds of the list." );
                throw std::out_of_range( "[LinkedList<T>::insert_Move( eadlib::LinkedList<T> &, int )] Index given is out of bounds of the list." );
            }
            LinkedListNode<T> *ptr = _head.get();
            for( int i = 0; i < index; i++ ) {
                ptr = ptr->_next.get();
            }
            list._tail->_next.reset( ptr->_next.release() );
            list._head->_previous = ptr;
            ptr->_next.reset( list._head.release() );
            for( int i = 0; i < list.size(); i++ ) {
                ptr = ptr->_next.get();
            }
            if( ptr->_next ) {
                ptr->_next->_previous = list._tail;
            } else {
                _tail = list._tail;
            }
            _numberOfElements += list.size();
            list._numberOfElements = 0;
        }
    }

    /**
     * Inserts (copy) a list into the list
     * @param list LinkedList<T>
     * @param index Index from which to insert
     * @exception std::out_of_range when index is not within the bounds of the list
     */
    template<class T> void LinkedList<T>::insert_Copy( const eadlib::LinkedList<T> &list, const int index ) {
        if( !list.empty() ) {
            if( index < 0 || index >= _numberOfElements ) {
                LOG_ERROR( "[LinkedList<T>::insert_Copy( const eadlib::LinkedList<T> &, ", index, " )] Index given is out of bounds of the list." );
                throw std::out_of_range( "[LinkedList<T>::insert_Copy( const eadlib::LinkedList<T> &, int )] Index given is out of bounds of the list." );
            }
            LinkedList<T> temp;
            LinkedListNode<T> *ptr = list._head.get();
            while( ptr ) {
                temp.addToTail( ptr->key() );
                ptr = ptr->_next.get();
            }
            try {
                insert_Move( temp, index );
            } catch( std::out_of_range & ) {
                throw;
            }
        }
    }

    /**
     * Removes from the beginning of the list
     * @return Element removed
     * @exception std::out_of_range when no items are in the list
     */
    template<class T> T LinkedList<T>::removeHead() {
        if( !_head ) {
            LOG_ERROR( "[eadlib::linkedList<T>::removeFromHead()] Trying to remove from an empty list." );
            throw std::out_of_range( "[eadlib::linkedList<T>::removeFromHead()] List is empty." );
        }
        T element = _head->key();
        popHead();
        return element;
    }

    /**
     * Removes from the end of the list
     * @return Element removed
     * @exception std::out_of_range when no items are in the list
     */
    template<class T> T LinkedList<T>::removeTail() {
        if( !_head ) {
            LOG_ERROR( "[eadlib::linkedList<T>::removeFromTail()] Trying to remove from an empty list." );
            throw std::out_of_range( "[eadlib::linkedList<T>::removeFromTail()] List is empty." );
        }
        T element = _tail->key();
        popTail();
        return element;
    }

    /**
     * Pops the head of the list
     * @return Success of poping (i.e.: if there is nothing left to pop => false)
     */
    template<class T> bool LinkedList<T>::popHead() {
        if( !empty() ) {
            if( _head->_next ) {
                _head.reset( _head->_next.release());
                _head->_previous = nullptr;
            } else {
                _head.reset( nullptr );
                _tail = nullptr;
            }
            _numberOfElements--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Pops the tail of the list
     * @return Success of poping (i.e.: if there is nothing left to pop => false)
     */
    template<class T> bool LinkedList<T>::popTail() {
        if( !empty() ) {
            if( _tail->_previous ) {
                _tail = _tail->_previous;
                _tail->_next.reset( nullptr );
            } else {
                _tail = nullptr;
                _head.reset( nullptr );
            }
            _numberOfElements--;
            return true;
        } else {
            return false;
        }
    }

    /**
     * Clears the entire list
     */
    template<class T> void LinkedList<T>::clear() {
        _head.reset( nullptr );
        _tail = nullptr;
        _numberOfElements = 0;
    }

    /**
     * Gets item stored at head of the list
     * @return Element at head
     */
    template<class T> T LinkedList<T>::head() const {
        if( !empty() ) {
            return _head->_key;
        } else {
            return T();
        }
    }

    /**
     * Gets item stored at tail of the list
     * @return Element at tail
     */
    template<class T> T LinkedList<T>::tail() const {
        if( !empty() ) {
            return _tail->_key;
        } else {
            return T();
        }
    }

    /**
     * Gets the size of the list
     * @return Number of items in the list
     */
    template<class T> unsigned int LinkedList<T>::size() const {
        return _numberOfElements;
    }

    /**
     * Gets the empty status of the list
     * @return Empty state
     */
    template<class T> bool LinkedList<T>::empty() const {
        return _numberOfElements == 0;
    }

    /**
     * Outputs list as string
     * @return String
     */
    template<class T> std::string LinkedList<T>::str() const {
        std::stringstream ss;
        ss << *this;
        return ss.str();
    }

    /**
     * Prints the list to a string
     * @return String containing the printed list
     */
    template<class T> std::string LinkedList<T>::forwardPrint() const {
        std::stringstream ss;
        if( _head ) {
            ss << "| ";
            LinkedListNode<T> *ptr = _head.get();
            while( ptr ) {
                ss << ptr->key() << " | ";
                ptr = ptr->_next.get();
            }
        }
        return ss.str();
    }

    /**
     * Prints the list in reverse to a string
     * @return String containing the printed list
     */
    template<class T> std::string LinkedList<T>::reversePrint() const {
        std::stringstream ss;
        if( _tail ) {
            ss << "| ";
            LinkedListNode<T> *ptr = _tail;
            while( ptr ) {
                ss << ptr->key() << " | ";
                ptr = ptr->_previous;
            }
        }
        return ss.str();
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LinkedList Iterator class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    template<class T> LinkedList<T>::iterator::iterator() :
        _pos( 0 )
    {}

    template<class T> LinkedList<T>::iterator::iterator( const LinkedList::iterator &it ) :
        _pos( it._pos )
    {}

    template<class T> typename LinkedList<T>::iterator & LinkedList<T>::iterator::operator =( const LinkedList::iterator &rhs ) {
        _pos = rhs._pos;
        return *this;
    }

    template<class T> typename LinkedList<T>::iterator & LinkedList<T>::iterator::operator ++() {
        _pos = _pos->_next.get();
        return *this;
    }

    template<class T> const typename LinkedList<T>::iterator LinkedList<T>::iterator::operator ++( int ) {
        iterator tmp( *this );
        _pos = _pos->_next.get();
        return tmp;
    }

    template<class T> typename LinkedList<T>::iterator::reference LinkedList<T>::iterator::operator *() const {
        return _pos->key();
    }

    template<class T> typename LinkedList<T>::iterator::pointer LinkedList<T>::iterator::operator ->() const {
        return _pos->key();
    }

    template<class T> bool LinkedList<T>::iterator::operator ==( const LinkedList::iterator &rhs ) const {
        return _pos == rhs._pos;
    }

    template<class T> bool LinkedList<T>::iterator::operator !=( const LinkedList::iterator &rhs ) const {
        return _pos != rhs._pos;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LinkedList Const Iterator class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    template<class T> LinkedList<T>::const_iterator::const_iterator() :
        _pos( 0 )
    {}

    template<class T> LinkedList<T>::const_iterator::const_iterator( const LinkedList::const_iterator &it ) :
        _pos( it._pos )
    {}

    template<class T> typename LinkedList<T>::const_iterator & LinkedList<T>::const_iterator::operator =( const LinkedList::const_iterator &rhs ) {
        _pos = rhs._pos;
        return *this;
    }

    template<class T> typename LinkedList<T>::const_iterator & LinkedList<T>::const_iterator::operator ++() {
        _pos = _pos->_next.get();
        return *this;
    }

    template<class T> const typename LinkedList<T>::const_iterator LinkedList<T>::const_iterator::operator ++( int ) {
        const_iterator tmp( *this );
        _pos = _pos->_next.get();
        return tmp;
    }

    template<class T> typename LinkedList<T>::const_iterator::reference LinkedList<T>::const_iterator::operator *() const {
        return _pos->key();
    }

    template<class T> typename LinkedList<T>::const_iterator::pointer LinkedList<T>::const_iterator::operator ->() const {
        return _pos->key();
    }

    template<class T> bool LinkedList<T>::const_iterator::operator ==( const LinkedList<T>::const_iterator &rhs ) const {
        return _pos == rhs._pos;
    }

    template<class T> bool LinkedList<T>::const_iterator::operator !=( const LinkedList<T>::const_iterator &rhs ) const {
        return _pos != rhs._pos;
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LinkedList Iterator methods implementations
    //-----------------------------------------------------------------------------------------------------------------
    template<class T> typename LinkedList<T>::iterator LinkedList<T>::begin() {
        iterator it;
        it._pos = _head.get();
        return it;
    }

    template<class T> typename LinkedList<T>::iterator LinkedList<T>::end() {
        return iterator();
    }

    template<class T> typename LinkedList<T>::const_iterator LinkedList<T>::cbegin() {
        const_iterator it;
        it._pos = _head.get();
        return it;
    }

    template<class T> typename LinkedList<T>::const_iterator LinkedList<T>::cend() {
        return const_iterator();
    }
}

#endif //EADLIB_LINKEDLIST_H