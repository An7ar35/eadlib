/**
    @class          eadlib::LinkedList
    @brief          [ADT] %Linked List node

    For double linked lists

    @dependencies   eadlib::GenericNode
    @author         E. A. Davison
    @copyright      E. A. Davison 2015
    @license        GNUv2 Public License
**/
#ifndef EADLIB_LISTNODE_H
#define EADLIB_LISTNODE_H

#include <memory>
#include "GenericNode.h"

namespace eadlib {
    template<class T> class LinkedListNode : public GenericNode<T> {
      public:
        explicit LinkedListNode( const T &element );
        LinkedListNode( const T &element, LinkedListNode *p );
        LinkedListNode( const T &element, std::unique_ptr<LinkedListNode> &n, LinkedListNode *p );
        ~LinkedListNode() = default;
        T & key();
        void setKey( const T &key );
        std::unique_ptr<LinkedListNode> _next;
        LinkedListNode *_previous;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // LinkedListNode class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param element Key of the node
     */
    template<class T> LinkedListNode<T>::LinkedListNode( const T &element ) {
        this->_key = element;
    }

    /**
     * Constructor
     * @param element Key of the node
     * @param p Previous node
     */
    template<class T> LinkedListNode<T>::LinkedListNode( const T &element, LinkedListNode *p ) :
        _previous( p )
    {
        this->_key = element;
    }

    /**
     * Constructor
     * @param element Key of the node
     * @param n Next node
     * @param p Previous node
     */
    template<class T> LinkedListNode<T>::LinkedListNode( const T &element, std::unique_ptr<LinkedListNode> &n, LinkedListNode *p ) :
        _previous( p )
    {
        this->_key = element;
        _next.reset( n.release() );
    }

    /**
     * Gets the key
     * @return Reference to key
     */
    template<class T> T & LinkedListNode<T>::key() {
        return this->_key;
    }

    /**
     * Sets the key
     * @param key Key
     */
    template<class T> void LinkedListNode<T>::setKey( const T &key ) {
        this->_key = key;
    }
}

#endif //EADLIB_LISTNODE_H
