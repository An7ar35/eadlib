/**
    @class          eadlib::Graph
    @brief          [ADT] Simple Matrix data structure
    @dependencies   eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_MATRIX_H
#define EADLIB_MATRIX_H

#include <vector>
#include "../logger/Logger.h"

namespace eadlib {
    template<class T> class Matrix {
      public:
        Matrix();
        Matrix( const size_t &size_x, const size_t &size_y );
        Matrix( const size_t &size_x, const size_t &size_y, const T &default_fill_value );
        Matrix( const Matrix &matrix );
        Matrix( Matrix &&matrix ) noexcept;
        ~Matrix() = default;
        //Operators
        T & operator()( const size_t &x, const size_t &y );
        //Methods
        const T & at( const size_t &x, const size_t &y ) const;
        size_t sizeX();
        size_t sizeY();
        std::string toCSVstr() const;
      private:
        size_t getIndex( const size_t &x, const size_t &y ) const;
        size_t m_sizeX;
        size_t m_sizeY;
        std::vector<T> m_matrix;
    };

    /**
     * Constructor (Default)
     */
    template<class T> Matrix<T>::Matrix() :
        m_sizeX( 0 ),
        m_sizeY( 0 ),
        m_matrix( std::vector<T>() )
    {}

    /**
     * Constructor
     * @param size_x Number of columns
     * @param size_y Number of rows
     */
    template<class T> Matrix<T>::Matrix( const size_t &size_x, const size_t &size_y ) :
        m_sizeX( size_x ),
        m_sizeY( size_y ),
        m_matrix( std::vector<T>( m_sizeX * m_sizeY, T() ) )
    {}

    /**
     * Constructor
     * @param size_x Number of columns
     * @param size_y Number of rows
     * @param default_fill_value Value to fill the matrix with at construction
     */
    template<class T> Matrix<T>::Matrix( const size_t &size_x, const size_t &size_y, const T &default_fill_value ) :
        m_sizeX( size_x ),
        m_sizeY( size_y ),
        m_matrix( std::vector<T>( m_sizeX * m_sizeY, default_fill_value ) )
    {}

    /**
     * Copy-Constructor
     * @param matrix Matrix to copy
     */
    template<class T> Matrix<T>::Matrix( const eadlib::Matrix<T> &matrix ) :
        m_sizeX( matrix.m_sizeX ),
        m_sizeY( matrix.m_sizeY ),
        m_matrix( matrix.m_matrix )
    {}

    /**
     * Move-Constructor
     * @param matrix Matrix to copy
     */
    template<class T> Matrix<T>::Matrix( eadlib::Matrix<T> &&matrix ) noexcept :
        m_sizeX( matrix.m_sizeX ),
        m_sizeY( matrix.m_sizeY ),
        m_matrix( matrix.m_matrix )
    {}

    /**
     * Operator ()
     * @param x X coordinate
     * @param y Y coordinate
     * @return Element at coordinate
     * @throws std::out_of_range when the index is out of bounds of the matrix
     */
    template<class T> T & Matrix<T>::operator ()( const size_t &x, const size_t &y ) {
        size_t index = getIndex( x, y );
        if( index >= m_matrix.size() ) {
            LOG_ERROR( "[eadlib::Matrix<T>::operator()( ", x, ", ", y, " )] Index is out of bound of the matrix." );
            throw std::out_of_range("[eadlib::Matrix<T>::operator()( const unsigned int &, const unsigned int & )] Index is out of bound of the matrix.");
        }
        return m_matrix.at( index );
    }

    /**
     * Access at grid coordinate
     * @param x X coordinate
     * @param y Y coordinate
     * @return Element at coordinate
     * @throws std::out_of_range when the index is out of bounds of the matrix
     */
    template<class T> const T & Matrix<T>::at( const size_t &x, const size_t &y ) const {
        size_t index = getIndex( x, y );
        if( index >= m_matrix.size() ) {
            LOG_ERROR( "[eadlib::Matrix<T>::at( ", x, ", ", y, " )] Index is out of bound of the matrix." );
            throw std::out_of_range("[eadlib::Matrix<T>::at()( const unsigned int &, const unsigned int & )] Index is out of bound of the matrix.");
        }
        return m_matrix.at( index );
    }

    /**
     * Gets the size on the x axis
     * @return Size X
     */
    template<class T> size_t Matrix<T>::sizeX() {
        return m_sizeX;
    }

    /**
     * Gets the size on the y axis
     * @return Size Y
     */
    template<class T> size_t Matrix<T>::sizeY() {
        return m_sizeY;
    }

    /**
     * Creates a csv string of the matrix's data
     * @return CSV string of the data
     */
    template<class T> std::string Matrix<T>::toCSVstr() const {
        if( m_matrix.size() < 1 ) {
            LOG_WARNING( "[eadlib::Matrix<T>::str()] Matrix is empty." );
            return "";
        }
        std::stringstream ss;
        size_t index = 0;
        for( size_t y = 0; y < m_sizeY; y++ ) {
            for( size_t x = 0; x < m_sizeX; x++ ) {
                index = getIndex( x, y );
                ss << m_matrix.at( index );
                if( x < m_sizeX - 1 ) ss << ";";
            }
            if( index < m_matrix.size() - 1 ) ss << "\n";
        }
        return ss.str();
    }

    /**
     * Gets the real index from coordinates
     * @param x X location on coordinate
     * @param y Y location on coordinate
     * @return Real index in vector
     */
    template<class T> size_t Matrix<T>::getIndex( const size_t &x, const size_t &y ) const {
        return x + y * m_sizeX;
    }
}

#endif //EADLIB_MATRIX_H
