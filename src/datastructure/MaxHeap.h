#ifndef EADLIB_MAXHEAP_H
#define EADLIB_MAXHEAP_H

#include "Heap.h"

namespace eadlib {
    // -----------
    // ADT MaxHeap
    // -----------
    template<class T> class MaxHeap : public Heap<T> {
      public:
        MaxHeap();
        MaxHeap( const std::initializer_list<T> list );
        MaxHeap( const LinearList<T> &list );
        MaxHeap( LinkedList<T> &list );
        virtual ~MaxHeap() {};
        T max() const;
        bool add( T &item );
        T remove();
      private:
        bool comparator( T &a, T &b );
        void buildMaxHeap();
        void maxHeapify( const int node_index, const int heap_size );
        void heapSort();
    };

    //-----------------------------------------------------------------------------------------------------------------
    // MaxHeap class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     */
    template<class T> MaxHeap<T>::MaxHeap() :
        Heap<T>()
    {}

    /**
     * Constructor
     * @param list Initializer list
     */
    template<class T> MaxHeap<T>::MaxHeap( const std::initializer_list<T> list ) :
        Heap<T>( list )
    {
        buildMaxHeap();
    }

    /**
     * Constructor
     * @param list LinearList<T>
     */
    template<class T> MaxHeap<T>::MaxHeap( const LinearList<T> &list ) :
        Heap<T>( list )
    {
        buildMaxHeap();
    }

    /**
     * Constructor
     * @param list LinkedList<T>
     */
    template<class T> MaxHeap<T>::MaxHeap( LinkedList<T> &list ) :
        Heap<T>( list )
    {
        buildMaxHeap();
    }

    /**
     * Gets the top element in the heap
     * @return Top element
     */
    template<class T> T MaxHeap<T>::max() const {
        if( this->m_content.size() < 1 ) {
            LOG_ERROR( "[eadlib::MaxHeap<T>::max()] Heap is empty." );
            throw std::out_of_range( "[eadlib::MaxHeap<T>::max()] Heap is empty." );
        } else {
            return this->m_content[ 0 ];
        }
    }

    /**
     * Adds item to the heap
     * @param item Item to add
     * @return Success of operation
     */
    template<class T> bool MaxHeap<T>::add( T &item ) {

        //TODO
    }

    /**
     * Deletes the top item in the heap and returns it
     * @return Top item in heap
     * @exception std::out_of_range when the heap is empty
     */
    template<class T> T MaxHeap<T>::remove() {
        if( this->size() <= 0 ) {
            LOG_ERROR( "[eadlib::Maxheap<T>::remove()] Trying to remove from an empty heap." );
            throw std::out_of_range( "[eadlib::Maxheap<T>::remove()] Trying to remove from an empty heap." );
        }
        T top = this->m_content[ 0 ];
        heapSort();
        this->m_content.remove( this->m_content.size() - 1 );
        return top;
    }

    /**
     * [PRIVATE] Comparator function to check if a > b
     * @param a Main item
     * @param b Item to check against
     * @return comparison state
     */
    template<class T> bool MaxHeap<T>::comparator( T &a, T &b ) {
        return a > b;
    }

    /**
     * [PRIVATE] Heap sort  //O(N log(N))
     */
    template<class T> void MaxHeap<T>::heapSort() {
        buildMaxHeap();
        int heap_size = this->size();
        for( int i = this->size(); i > 1; i-- ) {
            std::swap( this->m_content[ 0 ], this->m_content[ i - 1 ] );
            heap_size--;
            this->heapify( comparator, i, heap_size );
        }
    }
}
#endif //EADLIB_MAXHEAP_H
