#ifndef EADLIB_MINHEAP_H
#define EADLIB_MINHEAP_H

#include "Heap.h"

namespace eadlib {
    // -----------
    // ADT MinHeap
    // -----------
    template<class T> class MinHeap : Heap {
      public:
        MinHeap( unsigned int max_size = 10 );
        MinHeap( const std::initializer_list<T> list );
        MinHeap( const eadlib::LinearList<T> &list );
        virtual ~MinHeap() {};
        T min() const;
        bool add( T &item );
        T remove();
        bool isMinTree() const;
        bool isMinHeap() const;
        void sort();
    };

    //-----------------------------------------------------------------------------------------------------------------
    // MinHeap class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param max_size Heap capacity
     */
    template<class T> MinHeap<T>::MinHeap( unsigned int max_size ) :
        Heap<T>( max_size )
    {
        //TODO
    }

    /**
     * Constructor
     * @param list Initializer list
     */
    template<class T> MinHeap<T>::MinHeap( const std::initializer_list<T> list ) {

    }

    /**
     * Constructor
     * @param list LinearList<T>
     */
    template<class T> MinHeap<T>::MinHeap( const eadlib::LinearList<T> &list ) {

    }

    /**
     * Gets the top element in the heap
     * @return Top element
     */
    template<class T> T MinHeap<T>::min() const {
        if( m_content.size() < 1 ) {
            LOG_ERROR( "[eadlib::MinHeap<T>::min()] Heap is empty." );
            throw std::out_of_range( "[eadlib::MinHeap<T>::min()] Heap is empty." );
        } else {
            return m_content[ 0 ];
        }
    }

    /**
     * Adds item to the heap
     * @param item Item to add
     * @return Success of operation
     */
    template<class T> bool MinHeap<T>::add( T &item ) {
        //TODO
    }

    /**
     * Deletes the top item in the heap and returns it
     * @return Top item in heap
     */
    template<class T> T MinHeap<T>::remove() {
        //TODO
    }

    /**
     * Checks if the Heap is a minimum tree
     * @return Minimum tree status
     */
    template<class T> bool MinHeap<T>::isMinTree() const {
        //TODO
    }

    /**
     * Checks if the heap is a minimum heap
     * @return Minimum heap status
     */
    template<class T> bool MinHeap<T>::isMinHeap() const {
        //TODO
    }

    /**
     * Sorts the heap
     */
    template<class T> void MinHeap<T>::sort() {
        //TODO
    }


}
#endif //EADLIB_MINHEAP_H
