/**
    @class          eadlib::ObjectPool
    @brief          [ADT] ObjectPool

    @dependencies   eadlib::exception::resource_unavailable, eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License
**/
#ifndef EADLIB_OBJECTPOOL_H
#define EADLIB_OBJECTPOOL_H

#include <iostream>
#include <stack>
#include <memory>
#include "../logger/Logger.h"
#include "../exception/resource_unavailable.h"

namespace eadlib {
    template<class T> class ObjectPool {
      private:
        struct Deleter {
            explicit Deleter( std::weak_ptr<ObjectPool<T>*> pool ) : _pool( pool ) {}

            void operator()( T *ptr ) {
                if( auto pool_ptr = _pool.lock() )
                    /* Pool still exists so add object back to pool */
                    ( *pool_ptr.get() )->release( std::unique_ptr<T>{ ptr } );
                else
                    std::default_delete<T>{}( ptr );
            }
          private:
            std::weak_ptr<ObjectPool<T>*> _pool;
        };

      public:
        typedef std::unique_ptr<T, Deleter> ObjectPtr_t ;

        explicit ObjectPool( size_t init_size = 10 );
        ObjectPool( const ObjectPool & ) = delete;
        ObjectPool( ObjectPool && ) = delete;
        ObjectPool & operator=( const ObjectPool & ) = delete;
        virtual ~ObjectPool() = default;

        template<typename U> friend std::ostream & operator <<( std::ostream &out, const ObjectPool<U> &pool );

        ObjectPtr_t acquire();
        void release( std::unique_ptr<T> object_ptr );
        void release( ObjectPtr_t object_ptr );
        bool empty();
        size_t used();
        size_t available();

      private:
        size_t _init_size;
        size_t _used_count;
        std::stack<std::unique_ptr<T>> _pool;
        std::shared_ptr<ObjectPool<T> *> _this;
    };

    /**
     * Constructor
     * @tparam T Object type
     * @param init_size Initialisation size of pool (default=10)
     */
    template<class T> ObjectPool<T>::ObjectPool( size_t init_size ) :
        _init_size( init_size ),
        _used_count( 0 ),
        _this( new ObjectPool<T> *( this ) )
    {
        for( size_t i = 0; i < _init_size; i++ )
            _pool.emplace( std::make_unique<T>() );
    }

    /**
     * Output stream operator
     * @param out Output stream
     * @param pool ObjectPool
     * @return Output stream
     */
    template<typename U> std::ostream &operator <<( std::ostream &out, const ObjectPool<U> &pool ) {
        out << "ObjectPool{ usage: " << pool._used_count << "/" << pool._init_size << " }";
        return out;
    }

    /**
     * Gets an object from the pool
     * @return Pointer to pool object
     */
    template<class T> typename ObjectPool<T>::ObjectPtr_t ObjectPool<T>::acquire() {
        if(_pool.empty())
            throw eadlib::exception::resource_unavailable("ObjectPool is currently empty.");

        ObjectPtr_t tmp(
            _pool.top().release(),
            Deleter{ std::weak_ptr<ObjectPool<T>*>{ _this } }
        );

        _pool.pop();
        _used_count++;
        return std::move( tmp );
    }

    /**
     * Returns an object to the pool
     * @param object_ptr Pointer to object
     */
    template<class T> void ObjectPool<T>::release( std::unique_ptr<T> object_ptr ) {
        _pool.push( std::move( object_ptr ) );
        _used_count--;
    }

    /**
     * Returns an object to the pool
     * @param object_ptr Pointer to object
     */
    template<class T> void ObjectPool<T>::release( ObjectPtr_t object_ptr ) {
        _pool.emplace( object_ptr.release() );
        _used_count--;
    }

    /**
     * Gets the empty state of pool
     * @return Empty state
     */
    template<class T> bool ObjectPool<T>::empty() {
        return _pool.empty();
    }

    /**
     * Gets the object count being used
     * @return Used object count
     */
    template<class T> size_t ObjectPool<T>::used() {
        return _used_count;
    }

    /**
     * Gets the current count of pool objects
     * @return Count of objects in pool
     */
    template<class T> size_t ObjectPool<T>::available() {
        return _pool.size();
    }
}

#endif //EADLIB_OBJECTPOOL_H
