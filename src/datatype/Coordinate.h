/**
    @class          eadlib::Coordinate
    @brief          [ADT] %Coordinate container

    Coordinate object that can take in n numbers of named dimensions (x,y,z,....)

    @dependencies   eadlib::logger::Logger
    @author         E. A. Davison
    @copyright      E. A. Davison 2016
    @license        GNUv2 Public License
**/
#ifndef EADLIB_COORDINATE_H
#define EADLIB_COORDINATE_H

#include <utility>
#include <string>
#include <unordered_map>

#include "../logger/Logger.h"

namespace eadlib {
    template<class TName, class TValue> class Coordinate {
      public:
        Coordinate( std::initializer_list<std::pair<TName,TValue>> pairs );
        Coordinate( const Coordinate &coordinate );
        Coordinate( Coordinate &&coordinate ) noexcept;
        ~Coordinate() = default;
        //Operators
        Coordinate & operator=( Coordinate coordinate );
        bool operator ==( const Coordinate &rhs ) const;
        bool operator !=( const Coordinate &rhs ) const;
        //Access
        const TValue & at( const TName &name ) const;
        void set( const TName &name, const TValue &value );
        void reset( const TValue &reset_value );
        bool isReset( const TValue &reset_value );
        void setFlag( const bool &value );
        bool getFlag();
        void swap( Coordinate &coordinate );
        //Printing
        std::string str();
        std::string str( const std::vector<TName> &names );

      private:
        std::unique_ptr<std::unordered_map<TName, TValue>> m_coordinates;
        bool m_flag;
        size_t m_size;
    };

    /**
     * Constructor
     * @param pairs Initializer_list of name-value pairs
     */
    template<class TName, class TValue> Coordinate<TName, TValue>::Coordinate( std::initializer_list<std::pair<TName,TValue>> pairs ) :
        m_coordinates( std::make_unique<std::unordered_map<TName, TValue>>() ),
        m_flag( false ),
        m_size( 0 )
    {
        for( typename std::initializer_list<std::pair<TName, TValue>>::iterator it = pairs.begin(); it != pairs.end(); ++it ) {
            m_coordinates->insert( typename std::unordered_map<TName, TValue>::value_type( it->first, it->second ) );
            m_size++;
        }
    }

    /**
     * Copy-Constructor
     * @param coordinate Coordinate to copy over
     */
    template<class TName, class TValue> Coordinate<TName, TValue>::Coordinate( const Coordinate &coordinate ) :
        m_coordinates( std::make_unique<std::unordered_map<TName, TValue>>( *coordinate.m_coordinates.get() ) ),
        m_flag( coordinate.m_flag ),
        m_size( 0 )
    {}

    /**
     * Move-Constructor
     * @param coordinate Coordinate to move over
     */
    template<class TName, class TValue> Coordinate<TName, TValue>::Coordinate( Coordinate &&coordinate ) noexcept :
        m_flag( false ),
        m_size( 0 )
    {
        this->swap( coordinate );
    }

    /**
     * Assignment operator
     * @param coordinate Coordinate to assign with
     * @return Assigned Coordinate
     */
    template<class TName, class TValue> Coordinate<TName,TValue> & Coordinate<TName, TValue>::operator =( Coordinate coordinate ) {
       this->swap( coordinate );
    }

    /**
     * Equivalence operator
     * @param rhs Coordinate to check against
     * @return Equivalence state
     */
    template<class TName, class TValue> bool Coordinate<TName, TValue>::operator ==( const Coordinate &rhs ) const {
        return *m_coordinates == *rhs.m_coordinates &&
               m_flag == rhs.m_flag &&
               m_size == rhs.m_size;
    }

    /**
     * Negative equivalence operator
     * @param rhs Coordinate to check against
     * @return Negative equivalence state
     */
    template<class TName, class TValue> bool Coordinate<TName, TValue>::operator !=( const Coordinate &rhs ) const {
        return !( rhs == *this );
    }

    /**
     * Gets the value for the specified name
     * @param name Name
     * @return Value (read only)
     * @throws std::invalid_argument when name given is not in the Coordinate
     */
    template<class TName, class TValue> const TValue & Coordinate<TName, TValue>::at( const TName &name ) const {
        try {
            return m_coordinates->at( name );
        } catch( std::out_of_range ) {
            LOG_ERROR( "[eadlib::Coordinate<TName, TValue>::at( ", name, " )] Unknown name." );
            throw std::invalid_argument( "[eadlib::Coordinate<TName, TValue>::at(..)] Unknown name." );
        }
    }

    /**
     * Sets a value for the specified name
     * @param name Name
     * @param value Value to set
     * @return Success
     * @throws std::invalid_argument when name given is not in the Coordinate
     */
    template<class TName, class TValue> void Coordinate<TName, TValue>::set( const TName &name, const TValue &value ) {
        try {
            m_coordinates->at( name ) = value;
        } catch( std::out_of_range ) {
            LOG_ERROR( "[eadlib::Coordinate<TName, TValue>::set( ", name, ", ", value, " )] '", name, "' is unknown." );
            throw std::invalid_argument( "[eadlib::Coordinate<TName, TValue>::set(..)] Unknown name." );
        }
    }

    /**
     * Resets the Coordinate's values
     * @param reset_value Value to reset to
     */
    template<class TName, class TValue> void Coordinate<TName, TValue>::reset( const TValue &reset_value ) {
        for( typename std::unordered_map<TName, TValue>::iterator it = m_coordinates->begin(); it != m_coordinates->end(); ++it ) {
            it->second = reset_value;
        }
        m_flag = false;
    }

    /**
     * Check if Coordinate is reset
     * @param reset_value Reset value
     * @return Reset state
     */
    template<class TName, class TValue> bool Coordinate<TName, TValue>::isReset( const TValue &reset_value ) {
        if( m_flag  ) return false;
        for( typename std::unordered_map<TName, TValue>::iterator it = m_coordinates->begin(); it != m_coordinates->end(); ++it ) {
            if( it->second != reset_value ) return false;
        }
        return true;
    }

    /**
     * Sets the flag's value
     * @param value Flag value
     */
    template<class TName, class TValue> void Coordinate<TName, TValue>::setFlag( const bool &value ) {
        m_flag = value;
    }

    /**
     * Gets the state of the flag
     * @return Flag state
     */
    template<class TName, class TValue> bool Coordinate<TName, TValue>::getFlag() {
        return m_flag;
    }

    /**
     * Swaps Coordinate content
     * @param coordinate Coordinate to swap with
     */
    template<class TName, class TValue> void Coordinate<TName, TValue>::swap( Coordinate &coordinate ) {
        std::swap( this->m_coordinates, coordinate.m_coordinates );
        this->m_size = this->m_coordinates->size();
        coordinate.m_size = coordinate.m_coordinates->size();
        bool temp = this->m_flag;
        this->m_flag = coordinate.m_flag;
        coordinate.m_flag = temp;
    }

    /**
     * Gets the formatted string of the Coordinate
     * @return String of the Coordinate name/value pairs
     */
    template<class TName, class TValue> std::string Coordinate<TName, TValue>::str() {
        std::ostringstream oss;
        size_t  i { 0 };
        oss << "(";
        for( typename std::unordered_map<TName, TValue>::iterator it = m_coordinates->begin(); it != m_coordinates->end(); ++it ) {
            oss << "[" << it->first << "=" << it->second << "]";
            if( i < m_coordinates->size() - 1 ) {
                oss << ", ";
            }
            i++;
        }
        oss << ")";
        return oss.str();
    }

    /**
     * Gets a formatted string of the Coordinate
     * @param names Names of the dimensions to print out (ordered)
     * @return String of the Coordinate formated as '(x, y, n)'
     */
    template<class TName, class TValue> std::string Coordinate<TName, TValue>::str( const std::vector<TName> &names ) {
        std::ostringstream oss;
        size_t i { 0 };
        if( names.empty() ) {
            oss << "(null)";
        } else {
            oss << "(";
            for( auto n : names ) {
                try {
                    oss << m_coordinates->at( n );
                } catch( std::out_of_range ) {
                    LOG_ERROR( "[eadlib::Coordinate<TName, TValue>::print(..)] a name is unknown." );
                    oss << "unknown";
                }
                i++;
                if( i < names.size())
                    oss << ", ";
                else
                    oss << ")";

            }
        }
        return oss.str();
    }
}

#endif //EADLIB_COORDINATE_H
