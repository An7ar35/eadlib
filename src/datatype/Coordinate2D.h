/**
    @class          eadlib::Coordinate2D
    @brief          [ADT] %Coordinate container (2D)

    Type 'T' should be 0 assignable (e.g.: int, unsigned, double...)

    @author         E. A. Davison
    @copyright      E. A. Davison 2015 (updated 2018)
    @license        GNUv2 Public License
**/
#ifndef EADLIB_COORDINATE2D_H
#define EADLIB_COORDINATE2D_H

#include <iostream>
#include <math.h>

namespace eadlib {
    template<typename T> struct Coordinate2D {
        Coordinate2D();
        Coordinate2D( T x_coord, T y_coord );
        Coordinate2D( const Coordinate2D<T> &coordinate );
        Coordinate2D( Coordinate2D<T> &&coordinate ) noexcept;
        ~Coordinate2D() = default;
        //Operators
        Coordinate2D & operator =( Coordinate2D<T> coordinate );
        template<class U> friend std::ostream &operator <<( std::ostream &s, const Coordinate2D<U> &c );
        bool operator ==( const Coordinate2D<T> &rhs ) const;
        bool operator !=( const Coordinate2D<T> &rhs ) const;
        const Coordinate2D<T> operator +( const Coordinate2D<T> &rhs ) const;
        const Coordinate2D<T> operator -( const Coordinate2D<T> &rhs ) const;
        const Coordinate2D<T> & operator +=( const Coordinate2D<T> &rhs );
        const Coordinate2D<T> & operator -=( const Coordinate2D<T> &rhs );
        //Setters
        void set( T x, T y );
        void reset();
        //Variables
        T x;
        T y;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Coordinate2D struct method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @tparam T Coordinate value type
     */
    template<typename T> Coordinate2D<T>::Coordinate2D() :
        x( 0 ),
        y( 0 )
    {}

    /**
     * Constructor
     * @tparam T Coordinate value type
     * @param x_coord X coordinate
     * @param y_coord Y coordinate
     */
    template<typename T> Coordinate2D<T>::Coordinate2D( T x_coord, T y_coord ) :
        x( x_coord ),
        y( y_coord )
    {}

    /**
     * Copy-Constructor
     * @tparam T Coordinate value type
     * @param coordinate Coordinate to copy
     */
    template<typename T> Coordinate2D<T>::Coordinate2D( const Coordinate2D<T> &coordinate ) :
        x( coordinate.x ),
        y( coordinate.y )
    {}

    /**
     * Move-Constructor
     * @tparam T Coordinate value type
     * @param coordinate Coordinate to move
     */
    template<typename T> Coordinate2D<T>::Coordinate2D( Coordinate2D<T> &&coordinate ) noexcept :
        x( std::move( coordinate.x ) ),
        y( std::move( coordinate.y ) )
    {}

    /**
     * Assignment operator
     * @param rhs Coordinate to assign with
     * @return Assigned coordinate
     */
    template<typename T> Coordinate2D<T> & Coordinate2D<T>::operator =( Coordinate2D<T> rhs ) {
        if( this != &rhs ) {
            x = std::move( rhs.x );
            y = std::move( rhs.y );
        }
        return *this;
    }

    /**
     * Output stream
     * @param out Output stream
     * @param c   Coordinate2D
     * @return Output stream
     */
    template<typename T> std::ostream & operator <<( std::ostream &out, const Coordinate2D<T> &c ) {
        out << "(" << c.x << "," << c.y << ")";
        return out;
    }

    /**
     * Boolean equivalence operator
     * @param rhs Coordinate to compare to
     * @return Equivalence state
     */
    template<typename T> bool Coordinate2D<T>::operator ==( const Coordinate2D<T> &rhs ) const {
        return ( x == rhs.x && y == rhs.y );
    }

    /**
     * Boolean not-equivalent operator
     * @param rhs Coordinate to compare to
     * @return Not-equivalent state
     */
    template<typename T> bool Coordinate2D<T>::operator !=( const Coordinate2D<T> &rhs ) const {
        return !( *this == rhs );
    }

    /**
     * Transformation addition operator
     * @param rhs Coordinate to add with
     * @return Result
     */
    template<typename T> const Coordinate2D<T> Coordinate2D<T>::operator +( const Coordinate2D<T> &rhs ) const {
        return Coordinate2D<T>( ( x + rhs.x ), ( y + rhs.y ) );
    }

    /**
     * Transformation subtraction operator
     * @param rhs Coordinate to subtract with
     * @return Result
     */
    template<typename T> const Coordinate2D<T> Coordinate2D<T>::operator -( const Coordinate2D<T> &rhs ) const {
        return Coordinate2D<T>( ( x - rhs.x ), ( y - rhs.y ) );
    }

    /**
     * Transformation addition operator on self
     * @param rhs Coordinate to add with
     * @return Reference to this object post calculation
     */
    template<typename T> const Coordinate2D<T> & Coordinate2D<T>::operator +=( const Coordinate2D<T> &rhs ) {
        x += rhs._x;
        y += rhs._y;
        return *this;
    }

    /**
     * Transformation subtraction operator on self
     * @param rhs Coordinate to subtract with
     * @return Reference to this object post calculation
     */
    template<typename T> const Coordinate2D<T> & Coordinate2D<T>::operator -=( const Coordinate2D<T> &rhs ) {
        x -= rhs._x;
        y -= rhs._y;
        return *this;
    }

    /**
     * Sets coordinates
     * @param x X coordinate
     * @param y Y coordinate
     */
    template<typename T> void Coordinate2D<T>::set( T x, T y ) {
        this->x = x;
        this->y = y;
    }

    /**
     * Resets coordinates to 0
     */
    template<typename T> void Coordinate2D<T>::reset() {
        this->x = 0;
        this->y = 0;
    }
}

#endif //EADLIB_COORDINATE2D_H
