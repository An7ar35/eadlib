/**
    @class          eadlib::Coordinate3D
    @brief          3D %Coordinate data type
    @author         E. A. Davison
    @copyright      E. A. Davison 2014-17
    @license        GNUv2 Public License
**/
#ifndef EADLIB_COORDINATE3D_H
#define EADLIB_COORDINATE3D_H

#include <iostream>
#include <math.h>

namespace eadlib {
    class Coordinate3D {
      public:
        enum class Axis {
            X, Y, Z
        };
        //Constructors/Destructor
        Coordinate3D();
        Coordinate3D( double x, double y, double z );
        Coordinate3D( const Coordinate3D &coordinate );
        Coordinate3D( Coordinate3D &&coordinate ) noexcept;
        ~Coordinate3D() = default;
        //Operators
        Coordinate3D & operator =( Coordinate3D rhs );
        friend std::ostream & operator <<( std::ostream &out, const Coordinate3D &coord );
        bool operator ==( const Coordinate3D &rhs ) const;
        bool operator !=( const Coordinate3D &rhs ) const;
        const Coordinate3D operator +( const Coordinate3D &rhs ) const;
        const Coordinate3D operator -( const Coordinate3D &rhs ) const;
        const Coordinate3D & operator +=( const Coordinate3D &rhs );
        const Coordinate3D & operator -=( const Coordinate3D &rhs );
        //Getters
        double x() const;
        double y() const;
        double z() const;
        bool error() const;
        bool isReset() const;
        //Setters
        void setX( const double &x );
        void setY( const double &y );
        void setZ( const double &z );
        void setError( const bool &value = true );
        void reset();
        //Swappers
        void swap( eadlib::Coordinate3D &rhs );
        //Calculations
        double calcDistance( const Coordinate3D &dest ) const;
        const Coordinate3D calcDifference( const Coordinate3D &rhs ) const;
        const Coordinate3D calcDifference_abs( const Coordinate3D &rhs ) const;
        const Coordinate3D rotate( const Axis &axis, const double &angle ) const;
        const Coordinate3D & rotate( const Axis &axis, const double &angle );
      private:
        double _x;
        double _y;
        double _z;
        bool _error_flag;
    };

    /**
     * Constructor
     */
    inline Coordinate3D::Coordinate3D() :
        _x( 0 ),
        _y( 0 ),
        _z( 0 ),
        _error_flag( false )
    {}

    /**
     * Constructor
     * @param x X coordinate
     * @param y Y coordinate
     * @param z Z coordinate
     */
    inline Coordinate3D::Coordinate3D( double x, double y, double z ) :
        _x( x ),
        _y( y ),
        _z( z ),
        _error_flag( false )
    {}

    /**
     * Copy-Constructor
     * @param coordinate Coordinate to copy over
     */
    inline Coordinate3D::Coordinate3D( const Coordinate3D &coordinate )  :
        _x( coordinate._x ),
        _y( coordinate._y ),
        _z( coordinate._z ),
        _error_flag( false )
    {}

    /**
     * Move-Constructor
     * @param coordinate Coordinate to move over
     */
    inline Coordinate3D::Coordinate3D( Coordinate3D &&coordinate ) noexcept : Coordinate3D() {
        swap( coordinate );
    }

    /**
     * Assignment operator
     * @param rhs Coordinate object to assign with
     * @return Coordinate object
     */
    inline Coordinate3D & Coordinate3D::operator =( Coordinate3D rhs ) {
        if( this != &rhs )
            swap( rhs );
        return *this;
    }

    /**
     * Output stream operator
     * @param out   Output stream
     * @param coord Coordinate to stream out
     * @return Output stream
     */
    inline std::ostream & operator <<( std::ostream &out, const Coordinate3D &coord )  {
        out << "(" << coord._x << "," << coord._y << "," << coord._z << ")";
        return out;
    }

    /**
     * Boolean equivalence operator
     * @param rhs Coordinate to compare with
     * @return Equivalence state
     */
    inline bool Coordinate3D::operator ==( const Coordinate3D &rhs ) const {
        return ( this->_x == rhs._x && this->_y == rhs._y && this->_z == rhs._z
                 && this->_error_flag == rhs._error_flag );
    }

    /**
     * Boolean not-equivalent operator
     * @param rhs Coordinate to compare with
     * @return Not-equivalent state
     */
    inline bool Coordinate3D::operator !=( const Coordinate3D &rhs ) const {
        return !( this->operator ==( rhs ));
    }

    /**
     * 3D transformation addition operator
     * @param rhs Coordinate to add with
     * @return Result
     */
    inline const Coordinate3D Coordinate3D::operator +( const Coordinate3D &rhs ) const {
        return Coordinate3D( ( _x + rhs._x ), ( _y + rhs._y ), ( _z + rhs._z ) );
    }

    /**
     * 3D transformation subtraction operator
     * @param rhs Coordinate to subtract with
     * @return Result
     */
    inline const Coordinate3D Coordinate3D::operator -( const Coordinate3D &rhs ) const {
        return Coordinate3D( ( _x - rhs._x ), ( _y - rhs._y ), ( _z - rhs._z ) );
    }

    /**
     * 3D transformation addition operator on self
     * @param rhs Coordinate to add with
     * @return Reference to this object post calculation
     */
    inline const Coordinate3D &Coordinate3D::operator +=( const Coordinate3D &rhs ) {
        _x += rhs._x;
        _y += rhs._y;
        _z += rhs._z;
        return *this;
    }

    /**
     * 3D transformation subtraction operator on self
     * @param rhs Coordinate to subtract with
     * @return Reference to this object post calculation
     */
    inline const Coordinate3D &Coordinate3D::operator -=( const Coordinate3D &rhs ) {
        _x -= rhs._x;
        _y -= rhs._y;
        _z -= rhs._z;
        return *this;
    }

    /**
     * Gets the X coordinate value
     * @return Value of X
     */
    inline double Coordinate3D::x() const {
        return _x;
    }

    /**
     * Gets the Y coordinate value
     * @return Value of Y
     */
    inline double Coordinate3D::y() const {
        return _y;
    }

    /**
     * Gets the Z coordinate value
     * @return Value of Z
     */
    inline double Coordinate3D::z() const {
        return _z;
    }

    /**
     * Gets the error flag value
     * @return Error flag
     */
    inline bool Coordinate3D::error() const {
        return _error_flag;
    }

    /**
     * Checks if the coordinate is reset
     * @return Reset state
     */
    inline bool Coordinate3D::isReset() const {
        return ( _x == 0 && _y == 0 && _z == 0 && !_error_flag );
    }

    /**
     * Sets the X value of the coordinate
     * @param x X value
     */
    inline void Coordinate3D::setX( const double &x ) {
        _x = x;
    }

    /**
     * Sets the Y value of the coordinate
     * @param y Y value
     */
    inline void Coordinate3D::setY( const double &y ) {
        _y = y;
    }

    /**
     * Sets the Z value of the coordinate
     * @param z Z value
     */
    inline void Coordinate3D::setZ( const double &z ) {
        _z = z;
    }

    /**
     * Sets the error flag of the coordinate (default = true)
     * @param value Error flag value
     */
    inline void Coordinate3D::setError( const bool &value ) {
        _error_flag = value;
    }

    /**
     * Resets the coordinate values to 0 and the flag to false
     */
    inline void Coordinate3D::reset() {
        _x = 0;
        _y = 0;
        _z = 0;
        _error_flag = false;
    }

    /**
     * Swaps values with another Coordinate
     * @param rhs Coordinate to swap values with
     */
    inline void Coordinate3D::swap( eadlib::Coordinate3D &rhs ) {
        auto swap = []( double &a, double &b ) {
            double temp = a;
            a = b;
            b = temp;
        };
        auto swapFlags = []( bool &a, bool &b ) {
            bool temp = a;
            a = b;
            b = temp;
        };
        swap( _x, rhs._x );
        swap( _y, rhs._y );
        swap( _z, rhs._z );
        swapFlags( _error_flag, rhs._error_flag );
    }

    /**
     * Calculates the distance in space to another coordinate
     * @param dest Destination coordinate
     * @return Distance
     */
    inline double Coordinate3D::calcDistance( const Coordinate3D &dest ) const {
        return sqrt( pow( dest._x - _x, 2 ) + pow( dest._y - _y, 2 ) + pow( dest._z - _z, 2 ) );
    }

    /**
     * Calculates the difference between each dimensions of the coordinate with another
     * @param rhs Coordinate to calculate difference with
     * @return XYZ difference packaged as a Coordinate3D
     */
    inline const Coordinate3D Coordinate3D::calcDifference( const Coordinate3D &rhs ) const {
        return Coordinate3D( ( _x - rhs._x ), ( _y - rhs._y ), ( _z - rhs._z ) );
    }

    /**
     * Calculates the absolute difference between each dimensions of the coordinate with another
     * @param rhs Coordinate to calculate difference with
     * @return XYZ abs difference packaged as a Coordinate3D
     */
    inline const Coordinate3D Coordinate3D::calcDifference_abs( const Coordinate3D &rhs ) const {
        return eadlib::Coordinate3D( labs( _x - rhs._x ), labs( _y - rhs._y ), labs( _z - rhs._z ) );
    }

    /**
     * Rotates a coordinate point on a given axis
     * @param axis  Axis to rotate coordinate point on
     * @param angle Angle to rotate by
     * @return Result of the rotation as a new coordinate object
     */
    inline const Coordinate3D Coordinate3D::rotate( const Coordinate3D::Axis &axis, const double &angle ) const {
        return Coordinate3D( *this ).rotate( axis, angle );
    }

    /**
     * Rotates a coordinate point on a given axis
     * @param axis  Axis to rotate coordinate point on
     * @param angle Angle to rotate by
     * @return Reference to rotated coordinate
     */
    inline const Coordinate3D & Coordinate3D::rotate( const Coordinate3D::Axis &axis, const double &angle ) {
        Coordinate3D temp {};
        double rad { angle * M_PI / 180 };
        double c = cos( rad );
        double s = sin( rad );
        switch( axis ) {
            case Axis::Z:
                temp.setX( _x * c - _y * s );
                temp.setY( _x * s + _y * c );
                temp.setZ( _z );
                break;
            case Axis::Y:
                temp.setX( _x * c - _z * s );
                temp.setY( _y );
                temp.setZ( _x * s + _z * c );
                break;
            case Axis::X:
                temp.setX( _x );
                temp.setY( _y * c - _z * s );
                temp.setZ( _y * s + _z * c );
                break;
        }
        temp.setError( _error_flag );
        swap( temp );
        return *this;
    }
}

#endif //EADLIB_COORDINATE3D_H
