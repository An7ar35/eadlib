/*!
    @class          eadlib::Integer
    @brief          Variable bit-length Integer class with overflow/underflow exceptions
    @note           Big-endian internal ordering using 2's complement for math
    @dependencies
    @author         E. A. Davison
    @copyright      E. A. Davison 2019
    @license        GNUv2 Public License
*/
#ifndef EADLIB_INTEGER_H
#define EADLIB_INTEGER_H

#include <string>
#include <iostream>
#include <bitset>
#include <limits>
#include <type_traits>

namespace eadlib {
    template<size_t Size = 128> class Integer {
        static_assert( Size >= 4, "[eadlib::Integer] bit size is too small." );
        static_assert( Size <= ( std::numeric_limits<size_t>::max() - 2 ) / 2, "[eadlib::Integer] bit size is too large." );

      public:
        Integer();
        explicit Integer( bool value );
        explicit Integer( char value );
        explicit Integer( unsigned char value );
        explicit Integer( short value );
        explicit Integer( unsigned short value );
        explicit Integer( int value );
        explicit Integer( unsigned value );
        explicit Integer( long value );
        explicit Integer( unsigned long value );
        explicit Integer( long long value );
        explicit Integer( unsigned long long value );
        explicit Integer( const std::bitset<Size> &bitset );
        explicit Integer( std::unique_ptr<std::bitset<Size>> bitset_ptr );
        explicit Integer( const char *value );
        explicit Integer( const std::string &value );
        Integer( const Integer &integer );
        Integer( Integer &&integer ) noexcept;
        ~Integer() = default;
        //Factory & Conversion
        template<size_t N> static Integer<Size> create( const std::bitset<N> &bitset );
        template<size_t N> static Integer<Size> convert( const Integer<N> &integer );
        //Output
        template<size_t N> friend std::ostream & operator <<( std::ostream &os, const Integer<N> &integer );
        std::string str_base2() const;
        std::string str_base10() const;
        std::string str_base16() const;
        void printBase2( std::ostream &out ) const;
        void printBase10( std::ostream &out ) const;
        void printBase16( std::ostream &out ) const;
        //Input
        template<size_t N> friend std::istream & operator >>( std::istream &is, Integer<N> &integer );
        void inputBase2( std::istream &in );
        void inputBase10( std::istream &in );
        void inputBase16( std::istream &in );
        //Assignment
        Integer & operator =( const Integer &rhs );
        Integer & operator =( Integer &&rhs ) noexcept;
        void swap( Integer &rhs );
        //Evaluation Operators
        bool operator ==( const Integer &rhs ) const;
        bool operator !=( const Integer &rhs ) const;
        bool operator <( const Integer &rhs ) const;
        bool operator >( const Integer &rhs ) const;
        bool operator <=( const Integer &rhs ) const;
        bool operator >=( const Integer &rhs ) const;
        //Math Operators and methods
        Integer operator +( const Integer &rhs ) const;
        Integer operator -( const Integer &rhs ) const;
        Integer operator *( const Integer &rhs ) const;
        Integer operator /( const Integer &rhs ) const;
        Integer operator %( const Integer &rhs ) const;
        Integer operator ~() const;
        Integer operator &( const Integer &rhs ) const;
        Integer operator |( const Integer &rhs ) const;
        Integer operator ^( const Integer &rhs ) const;
        Integer operator <<( size_t pos ) const;
        Integer operator >>( size_t pos ) const;
        Integer & operator +=( const Integer &rhs );
        Integer & operator -=( const Integer &rhs );
        Integer & operator *=( const Integer &rhs );
        Integer & operator /=( const Integer &rhs );
        Integer & operator %=( const Integer &rhs );
        Integer & operator &=( const Integer &rhs );
        Integer & operator |=( const Integer &rhs );
        Integer & operator ^=( const Integer &rhs );
        Integer & operator <<=( size_t pos );
        Integer & operator >>=( size_t pos );
        Integer & operator ++();
        const Integer operator ++( int );
        Integer & operator --();
        const Integer operator --( int );
        std::pair<Integer<Size>, Integer<Size>> divide( const Integer<Size> &rhs ) const;
        Integer pow( const Integer &exp ) const;
        Integer pow( int iexp ) const;
        Integer modpow( const Integer &exp, const Integer &mod ) const; //TODO more testing
        Integer sqrt() const;
        Integer ars( size_t pos ) const;
        Integer lrs( size_t pos ) const;
        Integer & arsSelf( size_t pos );
        Integer & lrsSelf( size_t pos );
        Integer abs() const;
        Integer negate() const;
        //Info
        template<size_t N> int compareTo( const Integer<N> &rhs ) const;
        bool isNegative() const;
        bool isZero() const;
        size_t bitSize() const;
        size_t minReqBits() const;
        const std::bitset<Size> & bitset() const;
        //Limits
        static Integer<Size> min();
        static Integer<Size> max();

      private:
        static struct Bin2Hex {
            constexpr Bin2Hex() : _table {} {
                _table[ 0b0000 ] = '0'; _table[ 0b0001 ] = '1'; _table[ 0b0010 ] = '2'; _table[ 0b0011 ] = '3';
                _table[ 0b0100 ] = '4'; _table[ 0b0101 ] = '5'; _table[ 0b0110 ] = '6'; _table[ 0b0111 ] = '7';
                _table[ 0b1000 ] = '8'; _table[ 0b1001 ] = '9'; _table[ 0b1010 ] = 'A'; _table[ 0b1011 ] = 'B';
                _table[ 0b1100 ] = 'C'; _table[ 0b1101 ] = 'D'; _table[ 0b1110 ] = 'E'; _table[ 0b1111 ] = 'F';
            }

            constexpr char operator ()( char const bin_val ) const {
                return _table[ (char) bin_val ];
            }

            u_char _table[16];
        } constexpr bin2hex {};

        static struct Hex2Bin {
            constexpr Hex2Bin() : _table {} {
                _table[ '0' ] = 0b0000; _table[ '1' ] = 0b0001; _table[ '2' ] = 0b0010; _table[ '3' ] = 0b0011;
                _table[ '4' ] = 0b0100; _table[ '5' ] = 0b0101; _table[ '6' ] = 0b0110; _table[ '7' ] = 0b0111;
                _table[ '8' ] = 0b1000; _table[ '9' ] = 0b1001; _table[ 'A' ] = 0b1010; _table[ 'B' ] = 0b1011;
                _table[ 'C' ] = 0b1100; _table[ 'D' ] = 0b1101; _table[ 'E' ] = 0b1110; _table[ 'F' ] = 0b1111;
                _table[ 'a' ] = 0b1010; _table[ 'b' ] = 0b1011; _table[ 'c' ] = 0b1100; _table[ 'd' ] = 0b1101;
                _table[ 'e' ] = 0b1110; _table[ 'f' ] = 0b1111;
            }

            constexpr char operator ()( char const char_val ) const {
                return _table[ (char) char_val ];
            }

            u_char _table[128]; //(ascii)
        } constexpr hex2bin {};

        std::unique_ptr<std::bitset<Size>> _bitset;

        template<typename T> std::unique_ptr<std::bitset<Size>> integerToBitset( T val ) const;
        std::unique_ptr<std::bitset<Size>> stringToBitSet( std::string str ) const;

        std::vector<unsigned> x2( const std::vector<unsigned> &val ) const;
        unsigned d2( std::list<unsigned> &val ) const;
        std::vector<unsigned> add( const std::vector<unsigned> &a, const std::vector<unsigned> &b ) const;

        std::pair<Integer<Size>, Integer<Size>> divide( const Integer<Size> &dividend, const Integer<Size> &divisor ) const;
        std::unique_ptr<std::bitset<Size>> negate( const std::bitset<Size> &bitset ) const;
        template<size_t N> std::bitset<N> & ars( std::bitset<N> &bitset, size_t pos ) const;
        template<size_t N> std::bitset<N> & lrs( std::bitset<N> &bitset, size_t pos ) const;
    };
}

namespace std {
    template<size_t Size> class numeric_limits<eadlib::Integer<Size>> {
      public:
        static eadlib::Integer<Size> min() { return eadlib::Integer<Size>::min(); };
        static eadlib::Integer<Size> max() { return eadlib::Integer<Size>::max(); };
        static eadlib::Integer<Size> lowest() { return min(); };
        static constexpr bool is_signed     = true;
        static constexpr bool is_integer    = true;
        static constexpr bool has_infinity  = false;
        static constexpr bool has_quiet_NaN = false;
        static constexpr int  radix         = 2;
        static constexpr int  max_digits10  = 0;
    };
}

//--------------------------------------------------------------------------------------------------
// eadlib::Integer<> class method implementations
//--------------------------------------------------------------------------------------------------
namespace eadlib {
    /**
     * Default constructor
     * @tparam Size Base bit size of the integer structure
     */
    template<size_t Size> Integer<Size>::Integer() {
        _bitset = std::make_unique<std::bitset<Size>>();
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Boolean value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( bool value ) {
        try {
            _bitset = integerToBitset<bool>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( bool )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Char value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( char value ) {
        try {
            _bitset = integerToBitset<char>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( char )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Unsigned char value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( unsigned char value ) {
        try {
            _bitset = integerToBitset<unsigned char>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( unsigned char )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Short value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( short value ) {
        try {
            _bitset = integerToBitset<short>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( short )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Unsigned short value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( unsigned short value ) {
        try {
            _bitset = integerToBitset<unsigned short>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( unsigned short )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Signed integer value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( int value ) {
        try {
            _bitset = integerToBitset<int>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( int )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Unsigned integer value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( unsigned value ) {
        try {
            _bitset = integerToBitset<unsigned>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( unsigned )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Signed long value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( long value ) {
        try {
            _bitset = integerToBitset<long>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( long )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Unsigned long value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( unsigned long value ) {
        try {
            _bitset = integerToBitset<unsigned long>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( unsigned long )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Signed long long value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( long long value ) {
        try {
            _bitset = integerToBitset<long long>( value );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( long long )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Unsigned long long value
     * @throws std::overflow_error when value cannot fit into Integer
     */
    template<size_t Size> Integer<Size>::Integer( unsigned long long value ) {
        try {
            _bitset = integerToBitset<unsigned long long>( value );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param bitset bitset to use (must be Big-Endian formatted)
     */
    template<size_t Size> Integer<Size>::Integer( const std::bitset<Size> &bitset ) :
        _bitset( std::make_unique<std::bitset<Size>>( bitset ) )
    {}

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param bitset_ptr Bitset to use (must be Big-Endian formatted)
     */
    template<size_t Size> Integer<Size>::Integer( std::unique_ptr<std::bitset<Size>> bitset_ptr ) :
        _bitset( std::move( bitset_ptr ) )
    {}

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Integer base10 value as a c-style character string
     * @throws std::invalid_argument when value is not a valid integer
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size> Integer<Size>::Integer( const char *value ) {
        try {
            _bitset = stringToBitSet( value );
        } catch( std::invalid_argument &e ) {
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( const char * )] " + std::string( e.what() )
            );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( const char * )] " + std::string( e.what() )
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( const char * )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Constructor
     * @tparam Size Base bit size of the integer structure
     * @param value Integer base10 value as a string
     * @throws std::invalid_argument when value is not a valid integer
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size> Integer<Size>::Integer( const std::string &value ) {
        try {
            _bitset = stringToBitSet( value );
        } catch( std::invalid_argument &e ) {
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( std::string )] " + std::string( e.what() )
            );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( std::string )] " + std::string( e.what() )
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">( std::string )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Copy-Constructor
     * @tparam Size Base bit size of the integer structure
     * @param integer Integer to copy over
     */
    template<size_t Size> Integer<Size>::Integer( const Integer &integer ) {
        _bitset = std::make_unique<std::bitset<Size>>( *integer._bitset );
    }

    /**
     * Move-Constructor
     * @tparam Size Base bit size of the integer structure
     * @param integer Integer to move over
     */
    template<size_t Size> Integer<Size>::Integer( Integer &&integer ) noexcept {
        _bitset.reset( integer._bitset.release() );
    }

    /**
     * Creates an Integer object from a bitset (2s complement formatted)
     * @tparam Size  Target integer bit size
     * @tparam N     Source bitset size
     * @param bitset Source bitset
     * @return Integer
     * @throws std::overflow_error when integer represented in bitset is too large for the target Integer size
     * @throws std::underflow_error when integer represented in bitset is too small for the target Integer size
     */
    template<size_t Size> template<size_t N>
        Integer<Size> Integer<Size>::create( const std::bitset<N> &bitset )
    {
        bool is_negative = bitset.test( bitset.size() - 1 );

        try {
            auto   integer      = Integer<Size>();
            size_t bitset_index = N;
            size_t int_index    = Size;

            while( bitset_index > int_index ) { //when bitset > Integer size
                --bitset_index;
                if( bitset.test( bitset_index ) != is_negative ) //check bitset value can be contained in Integer
                    throw std::invalid_argument( "Value too large..." );
            }

            while( int_index > bitset_index ) { //when Integer size > bitset
                --int_index;
                integer._bitset->set( int_index, is_negative ); //fill with appropriate bit
            }

            while( bitset_index > 0 ) { //bitset_index == int_index
                --bitset_index;
                --int_index;
                integer._bitset->set( int_index, bitset.test( bitset_index ) );
            }

            return integer;

        } catch( std::invalid_argument &e ) {
            if( is_negative )
                throw std::underflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::create( std::bitset<" + std::to_string( N ) + "> )] Bitset value cannot fit into Integer of this size."
                );
            else
                throw std::overflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::create( std::bitset<" + std::to_string( N ) + "> )] Bitset value cannot fit into Integer of this size."
                );
        }
    }

    /**
     * Converts an Integer of one size to another
     * @tparam Size   Target Integer bit size
     * @tparam N      Source Integer bit size
     * @param integer Source Integer
     * @return Integer
     * @throws std::overflow_error when source Integer is too large for the target Integer size
     * @throws std::underflow_error when source Integer is too small for the target Integer size
     */
    template<size_t Size> template<size_t N>
        Integer<Size> Integer<Size>::convert( const Integer<N> &integer )
    {
        try {
            return Integer<Size>::create( integer.bitset() );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::convert( Integer<" + std::to_string( N ) + "> )] Bitset value cannot fit into Integer of this size."
            );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::convert( Integer<" + std::to_string( N ) + "> )] Bitset value cannot fit into Integer of this size."
            );
        }
    }

    /**
     * Output stream operator
     * @tparam Bits   Bit size of the Integer
     * @param os      Output stream
     * @param integer Integer to print out
     * @return Output stream
     */
    template<size_t N> std::ostream & operator <<( std::ostream &os, const Integer<N> &integer ) {
        integer.printBase10( os );
        return os;
    }

    /**
     * Outputs a Big-Endian Base2 representation of the Integer as a string
     * @return String representation (2's complement)
     */
    template<size_t Size> std::string Integer<Size>::str_base2() const {
        std::stringstream ss;
        printBase2( ss );
        return ss.str();
    }

    /**
     * Outputs a Base10 representation of the Integer as a string
     * @return String representation
     */
    template<size_t Size> std::string Integer<Size>::str_base10() const {
        std::stringstream ss;
        printBase10( ss );
        return ss.str();
    }

    /**
     * Outputs a Base16 representation of the Integer as a string
     * @return String representation (2's complement)
     */
    template<size_t Size> std::string Integer<Size>::str_base16() const {
        std::stringstream ss;
        printBase16( ss );
        return ss.str();
    }

    /**
     * Prints to a stream the Big-Endian Base2 representation of the Integer
     * @param out Output stream
     */
    template<size_t Size> void Integer<Size>::printBase2( std::ostream &out ) const {
        size_t index = _bitset->size();
        while( index > 0 ) {
            --index;
            out << _bitset->test( index );
        }
    }

    /**
     * Prints to a stream the Base10 representation of the Integer
     * @param out Output stream
     */
    template<size_t Size> void Integer<Size>::printBase10( std::ostream &out ) const {
        if( this->isNegative() ) {
            auto total     = std::vector<unsigned>( { 1 } );
            auto bit_value = std::vector<unsigned>( { 1 } );

            for( size_t i = 0; i < _bitset->size() - 1; ++i ) {
                if( !_bitset->test( i ) )
                    total = add( bit_value, total );

                bit_value = x2( bit_value );
            }

            out << '-';
            for( unsigned digit : total )
                out << digit;

        } else { //positive
            auto total     = std::vector<unsigned>( { 0 } );
            auto bit_value = std::vector<unsigned>( { 1 } );

            for( size_t i = 0; i < _bitset->size() - 1; ++i ) {
                if( _bitset->test( i ) )
                    total = add( bit_value, total );

                bit_value = x2( bit_value );
            }

            for( unsigned digit : total )
                out << digit;
        }
    }

    /**
     * Prints to a stream the Base16 representation of the Integer
     * @param out Output stream (2's complement)
     */
    template<size_t Size> void Integer<Size>::printBase16( std::ostream &out ) const {
        /**
         * [LAMBDA] Extracts a subset of a bitset and returns its value
         */
        auto subset = []( const std::bitset<Size> &bitset, size_t offset, size_t length ) {
            char c    = 0b0000;
            char mask = 1;

            for( size_t i = offset; i < ( offset + length ); ++i, mask *= 2 ) {
                if( bitset.test( i ) )
                    c += mask;
            }

            return c;
        };

        auto   char_stack = std::stack<char>();
        size_t char_count = this->bitSize() / 4;
        size_t remainder  = this->bitSize() % 4;
        size_t lsb_index  = 0;

        for( size_t i = 0; i < char_count; ++i ) {
            auto c = subset( *this->_bitset, lsb_index, 4 );
            char_stack.emplace( bin2hex( c ) );
            lsb_index += 4;
        }

        if( remainder ) {
            auto c = subset( *this->_bitset, lsb_index, remainder );
            char_stack.emplace( bin2hex( c ) );
        }
        //remove any preceding '0'
        if( char_stack.top() == '0' )
            while( char_stack.size() > 1 && char_stack.top() == '0' )
                char_stack.pop();
        //print
        while( !char_stack.empty() ) {
            out << char_stack.top();
            char_stack.pop();
        }
    }

    /**
     * Input stream operator
     * @tparam Bits Bit size of the Integer
     * @param is Input stream taking in base10 integer digits
     * @param integer Integer
     * @return Input stream
     */
    template<size_t N> std::istream & operator >>( std::istream &is, Integer<N> &integer ) {
        integer.inputBase10( is );
        return is;
    }

    /**
     * Takes in a base2 (2's complement big-endian ordered) representation of a number to place in the Integer
     * @param in Base2 binary input stream (space delimitation allowed)
     * @throws std::invalid_argument when value is not a valid binary representation
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size> void Integer<Size>::inputBase2( std::istream &in ) {
        std::string str;
        std::getline( in, str );

        //ERROR CONTROL: bad input stream
        if( in.fail() ) {
            in.ignore(); //clear stream of any remaining garbage
            in.clear();  //clear the error bits
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Input stream errors encountered."
            );
        }

        //remove whitespace
        str.erase( std::remove_if( str.begin(), str.end(), isspace ), str.end() );

        //ERROR CONTROL: check empty
        if( str.empty() )
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Invalid binary string: empty."
            );

        //ERROR CONTROL: check string validity
        if( str.find_first_not_of( "01" ) != std::string::npos )
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Invalid binary string: malformed."
            );

        //copy out bits from string
        auto   bitset    = std::make_unique<std::bitset<Size>>();
        size_t bit_index = 0;
        auto   str_it    = str.crbegin();

        while( bit_index < bitset->size() && str_it != str.crend() ) {
            bitset->set( bit_index, ( *str_it == '1' ) );
            ++bit_index;
            ++str_it;
        }

        auto msb_val  = bitset->test( bit_index - 1 );
        char msb_char = ( msb_val ? 0x31 : 0x30 );

        //negative padding
        if( bit_index < bitset->size() && msb_val ) {
            while( bit_index < bitset->size() ) {
                bitset->set( bit_index, msb_val );
                ++bit_index;
            }
        }
        //check rest of binary str is consistent with the msb in the bitset
        while( str_it != str.crend() ) {
            if( *str_it != msb_char ) {
                if( str.front() == 0x30 ) { //('0' so positive)
                    throw std::overflow_error(
                        "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Binary number too large to fit this Integer size."
                    );
                } else { //0x31 ('1' so negative)
                    throw std::underflow_error(
                        "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Binary number too small to fit this Integer size."
                    );
                }
            }
            ++str_it;
        }

        this->_bitset.swap( bitset );
    }

    /**
     * Takes in a base10 representation of a number to place in the Integer
     * @param in Base10 decimal digit input stream
     * @throws std::invalid_argument when value is not a valid decimal representation
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size> void Integer<Size>::inputBase10( std::istream &in ) {
        std::string str;
        std::getline( in, str );

        //ERROR CONTROL: bad input stream
        if( in.fail() ) {
            in.ignore(); //clear stream of any remaining garbage
            in.clear();  //clear the error bits
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase10( std::istream & )] Input stream errors encountered."
            );
        }

        try {
            auto bitset = stringToBitSet( str );
            this->_bitset.swap( bitset );

        } catch( std::invalid_argument &e ) {
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase10( std::istream & )] " + std::string( e.what() )
            );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase10( std::istream & )] " + std::string( e.what() )
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase10( std::istream & )] " + std::string( e.what() )
            );
        }
    }

    /**
     * Takes in a base16 (2's complement) representation of a number to place in the Integer
     * @param in Base16 hexadecimal input stream
     * @throws std::invalid_argument when value is not a valid hexadecimal representation
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size> void Integer<Size>::inputBase16( std::istream &in ) {
        std::string str;
        std::getline( in, str );

        //ERROR CONTROL: bad input stream
        if( in.fail() ) {
            in.ignore(); //clear stream of any remaining garbage
            in.clear();  //clear the error bits
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase16( std::istream & )] Input stream errors encountered."
            );
        }

        //remove whitespace
        str.erase( std::remove_if( str.begin(), str.end(), isspace ), str.end() );

        //ERROR CONTROL: check empty
        if( str.empty() )
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase2( std::istream & )] Invalid hexadecimal string: empty."
            );

        //ERROR CONTROL: check string validity
        if( str.find_first_not_of( "0123456789ABCDEFabcdef" ) != std::string::npos )
            throw std::invalid_argument(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase16( std::istream & )] Invalid hexadecimal string: malformed."
            );

        //convert each hex character into its binary form
        std::stringstream ss;
        for( char c : str ) {
            ss << std::bitset<4>( hex2bin( c ) ).to_string();
        }

        try {
            this->inputBase2( ss );

        } catch( std::invalid_argument &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase16( std::istream & )] Error converting hexadecimal number to binary."
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase16( std::istream & )] Hexadecimal number too small to fit this Integer size."
            );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::inputBase16( std::istream & )] Hexadecimal number too large to fit this Integer size."
            );
        }
    }

    /**
     * Copy assignment
     * @param rhs Integer to copy over
     * @return Copied Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator =( const Integer<Size> &rhs ) {
        if( &rhs == this )
            return *this;

        for( size_t index = 0; index < _bitset->size(); index++ )
            _bitset->set( index, rhs._bitset->test( index ) );

        return *this;
    }

    /**
     * Move assignment
     * @param rhs Integer to move over
     * @return Moved Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator =( Integer<Size> &&rhs ) noexcept {
        _bitset.reset( rhs._bitset.release() );
        return *this;
    }

    /**
     * Swaps two Integers
     * @param rhs Integer to swap with
     */
    template<size_t Size> void Integer<Size>::swap( Integer<Size> &rhs ) {
        _bitset.swap( rhs._bitset );
    }

    /**
     * Equality operator
     * @param rhs Comparator
     * @return Equality state
     */
    template<size_t Size> bool Integer<Size>::operator ==( const Integer<Size> &rhs ) const {
        for( size_t i = 0; i < this->_bitset->size(); ++i )
            if( this->_bitset->test( i ) != rhs._bitset->test( i ) )
                return false;
        return true;
    }

    /**
     * Not-Equal operator
     * @param rhs Comparator
     * @return Not-Equal state
     */
    template<size_t Size> bool Integer<Size>::operator !=( const Integer<Size> &rhs ) const {
        return !( *this == rhs );
    }

    /**
     * Lesser-Than operator
     * @param rhs Comparator
     * @return Lesser-Than state
     */
    template<size_t Size> bool Integer<Size>::operator <( const Integer<Size> &rhs ) const {
        if( this->isNegative() && !rhs.isNegative() ) {
            return true;
        } else if( !this->isNegative() && rhs.isNegative() ) {
            return false;
        } else {
            //this and rhs are both positive or negative
            size_t index = _bitset->size();
            while( index > 0 ) {
                --index;
                if( !_bitset->test( index ) && rhs._bitset->test( index ) )
                    return true;
                if( _bitset->test( index ) && !rhs._bitset->test( index ) )
                    return false;
            }
        }
        return false;
    }

    /**
     * Larger-Than operator
     * @param rhs Comparator
     * @return Larger-Than state
     */
    template<size_t Size> bool Integer<Size>::operator >( const Integer<Size> &rhs ) const {
        return !( operator <( rhs ) || operator ==( rhs ) );
    }

    /**
     * Smaller-Equal operator
     * @param rhs Comparator
     * @return Smaller-Equal state
     */
    template<size_t Size> bool Integer<Size>::operator <=( const Integer<Size> &rhs ) const {
        return !( operator >( rhs ) );
    }

    /**
     * Bigger-Equal operator
     * @param rhs Comparator
     * @return Bigger-Equal state
     */
    template<size_t Size> bool Integer<Size>::operator >=( const Integer<Size> &rhs ) const {
        return !( operator <( rhs ) );
    }

    /**
     * Addition operator
     * @param rhs Integer to add
     * @return Sum
     * @throws std::overflow_error  when addition results in integer overflow
     * @throws std::underflow_error when addition results in integer underflow
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator +( const Integer<Size> &rhs ) const {
        auto   result = Integer<Size>();
        bool   carry  = false;

        for( size_t index = 0; index < this->_bitset->size(); ++index ) {
            result._bitset->set( index, ( this->_bitset->test( index ) ^ rhs._bitset->test( index ) ^ carry ) ); //XOR
            carry = carry
                    ? ( this->_bitset->test( index ) | rhs._bitset->test( index ) )
                    : ( this->_bitset->test( index ) & rhs._bitset->test( index ) ); //at least 2/3
        }

        //ERROR CONTROL: check for overflow (i.e.: result > max)
        if( !this->isNegative() && !rhs.isNegative() && result.isNegative() )
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator +( Integer )] Addition results in overflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );
        //ERROR CONTROL: check for underflow (i.e.: result < min)
        if( this->isNegative() && rhs.isNegative() && !result.isNegative() )
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator +( Integer )] Addition results in underflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );

        return result;
    }

    /**
     * Subtraction operator
     * @param rhs Subtrahend Integer
     * @return Difference
     * @throws std::overflow_error  when subtraction results in integer overflow
     * @throws std::underflow_error when subtraction results in integer underflow
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator -( const Integer<Size> &rhs ) const {
        auto negated_rhs = rhs.negate();

        if( rhs.isNegative() && negated_rhs.isNegative() ) { //i.e.: rhs == numeric_limits<Integer<Size>>::min()
            try {
                auto negated_this = this->negate();

                if( this->isNegative() && negated_this.isNegative() ) //i.e.: this == numeric_limits<Integer<Size>>::min()
                    return Integer<Size>(); //0
                else
                    return negated_this + rhs;

            } catch( std::overflow_error &e ) {
                throw std::underflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -( Integer )] Subtraction results in underflow (" + this->str_base10() + " - " + rhs.str_base10() + ")."
                );
            } catch( std::underflow_error &e ) {
                throw std::overflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -( Integer )] Subtraction results in overflow (" + this->str_base10() + " - " + rhs.str_base10() + ")."
                );
            }
        } else {
            try {
                return *this + negated_rhs;

            } catch( std::overflow_error &e ) {
                throw std::overflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -( Integer )] Subtraction results in overflow (" + this->str_base10() + " - " + rhs.str_base10() + ")."
                );
            } catch( std::underflow_error &e ) {
                throw std::underflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -( Integer )] Subtraction results in underflow (" + this->str_base10() + " - " + rhs.str_base10() + ")."
                );
            }
        }
    }

    /**
     * Multiplication operator
     * @param rhs Factor Integer
     * @return Product
     * @throws std::overflow_error  when multiplication results in integer overflow
     * @throws std::underflow_error when multiplication results in integer underflow
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator *( const Integer<Size> &rhs ) const {
        //EARLY RETURN: When one value is 0 or 1 in which case there is no need to proceed through the multiplication
        {
            static auto zero = Integer();
            if( this->compareTo( zero ) == 0 || rhs.compareTo( zero ) == 0 )
                return zero;

            static auto one  = Integer<sizeof( int ) * 8>( 1 );
            if( this->compareTo( one ) == 0 )
                return rhs;
            if( rhs.compareTo( one ) == 0 )
                return *this;
        }

        const size_t REG_SIZE = 1 + Size + Size + 1;

        /**
         * [LAMBDA] converts a multiplicand bitset<Size> to a bitset<REG_SIZE>
         */
        auto convert = []( const std::bitset<Size> &value, std::bitset<REG_SIZE> &target_register ) {
            size_t target_index = 1 + Size;
            for( size_t i = 0; i < value.size(); i++ )
                target_register.set( target_index + i, value.test( i ) );
            if( value.test( Size - 1 ) )
                target_register.set( REG_SIZE - 1, true );
        };

        /**
         * [LAMBDA] Adder helper function for bitset sections
         * @param reg Product register
         * @param val Value to add to the product register
         */
        auto add = []( std::bitset<REG_SIZE> &reg, const std::bitset<REG_SIZE> &val ) {
            bool   carry = false;

            for( size_t index = 0; index < REG_SIZE; ++index ) {
                auto bit_val = reg.test( index ) ^ val.test( index ) ^ carry; //XOR
                carry = carry
                        ? ( reg.test( index ) | val.test( index ) )
                        : ( reg.test( index ) & val.test( index ) ); //at least 2/3
                reg.set( index, bit_val );
            }
        };

        /* =========================================================================================
         * Andrew Donald Booth's 2's complement multiplication algorithm (Rock on BBK! \m/)
         * =========================================================================================
         * Product Register (p_reg) structure:
         * · {x} An extra bit to the left is used to deal with multiplication involving numeric_limit<>::min()
         * · {A} sub-register of length <Size>
         * · {B} sub-register of length <Size> (<- multiplier)
         * · {q} The "Booth bit" is added at the end of the register
         * */
        auto   p_reg = std::bitset<REG_SIZE>(); //product register: [x,[A],[B],q]
        size_t q     = 0;
        size_t b_lsb = 1;
        size_t b_msb = Size;
        //size_t a_lsb = Size + 1; //a_msb = Size + Size
        size_t steps = Size;

        auto   multiplicand      = std::bitset<REG_SIZE>(); //M
        auto   multiplicand_n    = std::bitset<REG_SIZE>(); //(-M) for easier subtraction using addition instead

        {
            //adapt multiplicands (original and negated) to register size
            auto negated = negate( *this->_bitset );
            convert( *this->_bitset, multiplicand );
            convert( *negated, multiplicand_n );

            //copy out multiplier bits (rhs) to the the sub-register 'B'
            for( size_t i = 0, r = b_lsb; i < rhs._bitset->size(); i++, r++ ) {
                p_reg.set( r, rhs._bitset->test( i ) );
            }
        }

        /*
         * Algorithm:
         * a) {B[lsb], q} = {0,1}
         *    · P = P+M (i.e.: P += M)
         *    · Arithmetic right bit-shift by 1
         * b) {B[lsb], q} = {1,0}
         *    · P = P-M (i.e.: P += (-M))
         *    · Arithmetic right bit-shift by 1
         * c) B[lsb] == q (i.e.: {0,0} or {1,1})
         *    · Arithmetic right bit-shift by 1
         * Repeat <steps> times.
         */
        for( size_t i = 0; i < steps; i++ ) {
            if( !p_reg.test( b_lsb ) && p_reg.test( q ) ) //{01}
                add( p_reg, multiplicand );
            else if( p_reg.test( b_lsb ) && !p_reg.test( q ) ) //{10}
                add( p_reg, multiplicand_n );
            //else {00} or {11}

            this->ars<REG_SIZE>( p_reg, 1 ); //arithmetic right shift
        }

        //copy sub-register [B] as result
        auto result = Integer<Size>();
        for( size_t out_i = 0, src_i = b_lsb; out_i < result._bitset->size(); ++out_i, ++src_i )
            result._bitset->set( out_i, p_reg.test( src_i ) );

        //ERROR CONTROL: overflow detection
        if( this->isNegative() == rhs.isNegative() ) {
            for( size_t index = b_msb; index < REG_SIZE; ++index ) //check sub-register B's msb, sub-register A and x
                if( p_reg.test( index ) )
                    throw std::overflow_error(
                        "[eadlib::Integer<" + std::to_string( Size ) + ">::operator *( Integer )] Multiplication results in overflow (" + this->str_base10() + " * " + rhs.str_base10() + ")."
                    );
        }
        //ERROR CONTROL: underflow detection
        if( this->isNegative() ^ rhs.isNegative() ) {
            for( size_t index = b_msb; index < REG_SIZE; ++index ) //check sub-register B's msb, sub-register A and x
                if( !p_reg.test( index ) )
                    throw  std::underflow_error(
                        "[eadlib::Integer<" + std::to_string( Size ) + ">::operator *( Integer )] Multiplication results in underflow (" + this->str_base10() + " * " + rhs.str_base10() + ")."
                    );
        }

        return result;
    }

    /**
     * Division operator
     * @param rhs Divider Integer
     * @return Result
     * @throw std::runtime_error when dividing by 0
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator /( const Integer<Size> &rhs ) const {
        if( rhs.isZero() )
            throw std::runtime_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator /( Integer )] Attempt to divide by 0 (" + this->str_base10() + " / " + rhs.str_base10() + ")."
            );
        return divide( *this, rhs ).first;
    }

    /**
     * Modulo operator
     * @param rhs Modulo Integer
     * @return Result
     * @throw std::runtime_error when dividing by 0
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator %( const Integer<Size> &rhs ) const {
        if( rhs == Integer<Size>() )
            throw std::runtime_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator %( Integer )] Attempt to divide by 0 (" + this->str_base10() + " % " + rhs.str_base10() + ")."
            );
        return divide( *this, rhs ).second;
    }

    /**
     * Bitwise NOT operator
     * @return Result of bitwise NOT
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator ~() const {
        return Integer( ~*this->_bitset );
    }

    /**
     * Bitwise AND operator
     * @param rhs Integer to AND against
     * @return Result of bitwise AND
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator &( const Integer<Size> &rhs ) const {
        auto r = std::make_unique<std::bitset<Size>>( *this->_bitset & *rhs._bitset );
        return Integer( std::move( r ) );
    }

    /**
     * Bitwise OR operator
     * @param rhs Integer to OR against
     * @return Result of bitwise OR
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator |( const Integer<Size> &rhs ) const {
        auto r = std::make_unique<std::bitset<Size>>( *this->_bitset | *rhs._bitset );
        return Integer( std::move( r ) );
    }

    /**
     * Bitwise XOR
     * @param rhs Integer to XOR against
     * @return Result of bitwise XOR
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator ^( const Integer<Size> &rhs ) const {
        auto r = std::make_unique<std::bitset<Size>>( *this->_bitset ^ *rhs._bitset );
        return Integer( std::move( r ) );
    }

    /**
     * Binary bit-shift left operator (as implemented by std::bitset::operator<<)
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator <<( size_t pos ) const {
        return Integer<Size>( *this->_bitset << pos );
    }

    /**
     * Binary bit-shift right operator (as implemented by std::bitset::operator>>)
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> Integer<Size>::operator >>( size_t pos ) const {
        return Integer<Size>( *this->_bitset >> pos );
    }

    /**
     * Addition-assignment operator
     * @param rhs Integer value to add
     * @return Result
     * @throws std::overflow_error  when addition results in integer overflow (pre-operation Integer will remain as-is)
     * @throws std::underflow_error when addition results in integer underflow (pre-operation Integer will remain as-is)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator +=( const Integer<Size> &rhs ) {
        try {
            auto result = this->operator+( rhs );
            this->swap( result );
            return *this;
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator +=( Integer )] Addition results in overflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator +=( Integer )] Addition results in underflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );
        }
    }

    /**
     * Subtraction-assignment operator
     * @param rhs Subtrahend Integer
     * @return Result
     * @throws std::overflow_error  when subtraction results in integer overflow (pre-operation Integer will remain as-is)
     * @throws std::underflow_error when subtraction results in integer underflow (pre-operation Integer will remain as-is)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator -=( const Integer<Size> &rhs ) {
        try {
            auto result = this->operator-( rhs );
            this->swap( result );
            return *this;
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -=( Integer )] Subtraction results in overflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator -=( Integer )] Subtraction results in underflow (" + this->str_base10() + " + " + rhs.str_base10() + ")."
            );
        }
    }

    /**
     * Multiplication-assignment operator
     * @param rhs Multiplier integer
     * @return Result
     * @throws std::overflow_error  when multiplication results in integer overflow (pre-operation Integer will remain as-is)
     * @throws std::underflow_error when multiplication results in integer underflow (pre-operation Integer will remain as-is)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator *=( const Integer<Size> &rhs ) {
        try {
            auto result = this->operator*( rhs );
            this->swap( result );
            return *this;
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator *=( Integer )] Multiplication results in overflow (" + this->str_base10() + " x " + rhs.str_base10() + ")."
            );
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator *=( Integer )] Multiplication results in underflow (" + this->str_base10() + " x " + rhs.str_base10() + ")."
            );
        }
    }

    /**
     * Division-assignment operator
     * @param rhs Divider Integer
     * @return Result
     * @throw std::runtime_error when dividing by 0 (pre-operation Integer will remain as-is)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator /=( const Integer<Size> &rhs ) {
        try {
            auto result = this->operator/( rhs );
            this->swap( result );
            return *this;
        } catch( std::runtime_error &e ) {
            throw std::runtime_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator /=( Integer )] Attempt to divide by 0 (" + this->str_base10() + " / " + rhs.str_base10() + ")."
            );
        }
    }

    /**
     * Modulo-assignment operator
     * @param rhs Modulo Integer
     * @return Modulo result
     * @throw std::runtime_error when dividing by 0 (pre-operation Integer will remain as-is)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator %=( const Integer<Size> &rhs ) {
        try {
            auto result = this->operator%( rhs );
            this->swap( result );
            return *this;
        } catch( std::runtime_error &e ) {
            throw std::runtime_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator %=( Integer )] Attempt to divide by 0 (" + this->str_base10() + " % " + rhs.str_base10() + ")."
            );
        }
    }

    /**
     * Bitwise AND assignment operator
     * @param rhs Integer to AND against
     * @return Result of bitwise AND
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator &=( const Integer<Size> &rhs ) {
        auto result = this->operator&( rhs );
        this->swap( result );
        return *this;
    }

    /**
     * Bitwise OR assignment operator
     * @param rhs Integer to OR against
     * @return Result of bitwise OR
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator |=( const Integer<Size> &rhs ) {
        auto result = this->operator|( rhs );
        this->swap( result );
        return *this;
    }

    /**
     * Bitwise XOR assignment operator
     * @param rhs Integer to XOR against
     * @return Result of bitwise XOR
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator ^=( const Integer<Size> &rhs ) {
        auto result = this->operator^( rhs );
        this->swap( result );
        return *this;
    }

    /**
     * Bitshift left assignment operator (as implemented by std::bitset::operator<<)
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator <<=( size_t pos ) {
        auto result = this->operator<<( pos );
        this->swap( result );
        return *this;
    }

    /**
     * Bitshift right assignment operator (as implemented by std::bitset::operator>>)
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator >>=( size_t pos ) {
        auto result = this->operator>>( pos );
        this->swap( result );
        return *this;
    }

    /**
     * Pre-increment operator
     * @return Incremented Integer
     * @throws std::overflow when increment leads to Integer overflow (increment will still happen)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator ++() {
        bool was_negative = this->isNegative();
        bool carry        = true;

        for( size_t index = 0; index < this->_bitset->size(); ++index ) {
            bool bit_val = this->_bitset->test( index ) ^ carry; //XOR
            carry = this->_bitset->test( index ) & carry; //AND
            this->_bitset->set( index, bit_val );
        }

        //ERROR CONTROL: check for overflow (i.e.: result > max)
        if( !was_negative && this->isNegative() )
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator++()] Pre-increment results in overflow ( ++" + this->str_base10() + ")."
            );

        return *this;
    }

    /**
     * Post-increment operator
     * @return Pre-incremented Integer
     * @throws std::overflow_error when increment leads to Integer overflow (increment will still happen)
     */
    template<size_t Size> const Integer<Size> Integer<Size>::operator ++( int ) {
        try {
            auto temp = *this;
            this->operator ++();
            return temp;
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator++( int )] Post-increment results in overflow ( ++" + this->str_base10() + ")."
            );
        }
    }

    /**
     * Pre-decrement operator
     * @return Decremented Integer
     * throws std::underflow_error when decrement leads to Integer underflow (decrement will still happen)
     */
    template<size_t Size> Integer<Size> & Integer<Size>::operator --() {
        bool was_negative = this->isNegative();
        bool carry        = false;

        for( size_t index = 0; index < this->_bitset->size(); ++index ) { //essentially doing (*this) + (-1)
            bool bit_val = !this->_bitset->test( index ) ^ carry;
            carry = this->_bitset->test( index ) | carry;
            this->_bitset->set( index, bit_val );
        }

        //ERROR CONTROL: check for underflow (i.e.: result < min)
        if( was_negative && !this->isNegative() )
            throw std::underflow_error(
                 "[eadlib::Integer<" + std::to_string( Size ) + ">::operator--()] Pre-decrement results in underflow ( ++" + this->str_base10() + ")."
            );

        return *this;
    }

    /**
     * Post-decrement operator
     * @return Pre-incremented Integer
     * @throws std::underflow_error when decrement leads to Integer underflow (decrement will still happen)
     */
    template<size_t Size> const Integer<Size> Integer<Size>::operator --( int ) {
        try {
            auto temp = *this;
            this->operator--();
            return temp;
        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::operator--( int )] Post-decrement results in underflow ( ++" + this->str_base10() + ")."
            );
        }
    }

    /**
     * 2-1 Division/Modulo operator
     * @param rhs Divider Integer
     * @return Result of division as a pair { Quotient, Remainder }
     * @throw std::runtime_error when dividing by 0
     */
    template<size_t Size>
        std::pair<Integer<Size>, Integer<Size>>
            Integer<Size>::divide( const Integer<Size> & rhs ) const
    {
        if( rhs.isZero() )
            throw std::runtime_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::divide( Integer )] Attempt to divide by 0 (" + this->str_base10() + " / " + rhs.str_base10() + ")."
            );
        return divide( *this, rhs );
    }

    /**
     * Raises an Integer to the given power (x^y)
     * @param exp Exponent as Integer value
     * @return Base raised to the power of exp
     * @throws std::overflow_error  when pow results in integer overflow
     * @throws std::underflow_error when pow results in integer underflow
     */
    template<size_t Size> Integer<Size> Integer<Size>::pow( const Integer<Size> &exp ) const {
        try {
            static const auto zero = Integer<Size>();
            static const auto one  = Integer<Size>( true ); //1
            static const auto two  = Integer<Size>( "2" );  //2

            if( exp < zero )
                return zero;
            if( exp.isZero() )
                return one;

            auto exponent = Integer<Size>( exp );
            auto result   = Integer<Size>( one ); //1
            auto base     = Integer<Size>( *this );

            while( exponent > zero ) {
                auto div_res = divide( exponent, two );

                if( div_res.second.isZero() ) { //i.e.: is remainder == 0?
                    exponent = div_res.first;
                    base *= base;
                } else {
                    --exponent;
                    result   *= base;
                    exponent /= two;
                    if( !exponent.isZero() )
                        base *= base;
                }
            }

            return result;

        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( Integer )] Power results in underflow (" + this->str_base10() + "^" + exp.str_base10() + ")."
            );
        } catch( std::overflow_error &e ) {
            std::cout << e.what() << std::endl;
            if( this->isNegative() )
                throw std::underflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( Integer )] Power results in underflow (" + this->str_base10() + "^" + exp.str_base10() + ")."
                );
            else
                throw std::overflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( Integer )] Power results in overflow (" + this->str_base10() + "^" + exp.str_base10() + ")."
                );
        }
    }

    /**
     * Raises an Integer to the given power (x^y)
     * @param iexp Exponent as integer value
     * @return Base raised to the power of iexp
     * @throws std::overflow_error  when pow results in integer overflow
     * @throws std::underflow_error when pow results in integer underflow
     */
    template<size_t Size> Integer<Size> Integer<Size>::pow( int iexp ) const {
        try {
            if( iexp <  0 )
                return Integer<Size>(); //0
            if( iexp == 0 )
                return Integer<Size>( true ); //1

            auto exponent = iexp;
            auto result   = Integer<Size>( true ); //1
            auto base     = Integer<Size>( *this );

            while( exponent > 0 ) {
                if( exponent % 2 == 0 ) {
                    exponent /= 2;
                    base *= base;
                } else {
                    --exponent;
                    result *= base;
                    exponent    /= 2;
                    if( exponent != 0 )
                        base *= base;
                }
            }

            return result;

        } catch( std::underflow_error &e ) {
            throw std::underflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( int )] Power results in underflow (" + this->str_base10() + "^" + std::to_string( iexp ) + ")."
            );
        } catch( std::overflow_error &e ) {
            if( this->isNegative() )
                throw std::underflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( int )] Power results in underflow (" + this->str_base10() + "^" + std::to_string( iexp ) + ")."
                );
            else
                throw std::overflow_error(
                    "[eadlib::Integer<" + std::to_string( Size ) + ">::pow( int )] Power results in overflow (" + this->str_base10() + "^" + std::to_string( iexp ) + ")."
                );
        }
    }

    /**
     * Modular Pow
     * @param exp Exponent
     * @param mod Modulus
     * @return Result
     * @throw std::overflow_error when modulus overflows base
     */
    template<size_t Size>
        Integer<Size> Integer<Size>::modpow(
            const Integer<Size> &exp,
            const Integer<Size> &mod ) const
    {
        //EARLY RETURN
        if( mod == Integer<Size>( true ) ) //mod == 1
            return Integer<Size>();
        //ERROR CONTROL: check (modulus - 1) * (modulus - 1) does not overflow base
        try {
            ( mod - Integer<Size>( true ) ) * ( mod - Integer<Size>( true ) );
        } catch( std::overflow_error &e ) {
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::modpow( " + exp.str_base10() + ", " + mod.str_base10() + " )] Overflow detected."
            );
        }

        auto result   = Integer<Size>( true ); //1
        auto base     = *this % mod;
        auto exponent = exp;

        while( exponent > Integer() ) { //i.e.: exponent > 0
            if( exponent % Integer<Size>( 2 ) == Integer( true ) )
                result = ( result * base ) % mod;
            exponent >>= 1;
            base = ( base * base ) % mod;
        }
        return result;
    }

    /**
     * Square root (floored) of the Integer
     * @return Floored square root of abs( Integer )
     * @throws std::overflow_error when Integer cannot be converted with abs as it is std::numeric_limit<Integer<Size>>::min()
     */
    template<size_t Size> Integer<Size> Integer<Size>::sqrt() const {
        auto val = Integer<Size>( this->abs() );
        auto one = Integer<Size>( true ); //cached '1'

        //ERROR CONTROL: hit negative numeric limit
        if( val.isNegative() )
            throw std::overflow_error(
                "[eadlib::Integer<" + std::to_string( Size ) + ">::sqrt()] Integer cannot be converted to its absolute value for sqrt()."
            );

        //EARLY RETURN: when Integer is 0-1
        if( val.isZero() || val == one )
            return val;

        auto res = Integer<Size>();

        auto bit = Integer( one ) << ( Size - 2 );
        while( bit > val )
            bit >>= 2;

        while( !bit.isZero() ) {
            if( val >= res + bit ) {
                val -= res + bit;
                res = res.lrsSelf( 1 ) + bit;
            } else {
                res.lrsSelf( 1 );
            }
            bit.lrsSelf( 2 );
        }

        return res;
    }

    /**
     * Guaranteed binary Arithmetic-Right-Shift
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> Integer<Size>::ars( size_t pos ) const {
        auto bitset = std::make_unique<std::bitset<Size>>( *this->_bitset );
        ars( *bitset, pos );
        return Integer<Size>( std::move( bitset ) );
    }

    /**
     * Guaranteed binary Logical-Right-Shift
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> Integer<Size>::lrs( size_t pos ) const {
        auto bitset = std::make_unique<std::bitset<Size>>( *this->_bitset );
        lrs( *bitset, pos );
        return Integer<Size>( std::move( bitset ) );
    }

    /**
     * Guaranteed binary Arithmetic-Right-Shift assignment
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::arsSelf( size_t pos ) {
        ars( *this->_bitset, pos );
        return *this;
    }

    /**
     * Guaranteed binary Logical-Right-Shift
     * @param pos Number of bit locations to be shifted
     * @return Bit-shifted Integer
     */
    template<size_t Size> Integer<Size> & Integer<Size>::lrsSelf( size_t pos ) {
        lrs( *this->_bitset, pos );
        return *this;
    }

    /**
     * Negates the Integer
     * @return Negated version of the Integer
     */
    template<size_t Size> Integer<Size> Integer<Size>::negate() const {
        auto integer = Integer<Size>();
        integer._bitset = negate( *this->_bitset );
        return integer;
    }

    /**
     * Compares against another instance of Integer
     * @tparam N Bit size of rhs Integer
     * @param rhs Integer to compare with
     * @return Comparison result against rhs (-1: this is smaller, 0: this is equal, +1: this is bigger)
     */
    template<size_t Size> template<size_t N> int Integer<Size>::compareTo( const Integer<N> &rhs ) const {
        if( this->isNegative() != rhs.isNegative() ) //different signs
            return this->isNegative() ? -1 : 1;

        size_t index_this = this->bitSize();
        size_t index_rhs  = rhs.bitSize();

        if( this->bitSize() > rhs.bitSize() ) {
            while( index_this > index_rhs ) {
                --index_this;
                if( this->_bitset->test( index_this ) != this->isNegative() )
                    return -1;
            }
        } else { //this->bitSize() <= rhs.bitSize() )
            while( index_rhs > index_this ) {
                --index_rhs;
                if( rhs.bitset().test( index_rhs ) != this->isNegative() )
                    return 1;
            }
        }

        while( index_rhs > 0 ) { //index_this == index_rhs
            --index_this;
            --index_rhs;
            if( this->_bitset->test( index_this ) != rhs.bitset().test( index_rhs ) )
                return this->_bitset->test( index_this ) ? 1 : -1;
        }

        return 0;
    }

    /**
     * Gets the absolute value of the Integer
     * @return Math abs value
     */
    template<size_t Size> Integer<Size> Integer<Size>::abs() const {
        if( this->isNegative() )
            return ( Integer<Size>( *this ) ).negate();
        else
            return *this;
    }

    /**
     * Check negative Integer state
     * @return Negative state
     */
    template<size_t Size> bool Integer<Size>::isNegative() const {
        return _bitset->test( Size - 1 );
    }

    /**
     * Checks if Integer == 0
     * @return Zero state
     */
    template<size_t Size> bool Integer<Size>::isZero() const {
        return this->_bitset->count() == 0;
    }

    /**
     * Gets the bit size of the underlining bit structure
     * @return Bit size
     */
    template<size_t Size> size_t Integer<Size>::bitSize() const {
        return _bitset->size();
    }

    /**
     * Gets the minimum required number of bits to store the signed integer value into
     * @return Bit size requirement
     */
    template<size_t Size> size_t Integer<Size>::minReqBits() const {
        if( this->isNegative() ) {
            auto integer = this->negate();
            return integer.isNegative()
                   ? integer.bitSize() //has got to be Integer<Size>::min()
                   : integer.minReqBits();
        }

        //is positive
        size_t index = this->_bitset->size();
        while( index > 0 ) {
            --index;
            if( this->_bitset->test( index ) )
                return index + 2; //1st msb is found
        }

        return 2; //minimum amount of bits to express signed values (-1, 0, +1).
    }

    /**
     * Gets read access to the underlining bitset of the Integer
     * @return Raw bitset access
     */
    template<size_t Size> const std::bitset<Size> & Integer<Size>::bitset() const {
        return *_bitset;
    }

    /**
     * Gets the min numeric limit of the Integer
     * @tparam Size Bit size of integer
     * @return Numeric limit minimum
     */
    template<size_t Size> Integer<Size> Integer<Size>::min() {
        auto integer = Integer<Size>();
        integer._bitset->set( Size - 1, 1 );
        return integer;
    }

    /**
     * Gets the max numeric limit of the Integer
     * @tparam Size Bit size of the integer
     * @return Numeric limit maximum
     */
    template<size_t Size> Integer<Size> Integer<Size>::max() {
        auto integer = Integer<Size>();
        for( size_t index = 0; index < integer._bitset->size() - 1; ++index )
            integer._bitset->set( index, 1 );
        return integer;
    }

    /**
     * Converts an integer to a bitset
     * @param val Integer value
     * @return Bitset
     * @throws std::overflow when T value cannot fit into target bitset
     */
    template<size_t Size> template<typename T>
        std::unique_ptr<std::bitset<Size>> Integer<Size>::integerToBitset( T val ) const
    {
        constexpr size_t min_req_bits = sizeof( T ) * 8;

        //ERROR CONTROL: Integer bitset can't fit value type
        if( Size < min_req_bits )
            throw std::overflow_error(
                "Integer size (" + std::to_string( Size ) + "b) too small to fit T value (" + std::to_string( min_req_bits ) + "b)."
            );

        //EARLY RETURN: value is guaranteed to fit bitset size
        if( Size > min_req_bits )
            return std::make_unique<std::bitset<Size>>( val );

        auto   out     = std::make_unique<std::bitset<Size>>();
        auto   src     = std::bitset<min_req_bits>( val );
        size_t src_msb = src.size() - 1;
        size_t src_i   = 0;
        size_t out_i   = 0;

        while( src_i < src_msb ) { //copy src bits to out
            out->set( out_i, src.test( src_i ) );
            src_i++;
            out_i++;
        }

        if constexpr ( std::numeric_limits<T>::is_signed ) {
            constexpr size_t sign_bit = sizeof( T ) * 8 - 1;

            if( src.test( sign_bit ) ) { //i.e.: is it negative?
                while( out_i < out->size() ) {
                    out->set( out_i, 1 );
                    out_i++;
                }
            }

        } else { //unsigned
            if( min_req_bits == Size && src.test( src_msb ) )
                throw std::overflow_error(
                    "Integer size (" + std::to_string( Size ) + "b) too small to fit unsigned T value (" + std::to_string( min_req_bits ) + "b)."
                );
            out->set( out_i, src.test( src_i ) );
        }

        return std::move( out );
    }

    /**
     * Converts a string representation of an base10 integer to a bitset
     * @param str String-ed integer
     * @return Bitset
     * @throws std::invalid_argument when value is not a valid integer
     * @throws std::overflow_error   when positive value cannot fit into Integer<Size>
     * @throws std::underflow_error  when negative value cannot fit into Integer<Size>
     */
    template<size_t Size>
        std::unique_ptr<std::bitset<Size>>
            Integer<Size>::stringToBitSet( std::string str ) const
    {
        //Remove whitespace and commas
        str.erase( std::remove_if( str.begin(), str.end(), isspace ), str.end() );
        str.erase( std::remove( str.begin(), str.end(), ',' ), str.end() );

        //ERROR CONTROL: check empty
        if( str.empty() )
            throw std::invalid_argument( "Invalid integer string: empty." );

        //Check < 0 and remove sign from string
        bool is_negative = ( str.front() == '-' );

        if( is_negative ) {
            size_t new_begin = str.find_first_not_of( '-' );
            if( new_begin != std::string::npos )
                str = str.substr( new_begin );
        }

        //Remove any leading zeros
        size_t new_begin = str.find_first_not_of( '0' );
        if( new_begin != std::string::npos )
            str = str.substr( new_begin );
        else //EARLY RETURN: Zero value
            return std::make_unique<std::bitset<Size>>();

        //ERROR CONTROL: check string validity
        if( str.find_first_not_of( "0123456789" ) != std::string::npos )
            throw std::invalid_argument( "Invalid integer string: malformed." );

        //Convert char digits to unsigned list of digits
        auto digits = std::list<unsigned>();
        for( const auto &d : str )
            digits.emplace_back( d % 0x30 );

        //Keep dividing by 2 the represented value in list and checking the remainder for the bit value
        auto queue = std::deque<bool>();
        while( !digits.empty() )
            queue.emplace_back( d2( digits ) > 0 );

        auto req_bits = queue.size();
        auto bitset   = std::make_unique<std::bitset<Size>>();

        //Place bits values into the bitset in order
        size_t bit_index = 0;
        while( !queue.empty() && bit_index < bitset->size() ) {
            bitset->set( bit_index, queue.front() );
            queue.pop_front();
            ++bit_index;
        }

        //ERROR CONTROL: check for overflow/underflow
        if( !queue.empty() || ( !is_negative && bitset->test( Size - 1 ) ) ) {
            //remaining bits to place or the negative bit flipped on positive integer
            if( is_negative ) { //i.e.:: value < min
                throw std::underflow_error(
                    "Integer size (" + std::to_string( Size ) + "b) too small to fit unsigned T value (" + std::to_string( req_bits ) + "b)."
                );
            } else { //i.e.: value > max
                throw std::overflow_error(
                    "Integer size (" + std::to_string( Size ) + "b) too small to fit unsigned T value (" + std::to_string( req_bits ) + "b)."
                );
            }
        }

        if( is_negative ) { //flip bits and add 1
            bitset->flip();

            bool   carry = true;
            size_t index = 0;
            while( index < bitset->size() ) {
                bool bit_val = bitset->test( index ) ^ carry; //XOR
                carry = bitset->test( index ) & carry; //AND
                bitset->set( index, bit_val );
                ++index;
            }

            //ERROR CONTROL: check for underflow (i.e.: value < min)
            if( carry || !bitset->test( Size - 1 ) )
                throw std::underflow_error( //remaining bit to place or negative bit flipped to positive (underflow)
                    "Integer size (" + std::to_string( Size ) + "b) too small to fit unsigned T value (" + std::to_string( req_bits ) + "b)."
                );
        }

        return std::move( bitset );
    }

    /**
     * Multiplies an integer represented in a string by 2
     * @param val Integer string value
     * @return Double of the val
     */
    template<size_t Size>
        std::vector<unsigned> Integer<Size>::x2( const std::vector<unsigned> &val ) const
    {
        auto     stack = std::stack<unsigned>();
        unsigned carry = 0;

        for( auto it = val.crbegin(); it != val.crend(); ++it ) {
            unsigned r = *it * unsigned( 2 ) + carry;
            stack.push( r % unsigned( 10 ) );
            carry = r / unsigned( 10 );
        }

        if( carry )
            stack.push( carry );

        auto result = std::vector<unsigned>();

        while( !stack.empty() ) {
            result.emplace_back( stack.top() );
            stack.pop();
        }

        return result;
    }

    /**
     * Divide an integer represented as string by 2
     * @param val Integer string value
     * @return Remainder of the division
     */
    template<size_t Size>
        unsigned Integer<Size>::d2( std::list<unsigned> &val ) const
    {
        unsigned carry  = 0;

        for( unsigned &digit : val ) {
            auto r = ( digit + carry ) / 2;
            carry = ( digit % 2 ) * 10;
            digit = r;
        }

        auto it = val.begin();
        while( it != val.end() && *it == 0 )
            it = val.erase( it );

        return carry;
    }

    /**
     * Add 2 vector of digits together
     * @param a Vector A
     * @param b Vector B
     * @return Total
     */
    template<size_t Size>
        std::vector<unsigned> Integer<Size>::add(
            const std::vector<unsigned> &a,
            const std::vector<unsigned> &b ) const
    {
        auto     stack = std::stack<unsigned>();
        unsigned carry = 0;

        auto it_a = a.crbegin();
        auto it_b = b.crbegin();

        while( it_a != a.crend() && it_b != b.crend() ) {
            unsigned r = *it_a + *it_b + carry;
            stack.push( r % 10 );
            carry = r / 10;
            ++it_a;
            ++it_b;
        }

        //Deal with any remaining digits from the larger vector
        if( it_a != a.crend() ) {
            while( it_a != a.crend() ) {
                unsigned r = *it_a + carry;
                stack.push( r % 10 );
                carry = r / 10;
                ++it_a;
            }
        } else if( it_b != b.crend() ) {
            while( it_b != b.crend() ) {
                unsigned r = *it_b + carry;
                stack.push( r % 10 );
                carry = r / 10;
                ++it_b;
            }
        }

        if( carry )
            stack.push( carry );

        auto result = std::vector<unsigned>();

        while( !stack.empty() ) {
            result.emplace_back( stack.top() );
            stack.pop();
        }

        return result;
    }

    /**
     * Divide an Integer against another
     * @param dividend Dividend Integer
     * @param divisor  Divisor Integer
     * @return std::pair where first: Quotient and second: Remainder
     */
    template<size_t Size>
        std::pair<Integer<Size>, Integer<Size>> Integer<Size>::divide(
            const Integer<Size> & dividend,
            const Integer<Size> & divisor ) const
    {
        const auto dividend_abs = ( dividend.isNegative() ? dividend.negate() : dividend );
        const auto divisor_abs  = ( divisor.isNegative()  ? divisor.negate()  : divisor );

        auto compare = dividend_abs.compareTo( divisor_abs );

        //EARLY RETURNS
        if( compare < 0 )
            return std::make_pair( Integer<Size>(), dividend );
        if( compare == 0 )
            return std::make_pair( Integer<Size>( true ), Integer<Size>() ); //a.k.a.: {1, 0}
        if( divisor_abs == Integer<Size>( true ) ) //divided by 1
            return std::make_pair( ( dividend.isNegative() ^ divisor.isNegative() ? dividend.negate() : dividend ), Integer<Size>() );

        //reaches here when dividend > divisor && divisor > 1
        auto   quotient  = Integer<Size>();
        auto   remainder = dividend_abs; //i.e. absolute value of the dividend
        auto   q         = Integer<Size>( true ); //1
        auto   d         = divisor_abs;
        size_t bit_shift = 0;

        //Step 1a: keep doubling q and d until reaching remainder
        while( d <= remainder ) {
            q <<= 1; //double
            d <<= 1; //double
            ++bit_shift;
            if( d.isNegative() )
                break; //has overflowed
        }
        //Step 1b: reverse last loop so that d is < remainder
        if( bit_shift ) {
            q >>= 1;
            d >>= 1;
            --bit_shift;
        }

        //Step 2: half q and d and remove d from the remainder until < absolute value of divisor
        while( remainder >= divisor_abs ) {
            while( d > remainder && bit_shift > 0 ) {
                q >>= 1; //halve
                d >>= 1; //halve
                --bit_shift;
            }

            quotient  += q; //current q is added to the quotient total
            remainder -= d;
        }

        return std::make_pair(
            ( dividend.isNegative() ^ divisor.isNegative() ? quotient.negate()  : quotient ),
            ( dividend.isNegative()                        ? remainder.negate() : remainder )
        );
    }

    /**
     * Negates the Integer represented in a bitset
     * @param bitset Bitset to generate a negated version from
     * @return Negated bitset
     */
    template<size_t Size> std::unique_ptr<std::bitset<Size>> Integer<Size>::negate( const std::bitset<Size> & bitset ) const {
        auto negated = std::make_unique<std::bitset<Size>>();

        //flip
        for( size_t index = 0; index < bitset.size(); ++index )
            negated->set( index, !bitset.test( index ) );

        //add 1
        bool carry = true;

        for( size_t index = 0; index < negated->size(); ++index ) {
            bool bit_val = negated->test( index ) ^ carry; //XOR
            carry = negated->test( index ) & carry; //AND
            negated->set( index, bit_val );
        }

        return std::move( negated );
    }

    /**
     * Arithmetic binary Right Shift
     *   e.g.: asr( 0101 ) = 0010, asr( 1001 ) = 1100
     * @tparam N     Bitset size
     * @param bitset Bitset to shift
     * @param pos    Number of bit locations to be shifted
     * @return Reference to shifted bitset
     */
    template<size_t Size> template<size_t N>
        std::bitset<N> & Integer<Size>::ars( std::bitset<N> &bitset, size_t pos ) const
    {
        size_t msb     = N - 1;
        bool   flag    = bitset.test( msb );

        if( pos == 0 )
            return bitset;
        if( pos > bitset.size() )
            return ( flag ? bitset.set() : bitset.reset() );

        size_t write_i = 0;
        size_t read_i  = pos;

        while( read_i < bitset.size() ) {
            bitset.set( write_i, bitset.test( read_i ) );
            ++write_i;
            ++read_i;
        }

        for( ; write_i <  bitset.size(); ++write_i )
            bitset.set( write_i, flag );

        return bitset;
    }

    /**
     * Logical binary Right Shift
     *   e.g.: lrs( 0101 ) = 0010, lrs( 1001 ) = 0101
     * @tparam N     Bitset size
     * @param bitset Bitset to shift
     * @param pos    Number of bit locations to be shifted
     * @return Reference to shifted bitset
     */
    template<size_t Size> template<size_t N>
        std::bitset<N> & Integer<Size>::lrs( std::bitset<N> & bitset, size_t pos ) const
    {
        if( pos == 0 )
            return bitset;
        if( pos > bitset.size() )
            return bitset.reset();

        size_t write_i = 0;
        size_t read_i  = pos;

        while( read_i < bitset.size() ) {
            bitset.set( write_i, bitset.test( read_i ) );
            ++write_i;
            ++read_i;
        }

        for( ; write_i <  bitset.size(); ++write_i )
            bitset.set( write_i, false );

        return bitset;
    }
}

#endif //EADLIB_INTEGER_H