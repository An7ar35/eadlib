#ifndef EADLIB_ENUM_CAST_H
#define EADLIB_ENUM_CAST_H

#include <type_traits>

namespace eadlib {
    /**
     * Converts a scoped enum to integer
     * @tparam E Enumeration type
     * @param value Enumeration value
     * @return Integer value
     */
    template<typename E> auto enum_int( E const value ) -> typename std::underlying_type<E>::type {
        return static_cast<typename std::underlying_type<E>::type>( value );
    }

    /**
     * Converts a scoped enum's integer to string
     * @tparam E Enumeration type
     * @param value Enumeration value
     * @return Integer value as a string
     */
    template<typename E> std::string to_string( E const value ) {
        return std::to_string( enum_int<E>( value ) );
    }

    template<typename E> constexpr auto to_underlying( E const value ) noexcept {
        return static_cast<std::underlying_type_t<E>>( value );
    }
}

#endif //EADLIB_ENUM_CAST_H
