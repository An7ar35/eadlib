/**
   Component Name:  eadlib.exception.aborted_operation
   Language:        C++14

   License: GNUv2 Public License
   (c) Copyright E. A. Davison 2018

   Author: E. A. Davison

   Description: Exception for when an operation had to be aborted mid flow
**/
#ifndef EADLIB_ABORTED_H
#define EADLIB_ABORTED_H

#include <string>
#include <exception>
#include <utility>

namespace eadlib::exception {
    class aborted_operation : public std::exception {
      public:
        explicit aborted_operation( std::string msg ) : _msg( std::move( msg ) ) {}

        const char *what() const noexcept override {
            return _msg.c_str();
        }

      private:
        std::string _msg;
    };
}

#endif //EADLIB_ABORTED_H
