#ifndef EADLIB_BAD_ASSIGNEMENT_H
#define EADLIB_BAD_ASSIGNEMENT_H

#include <string>
#include <exception>

namespace eadlib::exception {
    class bad_assignment : public std::exception {
      public:
        bad_assignment( const std::string &msg ) : m_msg( msg ) {}

        virtual const char *what() const throw() {
            return m_msg.c_str();
        }

      private:
        std::string m_msg;
    };
}

#endif //EADLIB_BAD_ASSIGNEMENT_H
