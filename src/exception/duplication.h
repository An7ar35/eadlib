#ifndef EADLIB_DUPLICATION_H
#define EADLIB_DUPLICATION_H

#include <string>
#include <exception>

namespace eadlib {
    namespace exception {
        class duplication : public std::exception {
          public:
            duplication( const std::string &msg ) : m_msg( msg ) {}

            virtual const char *what() const throw() {
                return m_msg.c_str();
            }

          private:
            std::string m_msg;
        };
    }
}

#endif //EADLIB_DUPLICATION_H
