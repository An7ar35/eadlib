/**
   Component Name:  eadlib.exception.resource_missing
   Language:        C++14

   License: GNUv2 Public License
   (c) Copyright E. A. Davison 2018

   Author: E. A. Davison

   Description: Exception for when a resource does not exists
**/
#ifndef EADLIB_RESOURCE_MISSING_H
#define EADLIB_RESOURCE_MISSING_H

#include <string>
#include <exception>
#include <utility>

namespace eadlib {
    namespace exception {
        class resource_missing : public std::exception {
          public:
            explicit resource_missing( std::string msg ) : _msg( std::move( msg ) ) {}

            const char *what() const noexcept override {
                return _msg.c_str();
            }

          private:
            std::string _msg;
        };
    }
}

#endif //EADLIB_RESOURCE_MISSING_H
