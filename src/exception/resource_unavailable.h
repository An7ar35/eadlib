/**
   Component Name:  eadlib.exception.resource_unavailable
   Language:        C++14

   License: GNUv2 Public License
   (c) Copyright E. A. Davison 2018

   Author: E. A. Davison

   Description: Exception for when a resource exists but could not be reached (e.g.: in the case of threading)
**/
#ifndef EADLIB_RESOURCE_UNAVAILABLE_H
#define EADLIB_RESOURCE_UNAVAILABLE_H

#include <string>
#include <exception>
#include <utility>

namespace eadlib {
    namespace exception {
        class resource_unavailable : public std::exception {
          public:
            explicit resource_unavailable( std::string msg ) : _msg( std::move( msg ) ) {}

            const char *what() const noexcept override {
                return _msg.c_str();
            }

          private:
            std::string _msg {};
        };
    }
}

#endif //EADLIB_RESOURCE_UNAVAILABLE_H
