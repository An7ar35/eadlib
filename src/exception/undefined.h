#ifndef EADLIB_UNDEFINED_H
#define EADLIB_UNDEFINED_H

#include <string>
#include <exception>

namespace eadlib {
    namespace exception {
        class undefined : public std::exception {
          public:
            undefined( const std::string &msg ) : m_msg( msg ) {}

            virtual const char *what() const throw() {
                return m_msg.c_str();
            }

          private:
            std::string m_msg;
        };
    }
}

#endif //EADLIB_UNDEFINED_H
