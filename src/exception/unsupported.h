#ifndef EADLIB_UNSUPPORTED_H
#define EADLIB_UNSUPPORTED_H

#include <string>
#include <exception>

namespace eadlib {
    namespace exception {
        class unsupported : public std::exception {
          public:
            unsupported( const std::string &msg ) : m_msg( msg ) {}

            virtual const char *what() const throw() {
                return m_msg.c_str();
            }

          private:
            std::string m_msg;
        };
    }
}

#endif //EADLIB_UNSUPPORTED_H
