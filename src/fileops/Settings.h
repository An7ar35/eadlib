/**
   Component Name:  eadlib.fileops.Settings
   Language:        C++14

   License: GNUv2 Public License
   (c) Copyright E. A. Davison 2016

   Author: E. A. Davison

   Description: Key-Value pair config file reader
   Dependencies: eadlib.tool.Convert, eadlib.exception.resource_missing
**/
#ifndef EADLIB_SETTINGS_H
#define EADLIB_SETTINGS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <iterator>

#include "../tool/Convert.h"
#include "fileops_utils.h"
#include "../exception/resource_missing.h"

namespace eadlib {
    namespace fileIO {
        class Settings {
          public:
            explicit Settings( const std::string &config_file );
            ~Settings() = default;
            void readFile();
            void writeFile();
            void clearSettings();

            /**
             * Writes into the configuration the given key-value pair
             * @param key Key
             * @param value Value
             * @return Success
             */
            template<typename T> bool writeConfig( const std::string &key, const T &value ) {
                if( this->keyExists( key )) {
                    this->_config_content.find( key )->second = eadlib::tool::Convert::to_string( value );
                } else {
                    this->_config_content.insert(
                        std::make_pair<std::string, std::string>( key, eadlib::tool::Convert::to_string( value )));
                }
                writeFile();
                return true;
            }

            /**
             * Gets the Value of a specified Key
             * @param key Key
             * @param defaultValue Default value of the key
             * @return Value
             */
            template<typename T> T getValueOfKey( const std::string &key, T defaultValue ) {
                readFile();
                if( this->keyExists( key )) {
                    return eadlib::tool::Convert::string_to_type<T>(
                        _config_content.find( key )->second );
                } else {
                    return defaultValue;
                }
            }
          private:
            std::map<std::string, std::string> _config_content;
            std::string _fileName;
            //Private methods
            void extractContents( const std::string &line );
            void parseLine( const std::string &line, size_t lineNumber );
            void removeComment( std::string &line ) const;
            bool onlyWhitespace( const std::string &line ) const;
            bool validLine( const std::string &line ) const;

            /**
             * Checks if a key exists in the config_content map
             * @param key Key to check
             * @return Its existence
             */
            bool keyExists( const std::string &key ) {
                return _config_content.find( key ) != _config_content.end();
            }
        };

        /**
         * Constructor
         * @param config_file Name of the configuration file
         */
        inline Settings::Settings( const std::string &config_file ) {
            _fileName = config_file;
            readFile();
        }

        /**
         * Reads in from the configuration file and stores found key-value pairs
         * @throws eadlib::exception::resource_missing when issue arise accessing the setting file
         */
        inline void Settings::readFile() {
            clearSettings();
            if( !std::ifstream( _fileName ) ) {
                std::ofstream file( _fileName );
                if( !file )
                    throw eadlib::exception::resource_missing(
                        "Could not create new Settings file \"" + _fileName  + "\"."
                    );
            }
            std::ifstream file;
            file.open( _fileName );
            if( !file )
                throw eadlib::exception::resource_missing( "Could not find file \"" + _fileName  + "\"." );
            std::string line { };
            size_t lineNumber = 0;
            while( std::getline( file, line ) ) {
                lineNumber++;
                std::string temp = line;
                if( !temp.empty() ) {
                    removeComment( temp );
                    if( !onlyWhitespace( temp ) ) {
                        parseLine( temp, lineNumber );
                    }
                }
            }
            file.close();
        }

        /**
         * Writes config_content to file
         */
        inline void Settings::writeFile() {
            std::ofstream file;
            file.open( _fileName );
            for( auto item : _config_content ) {
                file << item.first << "=" << item.second;
                file << "\n";
            }
            file.close();
        }

        /**
         * Clears all settings loaded
         */
        inline void Settings::clearSettings() {
            _config_content.clear();
        }

        ///*********************************************************************
        /// Private methods
        ///*********************************************************************
        inline void Settings::extractContents( const std::string &line ) {
            /**
             * [Lambda] Extracts the Key from a line
             * @param delimiter Key-Value separator ('=')
             * @param line Line
             * @return The key
             */
            auto extractKey = []( size_t const &delimiter, const std::string &line ) {
                std::string key = line.substr( 0, delimiter );
                if( key.find( '\t' ) != std::string::npos || key.find( ' ' ) != std::string::npos ) {
                    key.erase( key.find_first_of( "\t " ) );
                }
                return key;
            };
            /**
             * [Lambda] Extracts the Value from a line
             * @param delimiter Key-Value separator ('=')
             * @param line Line
             * @return The value
             */
            auto extractValue = []( size_t const &delimiter, const std::string &line ) {
                std::string value = line.substr( delimiter + 1 );
                value.erase( 0, value.find_first_not_of( "\t " ) );
                value.erase( value.find_last_not_of( "\t " ) + 1 );
                return value;
            };

            std::string temp = line;
            temp.erase( 0, temp.find_first_not_of( "\t " ) ); // Erase leading whitespace
            size_t delimiter = temp.find( '=' );

            std::string key = extractKey( delimiter, temp );
            std::string value = extractValue( delimiter, temp );

            if( !keyExists( key ) ) {
                _config_content.insert( std::make_pair( key, value ) );
            } else {
                std::cerr << "[eadlib::fileIO::Settings::extractContents( \'" << line << "\' )] Key name <" << key << "> already exists." << std::endl;
            }
        }

        /**
         * Parses the line
         * @param line Line
         * @param lineNumber Line number
         */
        inline void Settings::parseLine( const std::string &line, size_t const lineNumber ) {
            if( line.find( '=' ) == std::string::npos ) {
                std::cerr << "[eadlib::fileIO::Settings::parseLine( \'" << line << "\', " << lineNumber << " )] '=' delimiter could not be found in line " << lineNumber << "." << std::endl;
            }
            if( !validLine( line ) ) {
                std::cerr << "[eadlib::fileIO::Settings::parseLine( \'" << line << "\', " << lineNumber << " )] Bad key-value pair format in line " << lineNumber << "." << std::endl;
            }
            extractContents( line );
        }

        /**
         * Checks for ';' and removed anything after and including that
         * @param line Line
         */
        inline void Settings::removeComment( std::string &line ) const {
            if( line.find( '#' ) != std::string::npos ) {
                line.erase( line.find( '#' ) );
            }
        }

        /**
         * Checks if the line is populated only by whitespace
         * @param line Line
         * @return Success
         */
        inline bool Settings::onlyWhitespace( const std::string &line ) const {
            return ( line.find_first_not_of( ' ' ) == std::string::npos );
        }

        /**
         * Checks for correct structure of line for a configuration format (key=value)
         * @param line Line
         * @return Success
         */
        inline bool Settings::validLine( const std::string &line ) const {
            std::string temp = line;
            temp.erase( 0, temp.find_first_not_of( "\t " ) );
            if( temp[ 0 ] == '=' ) {
                return false;
            }
            for( size_t i = temp.find( '=' ) + 1; i < temp.length(); i++ ) {
                if( temp[ i ] != ' ' ) {
                    return true;
                }
            }
            return false;
        }
    }
}

#endif //EADLIB_SETTINGS_H
