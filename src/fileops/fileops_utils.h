#ifndef EADLIB_FILEOPS_UTILS_H
#define EADLIB_FILEOPS_UTILS_H

#include <iostream>
#include <sstream>
#include <fstream>

namespace fileIO {
    /**
     * Gets the number of lines inside a file
     * @param fileName File to check
     * @return Number of lines
     */
    static int getNumberOfLines( std::string &fileName ) {
        int number_of_lines { 0 };
        std::string line { };
        std::ifstream myfile( fileName );
        while( std::getline( myfile, line ) ) {
            ++number_of_lines;
        }
        return number_of_lines;
    }
}

#endif //EADLIB_FILEOPS_UTILS_H
