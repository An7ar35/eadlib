/**
    @class          eadlib::interface::IObserver
    @brief          Observer interface to implement for use with any eadlib component taking in Observers
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_IOBSERVER_H
#define EADLIB_IOBSERVER_H

namespace eadlib::interface {
    class IObserver {
      public:
        /**
         * Updater method
         * @param percentage Progress as a decimal number between 0 and 1
         */
        virtual void update( const double &percentage ) = 0;
    };
}

#endif //EADLIB_IOBSERVER_H
