/**
    @class          eadlib::interface::IPrinter
    @brief          Printing interface to implement for use with any eadlib component taking in Printers
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_IPRINTER_H
#define EADLIB_IPRINTER_H

namespace eadlib::interface {
    template<class ...Args> class IPrinter {
      public:
        /**
         * Printer method
         * @param args Arguments passed for printing
         */
        virtual void print( Args...args ) = 0;
    };
}

#endif //EADLIB_IPRINTER_H
