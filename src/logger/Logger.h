/*!
    @class          eadlib::logger::Logger
    @brief          A logger with different selectable outputs and formatters

    Calls to the log are done with macros with any number of arguments separated with comas.
    %Outputs can be specified in the 'log_config.cfg' file which is generated on first run.

    @dependencies   eadlib::logger::*
    @author         E. A. Davison
    @copyright      E. A. Davison 2016
    @license        GNUv2 Public License

    @example        Logger_example.cpp
                    Example of all the calls available to the %Logger.
*/

#ifndef EADLIB_LOGGER_H
#define EADLIB_LOGGER_H

#include <iostream>
#include <sstream>
#include <vector>
#include <exception>
#include <mutex>
#include "log_configuration/LogConfig.h"

#ifndef NDEBUG
    #define LOG_FATAL   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::FATAL >
    #define LOG_ERROR   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::ERROR >
    #define LOG_WARNING eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::WARNING >
    #define LOG         eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::MSG >
    #define LOG_DEBUG   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::DEBUG >
    #define LOG_TRACE   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::TRACE >
#else
    #define LOG_FATAL   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::FATAL >
    #define LOG_ERROR   eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::ERROR >
    #define LOG_WARNING eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::WARNING >
    #define LOG(...)    eadlib::logger::Logger::getInstance().print< eadlib::logger::LogLevel_types::Type::MSG >
    #define LOG_DEBUG(...)
    #define LOG_TRACE(...)
#endif

namespace eadlib {
    namespace logger {
        class Logger {
          public:
            static Logger & getInstance();
            Logger();
            virtual ~Logger();
            //Logging
            template<eadlib::logger::LogLevel_types::Type event_type, typename...Args> void print( Args...args );
            template<eadlib::logger::LogLevel_types::Type event_type> void print_();
            template<eadlib::logger::LogLevel_types::Type event_type, typename H, typename...T> void print_( H head, T...tail );
          private:
            static Logger * _instance;
            std::unique_ptr<std::mutex> _mutex;
            uint32_t _number_of_entries;
            std::stringstream _log_stream;
            std::unique_ptr<LogConfig> _config;
            //Private Methods
            std::string getEntryNumber();
        };

        //-----------------------------------------------------------------------------------------------------------------
        // Logger class method implementations
        //-----------------------------------------------------------------------------------------------------------------
        /**
         * Gets the logger instance
         * @return Logger instance
         */
        inline Logger & Logger::getInstance() {
            static Logger *instance;
            static std::mutex mutex;
            std::lock_guard<std::mutex> guard( mutex );
            if( !instance ) {
                instance = new Logger();
            }
            if( !instance->_mutex ) {
                instance->_mutex = std::unique_ptr<std::mutex>( &mutex );
            }
            return *instance;
        }

        /**
         * Logger Constructor
         */
        inline Logger::Logger() :
            _number_of_entries( 0 ),
            _config( std::make_unique<eadlib::logger::LogConfig>( "log_config.cfg" ) )
        {}

        /**
         * Logger Destructor
         */
        inline Logger::~Logger() = default;

        /**
         * Prints log message
         * @tparam event_type Event type
         * @tparam Args       Argument types
         * @param args        Arguments in message
         */
        template<eadlib::logger::LogLevel_types::Type event_type, typename...Args> void Logger::print( Args...args ) {
            std::lock_guard<std::mutex> guard( *_mutex );
            Logger::print_<event_type>( args... );
        };

        /**
         * Print helper [end function for recursion]
         * @tparam event_type Event type
         */
        template<eadlib::logger::LogLevel_types::Type event_type> void Logger::print_() {
            _config->distributeMsg( TimeStamp(), _number_of_entries, event_type, _log_stream.str() );
            _number_of_entries++;
            _log_stream.str( std::string()); //emptying string stream
        }

        /**
         * Print helper [recursive function]
         * @tparam event_type Event type
         * @tparam H          Head type
         * @tparam T          Tail type
         * @param head        Head argument
         * @param tail        Rest of the arguments
         */
        template<eadlib::logger::LogLevel_types::Type event_type, typename H, typename...T> void Logger::print_( H head, T...tail ) {
            _log_stream << head;
            print_<event_type>( tail... );
        }

        /**
         * Gets the current log entry number
         * @return Entry number
         */
        inline std::string Logger::getEntryNumber() {
            std::stringstream ss;
            ss.fill( '0' );
            ss.width( 7 );
            ss << this->_number_of_entries;
            return ss.str();
        }
    }
}

#endif //EADLIB_LOGGER_H
