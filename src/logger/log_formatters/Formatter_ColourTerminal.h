#ifndef EADLIB_FORMATTER_COLOURTERMINAL_H
#define EADLIB_FORMATTER_COLOURTERMINAL_H

#include <sstream>
#include "Formatter.h"

namespace eadlib::logger {
    /**
     * Colour formatter for the terminal output
     */
    class Formatter_ColourTerminal : public Formatter {
      public:
        Formatter_ColourTerminal() = default;
        virtual ~Formatter_ColourTerminal() = default;
        std::string formatMsg( const TimeStamp &ts,
                               const uint32_t &msgNum,
                               const LogLevel_types::Type &level,
                               const std::string &msg ) override;
      protected:
        std::string formatMsgNumber( const uint32_t &msgNum ) override;
        std::string formatMsgLevel( const LogLevel_types::Type &level ) override;
      private:
        std::string RESET  = "\033[0m";
        std::string BLACK  = "\033[30m";
        std::string RED    = "\033[31m";
        std::string GREEN  = "\033[32m";
        std::string YELLOW = "\033[33m";
        std::string BLUE   = "\033[34m";
        std::string BROWN  = "\033[0;33m";
        std::string PURPLE = "\033[35m";
        std::string CYAN   = "\033[36m";
        std::string GREY   = "\033[0;37m";
        std::string WHITE  = "\033[37m";
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Public "Formatter_COLOUR_TERM" class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Formats the message for console output
     * @param ts        TimeStamp of the message
     * @param msgNum    Message's number in the session
     * @param level     Message level
     * @param msg       Log message
     * @return formatted output string stream
     */
    inline std::string Formatter_ColourTerminal::formatMsg( const TimeStamp &ts,
                                                            const uint32_t &msgNum,
                                                            const LogLevel_types::Type &level,
                                                            const std::string &msg ) {
        std::stringstream line;
        line << "\033[1m" << BLUE;
        line << "[";
        line << formatMsgNumber( msgNum );
        line << "] ";
        line << RESET;
        line << ts.getDate() << " - " << ts.getTime() << " ";
        line << formatMsgLevel( level );
        line << CYAN << msg << RESET;
        return line.str();
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Private "Formatter_COLOUR_TERM" class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Formats the message's session number for output
     * @param msgNum Message session number
     * @return Formatted string
     */
    inline std::string Formatter_ColourTerminal::formatMsgNumber( const uint32_t &msgNum ) {
        std::ostringstream oss;
        oss.fill( '0' );
        oss.width( 7 );
        oss << msgNum;
        return oss.str();
    }

    /**
     * Formats the message's log level
     * @param level Log level
     * @return Formatted string
     */
    inline std::string Formatter_ColourTerminal::formatMsgLevel( const LogLevel_types::Type &level ) {
        switch( level ) {
            case LogLevel_types::Type::FATAL:
                return RED + "|--FATAL--| " + RESET;
            case LogLevel_types::Type::ERROR:
                return PURPLE + "|--ERROR--| " + RESET;
            case LogLevel_types::Type::WARNING:
                return YELLOW + "|-WARNING-| " + RESET;
            case LogLevel_types::Type::MSG:
                return "|-MESSAGE-| ";
            case LogLevel_types::Type::DEBUG:
                return GREEN + "|--DEBUG--| " + RESET;
            case LogLevel_types::Type::TRACE:
                return GREY + "|--TRACE--| " + RESET;
            case LogLevel_types::Type::OFF:
                return "|---OFF---| ";
            default:
                return "!UNDEFINED!";
        }
    }
}

#endif //EADLIB_FORMATTER_COLOURTERMINAL_H
