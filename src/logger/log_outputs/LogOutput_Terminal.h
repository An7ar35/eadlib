#ifndef EADLIB_LOGOUTPUT_TERMINAL_H
#define EADLIB_LOGOUTPUT_TERMINAL_H

#include <iostream>
#include <mutex>
#include "LogOutput.h"

namespace eadlib {
    namespace logger {
        class LogOutput_Terminal : public LogOutput {
          public:
            LogOutput_Terminal() {};
            virtual ~LogOutput_Terminal() = default;
            void open_ostream( const std::string &name ) override;
            void close_ostream() override { };
            void write( const std::string &msg ) override;
          private:
            std::string _console_name;
            std::mutex _mutex;
        };
    }

    //-----------------------------------------------------------------------------------------------------------------
    // LogOutput_Terminal class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Sets Console name
     * @param name Console name
     */
    inline void eadlib::logger::LogOutput_Terminal::open_ostream( const std::string &name ) {
        this->_console_name = name;
    }


    /**
     * Writes to the console
     * @param msg Message to write
     */
    inline void eadlib::logger::LogOutput_Terminal::write( const std::string &msg ) {
        std::lock_guard<std::mutex> guard( _mutex );
        std::cout << msg << std::endl;
    }
}

#endif //EADLIB_LOGOUTPUT_TERMINAL_H
