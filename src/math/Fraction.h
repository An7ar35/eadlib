/**
    @class          eadlib::math::Fraction
    @brief          %Fraction data type
    @dependencies   eadlib::logger::Logger, eadlib::tool::Convert
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_FRACTION_H
#define EADLIB_FRACTION_H

#include <ostream>
#include <sstream>
#include <regex>
#include <complex>
#include <math.h>
#include "../logger/Logger.h"
#include "../tool/Convert.h"

namespace eadlib::math {
    class Fraction {
      public:
        explicit Fraction( int n );
        explicit Fraction( double number );
        Fraction( int n, int d );
        explicit Fraction( const char *fraction ) : Fraction( std::string( fraction ) ) { };
        explicit Fraction( std::string fraction );
        Fraction( const Fraction &fraction ) : _n( fraction._n ), _d( fraction._d ) { };
        ~Fraction() = default;
        //Operators
        friend std::ostream & operator <<( std::ostream &output, const Fraction &fraction );
        Fraction & operator =( const Fraction &rhs );
        const Fraction operator +( const Fraction &rhs );
        const Fraction operator -( const Fraction &rhs );
        const Fraction operator *( const Fraction &rhs );
        const Fraction operator /( const Fraction &rhs );
        bool operator <( const Fraction &rhs );
        bool operator >( const Fraction &rhs );
        bool operator <=( const Fraction &rhs );
        bool operator >=( const Fraction &rhs );
        bool operator !=( const Fraction &rhs );
        bool operator ==( const Fraction &rhs );
        //Functionalities
        const int nominator() const;
        const int denominator() const;
        Fraction abs();
        Fraction negate();
        Fraction inverse();
        std::string str() const;
        double asDouble() const;
      private:
        static const long long int MAX_INT_VALUE { std::numeric_limits<int>::max() };
        static const long long int MIN_INT_VALUE { std::numeric_limits<int>::min() };
        Fraction( long long int, long long int );
        bool checkLimits( long long i ) const;
        void normalise( long long int n, long long int d );
        std::string & rounder( std::string &mantissa );
        long long int _n, _d;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Fraction class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Fraction constructor
     * @param n Nominator
     */
    inline Fraction::Fraction( int n ) :
        _n( n ),
        _d( 1 )
    {
        normalise( n, 1 );
    }

    /**
     * Fraction constructor
     * @param number Number with decimals to convert to fraction
     */
    inline Fraction::Fraction( double number ) :
        _n( 0 ),
        _d( 0 )
    {
        std::string s = eadlib::tool::Convert::to_string<double>( number );
        size_t delimiter = s.find( '.' );
        std::string left = s.substr( 0, delimiter );
        std::string right = s.substr( delimiter + 1, delimiter + 8 );
        long mantissa = eadlib::tool::Convert::string_to_type<long>( rounder( right ) );
        long characteristic = eadlib::tool::Convert::string_to_type<long>( left );

        int j = 0;
        long temp { mantissa };
        while( temp >= 1 ) {
            temp *= 0.1;
            j++;
        }
        long mantissa_d = pow( 10, j );
        long result_n = characteristic * mantissa_d + 1 * mantissa;
        long result_d = 1 * mantissa_d;
        LOG_DEBUG( result_n, " / ", result_d );
        normalise( result_n, result_d );
    }

    /**
     * Fraction constructor
     * @param n Nominator
     * @param d Denominator
     * @exception eadlib::invalid_method_argument when denominator is 0
     */
    inline Fraction::Fraction( int n, int d ) :
        _n( n ),
        _d( d )
    {
        if( d == 0 ) {
            LOG_ERROR( "[eadlib::math::Fraction::Fraction( ", n, ", ", d, " )] Denominator cannot be 0." );
            throw std::invalid_argument( "Fraction(n/0) : Denominator cannot be 0." );
        }
        normalise( n, d );
    }

    /**
     * Fraction constructor
     * @param fraction Fraction as a string
     * @exception eadlib::invalid_method_argument when denominator is 0
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline Fraction::Fraction( std::string fraction ) :
        _n( 0 ),
        _d( 0 )
    {
        std::regex fraction_format( R"([\s]?[\x2D]?[\s]?[\d]+[\s]?[\x2F][\s]?[\x2D]?[\s]?[\d]+[\s]?)" );
        if( !std::regex_match( fraction, fraction_format ) ) {
            LOG_ERROR( "[eadlib::math::Fraction::Fraction( \'", fraction, "\' )] Argument is not recognised as a fraction." );
            throw std::invalid_argument( "Fraction( std::string ) : argument is not recognised as a fraction." );
        }
        fraction.erase( std::remove_if( fraction.begin(), fraction.end(), isspace ), fraction.end() );
        size_t delimiter = fraction.find( '/' );
        std::string string_n = fraction.substr( 0, delimiter );
        std::string string_d = fraction.substr( delimiter + 1 );
        auto result_n = eadlib::tool::Convert::string_to_type<long long int>( string_n );
        auto result_d = eadlib::tool::Convert::string_to_type<long long int>( string_d );
        if( result_d == 0 ) {
            LOG_ERROR( "[eadlib::math::Fraction::Fraction( \'", fraction, "\' )] Denominator cannot be 0." );
            throw std::invalid_argument( "Fraction( std::string ) : Denominator cannot be 0." );
        }
        try {
            normalise( result_n, result_d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Output Stream Operator
     * @param output   Output stream
     * @param fraction Fraction
     * @return Output stream
     */
    inline std::ostream & operator <<( std::ostream &output, const eadlib::math::Fraction &fraction ) {
        output << fraction._n << "/" << fraction._d;
        return output;
    }

    /**
     * Assignment Operator (=)
     * @param rhs Fraction to assign with
     * @return This Fraction
     */
    inline Fraction & Fraction::operator =( const Fraction &rhs ) {
        _n = rhs._n;
        _d = rhs._d;
        return *this;
    }

    /**
     * Addition Operator (+)
     * @param rhs Fraction to add with
     * @return Result
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline const Fraction Fraction::operator +( const Fraction &rhs ) {
        long long int result_n = _n * rhs._d + _d * rhs._n;
        long long int result_d = _d * rhs._d;
        try {
            return Fraction( result_n, result_d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Subtraction Operator (-)
     * @param rhs Fraction to subtract with
     * @return Result
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline const Fraction Fraction::operator -( const Fraction &rhs ) {
        long long int result_n = _n * rhs._d - _d * rhs._n;
        long long int result_d = _d * rhs._d;
        try {
            return Fraction( result_n, result_d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Multiplication Operator (*)
     * @param rhs Fraction to multiply with
     * @return Result
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline const Fraction Fraction::operator *( const Fraction &rhs ) {
        long long int result_n = _n * rhs._n;
        long long int result_d = _d * rhs._d;
        try {
            return Fraction( result_n, result_d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Division Operator (/)
     * @param rhs Fraction to divide with
     * @return Result
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline const Fraction Fraction::operator /( const Fraction &rhs ) {
        long long int result_n = _n * rhs._d;
        long long int result_d = _d * rhs._n;
        try {
            return Fraction( result_n, result_d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Boolean Smaller Operator (<)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator <( const eadlib::math::Fraction &rhs ) {
        return ( rhs._n * _d > rhs._d * _n );
    }

    /**
     * Boolean Bigger Operator (>)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator >( const eadlib::math::Fraction &rhs ) {
        return !( operator <( rhs ) || operator ==( rhs ) );
    }

    /**
     * Boolean Smaller-Equal Operator (<=)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator <=( const eadlib::math::Fraction &rhs ) {
        return !( operator >( rhs ) );
    }

    /**
     * Boolean Bigger-Equal Operator (>=)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator >=( const eadlib::math::Fraction &rhs ) {
        return !( operator <( rhs ) );
    }

    /**
     * Boolean Not-Equivalent Operator (!=)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator !=( const eadlib::math::Fraction &rhs ) {
        return !( operator ==( rhs ) );
    }

    /**
     * Boolean Equivalent Operator (==)
     * @param rhs Fraction to compare to
     * @return Result of comparison
     */
    inline bool Fraction::operator ==( const eadlib::math::Fraction &rhs ) {
        return ( _n == rhs._n && _d == rhs._d );
    }

    /**
     * Gets the nominator of the fraction
     * @return nominator
     */
    inline const int Fraction::nominator() const {
        return static_cast<int>( _n );
    }

    /**
     * Gets the denominator of the fraction
     * @return denominator
     */
    inline const int Fraction::denominator() const {
        return static_cast<int>( _d );
    }

    /**
     * Calculates the absolute value of the fraction
     * @return The abs of the fraction
     */
    inline Fraction Fraction::abs() {
        if( _n < 0 || _d < 0 ) {
            return negate();
        } else {
            return *this;
        }
    }

    /**
     * Calculates the negative of the fraction
     * @return The negative of the fraction
     */
    inline Fraction Fraction::negate() {
        return Fraction( ( _n * -1 ), _d );
    }

    /**
     * Calculates the reverse of the fraction
     * @return The inverse of the fraction
     */
    inline Fraction Fraction::inverse() {
        return Fraction( _d, _n );
    }

    /**
     * Converts Fraction to a string
     * @return Fraction as string
     */
    inline std::string Fraction::str() const {
        std::stringstream ss;
        ss << _n << "/" << _d;
        return ss.str();
    }

    /**
     * Converts Fraction to a double
     * @return Fraction as a double
     */
    inline double Fraction::asDouble() const {
        return static_cast<double>( _n ) / static_cast<double>( _d );
    }

    /**
     * Checks if the n and d are within the Integer limits
     * @param n Nominator
     * @param d Denominator
     * @return If both n and d are within the Integer limits
     */
    inline bool Fraction::checkLimits( long long int i ) const {
        return ( i >= MIN_INT_VALUE && i <= MAX_INT_VALUE );
    }

    //-----------------------------------------------------------------------------------------------------------------
    // Fraction class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Fraction constructor
     * @param n Nominator
     * @param d Denominator
     * @exception eadlib::invalid_method_argument when denominator is 0
     * @exception std::overflow_error when normalisation falls outside the bounds of the int type
     */
    inline Fraction::Fraction( long long int n, long long int d ) :
        _n( n ),
        _d( d )
    {
        if( d == 0 ) {
            LOG_ERROR( "[eadlib::math::Fraction::Fraction( ", n, ", ", d, " )] Denominator cannot be 0." );
            throw std::invalid_argument( "Fraction(n/0) : Denominator cannot be 0." );
        }
        try {
            normalise( n, d );
        } catch( std::overflow_error &e ) {
            throw e;
        }
    }

    /**
     * Normalises the entering arguments of a fraction
     * @param n Nominator
     * @param d Denominator
     * @exception std::overflow_error when result of normalisation is not within the limits of the int type
     */
    inline void Fraction::normalise( long long int n, long long int d ) {
        //check for negatives and hold flag until the end of normalise
        bool negative = false;
        if( n < 0 && d > 0 ) {
            n *= -1;
            negative = true;
        }
        if( d < 0 && n >= 0 ) {
            d *= -1;
            negative = true;
        }
        //normalisation
        if( n == 0 ) {
            _n = 0;
            _d = 1;
        } else {
            long long int a = n;
            long long int b = d;
            long long int t = 0;
            while( b != 0 ) {
                t = b;
                b = a % t;
                a = t;
            }
            long long int result_n = n / a;
            long long int result_d = d / a;
            if( negative ) {
                result_n *= -1;
            }
            if( checkLimits( result_n ) && checkLimits( result_d ) ) {
                _n = result_n;
                _d = result_d;
            } else {
                LOG_ERROR( "[eadlib::math::Fraction::normalise( ", n, ", ", d, " )] Normalisation results (", result_n, "/", result_d, ") not within the integer type limits." );
                throw std::overflow_error( "eadlib::math::Fraction::normalise( n, d ) : Normalisation results not within the integer type limits." );
            }
        }
    }

    /**
     * Mantissa rounder
     * @param mantissa Mantissa number string to round
     * @return Mantissa number string rounded
     */
    inline std::string &Fraction::rounder( std::string &mantissa ) {
        if( !mantissa.empty() && mantissa.back() == '9' ) {
            bool carry = true;
            auto it    = mantissa.rbegin();
            do {
                if( carry ) {
                    if( *it == '9' ) {
                        *it = '0';
                    } else {
                        *it++;
                        carry = false;
                    }
                }
                ++it;
            } while( it != mantissa.rend() && carry );
        }
        return mantissa;
    }
}

#endif //EADLIB_FRACTION_H
