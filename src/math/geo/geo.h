/**
    \ingroup        Math
    \defgroup       Geo Geometry
    @brief          Geometry related functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_GEO_H_H
#define EADLIB_GEO_H_H

namespace eadlib::math::geo {
    /**
     * \ingroup Geo
     * 3D Plane describer
     */
    enum class Plane3D {
        XY, YZ, XZ
    };
}

#endif //EADLIB_GEO_H_H
