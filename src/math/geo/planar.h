/**
    \ingroup        Geo
    \defgroup       Planar
    @brief          Planar geometry related functions
    @dependencies   eadlib::logger::Logger, eadlib::Coordinate2D, eadlib::math::shape::Circle, eadlib::math::trigonometry::
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_PLANAR_H
#define EADLIB_PLANAR_H

#include "trigonometry.h"
#include "../shape/Circle.h"
#include "../../datatype/Coordinate2D.h"
#include "../../logger/Logger.h"

namespace eadlib::math::geo::planar {
    /**
     * \ingroup Planar
     * Calculates the intersection points of 2 circles
     * @param a Circle A
     * @param b Circle B
     * @param A Coordinates of circle A
     * @param B Coordinates of circle B
     * @param intersect1 Container for 1st intersection
     * @param intersect2 Container for 2nd intersection
     * @return Number of intersection found (0-2)
     */
    inline unsigned int calcCircleIntersections( const eadlib::math::shape::Circle &a,
                                                 const eadlib::math::shape::Circle &b,
                                                 const eadlib::Coordinate2D<double> &A,
                                                 const eadlib::Coordinate2D<double> &B,
                                                 eadlib::Coordinate2D<double> &intersect1,
                                                 eadlib::Coordinate2D<double> &intersect2 )
    {
        // distance between the centres
        double distance = eadlib::math::geo::trigonometry::norm( A.x - B.x, A.y - B.y );
        // find number of solutions
        if( distance > ( a.getRadius() + b.getRadius() ) ) {
            LOG_DEBUG( "[eadlib::math::geo::plane::calcCircleIntersections( ", a, ", ", b, ", ", A, ", ", B, ", ", intersect1, ", ", intersect2, " )] Circles are too far apart." );
            return 0;
        } else if( distance == 0 && a.getRadius() == b.getRadius() ) {
            LOG_DEBUG( "[eadlib::math::geo::plane::calcCircleIntersections( ", a, ", ", b, ", ", A, ", ", B, ", ", intersect1, ", ", intersect2, " )] Circles coincide." );
            return 0;
        } else if( distance + std::min( a.getRadius(), b.getRadius() ) < std::max( a.getRadius(), b.getRadius() ) ) {
            LOG_DEBUG( "[eadlib::math::geo::plane::calcCircleIntersections( ", a, ", ", b, ", ", A, ", ", B, ", ", intersect1, ", ", intersect2, " )] One circle contains the other." );
            return 0;
        } else {
            double result_a { ( a.getRadius() * a.getRadius() - b.getRadius() * b.getRadius() + distance * distance ) / ( 2.0 * distance ) };
            double result_h = sqrt( a.getRadius() * a.getRadius() - result_a * result_a );
            // find p2
            Coordinate2D<double> p2( ( A.x + ( result_a * ( B.x - A.x ) ) / distance ), A.y + ( result_a * ( B.y - A.y ) ) / distance );
            // find intersection points p3
            intersect1.set( ( p2.x + ( result_h * ( B.y - A.y ) / distance ) ),
                            ( p2.y - ( result_h * ( B.x - A.x ) / distance ) ) );
            intersect2.set( ( p2.x - ( result_h * ( B.y - A.y ) / distance ) ),
                            ( p2.y + ( result_h * ( B.x - A.x ) / distance ) ) );
            if( distance == a.getRadius() + b.getRadius() ) {
                return 1;
            }
            return 2;
        }
    }
}
#endif //EADLIB_PLANAR_H
