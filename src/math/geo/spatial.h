/**
    \ingroup        Geo
    \defgroup       Spacial
    @brief          Spacial geometry related functions
    @dependencies   eadlib::logger::Logger, eadlib::Coordinate2D, eadlib::Coordinate3D, eadlib::math::geo::Plane3D
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_SPATIAL_H
#define EADLIB_SPATIAL_H

#include <iostream>
#include <stdlib.h>
#include <math.h>

#include "../../logger/Logger.h"
#include "../../datatype/Coordinate2D.h"
#include "../../datatype/Coordinate3D.h"
#include "geo.h"
#include "trigonometry.h"

namespace eadlib::math::geo::spatial {
    /**
     * \ingroup Spacial
     * Calculates the angle of a point to an axis on a plane
     * @param axis Axis (X/Y/Z)
     * @param plane Plane (XY/XZ/YZ)
     * @param point Point in space
     * @return The angle
     */
    inline double calcAngleTo( eadlib::Coordinate3D::Axis axis,
                               Plane3D plane,
                               eadlib::Coordinate3D &point )
    {
        long double h { };
        switch( plane ) {
            case Plane3D::XY:
                switch( axis ) {
                    case eadlib::Coordinate3D::Axis::X:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.x(), point.y() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.y(), h, point.x() );
                    case eadlib::Coordinate3D::Axis::Y:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.y(), point.x() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.x(), point.y(), h );
                    default:
                        break;
                }
                break;
            case Plane3D::XZ:
                switch( axis ) {
                    case eadlib::Coordinate3D::Axis::X:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.x(), point.z() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.z(), h, point.x() );
                    case eadlib::Coordinate3D::Axis::Z:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.z(), point.x() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.x(), point.z(), h );
                    default:
                        break;
                }
                break;
            case Plane3D::YZ:
                switch( axis ) {
                    case eadlib::Coordinate3D::Axis::Y:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.y(), point.z() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.z(), point.y(), h );
                    case eadlib::Coordinate3D::Axis::Z:
                        h = eadlib::math::geo::trigonometry::hypotenuse( point.z(), point.y() );
                        return eadlib::math::geo::trigonometry::angleSSS( point.y(), h, point.z() );
                    default:
                        break;
                }
                break;
        }
        LOG_ERROR( "[eadlib::math::spatial::calcAngleTo(..)] AXIS (", int(axis), ") chosen does not belong to the PLANE (", int(plane), ") selected." );
        return 0;
    }

    /**
     * \ingroup Spacial
     * Calculates the XYZ lowest and XYZ highest point in the ab spatial cube
     * @param a Point A
     * @param b Point B
     * @param l Lower bound point
     * @param u Upper bound point
     */
    inline void calcBounds( const eadlib::Coordinate3D &a,
                            const eadlib::Coordinate3D &b,
                            eadlib::Coordinate3D &l,
                            eadlib::Coordinate3D &u )
    {
        a.x() < b.x() ? ( l.setX( a.x() ), u.setX( b.x() ) )
                      : ( u.setX( a.x() ), l.setX( b.x() ) );
        a.y() < b.y() ? ( l.setY( a.y() ), u.setY( b.y() ) )
                      : ( u.setY( a.y() ), l.setY( b.y() ) );
        a.z() < b.z() ? ( l.setZ( a.z() ), u.setZ( b.z() ) )
                      : ( u.setZ( a.z() ), l.setZ( b.z() ) );
    }

    /**
     * \ingroup Spacial
     * Orders points to form a triangle ABC where A is the at the far left end and B is at the far right end on the X axis
     * @param A Point A
     * @param B Point B
     * @param C Point C
     * @param ax Distance AX (where X is the unknown point)
     * @param bx Distance BX (where X is the unknown point)
     * @param cx Distance CX (where X is the unknown point)
     */
    inline void orderPoints( eadlib::Coordinate3D &A,
                             eadlib::Coordinate3D &B,
                             eadlib::Coordinate3D &C,
                             double &ax,
                             double &bx,
                             double &cx )
    {
        long double ab = A.calcDistance( B );
        long double ac = A.calcDistance( C );
        long double bc = B.calcDistance( C );
        if( ac > ab && ac > bc ) { //ac is the hypotenuse
            B.swap( C );
            std::swap( bx, cx );
        } else if( bc > ab && bc > ac ) { //bc is the hypotenuse
            A.swap( B );
            std::swap( ax, bx );
            B.swap( C );
            std::swap( bx, cx );
        }
        if( B.x() < A.x() ) { //B is on the left
            B.swap( A );
            std::swap( bx, ax );
        }
    }

    /**
     * \ingroup Spacial
     * Works out the position a point X relative to ABC
     * Assumes that ABC is flat on the XY plane and A is at (0,0,0), B is at (ab,0,0)
     * @param ab Distance AB
     * @param ac Distance AC
     * @param bc Distance BC
     * @param ax Distance AX
     * @param bx Distance BX
     * @param cx Distance CX
     * @param X1 Relative coordinate of point X where X.z can be either +/- its value (1st possibility).
     * @param X2 Relative coordinate of point X where X.z can be either +/- its value (2nd possibility).
     * @return Success
     */
    inline bool trilaterate( double ab,
                             double ac,
                             double bc,
                             double ax,
                             double bx,
                             double cx,
                             eadlib::Coordinate3D &X1,
                             eadlib::Coordinate3D &X2 )
    {
        X1.reset();
        X2.reset();
        long double a = eadlib::math::geo::trigonometry::angleSSS( bc, ac, ab );
        double rad_a = eadlib::math::geo::trigonometry::deg2rad( a );
        double i { ac * cos( rad_a ) }; //x for point C
        double j { ac * sin( rad_a ) }; //_y for point C
        X1.setX( ( pow( ax, 2 ) - pow( bx, 2 ) + pow( ab, 2 ) ) / ( 2 * ab ) );
        X1.setY( ( pow( ax, 2 ) - pow( cx, 2 ) + pow( j, 2 ) + pow( i, 2 ) - ( ( i * ( pow( ax, 2 ) - pow( bx, 2 ) + pow( ab, 2 ) ) ) / ab ) ) / ( 2 * j ) );
        X1.setZ( sqrt( pow( ax, 2 ) - pow( X1.x(), 2 ) - pow( X1.y(), 2 ) ) );
        X2 = X1;
        if( ( X1.x() != X1.x() ) || ( X1.y() != X1.y() ) || ( X1.z() != X1.z() ) ) { //To make sure there is no 'NaN' values
            LOG_ERROR( "[eadlib::math::spatial::trilaterate( ", ab, ", ", ac, ", ", bc, ", ", ax, ", ", bx, ", ", cx, ", .. )] Cannot find intersection point." );
            return false;
        } else {
            X2.setZ( -X2.z() );
            LOG_DEBUG( "[eadlib::math::spatial::trilaterate( ", ab, ", ", ac, ", ", bc, ", ", ax, ", ", bx, ", ", cx, ", .. )] Found point X as ", X1, " or ", X2 );
            return true;
        }
    }

    /**
     * \ingroup Spacial
     * Find the possible positions of point X in 3D space
     * @param A Point A
     * @param B Point B
     * @param C Point C
     * @param ax Distance AX
     * @param bx Distance BX
     * @param cx Distance CX
     * @param X1 Possible point (1st possibility)
     * @param X2 Possible point (2nd possibility)
     * @return Success
     */
    inline bool trilaterate( const eadlib::Coordinate3D &A,
                             const eadlib::Coordinate3D &B,
                             const eadlib::Coordinate3D &C,
                             double ax,
                             double bx,
                             double cx,
                             eadlib::Coordinate3D &X1,
                             eadlib::Coordinate3D &X2 )
    {
        eadlib::Coordinate3D A_temp = A;
        eadlib::Coordinate3D B_temp = B;
        eadlib::Coordinate3D C_temp = C;
        //Order the points on so that ab is the hypotenuse
        eadlib::math::geo::spatial::orderPoints( A_temp, B_temp, C_temp, ax, bx, cx );
        //Calculate distances
        double ab = A_temp.calcDistance( B_temp );
        double ac = A_temp.calcDistance( C_temp );
        double bc = B_temp.calcDistance( C_temp );
        //Transform step.1 - transform so that point A is @ (0,0,0) [i.e.: shift everything]
        eadlib::Coordinate3D shift = Coordinate3D( 0, 0, 0 ).calcDifference( A_temp );
        A_temp += shift;
        B_temp += shift;
        C_temp += shift;
        //Transform step.2 - transform so that point B is @ (ab,0,0) [i.e.: rotate on z axis and _y axis]
        double angle_z = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X,
                                                                  eadlib::math::geo::Plane3D::XY,
                                                                  B_temp
        );
        if( B_temp.y() > 0 ) {
            angle_z = -angle_z;
        }
        B_temp.rotate( eadlib::Coordinate3D::Axis::Z, angle_z );
        C_temp.rotate( eadlib::Coordinate3D::Axis::Z, angle_z );
        double angle_y = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X,
                                                                  eadlib::math::geo::Plane3D::XZ,
                                                                  B_temp
        );
        if( B_temp.z() > 0 ) {
            angle_y = -angle_y;
        }
        B_temp.rotate( eadlib::Coordinate3D::Axis::Y, angle_y );
        C_temp.rotate( eadlib::Coordinate3D::Axis::Y, angle_y );
        //Transform step.3 - transform so that point C is flat on the XY plane along with A & B [i.e.: rotate on X axis]
        double angle_x = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y,
                                                                  eadlib::math::geo::Plane3D::YZ,
                                                                  C_temp
        );
        if( C_temp.z() > 0 ) {
            angle_x = -angle_x;
        }
        C_temp.rotate( eadlib::Coordinate3D::Axis::X, angle_x );
        //Trilaterate to find point X
        bool success = eadlib::math::geo::spatial::trilaterate( ab, ac, bc, ax, bx, cx, X1, X2 );
        //Transform X by reversing order on the steps
        X1.rotate( eadlib::Coordinate3D::Axis::X, -angle_x );
        X2.rotate( eadlib::Coordinate3D::Axis::X, -angle_x );
        X1.rotate( eadlib::Coordinate3D::Axis::Y, -angle_y );
        X2.rotate( eadlib::Coordinate3D::Axis::Y, -angle_y );
        X1.rotate( eadlib::Coordinate3D::Axis::Z, -angle_z );
        X2.rotate( eadlib::Coordinate3D::Axis::Z, -angle_z );
        X1 -= shift;
        X2 -= shift;
        return success;
    }
}

#endif //EADLIB_SPATIAL_H
