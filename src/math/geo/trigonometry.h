/**
    \ingroup        Geo
    \defgroup       Trigonometry
    @brief          Trigonometry related functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_TRIGONOMETRY_H
#define EADLIB_TRIGONOMETRY_H

#include <math.h>
#include "../math.h"

namespace eadlib::math::geo::trigonometry {
    /**
     * \ingroup Trigonometry
     * Converts Radians to Degrees
     * @param val Value to convert
     * @return Value in degrees
     */
    inline double rad2deg( double val ) {
        return val * ( 180 / pi );
    }

    /**
     * \ingroup Trigonometry
     * Converts Degrees to Radians
     * @param val Value to convert
     * @return Value in Rads
     */
    inline double deg2rad( double val ) {
        return val * ( pi / 180 );
    }

    /**
     * \ingroup Trigonometry
     * Calculates the cotan of a value
     * @param val Value
     * @return Cotan
     */
    inline double cotan( double val ) {
        return ( 1 / tan( val ) );
    }

    /**
     * \ingroup Trigonometry
     * Calculates Minkowski's norm
     * @param x X value
     * @param y Y value
     * @return Norm
     */
    inline double norm( const double x, const double y ) {
        return sqrt( x * x + y * y );
    }

    /**
     * \ingroup Trigonometry
     * Calculates an angle of a triangle with 3 lengths known
     * @param a Length a
     * @param b Length b
     * @param c Length c
     * @return Angle A
     */
    inline double angleSSS( double a, double b, double c ) {
        return acos( ( pow( b, 2 ) + pow( c, 2 ) - pow( a, 2 ) ) / ( 2 * b * c ) ) * 180 / pi;
    }

    /**
     * \ingroup Trigonometry
     * Calculate a side (AAS)
     * @param A Angle A
     * @param C Angle C
     * @param c Length c
     * @return Length a
     */
    inline double sideAAS( double A, double C, double c ) {
        return ( c * sin( ( A * pi / 180 ) ) / sin( ( C * pi / 180 ) ) );
    }

    /**
     * \ingroup Trigonometry
     * Calculate a side (SAS)
     * @param b Length b
     * @param c Length c
     * @param A Angle A
     * @return Length a
     */
    inline double sideSAS( double b, double A, double c ) {
        return sqrt( pow( b, 2 ) + pow( c, 2 ) - 2 * b * c * cos( ( A * pi / 180 ) ) );
    }

    /**
     * \ingroup Trigonometry
     * Using Pythagoras' theorem; gets the length of a side of a right angled triangle
     * @param s Side
     * @param h Hypotenuse
     * @return Length of other side
     */
    inline double pythagoras( double s, double h ) {
        return sqrt( pow( h, 2 ) - pow( s, 2 ) );
    }

    /**
     * \ingroup Trigonometry
     * Using Pythagoras' theorem; gets the length of the hypotenuse of a right angled triangle
     * @param s1 Length of Side 1
     * @param s2 Length of side 2
     * @return Length of the hypotenuse
     */
    inline double hypotenuse( double s1, double s2 ) {
        return sqrt( pow( s1, 2 ) + pow( s2, 2 ) );
    }

    /**
     * \ingroup Trigonometry
     * Calculates the area of a triangle using Heron's Formula
     * @param a Vertex A
     * @param b Vertex B
     * @param c Vertex C
     * @return Area of triangle
     */
    inline double triangleArea( double a, double b, double c ) {
        double p { ( a + b + c ) / 2 };
        return sqrt( ( p * ( p - a ) * ( p - b ) * ( p - c ) ) );
    }
}

#endif //EADLIB_TRIGONOMETRY_H
