/**
    \defgroup       Math
    @brief          General Mathematics functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_MATH_H
#define EADLIB_MATH_H

#include <cmath>
#include <limits>
#include <exception>
#include <string>

namespace eadlib::math {
    const double pi = 3.141592653589793238462643383279502884e+00;

    /**
     * \ingroup Math
     * Fibonacci recursive helper function
     * @param counter Current position
     * @param target Target position
     * @param n1 number at n-1
     * @param n2 number at n-2
     * @return Result
     * throws std::overflow_error when the recursion hits the integer upper limit
     * throws std::underflow_error when the recursion hits the integer lower limit
     */
    inline long long fibonacci( int counter, int target, long long n1, long long n2 ) {
        if( counter == target ) {
            if ( ( n1 > 0 ) && ( n2 > std::numeric_limits<int>::max() - n1 ) )
                throw std::overflow_error( "fibonacci hit integer limit at count=" + std::to_string( counter ) );
            if ( ( n1 < 0 ) && ( n2 < std::numeric_limits<int>::min() - n1 ) )
                throw std::underflow_error( "fibonacci hit integer limit at count=" + std::to_string( counter ) );
            return n1 + n2;
        } else {
            return fibonacci( ++counter, target, ( n1 + n2 ), n1 );
        }
    }

    /**
     * \ingroup Math
     * Gets the nth number in the Fibonacci sequence (tail-recursive implementation)
     * @param n position
     * @return number at position
     */
    inline long long fibonacci( int n ) {
        return ( n < 1 )  ? 0 :
               ( n == 1 ) ? 1 :
               /* else */   fibonacci( 2, n, 1, 0 );
    }

    /**
     * \ingroup Math
     * Greatest Common Denominator
     * @param a First number
     * @param b Second number
     * @return The GCD of the numbers
     */
    inline unsigned int gcd( unsigned int a, unsigned int b ) {
        return b == 0 ? a
                      : gcd( b, a % b );
    }
}

#endif //EADLIB_MATH_H
