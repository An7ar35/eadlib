/**
    @class          eadlib::math::shape::Circle
    @brief          %Circle data type/geometric shape
    @author         E. A. Davison
    @copyright      E. A. Davison 2014
    @license        GNUv2 Public License
**/
#ifndef EADLIB_CIRCLE_H
#define EADLIB_CIRCLE_H

#include <iostream>

namespace eadlib::math::shape {
    class Circle {
      public:
        explicit Circle( double r );
        ~Circle() = default;
        friend std::ostream &operator <<( std::ostream &output, Circle circle );
        double getRadius() const;
      private:
        double radius;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Circle class method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     * @param r Radius of circle
     */
    inline Circle::Circle( double r ) {
        radius = r;
    }

    /**
     * Gets the radius of the circle
     * @return The radius
     */
    inline double Circle::getRadius() const {
        return radius;
    }

    /**
     * Output stream operator
     * @param output Output stream
     * @param circle Circle object
     * @return Output stream
     */
    std::ostream & operator <<( std::ostream &output, const Circle circle )  {
        output << "Circle( " << circle.getRadius() << " )";
        return output;
    }
}


#endif //EADLIB_CIRCLE_H
