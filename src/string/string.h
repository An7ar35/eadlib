/**
    \defgroup       String
    @brief          String functions
    @author         E. A. Davison
    @copyright      E. A. Davison 2018
    @license        GNUv2 Public License
**/
#ifndef EADLIB_STRING_H
#define EADLIB_STRING_H

#include <cmath>
#include <cstddef>
#include <ostream>
#include <string>

namespace eadlib::string {
    /**
     * \ingroup String
     * Splits a string
     * @param s String
     * @param c Delimiter character
     * @return  Split strings at the first instance of the delimiter
     */
    inline std::pair<std::string, std::string> split( const std::string &s, const char &c ) {
        auto delimiter = s.find( c );
        if( delimiter == std::string::npos )
            return { s, "" };
        else
            return { s.substr( 0, delimiter ), s.substr( delimiter + 1 ) };
    }

    /**
     * \ingroup String
     * Gets the char length of a value's characteristic + negative sign is any
     * @tparam T Integer of floating type
     * @param value Value to get the characteristic length from (integer part)
     * @return Char length
     */
    template <typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>>
        size_t length_int( const T & value )
    {
        size_t characteristic { 0 };

        if( value == 0 )
            return 1;
        if( value < 0 )
            characteristic++; //'-' char sign
        if( trunc( value ) == 0 )
            characteristic++; //'0' char for values -1 < x < +1

        T temp { abs( value ) };
        while( temp >= 1 ) {
            temp *= 0.1;
            characteristic++;
        }

        return characteristic;
    }

    /**
     * \ingroup String
     * Gets the char length of a floating value's mantissa
     * @tparam T Floating type
     * @param value Value to get mantissa width from
     * @param rounding_precision Mantissa rounding precision
     * @return Length of mantissa
     */
    template<typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>>
        size_t length_frac( const T &value, int rounding_precision )
    {
        size_t             pos { 2 };
        std::ostringstream out;

        out.precision( rounding_precision );
        out << std::fixed << ( abs( value ) - trunc( value ) );
        auto string = out.str();


        while( pos < string.length() && string.at( pos ) == '0' ) {
            pos++;
        }

        if( pos == string.length() )
            return 0;

        for( auto i = pos; i < string.length(); i++ ) {
            if( string.at( i ) != '0' )
                pos = i;
        }

        return pos - 1;
    }

    /**
     * \ingroup String
     * Gets the char length of a number
     * Note: Very small numbers (1e-18 or more) will have a degenerating accuracy
     *       so it may be advisable NOT to rely on this method to get the length as
     *       it will including those inaccuracies.
     * @tparam T Integer or floating type
     * @param value Value to get char length from
     * @return Char length
     */
    template<typename T, typename = typename std::enable_if<std::is_arithmetic<T>::value>>
        size_t length( const T &value )
        {
            size_t characteristic = length_int<T>( value );
            size_t mantissa       = 0;

            if( std::is_floating_point<T>::value ) {
                T temp = abs( value ) - trunc( value );
                while( temp > trunc( temp ) && temp < ceil( temp ) ) {
                    temp *= 10;
                    mantissa++;
                }
            }

            return characteristic + ( mantissa ? ( mantissa + 1 ) : mantissa ); //'.' char or not to add to characteristic
        }
}

#endif //EADLIB_STRING_H
