#ifndef EADLIB_EQUALISH_H
#define EADLIB_EQUALISH_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>

namespace eadlib {
    namespace testing {
        namespace numeric {
            /**
             * Tests 2 elements of the same type for similarity
             * @param a Element A
             * @param b Element B
             * @param precision Degree of precision to test for
             * @return pass/fail
             */
            template<typename T> bool equalish( T a, T b, int precision ) {
                    std::stringstream str1;
                    std::stringstream str2;
                    str1 << std::fixed << std::setprecision( precision ) << a;
                    str2 << std::fixed << std::setprecision( precision ) << b;
                    std::string s1 = str1.str();
                    std::string s2 = str2.str();
                    /*
                    std::cout << "EQUALISH with precision( " << precision << " ): " << std::endl;
                    std::cout << s1 << std::endl;
                    std::cout << s2 << std::endl;
                    */
                    return s1 == s2;
            }

            /**
             * Tests an element against a reference of the same type with a given error margin
             * @param to_check Element to check
             * @param reference Element to check against
             * @param error_margin Acceptable margin of error for the test to pass
             * @return pass/fail
             */
            template<typename T> bool equalish( T to_check, T reference, double error_margin ) {
                    double top { reference + ( 0.5 * error_margin ) };
                    double bottom { reference - ( 0.5 * error_margin ) };
                    //std::cout << bottom << " <= " << to_check << " <= " << top << std::endl;
                    return ( to_check <= top && to_check >= bottom );
            }
        }
    }
}



#endif //EADLIB_EQUALISH_H
