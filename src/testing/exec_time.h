#ifndef EADLIB_EXEC_TIME_H
#define EADLIB_EXEC_TIME_H

#include <iostream>
#include <ctime>

namespace eadlib {
    namespace testing {
        template<typename T> void exec_time(  T(*function)(), std::string fname, int n_exec ) {
            std::clock_t start;
            start = std::clock();
            for( int i = 0; i < n_exec; i++ ) {
                function();
            }
            std::cout << "             >Function '" << fname << "' x " << n_exec << " executions took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        }

        void exec_time( void(*function)(), std::string fname, int n_exec ) {
            std::clock_t start;
            start = std::clock();
            for( int i = 0; i < n_exec; i++ ) {
                function();
            }
            std::cout << "             >Function '" << fname << "' x " << n_exec << " executions took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        }
    }
}

#endif //EADLIB_EXEC_TIME_H
