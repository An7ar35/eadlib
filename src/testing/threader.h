#ifndef EADLIB_THREADER_H
#define EADLIB_THREADER_H

#include <iostream>
#include <ctime>
#include <thread>

namespace eadlib {
    namespace testing {
        /**
         * Executes a given function on a set of n threads
         * @param function Function to run
         * @param fname Name of the function
         * @param n_threads Number of threads to run the function on
         */
        template<typename T> void threader(  T(*function)(), std::string fname, int n_threads ) {
            std::clock_t start;
            start = std::clock();
            std::thread threads[n_threads];
            for( int i = 0; i < n_threads; i++ ) {
                threads[ i ] = std::thread( function );
            }
            for( int i = 0; i < n_threads; i++ ) {
                threads[ i ].join();
            }
            std::cout << "             >Function '" << fname << "' x " << n_threads << " threads took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        }

        /**
         * Executes a given function on a set of n threads
         * @param function Function to run
         * @param fname Name of the function
         * @param n_threads Number of threads to run the function on
         */
        void threader( void(*function)(), std::string fname, int n_threads ) {
            std::clock_t start;
            start = std::clock();
            std::thread threads[n_threads];
            for( int i = 0; i < n_threads; i++ ) {
                threads[ i ] = std::thread( function );
            }
            for( int i = 0; i < n_threads; i++ ) {
                threads[ i ].join();
            }
            std::cout << "             >Function '" << fname << "' x " << n_threads << " threads took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
        }
    }
}

#endif //EADLIB_THREADER_H
