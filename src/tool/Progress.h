/**
    @class          eadlib::tool::Progress
    @brief          Progression tracker that accepts Observers
    @dependencies   eadlib::tool::IObserver, eadlib::exception::aborted
    @author         E. A. Davison
    @copyright      E. A. Davison 2017
    @license        GNUv2 Public License
**/
#ifndef EADLIB_PROGRESS_H
#define EADLIB_PROGRESS_H

#include <cstdint>
#include <list>

#include "../interface/IObserver.h"
#include "../exception/aborted_operation.h"

namespace eadlib::tool {
    class Progress {
      public:
        Progress();

        void begin( const unsigned &steps );
        void end();
        void abort();
        double total();
        bool isActive();

        const Progress & operator ++(); //throw exception if aborted
        const Progress operator ++( int ); //throw exception if aborted

        void attachObserver( interface::IObserver &observer );
        void detachObserver( interface::IObserver &observer );
      private:
        bool _abort_flag;
        unsigned _current_step;
        unsigned _total_steps;
        std::list<interface::IObserver *> _observers;
    };

    //-----------------------------------------------------------------------------------------------------------------
    // Progress class public method implementations
    //-----------------------------------------------------------------------------------------------------------------
    /**
     * Constructor
     */
    inline Progress::Progress() :
        _abort_flag( false ),
        _total_steps( 0 ),
        _current_step( 0 )
    {}

    /**
     * Begin a new set of operations
     * @param steps Number of steps
     */
    inline void Progress::begin( const unsigned &steps ) {
        if( _abort_flag ) {
            throw exception::aborted_operation( "Operation was aborted_operation." );
        } else {
            _total_steps += steps;
        }
    }

    /**
     * Ends current operation set
     */
    inline void Progress::end() {
        if( _abort_flag ) {
            throw exception::aborted_operation( "Cannot end an operation that was already aborted_operation." );
        } else {
            _current_step = _total_steps;
            for( auto o : _observers ) {
                o->update( (double) _current_step / (double) _total_steps );
            }
        }
    }

    /**
     * Aborts the progress tracking
     */
    inline void Progress::abort() {
        _abort_flag = true;
    }

    /**
     * Gets the current progress
     * @return Current progress as a decimal between 0 and 1
     */
    inline double Progress::total() {
        return _current_step / _total_steps;
    }

    /**
     * Gets the active status of the progress tracker
     * @return Active status
     */
    inline bool Progress::isActive() {
        return _current_step > 0 && _current_step <= _total_steps && !_abort_flag;
    }

    /**
     * Pre-increment operator
     * @return Incremented progress tracker
     */
    inline const Progress &Progress::operator ++() {
        if( _abort_flag ) {
            throw exception::aborted_operation( "Cannot end an operation that was already aborted_operation." );
        } else {
            _current_step++;
            for( auto o : _observers ) {
                o->update( (double) _current_step / (double) _total_steps );
            }
        }
        return *this;
    }

    /**
     * Post-increment operator
     * @return Progress tracker before increment
     */
    inline const Progress Progress::operator ++( int ) {
        Progress temp = *this;
        ++*this;
        return temp;
    }

    /**
     * Attaches an observer to the tracker
     * @param observer Observer instance
     */
    inline void Progress::attachObserver( interface::IObserver &observer ) {
        _observers.emplace_back( &observer );
    }

    /**
     * Detaches an observer from the tracker
     * @param observer
     */
    inline void Progress::detachObserver( interface::IObserver &observer ) {
        _observers.remove( &observer );
    }
}

#endif //EADLIB_PROGRESS_H
