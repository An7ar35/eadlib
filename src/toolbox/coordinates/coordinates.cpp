#include "coordinates.h"
#include "../../logger/Logger.h"
#include "../../math/geo/planar.h"
#include "../../math/geo/spatial.h"
#include "../../testing/equalish.h"


/**
 * Calculates the coordinate of a point on a 1-dimensional plane
 * @param A Coordinate of known point
 * @param ax Distance between known point (A) and unknown point (X)
 * @param X Coordinate container for the unknown point
 * @return Success
 */
bool eadlib::toolbox::coordinates::calc1D( const eadlib::Coordinate3D &A, double ax, eadlib::Coordinate3D &X ) {
    if( A.isReset() && X.isReset() && ax > 0 ) {
        X.setX( ax );
        return true;
    }
    LOG_ERROR( "[eadlib::toolbox::coord3D::coordinates::calc1D( ", A, ", ", ax, ", ", X, " )] Problem with the inputs." );
    return false;
}

/**
 * Calculates the coordinate of a point on a 2-dimensional plane
 * @param A Coordinate of known point
 * @param B Coordinate of known point
 * @param ax Distance between point A and X
 * @param bx Distance between point B and X
 * @param X1 Coordinate container for the unknown point (1st possible location)
 * @param X2 Coordinate container for the unknown point (2nd possible location)
 * @return Number of points found
 */
unsigned int eadlib::toolbox::coordinates::calc2D( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, double ax, double bx,
                                                   eadlib::Coordinate3D &X1, eadlib::Coordinate3D &X2 ) {
    if( ( A.isReset() || B.isReset() ) && X1.isReset() && X2.isReset() && ( ax > 0 && bx > 0 ) ) {
        eadlib::math::shape::Circle cA = eadlib::math::shape::Circle( ax );
        eadlib::math::shape::Circle cB = eadlib::math::shape::Circle( bx );
        Coordinate2D<double> i1 { }, i2 { };
        unsigned int n_intersects = eadlib::math::geo::planar::calcCircleIntersections( cA,
                                                                                       cB,
                                                                                       eadlib::Coordinate2D<double>( A.x(), A.y() ),
                                                                                       eadlib::Coordinate2D<double>( B.x(), B.y() ),
                                                                                       i1,
                                                                                       i2 );
        if( n_intersects < 1 ) {
            LOG_ERROR( "[eadlib::toolbox::coordinates::calc2D( ", A, ", ", B, ", ", ax, ", ", bx, ", ", X1, ", ", X2, " )] Couldn't find the intersection of the 2 circles." );
            return 0;
        } else {
            X1.setX( i1.x );
            X1.setY( i1.y );
            X2.setX( i2.x );
            X2.setY( i2.y );
            return n_intersects;
        }
    } else {
        LOG_ERROR( "[eadlib::toolbox::coordinates::calc2D( ", A, ", ", B, ", ", ax, ", ", bx, ", ", X1, ", ", X2, " )] Problem in the inputs." );
        return 0;
    }
}

/**
 * Calculates the coordinates of a point on a newly formed on a 3-dimensional space
 * @param A Coordinate of known point
 * @param B Coordinate of known point
 * @param C Coordinate of known point
 * @param ab Distance between point A and B
 * @param ac Distance between point A and C
 * @param ax Distance between point A and X
 * @param bc Distance between point B and C
 * @param bx Distance between point B and X
 * @param cx Distance between point C and X
 * @param X1 Coordinate container for the unknown point (1st possible location)
 * @param X2 Coordinate container for the unknown point (2nd possible location)
 * @return Success
 */
bool eadlib::toolbox::coordinates::calc3D( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, const eadlib::Coordinate3D &C, double ab,
                                           double ac, double ax, double bc, double bx, double cx, eadlib::Coordinate3D &X1, eadlib::Coordinate3D &X2 ) {
    if( eadlib::math::geo::spatial::trilaterate( A, B, C, ax, bx, cx, X1, X2 ) ) {
        LOG_DEBUG( "[eadlib::toolbox::coordinates::calc3D::calc3D( ", A, ", ", B, ", ", C, ", ", ab, ", ", ac, ", ", ax, ", ", bc, ", ", bx , ", ", cx, ", ", X1, ", ", X2, " )] Got ", X1, " and ", X2, "." );
        return true;
    };
    return false;
}

/**
 * Calculates the coordinates of a point in a 3-dimensional space
 * @param A Coordinate of known point
 * @param B Coordinate of known point
 * @param C Coordinate of known point
 * @param D Coordinate of known point (discriminator)
 * @param ax Distance between point A and X
 * @param bx Distance between point B and X
 * @param cx Distance between point C and X
 * @param dx Distance between point D and X
 * @param X Coordinate container for the unknown point
 */
bool eadlib::toolbox::coordinates::calcCoordinates( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, const eadlib::Coordinate3D &C,
                                                    const eadlib::Coordinate3D &D, double ax, double bx, double cx, double dx, eadlib::Coordinate3D &X ) {
    eadlib::Coordinate3D X1 { };
    eadlib::Coordinate3D X2 { };
    if( eadlib::math::geo::spatial::trilaterate( A, B, C, ax, bx, cx, X1, X2 ) ) {
        double d1 = D.calcDistance( X1 );
        double d2 = D.calcDistance( X2 );
        if( eadlib::testing::numeric::equalish<double>( fabs( d1 - dx ), 0, 0.005 ) ) {
            X = X1;
            LOG_DEBUG( "[eadlib::toolbox::coordinates::calcCoordinates( ", A, ", ", B, ", ", C, ", ", D, ", ", ax, ", ", bx, ", ", cx, ", ", dx, ", ", X , " )] Got ", X1,  " and ", X2, " - Unknown point calculated as ", X );
            return true;
        } else if( eadlib::testing::numeric::equalish<double>( fabs( d2 - dx ), 0, 0.005 ) ) {
            X = X2;
            LOG_DEBUG( "[eadlib::toolbox::coordinates::calcCoordinates( ", A, ", ", B, ", ", C, ", ", D, ", ", ax, ", ", bx, ", ", cx, ", ", dx, ", ", X , " )] Got ", X1,  " and ", X2, " - Unknown point calculated as ", X );
            return true;
        } else {
            LOG_ERROR( "[eadlib::toolbox::coordinates::calcCoordinates( ", A, ", ", B, ", ", C, ", ", D, ", ", ax, ", ", bx, ", ", cx, ", ", dx, ", ", X , " )] Failed. Got ", X1, " distance '", d1, "' and ", X2, " distance '", d2, "' when stated distance is ", dx );
            return false;
        }
    } else {
        LOG_ERROR( "[eadlib::toolbox::coordinates::calcCoordinates( ", A, ", ", B, ", ", C, ", ", D, ", ", ax, ", ", bx, ", ", cx, ", ", dx, ", ", X , " )] Coordinates for unknown point not found." );
        return false;
    }
}
