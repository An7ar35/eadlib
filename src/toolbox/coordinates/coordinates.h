#ifndef EADLIB_COORDINATES_H
#define EADLIB_COORDINATES_H

#include "../../datatype/Coordinate3D.h"
#include "../../datatype/Coordinate2D.h"

namespace eadlib {
    namespace toolbox {
        namespace coordinates {
            bool calc1D( const eadlib::Coordinate3D &A, double ax, eadlib::Coordinate3D &X );
            unsigned int calc2D( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, double ax, double bx, eadlib::Coordinate3D &X1, eadlib::Coordinate3D &X2 );
            bool calc3D( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, const eadlib::Coordinate3D &C,
                         double ab, double ac, double ax, double bc, double bx, double cx, eadlib::Coordinate3D &X1, eadlib::Coordinate3D &X2 );
            bool calcCoordinates( const eadlib::Coordinate3D &A, const eadlib::Coordinate3D &B, const eadlib::Coordinate3D &C, const eadlib::Coordinate3D &D,
                                  double ax, double bx, double cx, double dx, Coordinate3D &X );
        }
    }
}
#endif //EADLIB_COORDINATES_H
