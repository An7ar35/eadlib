#ifndef EADLIB_TARJAN_TEST_H
#define EADLIB_TARJAN_TEST_H

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../../src/algorithm/Tarjan.h"

/**
 * Prints the SCCs
 * @param t Tarjan object
 */
template<class T> void printSCC( const eadlib::algo::Tarjan<T> &t ) {
    for( auto it1 = t.cbegin(); it1 != t.cend(); ++it1 ) {
        for( auto it2 = it1->cbegin(); it2 != it1->cend(); ++it2 ) {
            std::cout << *it2 << " ";
        }
        std::cout << std::endl;
    }
}

/**
 * Function to sort in increasing order the SCCs
 * @param t Tarjan object
 */
template<class T> void sortSCCList( eadlib::algo::Tarjan<T> & t ) {
    for( auto it = t.begin(); it != t.end(); ++it ) {
        it->sort( []( T a, T b ) { return a < b; } );
    }
    t.sort( []( std::list<T> a, std::list<T> b ) { return *a.begin() < *b.begin(); } );
}

TEST( Tarjan_Tests, Constructor_1 ) {
    eadlib::Graph<int> g1( { 0, 1, 2, 3, 4 } );
    g1.createDirectedEdge( 1, 0 );
    g1.createDirectedEdge( 0, 2 );
    g1.createDirectedEdge( 2, 1 );
    g1.createDirectedEdge( 0, 3 );
    g1.createDirectedEdge( 3, 4 );
    eadlib::algo::Tarjan<int> t1( g1 );
    sortSCCList( t1 );
    ASSERT_EQ( 3, t1.size());
    EXPECT_NO_THROW(
        auto it = t1.begin();
        std::list<int> tarjan1( *it );
        std::list<int> expected1( { 0, 1, 2 } );
        if( it != t1.end() ) ++it;
        std::list<int> tarjan2( *it );
        std::list<int> expected2( { 3 } );
        if( it != t1.end() ) ++it;
        std::list<int> tarjan3( *it );
        std::list<int> expected3( { 4 } );
        EXPECT_THAT( tarjan1, ::testing::ContainerEq( expected1 ) );
        EXPECT_THAT( tarjan2, ::testing::ContainerEq( expected2 ) );
        EXPECT_THAT( tarjan3, ::testing::ContainerEq( expected3 ) );
    );
}

TEST( Tarjan_Tests, Constructor_2 ) {
    eadlib::Graph<int> g2( { 0, 1, 2, 3 } );
    g2.createDirectedEdge( 0, 1 );
    g2.createDirectedEdge( 1, 2 );
    g2.createDirectedEdge( 2, 3 );
    eadlib::algo::Tarjan<int> t2( g2 );
    sortSCCList( t2 );
    ASSERT_EQ( 4, t2.size());
    EXPECT_NO_THROW(
        auto it = t2.begin();
        std::list<int> tarjan1( *it );
        std::list<int> expected1( { 0 } );
        if( it != t2.end() ) ++it;
        std::list<int> tarjan2( *it );
        std::list<int> expected2( { 1 } );
        if( it != t2.end() ) ++it;
        std::list<int> tarjan3( *it );
        std::list<int> expected3( { 2 } );
        if( it != t2.end() ) ++it;
        std::list<int> tarjan4( *it );
        std::list<int> expected4( { 3 } );
        EXPECT_THAT( tarjan1, ::testing::ContainerEq( expected1 ) );
        EXPECT_THAT( tarjan2, ::testing::ContainerEq( expected2 ) );
        EXPECT_THAT( tarjan3, ::testing::ContainerEq( expected3 ) );
        EXPECT_THAT( tarjan4, ::testing::ContainerEq( expected4 ) );
    );
}

TEST( Tarjan_Tests, Constructor_3 ) {
    eadlib::Graph<int> g3( { 0, 1, 2, 3, 4, 5, 6 } );
    g3.createDirectedEdge( 0, 1 );
    g3.createDirectedEdge( 1, 2 );
    g3.createDirectedEdge( 2, 0 );
    g3.createDirectedEdge( 1, 3 );
    g3.createDirectedEdge( 1, 4 );
    g3.createDirectedEdge( 1, 6 );
    g3.createDirectedEdge( 3, 5 );
    g3.createDirectedEdge( 4, 5 );
    eadlib::algo::Tarjan<int> t3( g3 );
    sortSCCList( t3 );
    ASSERT_EQ( 5, t3.size());
    EXPECT_NO_THROW(
        auto it = t3.begin();
        std::list<int> tarjan1( *it );
        std::list<int> expected1( { 0, 1, 2 } );
        if( it != t3.end() ) ++it;
        std::list<int> tarjan2( *it );
        std::list<int> expected2( { 3 } );
        if( it != t3.end() ) ++it;
        std::list<int> tarjan3( *it );
        std::list<int> expected3( { 4 } );
        if( it != t3.end() ) ++it;
        std::list<int> tarjan4( *it );
        std::list<int> expected4( { 5 } );
        if( it != t3.end() ) ++it;
        std::list<int> tarjan5( *it );
        std::list<int> expected5( { 6 } );
        EXPECT_THAT( tarjan1, ::testing::ContainerEq( expected1 ) );
        EXPECT_THAT( tarjan2, ::testing::ContainerEq( expected2 ) );
        EXPECT_THAT( tarjan3, ::testing::ContainerEq( expected3 ) );
        EXPECT_THAT( tarjan4, ::testing::ContainerEq( expected4 ) );
        EXPECT_THAT( tarjan5, ::testing::ContainerEq( expected5 ) );
    );
}

TEST( Tarjan_Tests, Constructor_4 ) {
    eadlib::Graph<int> g4( { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g4.createDirectedEdge( 0, 1 );
    g4.createDirectedEdge( 0, 3 );
    g4.createDirectedEdge( 1, 2 );
    g4.createDirectedEdge( 1, 4 );
    g4.createDirectedEdge( 2, 0 );
    g4.createDirectedEdge( 2, 6 );
    g4.createDirectedEdge( 3, 2 );
    g4.createDirectedEdge( 4, 5 );
    g4.createDirectedEdge( 4, 6 );
    g4.createDirectedEdge( 5, 6 );
    g4.createDirectedEdge( 5, 7 );
    g4.createDirectedEdge( 5, 8 );
    g4.createDirectedEdge( 5, 9 );
    g4.createDirectedEdge( 6, 4 );
    g4.createDirectedEdge( 7, 9 );
    g4.createDirectedEdge( 8, 9 );
    g4.createDirectedEdge( 9, 8 );
    eadlib::algo::Tarjan<int> t4( g4 );
    sortSCCList( t4 );
    ASSERT_EQ( 5, t4.size());
    EXPECT_NO_THROW(
        auto it = t4.begin();
        std::list<int> tarjan1( *it );
        std::list<int> expected1( { 0, 1, 2, 3 } );
        if( it != t4.end() ) ++it;
        std::list<int> tarjan2( *it );
        std::list<int> expected2( { 4, 5, 6 } );
        if( it != t4.end() ) ++it;
        std::list<int> tarjan3( *it );
        std::list<int> expected3( { 7 } );
        if( it != t4.end() ) ++it;
        std::list<int> tarjan4( *it );
        std::list<int> expected4( { 8, 9 } );
        if( it != t4.end() ) ++it;
        std::list<int> tarjan5( *it );
        std::list<int> expected5( { 10 } );
        EXPECT_THAT( tarjan1, ::testing::ContainerEq( expected1 ) );
        EXPECT_THAT( tarjan2, ::testing::ContainerEq( expected2 ) );
        EXPECT_THAT( tarjan3, ::testing::ContainerEq( expected3 ) );
        EXPECT_THAT( tarjan4, ::testing::ContainerEq( expected4 ) );
        EXPECT_THAT( tarjan5, ::testing::ContainerEq( expected5 ) );
    );
}

TEST( Tarjan_Tests, Constructor_5 ) {
    eadlib::Graph<int> g5( { 0, 1, 2, 3, 4 } );
    g5.createDirectedEdge(0,1);
    g5.createDirectedEdge(1,2);
    g5.createDirectedEdge(2,3);
    g5.createDirectedEdge(2,4);
    g5.createDirectedEdge(3,0);
    g5.createDirectedEdge(4,2);
    eadlib::algo::Tarjan<int> t5( g5 );
    sortSCCList( t5 );
    ASSERT_EQ( 1, t5.size());
    EXPECT_NO_THROW(
        auto it = t5.begin();
        std::list<int> tarjan1( *it );
        std::list<int> expected1( { 0, 1, 2, 3, 4 } );
        EXPECT_THAT( tarjan1, ::testing::ContainerEq( expected1 ) );
    );
}

#endif //EADLIB_TARJAN_TEST_H
