#ifndef EADLIB_SORT_TEST_H
#define EADLIB_SORT_TEST_H

#include "../../src/algorithm/sort.h"
#include <list>
#include <vector>
#include <gmock/gmock-matchers.h>

//====Insertion Sort Tests====//
TEST( sort_Tests, insertionSort_default_less_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::insertionSort( list.begin(), list.end() );
    auto expected = std::list<int>( { 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, insertionSort_default_less_than_comparator_signed ) {
    auto list = std::list<int>( { 3, -7, 7, 4, 9, 5, -2, 6, 2, 1, -4 } );
    eadlib::algo::insertionSort( list.begin(), list.end() );
    auto expected = std::list<int>( { -7, -4, -2, 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, insertionSort_greater_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::insertionSort( list.begin(), list.end(), std::greater<int>() );
    auto expected = std::list<int>( { 9, 7, 6, 5, 4, 3, 2, 1 });
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

//====Selection Sort Tests====//
TEST( sort_Tests, selectionSort_default_less_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::selectionSort( list.begin(), list.end() );
    auto expected = std::list<int>( { 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, selectionSort_default_less_than_comparator_signed ) {
    auto list = std::list<int>( { 3, -7, 7, 4, 9, 5, -2, 6, 2, 1, -4 } );
    eadlib::algo::selectionSort( list.begin(), list.end() );
    auto expected = std::list<int>( { -7, -4, -2, 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, selectionSort_greater_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::selectionSort( list.begin(), list.end(), std::greater<int>() );
    auto expected = std::list<int>( { 9, 7, 6, 5, 4, 3, 2, 1 });
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

//====Bubble Sort Tests====//
TEST( sort_Tests, bubbleSort_default_less_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::bubbleSort( list.begin(), list.end() );
    auto expected = std::list<int>( { 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, bubbleSort_default_less_than_comparator_signed ) {
    auto list = std::list<int>( { 3, -7, 7, 4, 9, 5, -2, 6, 2, 1, -4 } );
    eadlib::algo::bubbleSort( list.begin(), list.end() );
    auto expected = std::list<int>( { -7, -4, -2, 1, 2, 3, 4, 5, 6, 7, 9 } );
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}

TEST( sort_Tests, bubbleSort_greater_than_comparator ) {
    auto list = std::list<int>( { 3, 7, 4, 9, 5, 2, 6, 1 } );
    eadlib::algo::bubbleSort( list.begin(), list.end(), std::greater<int>() );
    auto expected = std::list<int>( { 9, 7, 6, 5, 4, 3, 2, 1 });
    EXPECT_THAT( expected, ::testing::ContainerEq( list ) );
}
#endif //EADLIB_SORT_TEST_H
