#ifndef EADLIB_PARSER_TEST_H
#define EADLIB_PARSER_TEST_H

#include "gtest/gtest.h"
#include "../../src/cli/parser/Parser.h"

TEST( Parser_Tests, constructor ) {
    auto parser = eadlib::cli::Parser();
    parser.option( "group1", "-a", "-a", "digits only", true,
                   { { std::regex( "[\\digit]+" ), "Wrong input" } } );
    parser.option( "group1", "-b", "-b", "alpha only", true,
                   { { std::regex( "^[a-zA-Z]+$" ), "Wrong input" } } );
    parser.option( "group2", "-c", "-c", "filename", true,
                   { { std::regex( "^(.*/)?(?:$|(.+?)(?:(\\.[^.]*$)|$))+" ), "Wrong input", "file" } } );
    parser.option( "group2", "-d", "-d", "Type of letter set for genome creation (DNA, RNA).", false,
                   { { std::regex( "^DNA$|^RNA$", std::regex::icase ), "Letter type must be either \'DNA\' or \'RNA\'", "DNA"} }  );
    parser.option( "group3", "-e", "-e", "double between 0-1", false,
                   { { std::regex( "^[0-1]$|^0\\.[0-9]+$" ), "Number should be between 0-1 inc." } } );
    parser.option( "group3", "-f", "-f", "alphanumeric + underscore", false,
                   { { std::regex( "(_?[0-9a-zA-Z]+_?)+" ), "bad input" } } );
    parser.printInfo( std::cout );

    int argc = 13;
    char *argv[] = { "tester_app",
                     "-a", "5000",
                     "-b", "aBcDe",
                     "-c", "~/somefolder/some_file.extention",
                     "-d", "dna",
                     "-e", "1",
                     "-f", "gene_file_" };

    ASSERT_TRUE( parser.parse( argc, argv ) );


}

#endif //EADLIB_PARSER_TEST_H
