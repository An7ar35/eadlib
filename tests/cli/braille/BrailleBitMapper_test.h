#ifndef EADLIB_BRAILLEBITMAPPER_TEST_H
#define EADLIB_BRAILLEBITMAPPER_TEST_H

#include "gtest/gtest.h"
#include "../../../src/cli/braille/BrailleBitMapper.h"

TEST( BrailleBitMapper_Tests, Constructor ) {
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<30,40>();
    ASSERT_EQ( 40, bit_mapper.getBitmap().size() );
    ASSERT_EQ( 30, bit_mapper.getBitmap().at( 0 ).size() );
}

TEST( BrailleBitMapper_Tests, set ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Sanity check...
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_FALSE( bit );
        }
    }
    //Test
    bit_mapper.set( 0, 0 );
    bit_mapper.set( 1, 1 );
    bit_mapper.set( 2, 2 );
    bit_mapper.set( 0, 2 );

    auto bitmap = bit_mapper.getBitmap();
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            if( i == j || ( i == 2 && j == 0 ) ) {
                ASSERT_TRUE( bitmap.at( i ).at( j ) );
            } else {
                ASSERT_FALSE( bitmap.at( i ).at( j ) );
            }
        }
    }
}

TEST( BrailleBitMapper_Tests, unset ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Set all pixels...
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            bit_mapper.set( i, j );
        }
    }
    //Sanity check
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_TRUE( bit );
        }
    }
    //Test
    bit_mapper.unset( 0, 0 );
    bit_mapper.unset( 1, 1 );
    bit_mapper.unset( 2, 2 );
    bit_mapper.unset( 0, 2 );

    auto bitmap = bit_mapper.getBitmap();
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            if( i == j || ( i == 2 && j == 0 ) ) {
                ASSERT_FALSE( bitmap.at( i ).at( j ) );
            } else {
                ASSERT_TRUE( bitmap.at( i ).at( j ) );
            }
        }
    }
}

TEST( BrailleBitMapper_Tests, flip ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Test
    for( size_t i = 0; i < x; i++ ) {
        bit_mapper.flip( i, 0 );
        bit_mapper.flip( i, 2 );
    }

    auto bitmap = bit_mapper.getBitmap();
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            if( i == 0 || i == 2 ) {
                ASSERT_TRUE( bitmap.at( i ).at( j ) );
            } else {
                ASSERT_FALSE( bitmap.at( i ).at( j ) );
            }
        }
    }
}

TEST( BrailleBitMapper_Tests, flip_all ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Set all pixels...
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            bit_mapper.set( i, j );
        }
    }
    //Sanity check
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_TRUE( bit );
        }
    }
    //Test
    bit_mapper.flip();
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_FALSE( bit );
        }
    }
}

TEST( BrailleBitMapper_Tests, reset ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Set all pixels...
    for( size_t i = 0; i < y; i++ ) {
        for( size_t j = 0; j < x; j++ ) {
            bit_mapper.set( i, j );
        }
    }
    //Sanity check
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_TRUE( bit );
        }
    }
    //Test
    bit_mapper.reset();
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_FALSE( bit );
        }
    }
}

TEST( BrailleBitMapper_Tests, createBrailleMap ) {
    const size_t x = 3, y = 3;
    auto bit_mapper = eadlib::cli::braille::BrailleBitMapper<x, y>();
    //Sanity check...
    for( auto &row : bit_mapper.getBitmap() ) {
        for( const auto &bit : row ) {
            ASSERT_FALSE( bit );
        }
    }
    //Test
    constexpr size_t expected[2] = { 0x2815, 0x2804 };
    bit_mapper.set( 0, 0 );
    bit_mapper.set( 1, 1 );
    bit_mapper.set( 2, 2 );
    bit_mapper.set( 0, 2 );

    auto braille_map = bit_mapper.createBrailleMap();
    ASSERT_EQ( 1, braille_map->size() );
    ASSERT_EQ( 2, braille_map->at( 0 ).size() );
    ASSERT_EQ( expected[0], braille_map->at( 0 ).at( 0 ) );
    ASSERT_EQ( expected[1], braille_map->at( 0 ).at( 1 ) );

    //Print
//    std::setlocale( LC_ALL, "" );
//    for( auto &row : *braille_map ) {
//        for( auto &block : row )
//            printf( "%lc", ( wchar_t ) block );
//        std::cout << std::endl;
//    }
}

#endif //EADLIB_BRAILLEBITMAPPER_TEST_H
