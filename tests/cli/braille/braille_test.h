#ifndef EADLIB_CLI_BRAILLE_TEST_H
#define EADLIB_CLI_BRAILLE_TEST_H

#include "gtest/gtest.h"
#include "../../../src/cli/braille/braille.h"

TEST( braille_Tests, charWidth ) {
    using eadlib::cli::braille::charWidth;
    ASSERT_EQ( 0, charWidth( 0 ) );
    ASSERT_EQ( 1, charWidth( 1 ) );
    ASSERT_EQ( 1, charWidth( 2 ) );
    ASSERT_EQ( 2, charWidth( 3 ) );
    ASSERT_EQ( 2, charWidth( 4 ) );
    ASSERT_EQ( 3, charWidth( 5 ) );
}

TEST( braille_Tests, charHeight ) {
    using eadlib::cli::braille::charHeight;
    ASSERT_EQ( 0, charHeight( 0 ) );
    ASSERT_EQ( 1, charHeight( 1 ) );
    ASSERT_EQ( 1, charHeight( 4 ) );
    ASSERT_EQ( 2, charHeight( 5 ) );
    ASSERT_EQ( 3, charHeight( 9 ) );
    ASSERT_EQ( 3, charHeight( 12 ) );
    ASSERT_EQ( 24, charHeight( 96 ) );
}

TEST( braille_Tests, pixel_cast ) {
    using eadlib::cli::braille::pixel_cast;
    ASSERT_EQ( 1, pixel_cast<float>( 1 ) );
    ASSERT_EQ( 0, pixel_cast<double>( 0 ) );
    ASSERT_EQ( 1, pixel_cast<double>( 0.5 ) );
    ASSERT_EQ( 2, pixel_cast<double>( 1.5 ) );
    ASSERT_EQ( 2, pixel_cast<double>( 1.5000001 ) );
}

TEST( braille_Tests, largestCharSpacing_test1 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::largestCharSpacing;
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::X>( 1 ) );
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::X>( 2 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::X>( 3 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::X>( 4 ) );
    ASSERT_EQ( 4, largestCharSpacing<BrailleCharAxis::X>( 5 ) );
    ASSERT_EQ( 4, largestCharSpacing<BrailleCharAxis::X>( 6 ) );
    ASSERT_EQ( 5, largestCharSpacing<BrailleCharAxis::X>( 7 ) );
}

TEST( braille_Tests, largestCharSpacing_test2 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::largestCharSpacing;
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::Y>( 1 ) );
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::Y>( 2 ) );
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::Y>( 3 ) );
    ASSERT_EQ( 2, largestCharSpacing<BrailleCharAxis::Y>( 4 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::Y>( 5 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::Y>( 6 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::Y>( 7 ) );
    ASSERT_EQ( 3, largestCharSpacing<BrailleCharAxis::Y>( 8 ) );
    ASSERT_EQ( 4, largestCharSpacing<BrailleCharAxis::Y>( 9 ) );
}

TEST( braille_Tests, smallestCharSpacing_test1 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::smallestCharSpacing;
    ASSERT_EQ( 1, smallestCharSpacing<BrailleCharAxis::X>( 1 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::X>( 2 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::X>( 3 ) );
    ASSERT_EQ( 3, smallestCharSpacing<BrailleCharAxis::X>( 4 ) );
    ASSERT_EQ( 3, smallestCharSpacing<BrailleCharAxis::X>( 5 ) );
    ASSERT_EQ( 4, smallestCharSpacing<BrailleCharAxis::X>( 6 ) );
    ASSERT_EQ( 4, smallestCharSpacing<BrailleCharAxis::X>( 7 ) );
    ASSERT_EQ( 6, smallestCharSpacing<BrailleCharAxis::X>( 10 ) );
    ASSERT_EQ( 8, smallestCharSpacing<BrailleCharAxis::X>( 15 ) );
}

TEST( braille_Tests, smallestCharSpacing_test2 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::smallestCharSpacing;
    ASSERT_EQ( 1, smallestCharSpacing<BrailleCharAxis::Y>( 1 ) );
    ASSERT_EQ( 1, smallestCharSpacing<BrailleCharAxis::Y>( 2 ) );
    ASSERT_EQ( 1, smallestCharSpacing<BrailleCharAxis::Y>( 3 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::Y>( 4 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::Y>( 5 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::Y>( 6 ) );
    ASSERT_EQ( 2, smallestCharSpacing<BrailleCharAxis::Y>( 7 ) );
    ASSERT_EQ( 3, smallestCharSpacing<BrailleCharAxis::Y>( 8 ) );
    ASSERT_EQ( 3, smallestCharSpacing<BrailleCharAxis::Y>( 9 ) );
}

#endif //EADLIB_CLI_BRAILLE_TEST_H
