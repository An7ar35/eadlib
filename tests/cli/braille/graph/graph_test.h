#ifndef EADLIB_CLI_BRAILLE_GRAPH_TEST_H
#define EADLIB_CLI_BRAILLE_GRAPH_TEST_H

#include "gtest/gtest.h"
#include "../../../../src/cli/braille/graph/graph.h"

namespace unit_tests::braille_graph_Tests {}

TEST( braille_graph_Tests, offsetValueX_test1 ) {
    using eadlib::cli::braille::graph::offsetValueX;
    //positive axis
    ASSERT_DOUBLE_EQ( 17, offsetValueX<double>( 7, 10 ) );
}

TEST( braille_graph_Tests, offsetValueX_test2 ) {
    using eadlib::cli::braille::graph::offsetValueX;
    //negative axis
    ASSERT_DOUBLE_EQ( 3, offsetValueX<double>( -7, 10 ) );
}

TEST( braille_graph_Tests, offsetValueX_test_fail1 ) {
    using eadlib::cli::braille::graph::offsetValueX;
    ASSERT_THROW(
        offsetValueX<double>( -6, 5 ),
        std::underflow_error
    );
}

TEST( braille_graph_Tests, offsetValueY_test1 ) {
    using eadlib::cli::braille::graph::offsetValueY;
    //positive axis
    ASSERT_DOUBLE_EQ( 3, offsetValueY<double>( 7, 10 ) );
}

TEST( braille_graph_Tests, offsetValueY_test2 ) {
    using eadlib::cli::braille::graph::offsetValueY;
    //negative axis
    ASSERT_DOUBLE_EQ( 17, offsetValueY<double>( -7, 10 ) );
}

TEST( braille_graph_Tests, offsetValueY_test_fail1 ) {
    using eadlib::cli::braille::graph::offsetValueY;
    ASSERT_THROW(
        offsetValueY<double>( 6, 5 ),
        std::underflow_error
    );
}

TEST( braille_graph_Tests, scale_test1 ) {
    //positive up-scaling
    using eadlib::cli::braille::graph::scale;
    using eadlib::Coordinate2D;
    auto p = Coordinate2D<double>( 1.5, 1.5 );
    auto f = Coordinate2D<double>( 2.5, 5 );
    auto c = Coordinate2D<size_t>( 10, 10 );
    auto r = scale<double>( p, f, c );
    EXPECT_DOUBLE_EQ( 13.75, r.x );
    EXPECT_DOUBLE_EQ( 2.5, r.y );
}

TEST( braille_graph_Tests, scale_test2 ) {
    //negative up-scaling
    using eadlib::cli::braille::graph::scale;
    using eadlib::Coordinate2D;
    auto p = Coordinate2D<double>( -1.5, -1.5 );
    auto f = Coordinate2D<double>( 2.5, 5 );
    auto c = Coordinate2D<size_t>( 10, 10 );
    auto r = scale<double>( p, f, c );
    EXPECT_DOUBLE_EQ( 6.25, r.x );
    EXPECT_DOUBLE_EQ( 17.5, r.y );
}

TEST( braille_graph_Tests, scale_test3 ) {
    //down-scaling
    using eadlib::cli::braille::graph::scale;
    using eadlib::Coordinate2D;
    auto p = Coordinate2D<double>( 1.5, 1.5 );
    auto f = Coordinate2D<double>( 0.25, 0.1 );
    auto c = Coordinate2D<size_t>( 10, 10 );
    auto r = scale<double>( p, f, c );
    EXPECT_DOUBLE_EQ( 10.375, r.x );
    EXPECT_DOUBLE_EQ( 9.85, r.y );
}

TEST( braille_graph_Tests, scale_test4 ) {
    //shifting only
    using eadlib::cli::braille::graph::scale;
    using eadlib::Coordinate2D;
    auto p = Coordinate2D<double>( 5, 3 );
    auto f = Coordinate2D<double>( 1, 1 );
    auto c = Coordinate2D<size_t>( 10, 20 );
    auto r = scale<double>( p, f, c );
    EXPECT_DOUBLE_EQ( 15., r.x );
    EXPECT_DOUBLE_EQ( 17., r.y );
}

TEST( braille_graph_Tests, scale_test5 ) {
    //scaling + shifting
    using eadlib::cli::braille::graph::scale;
    using eadlib::Coordinate2D;
    auto p = Coordinate2D<double>( 5, 3 );
    auto f = Coordinate2D<double>( 1.5, 0.5 );
    auto c = Coordinate2D<size_t>( 5, 8 );
    auto r = scale<double>( p, f, c );
    EXPECT_DOUBLE_EQ( ( 5 + 7.5 ), r.x );
    EXPECT_DOUBLE_EQ( ( 8 - 1.5 ), r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test1 ) {
    //positive up-scaling - simple
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( 1, 1 );
    auto centre        = Coordinate2D<size_t>( 10, 10 );
    auto data_interval = Coordinate2D<double>( 1, 1 );
    auto tick_interval = Coordinate2D<size_t>( 5, 5 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 15, r.x );
    ASSERT_EQ( 5, r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test2 ) {
    //positive up-scaling - fractions
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( 1.5, 1.5 );
    auto centre        = Coordinate2D<size_t>( 10, 10 );
    auto data_interval = Coordinate2D<double>( 1, 1 );
    auto tick_interval = Coordinate2D<size_t>( 5, 5 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 18, r.x );
    ASSERT_EQ( 3, r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test3 ) {
    //negative up-scaling
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( -1.5, -1.5 );
    auto centre        = Coordinate2D<size_t>( 10, 10 );
    auto data_interval = Coordinate2D<double>( 1, 1 );
    auto tick_interval = Coordinate2D<size_t>( 5, 5 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 3, r.x );
    ASSERT_EQ( 18, r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test4 ) {
    //down-scaling
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( 150, 150 );
    auto centre        = Coordinate2D<size_t>( 5, 200 );
    auto data_interval = Coordinate2D<double>( 10, 10 );
    auto tick_interval = Coordinate2D<size_t>( 4, 4 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 65, r.x );
    ASSERT_EQ( 140, r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test5 ) {
    //shifting only
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( 28, 26 );
    auto centre        = Coordinate2D<size_t>( 50, 50 );
    auto data_interval = Coordinate2D<double>( 10, 10 );
    auto tick_interval = Coordinate2D<size_t>( 10, 10 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 78, r.x );
    ASSERT_EQ( 24, r.y );
}

TEST( braille_graph_Tests, scaleAmortised_test6 ) {
    //scaling + shifting
    using eadlib::cli::braille::graph::scaleAmortised;
    using eadlib::Coordinate2D;
    auto point         = Coordinate2D<double>( 28, 26 );
    auto centre        = Coordinate2D<size_t>( 50, 50 );
    auto data_interval = Coordinate2D<double>( 10, 10 );
    auto tick_interval = Coordinate2D<size_t>( 15, 12 );
    auto r = scaleAmortised( point, centre, data_interval, tick_interval );
    ASSERT_EQ( 92, r.x );
    ASSERT_EQ( 19, r.y );
}

TEST( braille_graph_Tests, calculateScale_test1 ) {
    //zero-scaling
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 0.8, 0.6 );
    auto t = Coordinate2D<double>( 0.8, 0.6 );
    auto r = calculateScale<double, double>( o, t );
    EXPECT_DOUBLE_EQ( 1., r.x );
    EXPECT_DOUBLE_EQ( 1., r.y );
}

TEST( braille_graph_Tests, calculateScale_test2 ) {
    //up-scaling to whole number
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 0.8, 0.6 );
    auto t = Coordinate2D<double>( 1, 1 );
    auto r = calculateScale<double, double>( o, t, false );
    EXPECT_DOUBLE_EQ( 1.25, r.x );
    EXPECT_DOUBLE_EQ( 1.6666666666666667, r.y );
}

TEST( braille_graph_Tests, calculateScale_test3 ) {
    //up-scaling
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 1., 5. );
    auto t = Coordinate2D<double>( 20., 50. );
    auto r = calculateScale<double, double>( o, t, false );
    EXPECT_DOUBLE_EQ( 20., r.x );
    EXPECT_DOUBLE_EQ( 10., r.y );
}

TEST( braille_graph_Tests, calculateScale_test4 ) {
    //down-scaling
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 20., 50. );
    auto t = Coordinate2D<double>( 1., 5. );
    auto r = calculateScale<double, double>( o, t, false );
    EXPECT_DOUBLE_EQ( 0.05, r.x );
    EXPECT_DOUBLE_EQ( 0.1, r.y );
}

TEST( braille_graph_Tests, calculateScale_test5 ) {
    //up-scale and maintain aspect ratio on X-axis
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 5., 5. );
    auto t = Coordinate2D<double>( 10, 50 );
    auto r = calculateScale<double, double>( o, t, true );
    EXPECT_DOUBLE_EQ( 2., r.x );
    EXPECT_DOUBLE_EQ( 2., r.y );
}

TEST( braille_graph_Tests, calculateScale_test6 ) {
    //up-scale and maintain aspect ratio on Y-axis
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 5., 5. );
    auto t = Coordinate2D<double>( 10, 8 );
    auto r = calculateScale<double, double>( o, t, true );
    EXPECT_DOUBLE_EQ( 1.6, r.x );
    EXPECT_DOUBLE_EQ( 1.6, r.y );
}

TEST( braille_graph_Tests, calculateScale_test_fail1 ) {
    //scale from 0
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 0, 0 );
    auto t = Coordinate2D<double>( 1, 1 );
    EXPECT_THROW( calculateScale( o, t, false ), std::invalid_argument );
}

TEST( braille_graph_Tests, calculateScale_test_fail2 ) {
    //scale from negative
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( -0.2, -0.5 );
    auto t = Coordinate2D<double>( 1, 1 );
    EXPECT_THROW( calculateScale( o, t, false ), std::invalid_argument );
}

TEST( braille_graph_Tests, calculateScale_test_fail3 ) {
    //scale to negative
    using eadlib::cli::braille::graph::calculateScale;
    using eadlib::Coordinate2D;
    auto o = Coordinate2D<double>( 2, 5 );
    auto t = Coordinate2D<double>( -10, -20 );
    EXPECT_THROW( calculateScale( o, t, false ), std::invalid_argument );
}

TEST( braille_graph_Tests, calculateAxisXPosition ) {
    using eadlib::cli::braille::graph::calculateXAxisPosition;

    size_t canvas_size_y   = 80;
    size_t canvas_margin_y =  5;
    double graph_size_y    = 10;
    double max_y           =  5;

    auto r = calculateXAxisPosition( canvas_size_y, canvas_margin_y, graph_size_y, max_y );
    EXPECT_EQ( 45, r );
}

TEST( braille_graph_Tests, calculateAxisYPosition ) {
    using eadlib::cli::braille::graph::calculateYAxisPosition;

    size_t canvas_size_x   =  160;
    size_t canvas_margin_x =    3;
    double graph_size_x    =    8;
    double min_x           = -  2;

    auto r = calculateYAxisPosition( canvas_size_x, canvas_margin_x, graph_size_x, min_x );
    EXPECT_EQ( 43, r );

}

TEST( braille_graph_Tests, calculateAxisCenter ) {
    using eadlib::cli::braille::graph::calculateXAxisPosition;
    using eadlib::cli::braille::graph::calculateYAxisPosition;
    using eadlib::cli::braille::graph::calculateAxisCenter;
    using eadlib::Coordinate2D;

    auto   canvas_size   = Coordinate2D<size_t>( 160, 80 );
    auto   canvas_margin = Coordinate2D<size_t>( 3, 5 );
    auto   graph_size    = Coordinate2D<double>( 6, 8.5 );
    double min_x         = -1.5;
    double max_y         = 5;

    auto x = calculateYAxisPosition( canvas_size.x, canvas_margin.x, graph_size.x, min_x );
    auto y = calculateXAxisPosition( canvas_size.y, canvas_margin.y, graph_size.y, max_y );
    auto r = calculateAxisCenter( canvas_size, canvas_margin, graph_size, min_x, max_y );
    EXPECT_EQ( x, r.x );
    EXPECT_EQ( y, r.y );
}

TEST( braille_graph_Tests, calculateTickInterval_test_fail1 ) {
    //bad argument #1
    using eadlib::cli::braille::BRAILLE_PIXEL_WIDTH;
    using eadlib::cli::braille::graph::calculateTickInterval;
    EXPECT_THROW(
        calculateTickInterval<double>( 0, 10, BRAILLE_PIXEL_WIDTH, 2, 0.5 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateTickInterval_test_fail2 ) {
    //bad argument #2
    using eadlib::cli::braille::BRAILLE_PIXEL_WIDTH;
    using eadlib::cli::braille::graph::calculateTickInterval;
    EXPECT_THROW(
        calculateTickInterval<double>( 5, 0, BRAILLE_PIXEL_WIDTH, 2, 0.5 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateTickInterval_test_fail3 ) {
    //bad argument #3
    using eadlib::cli::braille::BRAILLE_PIXEL_WIDTH;
    using eadlib::cli::braille::graph::calculateTickInterval;
    EXPECT_THROW(
        calculateTickInterval<double>( 5, 10, 0, 2, 0.5 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateTickInterval_test_fail4 ) {
    //bad argument #4
    using eadlib::cli::braille::BRAILLE_PIXEL_WIDTH;
    using eadlib::cli::braille::graph::calculateTickInterval;
    EXPECT_THROW(
        calculateTickInterval<double>( 5, 10, BRAILLE_PIXEL_WIDTH, 0, 0.5 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateTickInterval_test_fail5 ) {
    //bad argument #5
    using eadlib::cli::braille::BRAILLE_PIXEL_WIDTH;
    using eadlib::cli::braille::graph::calculateTickInterval;
    EXPECT_THROW(
        calculateTickInterval<double>( 5, 10, BRAILLE_PIXEL_WIDTH, 2, 0 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateTickInterval_test1 ) {
    using eadlib::cli::braille::graph::calculateTickInterval;
    //scale up without interval change
    auto r = calculateTickInterval<double>( 1.5, 15, 2, 2, 0.5 );
    ASSERT_EQ( 5, r.first );
    ASSERT_EQ( 0.5, r.second );
}

TEST( braille_graph_Tests, calculateTickInterval_test2 ) {
    using eadlib::cli::braille::graph::calculateTickInterval;
    //scale up with interval change
    auto r = calculateTickInterval<double>( 1.5, 15, 5, 3, 0.5 );
    ASSERT_EQ( 20, r.first );
    ASSERT_EQ(  2, r.second );
}

TEST( braille_graph_Tests, calculateTickInterval_test3 ) {
    using eadlib::cli::braille::graph::calculateTickInterval;
    //scale down without interval change
    auto r = calculateTickInterval<double>( 200, 20, 1, 1, 10 );
    ASSERT_EQ(  1, r.first );
    ASSERT_EQ( 10, r.second );
}

TEST( braille_graph_Tests, calculateTickInterval_test4 ) {
    using eadlib::cli::braille::graph::calculateTickInterval;
    //scale down with interval change
    auto r = calculateTickInterval<double>( 200, 20, 2, 2, 10 );
    ASSERT_EQ(  4, r.first );
    ASSERT_EQ( 40, r.second );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test1 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //match made in heaven
    ASSERT_EQ( 5, calculateCategoryInterval<BrailleCharAxis::Y>( 25, 5 ) );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test2 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //upper bound match
    ASSERT_EQ( 5, calculateCategoryInterval<BrailleCharAxis::Y>( 29, 5 ) );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test3 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //lower bound match
    ASSERT_EQ( 4, calculateCategoryInterval<BrailleCharAxis::Y>( 24, 5 ) );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test_fail1 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //No categories
    EXPECT_THROW(
        calculateCategoryInterval<BrailleCharAxis::X>( 10, 0 ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test_fail2 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //3 categories on 2 blocks (X)
    EXPECT_THROW(
        calculateCategoryInterval<BrailleCharAxis::X>( 5, 3 ),
        eadlib::exception::aborted_operation
    );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test_fail3 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //3 categories on 2 blocks (Y)
    EXPECT_THROW(
        calculateCategoryInterval<BrailleCharAxis::Y>( 11, 3 ),
        eadlib::exception::aborted_operation
    );
}

TEST( braille_graph_Tests, calculateCategoryInterval_test_fail4 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::graph::calculateCategoryInterval;
    //1 categories on < 1 blocks (Y)
    EXPECT_THROW(
        calculateCategoryInterval<BrailleCharAxis::Y>( 3, 1 ),
        eadlib::exception::aborted_operation
    );
}

TEST( braille_graph_Tests, removeOverlappingTicks_test1 ) {
    using eadlib::cli::braille::BrailleCharAxis;
    using eadlib::cli::braille::smallestCharSpacing;
    using eadlib::cli::braille::graph::removeOverlappingTicks;
    using eadlib::cli::braille::graph::TickList_t;
    using eadlib::Coordinate2D;

    auto list = TickList_t<double>( {
        { Coordinate2D<size_t>( 10, 1 ), -0.5 },
        { Coordinate2D<size_t>( 14, 1 ), 0 },
        { Coordinate2D<size_t>( 18, 1 ), 0.5 },
        { Coordinate2D<size_t>( 22, 1 ), 1.0 },
        { Coordinate2D<size_t>( 24, 1 ), 1.5 },
        { Coordinate2D<size_t>( 28, 1 ), 2.0 },
        { Coordinate2D<size_t>( 32, 1 ), 2.5 }
    } );
    auto expected = TickList_t<double>( {
        { Coordinate2D<size_t>( 10, 1 ), -0.5 },
        { Coordinate2D<size_t>( 18, 1 ), 0.5 },
        { Coordinate2D<size_t>( 24, 1 ), 1.5 },
        { Coordinate2D<size_t>( 32, 1 ), 2.5 }
    } );

    removeOverlappingTicks( 3, smallestCharSpacing<BrailleCharAxis::X>( 4 ), list );
    //Test
    ASSERT_EQ( 4, list.size() );
    auto list_it = list.begin();
    auto expected_it = expected.begin();
    while( list_it != list.end() && expected_it != expected.end() ) {
        ASSERT_DOUBLE_EQ( expected_it->second, list_it->second );
        ++list_it;
        ++expected_it;
    }
}

TEST( braille_graph_Tests, removeOverlappingTicks_test_fail1 ) {
    //invalid arg #1
    using eadlib::cli::braille::graph::removeOverlappingTicks;
    using eadlib::cli::braille::graph::TickList_t;

    auto list = TickList_t<double>();

    ASSERT_THROW(
        removeOverlappingTicks( 0, 2, list ),
        std::invalid_argument
    );
}

TEST( braille_graph_Tests, removeOverlappingTicks_test_fail2 ) {
    //invalid arg #2
    using eadlib::cli::braille::graph::removeOverlappingTicks;
    using eadlib::cli::braille::graph::TickList_t;

    auto list = TickList_t<double>();

    ASSERT_THROW(
        removeOverlappingTicks( 1, 1, list ),
        std::invalid_argument
    );
}
#endif //EADLIB_CLI_BRAILLE_GRAPH_TEST_H
