#ifndef EADLIB_CLI_BRAILLE_GRAPH_LABELLING_TEST_H
#define EADLIB_CLI_BRAILLE_GRAPH_LABELLING_TEST_H

#include "gtest/gtest.h"
#include "../../../../../src/cli/braille/graph/labelling/labelling.h"

namespace unit_test::braille_graph_labelling_Tests {
    void printText( std::vector<size_t> &line ) {
        std::setlocale( LC_ALL, "" );
        std::cout << "\"";
        for( auto &block : line )
            printf( "%lc", ( wchar_t ) block );
        std::cout << "\""<< std::endl;
    }

    void printValues( std::vector<size_t> &line ) {
        std::setlocale( LC_ALL, "" );
        std::cout << "\"";
        for( auto &block : line )
            std::cout << block << ", ";
        std::cout << "\""<< std::endl;
    }
}

TEST( braille_graph_labelling_Tests, getAbsoluteAxisPosition ) {
    using eadlib::cli::braille::graph::labelling::Axis;
    using eadlib::cli::braille::graph::labelling::Position;
    using eadlib::cli::braille::graph::labelling::getAbsoluteLabelPosition;

    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::X, Position::VOID ) );
    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::Y, Position::VOID ) );

    ASSERT_EQ( Position::NORTH, getAbsoluteLabelPosition( Axis::X, Position::NORTH ) );
    ASSERT_EQ( Position::NORTH, getAbsoluteLabelPosition( Axis::X, Position::NE ) );
    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::X, Position::EAST ) );
    ASSERT_EQ( Position::SOUTH, getAbsoluteLabelPosition( Axis::X, Position::SE ) );
    ASSERT_EQ( Position::SOUTH, getAbsoluteLabelPosition( Axis::X, Position::SOUTH ) );
    ASSERT_EQ( Position::SOUTH, getAbsoluteLabelPosition( Axis::X, Position::SW ) );
    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::X, Position::WEST ) );
    ASSERT_EQ( Position::NORTH, getAbsoluteLabelPosition( Axis::X, Position::NW ) );

    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::Y, Position::NORTH ) );
    ASSERT_EQ( Position::EAST, getAbsoluteLabelPosition( Axis::Y, Position::NE ) );
    ASSERT_EQ( Position::EAST, getAbsoluteLabelPosition( Axis::Y, Position::EAST ) );
    ASSERT_EQ( Position::EAST, getAbsoluteLabelPosition( Axis::Y, Position::SE ) );
    ASSERT_EQ( Position::VOID, getAbsoluteLabelPosition( Axis::Y, Position::SOUTH ) );
    ASSERT_EQ( Position::WEST, getAbsoluteLabelPosition( Axis::Y, Position::SW ) );
    ASSERT_EQ( Position::WEST, getAbsoluteLabelPosition( Axis::Y, Position::WEST ) );
    ASSERT_EQ( Position::WEST, getAbsoluteLabelPosition( Axis::Y, Position::NW ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test1 ) { //Middle alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, MIDDLE_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test2 ) { //Left alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, LEFT_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test3 ) { //Right alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, RIGHT_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test4 ) { //Middle alignment for X axis label with even length
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 32, 49, 48, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 9, 0 ), 10.5 );

    writeNumericXLabel<double>( label_specs, MIDDLE_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test5 ) { //Middle alignment for X axis label with odd length
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 32, 32, 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 9, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, MIDDLE_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test6 ) { //Left alignment for X axis label
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 32, 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 10, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, LEFT_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericXLabel_test7 ) { //Right alignment for X axis label
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericXLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -15 /*min value*/, 15 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 32, 32, 32, 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 10, 0 ), 1.5 );

    writeNumericXLabel<double>( label_specs, RIGHT_ALIGN, 5, 0, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}


TEST( braille_graph_labelling_Tests, writeNumericYLabel_test1 ) { //Middle alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::NumLabelProperty;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -100 /*min value*/, 100 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 45, 49, 46, 53, 32 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), -1.501 );

    writeNumericYLabel<double>( label_specs, MIDDLE_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericYLabel_test2 ) { //Left alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::NumLabelProperty;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -100 /*min value*/, 100 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 45, 49, 46, 53, 32, 32 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), -1.501 );

    writeNumericYLabel<double>( label_specs, LEFT_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeNumericYLabel_test3 ) { //Right alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::NumLabelProperty;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeNumericYLabel;

    auto label_specs = eadlib::cli::braille::graph::NumLabelProperty<double>(
        0.5 /*interval*/, -100 /*min value*/, 100 /*max value*/, 2 /*fractional rounding precision*/
    );
    auto expected    = std::vector<size_t>( { 32, 32, 45, 49, 46, 53 } );
    auto returned    = std::vector<size_t>();
    auto tick_val    = TickValue_t<double>( Coordinate2D<size_t>( 0, 0 ), -1.501 );

    writeNumericYLabel<double>( label_specs, RIGHT_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test1 ) { //Middle alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 109, 105, 100, 100, 108, 101 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "middle" );

    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test2 ) { //Left alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 108, 101, 102, 116 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "left" );

    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test3 ) { //Right alignment for X axis labels at (0,0)
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 114, 105, 103, 104, 116 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "right" );

    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test4 ) { //Middle alignment for X axis labels with even length
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 32, 32, 109, 105, 100, 100, 108, 101 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 10, 0 ), "middle" ); //start char on i=3, middle i=5

    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test5 ) { //Middle alignment for X axis labels with odd length
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 32, 32, 109, 105, 100, 100, 108, 101, 48 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 10, 0 ), "middle0" ); //start char on i=2, middle i=5

    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test6 ) { //Left alignment for X axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 32, 108, 101, 102, 116 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 10, 0 ), "left" ); //start char on i=2, end aligned to i=5

    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test7 ) { //Right alignment for X axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 32, 32, 32, 32, 114, 105, 103, 104, 116 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 10, 0 ), "right" ); //start char and aligned on i=5

    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test8 ) { //Multiple middle alignments for X axis labels w/o overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 32, 99, 97, 116, 50, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 4, 0 ), "cat1" );  //char i=2
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 15, 0 ), "cat2" ); //char i=8
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 26, 0 ), "cat3" ); //char i=13

    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test9 ) { //Multiple left alignments for X axis labels w/o overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 32, 99, 97, 116, 50, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 8, 0 ), "cat1" );  //char i=4
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 19, 0 ), "cat2" ); //char i=10
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 30, 0 ), "cat3" ); //char i=15

    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test10 ) { //Multiple right alignments for X axis labels w/o overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 32, 99, 97, 116, 50, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 1, 0 ), "cat1" );  //char i=1
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 13, 0 ), "cat2" ); //char i=7
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 24, 0 ), "cat3" ); //char i=12

    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test11 ) { //Middle alignments for X axis labels with overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 99, 97, 116, 50, 32, 32, 32, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 1, 0 ), "cat1" );  //char i=1
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 10, 0 ), "cat2" ); //char i=5 so starts at i=4
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 30, 0 ), "cat3" ); //char i=15, no overlaps

    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, MIDDLE_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test12 ) { //Left alignments for X axis labels with overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 99, 97, 116, 50, 32, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 4, 0 ), "cat1" );  //char i=2, starts at i=-3 so overlaps on 0
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 8, 0 ), "cat2" );  //char i=4, starts at i=1  so overlaps cat1
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 30, 0 ), "cat3" ); //char i=15, starts at i=12, no overlaps

    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, LEFT_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test13 ) { //Right alignments for X axis labels with overlaps
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 49, 32, 99, 97, 116, 50, 32, 32, 32, 32, 99, 97, 116, 51 } );
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 1, 0 ), "cat1" );  //char i=1
    auto tick_val2 = TickValue_t<std::string>( Coordinate2D<size_t>( 4, 0 ), "cat2" );  //char i=2
    auto tick_val3 = TickValue_t<std::string>( Coordinate2D<size_t>( 28, 0 ), "cat3" ); //char i=14, no overlaps

    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val1, returned );
    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val2, returned );
    writeTextXLabel<std::string>( 12, RIGHT_ALIGN, 0, tick_val3, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextXLabel_test14 ) { //String trimming when label is larger than max length allowed
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextXLabel;

    auto expected = std::vector<size_t>( { 99, 97, 116, 101, 46 } ); //"cate."
    auto returned = std::vector<size_t>();
    auto tick_val1 = TickValue_t<std::string>( Coordinate2D<size_t>( 1, 0 ), "category" );  //char i=1

    writeTextXLabel<std::string>( 5, RIGHT_ALIGN, 0, tick_val1, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextYLabel_test1 ) { //Middle alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::MIDDLE_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextYLabel;

    auto expected = std::vector<size_t>( { 32, 32, 32, 109, 105, 100, 100, 108, 101, 32, 32, 32 } );
    auto returned = std::vector<size_t>();
    auto tick_val = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "middle" );

    writeTextYLabel<std::string>( 12, MIDDLE_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextYLabel_test2 ) { //Left alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::LEFT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextYLabel;

    auto expected = std::vector<size_t>( { 108, 101, 102, 116, 32, 32, 32, 32, 32, 32, 32, 32 } );
    auto returned = std::vector<size_t>();
    auto tick_val = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "left" );

    writeTextYLabel<std::string>( 12, LEFT_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

TEST( braille_graph_labelling_Tests, writeTextYLabel_test3 ) { //Right alignment for Y axis labels
    using eadlib::Coordinate2D;
    using eadlib::cli::braille::graph::labelling::TickValue_t;
    using eadlib::cli::braille::graph::labelling::RIGHT_ALIGN;
    using eadlib::cli::braille::graph::labelling::writeTextYLabel;

    auto expected = std::vector<size_t>( { 32, 32, 32, 32, 32, 32, 32, 114, 105, 103, 104, 116 } );
    auto returned = std::vector<size_t>();
    auto tick_val = TickValue_t<std::string>( Coordinate2D<size_t>( 0, 0 ), "right" );

    writeTextYLabel<std::string>( 12, RIGHT_ALIGN, tick_val, returned );

    //Testing
    ASSERT_EQ( expected.size(), returned.size() );
    for( size_t r = 0; r < expected.size(); r++ )
        ASSERT_EQ( expected.at( r ), returned.at( r ) );
}

#endif //EADLIB_CLI_BRAILLE_GRAPH_LABELLING_TEST_H
