#include "gtest/gtest.h"
#include "../../../../src/cli/braille/raster/raster.h"

#ifndef EADLIB_BRAILLE_RASTER_TEST_H
#define EADLIB_BRAILLE_RASTER_TEST_H

TEST( braille_raster_Tests, getLinePoints_test1 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //same point
    auto l = getLinePoints<size_t>( Coordinate2D<size_t>( 5, 5 ), Coordinate2D<size_t>( 5, 5 ) );
    ASSERT_TRUE( l.empty() );
}

TEST( braille_raster_Tests, getLinePoints_test2 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //neighbouring points on 0 inclination
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                        Coordinate2D<size_t>( 4, 5 ),
                                                        Coordinate2D<size_t>( 5, 5 )
                                                    } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 4, 5 ), Coordinate2D<size_t>( 5, 5) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test3 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //straight horizontal line
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                        Coordinate2D<size_t>( 1, 1 ),
                                                        Coordinate2D<size_t>( 2, 1 ),
                                                        Coordinate2D<size_t>( 3, 1 ),
                                                        Coordinate2D<size_t>( 4, 1 ),
                                                        Coordinate2D<size_t>( 5, 1 )
                                                    } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 1 ), Coordinate2D<size_t>( 5, 1 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test4 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //straight vertical line
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 1, 1 ),
                                                         Coordinate2D<size_t>( 1, 2 ),
                                                         Coordinate2D<size_t>( 1, 3 ),
                                                         Coordinate2D<size_t>( 1, 4 ),
                                                         Coordinate2D<size_t>( 1, 5 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 1 ), Coordinate2D<size_t>( 1, 5 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test5 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //diagonal where x > y
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 1, 1 ),
                                                         Coordinate2D<size_t>( 2, 1 ),
                                                         Coordinate2D<size_t>( 3, 2 ),
                                                         Coordinate2D<size_t>( 4, 2 ),
                                                         Coordinate2D<size_t>( 5, 2 ),
                                                         Coordinate2D<size_t>( 6, 3 ),
                                                         Coordinate2D<size_t>( 7, 3 ),
                                                         Coordinate2D<size_t>( 8, 3 ),
                                                         Coordinate2D<size_t>( 9, 4 ),
                                                         Coordinate2D<size_t>( 10, 4 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 1 ), Coordinate2D<size_t>( 10, 4 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test6 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //diagonal where x < y (i.e.: steep incline)
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 1, 1 ),
                                                         Coordinate2D<size_t>( 1, 2 ),
                                                         Coordinate2D<size_t>( 1, 3 ),
                                                         Coordinate2D<size_t>( 2, 4 ),
                                                         Coordinate2D<size_t>( 2, 5 ),
                                                         Coordinate2D<size_t>( 2, 6 ),
                                                         Coordinate2D<size_t>( 2, 7 ),
                                                         Coordinate2D<size_t>( 3, 8 ),
                                                         Coordinate2D<size_t>( 3, 9 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 1 ), Coordinate2D<size_t>( 3, 9 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test7 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //starting at (0,0)
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 0, 0 ),
                                                         Coordinate2D<size_t>( 1, 0 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 0, 0 ), Coordinate2D<size_t>( 1, 0 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test8 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //shallow line
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 0, 0 ),
                                                         Coordinate2D<size_t>( 1, 0 ),
                                                         Coordinate2D<size_t>( 2, 0 ),
                                                         Coordinate2D<size_t>( 3, 0 ),
                                                         Coordinate2D<size_t>( 4, 0 ),
                                                         Coordinate2D<size_t>( 5, 1 ),
                                                         Coordinate2D<size_t>( 6, 1 ),
                                                         Coordinate2D<size_t>( 7, 1 ),
                                                         Coordinate2D<size_t>( 8, 1 ),
                                                         Coordinate2D<size_t>( 9, 1 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 0, 0 ), Coordinate2D<size_t>( 9, 1) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test9 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //straight negative vertical line
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 1, 5 ),
                                                         Coordinate2D<size_t>( 1, 4 ),
                                                         Coordinate2D<size_t>( 1, 3 ),
                                                         Coordinate2D<size_t>( 1, 2 ),
                                                         Coordinate2D<size_t>( 1, 1 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 5 ), Coordinate2D<size_t>( 1, 1 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test10 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //shallow negative line
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 0, 1 ),
                                                         Coordinate2D<size_t>( 1, 1 ),
                                                         Coordinate2D<size_t>( 2, 1 ),
                                                         Coordinate2D<size_t>( 3, 1 ),
                                                         Coordinate2D<size_t>( 4, 1 ),
                                                         Coordinate2D<size_t>( 5, 0 ),
                                                         Coordinate2D<size_t>( 6, 0 ),
                                                         Coordinate2D<size_t>( 7, 0 ),
                                                         Coordinate2D<size_t>( 8, 0 ),
                                                         Coordinate2D<size_t>( 9, 0 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 0, 1 ), Coordinate2D<size_t>( 9, 0 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test11 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //negative diagonal where x < y (i.e.: steep decline)
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 1, 9 ),
                                                         Coordinate2D<size_t>( 1, 8 ),
                                                         Coordinate2D<size_t>( 1, 7 ),
                                                         Coordinate2D<size_t>( 2, 6 ),
                                                         Coordinate2D<size_t>( 2, 5 ),
                                                         Coordinate2D<size_t>( 2, 4 ),
                                                         Coordinate2D<size_t>( 2, 3 ),
                                                         Coordinate2D<size_t>( 3, 2 ),
                                                         Coordinate2D<size_t>( 3, 1 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 1, 9 ), Coordinate2D<size_t>( 3, 1 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, getLinePoints_test12 ) {
    using eadlib::cli::braille::raster::getLinePoints;
    using eadlib::Coordinate2D;
    //very steep negative decline
    auto expected = std::list<Coordinate2D<size_t>>( {
                                                         Coordinate2D<size_t>( 0, 9 ),
                                                         Coordinate2D<size_t>( 0, 8 ),
                                                         Coordinate2D<size_t>( 0, 7 ),
                                                         Coordinate2D<size_t>( 0, 6 ),
                                                         Coordinate2D<size_t>( 0, 5 ),
                                                         Coordinate2D<size_t>( 1, 4 ),
                                                         Coordinate2D<size_t>( 1, 3 ),
                                                         Coordinate2D<size_t>( 1, 2 ),
                                                         Coordinate2D<size_t>( 1, 1 ),
                                                         Coordinate2D<size_t>( 1, 0 )
                                                     } );
    auto returned = getLinePoints<size_t>( Coordinate2D<size_t>( 0, 9 ), Coordinate2D<size_t>( 1, 0 ) );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, generateRasterAreaPoints_test1 ) {
    using eadlib::cli::braille::raster::generateRasterAreaPoints;
    using eadlib::Coordinate2D;
    //1x1 box
    auto returned = generateRasterAreaPoints<unsigned>( Coordinate2D<unsigned>( 1, 1 ), Coordinate2D<unsigned>( 1, 1 ) );
    auto expected = std::list<Coordinate2D<unsigned>>( { Coordinate2D<unsigned>( 1, 1 ) } );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, generateRasterAreaPoints_test2 ) {
    using eadlib::cli::braille::raster::generateRasterAreaPoints;
    using eadlib::Coordinate2D;
    //2x1 box
    auto returned = generateRasterAreaPoints<unsigned>( Coordinate2D<unsigned>( 1, 1 ), Coordinate2D<unsigned>( 2, 1 ) );
    auto expected = std::list<Coordinate2D<unsigned>>( {
                                                           Coordinate2D<unsigned>( 1, 1 ),
                                                           Coordinate2D<unsigned>( 2, 1 )
                                                       } );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, generateRasterAreaPoints_test3 ) {
    using eadlib::cli::braille::raster::generateRasterAreaPoints;
    using eadlib::Coordinate2D;
    //1x2 box
    auto returned = generateRasterAreaPoints<unsigned>( Coordinate2D<unsigned>( 1, 1 ), Coordinate2D<unsigned>( 1, 2 ) );
    auto expected = std::list<Coordinate2D<unsigned>>( {
                                                           Coordinate2D<unsigned>( 1, 1 ),
                                                           Coordinate2D<unsigned>( 1, 2 )
                                                       } );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, generateRasterAreaPoints_test4 ) {
    using eadlib::cli::braille::raster::generateRasterAreaPoints;
    using eadlib::Coordinate2D;
    //2x2 box
    auto returned = generateRasterAreaPoints<unsigned>( Coordinate2D<unsigned>( 0, 0 ), Coordinate2D<unsigned>( 1, 1 ) );
    auto expected = std::list<Coordinate2D<unsigned>>( {
                                                           Coordinate2D<unsigned>( 0, 0 ),
                                                           Coordinate2D<unsigned>( 0, 1 ),
                                                           Coordinate2D<unsigned>( 1, 0 ),
                                                           Coordinate2D<unsigned>( 1, 1 )
                                                       } );
    ASSERT_EQ( expected.size(), returned.size() );
    auto expected_it = expected.cbegin();
    auto returned_it = returned.cbegin();
    while( expected_it != expected.cend() && returned_it != returned.cend() ) {
        ASSERT_EQ( *expected_it, *returned_it );
        expected_it++;
        returned_it++;
    }
}

TEST( braille_raster_Tests, generateRasterAreaPoints_test5 ) {
    using eadlib::cli::braille::raster::generateRasterAreaPoints;
    using eadlib::Coordinate2D;
    //4x5 box
    auto returned = generateRasterAreaPoints<unsigned>( Coordinate2D<unsigned>( 1, 1 ), Coordinate2D<unsigned>( 4, 5 ) );
    ASSERT_EQ( 20, returned.size() );
}

#endif //EADLIB_BRAILLE_RASTER_TEST_H
