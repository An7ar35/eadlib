#ifndef EADLIB_TESTS_AVLTREE_TEST_H
#define EADLIB_TESTS_AVLTREE_TEST_H

#include <random>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../../src/datastructure/AVLTree.h"

namespace AVLTree_Test {
    template<typename K, typename V> void print( K &key, V &value ) {
        std::cout << "[" << key << "]=" << value << std::endl;
    }
}

TEST( AVLTree_Tests, Constructor ) {
    eadlib::AVLTree<int, std::string> avl;
    ASSERT_TRUE( avl.empty() );
    ASSERT_EQ( 0, avl.size() );
    ASSERT_EQ( 0, avl.height() );
}

TEST( AVLTree_Tests, Copy_Constructor ) {
    auto avl = eadlib::AVLTree<int, std::string>( { { 0, "zero" }, { 1, "one" }, { 2, "two" }, { 3, "three" } } );
    auto copy = avl;
    std::cout << avl.str() << std::endl;
    std::cout << copy.str() << std::endl;
    ASSERT_EQ( copy.size(), avl.size() );
    ASSERT_EQ( copy.height(), avl.height() );
    for( auto it1 = avl.cbegin_levelorder(), it2 = copy.cbegin_levelorder();
         it1 != avl.cend_levelorder() && it2 != copy.cend_levelorder();
         ++it1, ++it2 ) {
        ASSERT_EQ( it1->key(), it2->key() );
        ASSERT_EQ( it1->value(), it2->value() );
    }
}

TEST( AVLTree_Tests, Move_Constructor ) {
    auto avl = eadlib::AVLTree<int, std::string>( { { 0, "zero" }, { 1, "one" }, { 2, "two" }, { 3, "three" } } );
    auto moved = std::move( avl );
    ASSERT_TRUE( avl.empty() );
    ASSERT_THROW( avl.value( 0 ), std::out_of_range );
    ASSERT_EQ( 4, moved.size() );

    auto returned = std::list<int>();
    for( auto it = moved.cbegin_inorder(); it != moved.cend_inorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 0, 1, 2, 3 ) );
}

TEST( AVLTree_Tests, add_duplicate_fail ) {
    eadlib::AVLTree<int, std::string> avl;
    avl.add( 0, "val_" + std::to_string( 0 ) );
    ASSERT_FALSE( avl.add(0, "") );
}

TEST( AVLTree_Tests, add1 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 6, 6 );
    avl.add( 7, 7 );
    avl.add( 4, 4 );
    avl.add( 5, 5 );
    avl.add( 3, 3 );
    avl.add( 2, 2 );
    ASSERT_EQ( 6, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 4, 3, 6, 2, 5, 7 ) );
}

TEST( AVLTree_Tests, add2 ) {
    std::list<int> returned;
    auto avl = eadlib::AVLTree<int, std::string>(
        { { 408, "408" }, { 68, "68" }, { 453, "453" }, { 418, "418" }, { 64, "64" },
          { 485, "485" }, { 457, "457" }, { 111, "111" }, { 317, "317" }, { 155, "155 rl" },
          { 49, "49" }, { 274, "274" }, { 140, "140" }, { 95, "95" }, { 497, "497" },
          { 479, "479 rl" }, { 499, "499" }, { 483, "483" }, { 484, "484 *" }
        }
    );
    ASSERT_EQ( 19, avl.size() );
    ASSERT_EQ( 5, avl.height() );
    std::list<int> level_order = { 408, 68, 457, 64, 155, 453, 485, 49, 111, 317, 418, 483, 497, 95, 140, 274, 479, 484, 499 };
    auto avl_it1 = avl.cbegin_levelorder();
    auto lo_it = level_order.cbegin();
    for( ; avl_it1 != avl.cend_levelorder() && lo_it != level_order.cend(); ++avl_it1, ++lo_it ) {
        ASSERT_EQ( *lo_it, avl_it1->key() );
    }
    std::list<int> in_order = { 49, 64, 68, 95, 111, 140, 155, 274, 317, 408, 418, 453, 457, 479, 483, 484, 485, 497, 499 };
    auto avl_it2 = avl.cbegin_inorder();
    auto io_it = in_order.cbegin();
    for( ; avl_it2 != avl.cend_inorder() && io_it != in_order.cend(); ++avl_it2, ++io_it ) {
        ASSERT_EQ( *io_it, avl_it2->key() );
    }
}

TEST( AVLTree_Tests, add3 ) {
    auto avl = eadlib::AVLTree<int, std::string>(
        { { 3771, "3771" }, { 2685, "2685" }, { 310, "310" }, { 4826, "4826" }, { 1324, "1324" },
          { 4428, "4428" }, { 2163, "2163" }, { 691, "691" }, { 4946, "4946" }, { 2937, "2937" }
        }
    );
    ASSERT_EQ( 10, avl.size() );
    ASSERT_EQ( 4, avl.height() );
    std::list<int> returned;
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 2685, 1324, 4428, 310, 2163, 3771, 4826, 691, 2937, 4946 ) );
    std::list<int> inorder;
    for( auto it2 = avl.cbegin_inorder(); it2 != avl.cend_inorder(); ++it2 ) {
        inorder.emplace_back( it2->key() );
    }
    ASSERT_THAT( inorder, testing::ElementsAre( 310, 691, 1324, 2163, 2685, 2937, 3771, 4428, 4826, 4946 ) );
}

TEST( AVLTree_Tests, add_fuzz_random ) {
    std::mt19937 gen;
    gen.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> dice(1, 5000);
    std::vector<unsigned long> expected;
    eadlib::AVLTree<unsigned long, unsigned long> avl;
    for( int i = 0; i < 2500; i++ ) {
        unsigned long number = dice(gen);
        std::cout << "Trying to add " << number << std::endl;
        if( avl.add( number, number ) )
            expected.emplace_back( number );
    }
    std::sort(expected.begin(), expected.end());
    auto it1 = avl.cbegin_inorder();
    auto it2 = expected.cbegin();
    std::cout << "expected.size: " << expected.size() << "\t avl.size: " << avl.size() << std::endl;
    ASSERT_EQ(expected.size(), avl.size());
    for( ; it1 != avl.cend_inorder() && it2 != expected.cend(); ++it1, ++it2 ) {
        std::cout << "expected: " << *it2 << "\t got: " << it1->key() << std::endl;
    }
    auto avl_it = avl.cbegin_inorder();
    auto vector_it = expected.cbegin();
    for( ; avl_it != avl.cend_inorder() && vector_it != expected.cend(); ++avl_it, ++vector_it ) {
        ASSERT_EQ(*vector_it, avl_it->_key);
    }
}

/**
 *    5
 *     \
 *      10
 *     /
 *    6
 */
TEST( AVLTree_Tests, add_zig_zag1 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 5, 5 );
    avl.add( 10, 10 );
    avl.add( 6, 6 );
    ASSERT_EQ( 3, avl.size() );
    ASSERT_EQ( 2, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 6, 5, 10 ) );
}

/**
 *    14
 *   /  \
 *  13  23
 *        \
 *        64
 *        /
 *       31
 */
TEST( AVLTree_Tests, add_zig_zag2 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 14, 14 );
    avl.add( 13, 13 );
    avl.add( 23, 23 );
    avl.add( 64, 64 );
    avl.add( 31, 31 );
    ASSERT_EQ( 5, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 14, 13, 31, 23, 64 ) );
}

/**
 *    10
 *    /
 *   5
 *    \
 *     6
 */
TEST( AVLTree_Tests, add_zag_zig1 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 10, 10 );
    avl.add( 5, 5 );
    avl.add( 6, 6 );
    ASSERT_EQ( 3, avl.size() );
    ASSERT_EQ( 2, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 6, 5, 10 ) );
}

TEST( AVLTree_Tests, add_in_increasing_order1 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 0, 0 );
    avl.add( 1, 1 );
    avl.add( 2, 2 );
    avl.add( 3, 3 );
    ASSERT_EQ( 4, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 1, 0, 2, 3 ) );
}

TEST( AVLTree_Tests, add_in_decreasing_order1 ) {
    eadlib::AVLTree<int, int> avl;
    std::list<int> returned;
    avl.add( 3, 3 );
    avl.add( 2, 2 );
    avl.add( 1, 1 );
    avl.add( 0, 0 );
    ASSERT_EQ( 4, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 2, 1, 3, 0 ) );
}

TEST( AVLTree_Tests, add_zig_zag3 ) {
    eadlib::AVLTree<int, std::string> avl;
    std::list<int> returned;
    //6, 4, 7, 3, 8, 2, 9, 1, 10, 0
    for( int i = 1, val = 5; i <= 5; i++ ) {
        avl.add( val + i, "val_" + std::to_string( val + i ) );
        avl.add( val - i, "val_" + std::to_string( val - i ) );
    }
    ASSERT_EQ( 10, avl.size() );
    ASSERT_EQ( 4, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 6, 3, 8, 1, 4, 7, 9, 0, 2, 10 ) );
}

TEST( AVLTree_Tests, add_zag_zig2 ) {
    eadlib::AVLTree<int, std::string> avl;
    std::list<int> returned;
    //4, 6, 3, 7, 2, 8, 1, 9, 0, 10
    for( int i = 1, val = 5; i <= 5; i++ ) {
        avl.add( val - i, "val_" + std::to_string( val - i) );
        avl.add( val + i, "val_" + std::to_string( val + i ) );
    }
    ASSERT_EQ( 10, avl.size() );
    ASSERT_EQ( 4, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 4, 2, 7, 1, 3, 6, 9, 0, 8, 10 ) );
}

TEST( AVLTree_Tests, add_in_increasing_order2 ) {
    eadlib::AVLTree<int, std::string> avl;
    std::list<int> returned;
    for( int i = 0; i < 10; i++ ) {
        avl.add( i, "val_" + std::to_string( i ) );
    }
    ASSERT_EQ( 10, avl.size() );
    ASSERT_EQ( 4, avl.height() );
    for( auto it = avl.cbegin_inorder(); it != avl.cend_inorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ) );
}

TEST( AVLTree_Tests, add_in_decreasing_order2 ) {
    eadlib::AVLTree<int, std::string> avl;
    std::list<int> returned;
    for( int i = 9; i >= 0; i-- ) {
        avl.add( i, "val_" + std::to_string( i ) );
    }
    ASSERT_EQ( 10, avl.size() );
    ASSERT_EQ( 4, avl.height() );
    for( auto it = avl.cbegin_inorder(); it != avl.cend_inorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ) );
}

TEST( AVLTree_Tests, remove1 ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i <= 10; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
        ASSERT_NO_THROW( ASSERT_EQ( std::string("val_") + std::to_string( i ), avl.value( i ) ) );
        ASSERT_EQ( i + 1, avl.size());
    }
    avl.remove(7);
    avl.remove(4);
    avl.remove(1);
    avl.remove(2);
    std::list<int> returned;
    ASSERT_EQ( 7, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 6, 3, 9, 0, 5, 8, 10 ) );
}

TEST( AVLTree_Tests, remove2 ) {
    auto avl = eadlib::AVLTree<int, std::string>(
        { { 6, "val_6" }, { 4, "val_4" }, { 7, "val_7" }, { 1, "val_1" }, { 5, "val_5" },
          { 8, "val_8" }, { 0, "val_0" }, { 2, "val_2" }, { 3, "val_3" }
        }
    );
    avl.remove(7);
    avl.remove(5);
    avl.remove(2);
    std::list<int> returned;
    ASSERT_EQ( 6, avl.size() );
    ASSERT_EQ( 3, avl.height() );
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned.emplace_back( it->key() );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 4, 1, 6, 0, 3, 8 ) );
}

TEST( AVLTree_Tests, remove_in_increasing_order1 ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 5; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
        ASSERT_NO_THROW( ASSERT_EQ( std::string("val_") + std::to_string( i ), avl.value( i ) ) );
        ASSERT_EQ( i + 1, avl.size());
    }

    auto size = avl.size() - 1;
    for( int i = 0; i < 5; i++ ) {
        avl.remove( i );
        ASSERT_THROW( avl.value( i ), std::out_of_range );
        ASSERT_EQ( size--, avl.size() );
    }
}

TEST( AVLTree_Tests, remove_in_increasing_order2 ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 100; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
        ASSERT_NO_THROW( ASSERT_EQ( std::string("val_") + std::to_string( i ), avl.value( i ) ) );
        ASSERT_EQ( i + 1, avl.size());
    }

    auto size = avl.size() - 1;
    for( int i = 0; i < 100; i++ ) {
        avl.remove( i );
        ASSERT_THROW( avl.value( i ), std::out_of_range );
        ASSERT_EQ( size--, avl.size() );
    }
}

TEST( AVLTree_Tests, remove_in_decreasing_order1 ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 5; i++) {
        avl.add( i, "val_" + std::to_string( i ) );
        ASSERT_EQ( i + 1, avl.size());
    }

    auto size = avl.size() - 1;
    for( int i = 4; i >= 0; i-- ) {
        avl.remove( i );
        ASSERT_THROW( avl.value( i ), std::out_of_range );
        ASSERT_EQ( size--, avl.size() );
    }
}

TEST( AVLTree_Tests, remove_in_decreasing_order2 ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 100; i++) {
        avl.add( i, "val_" + std::to_string( i ) );
        ASSERT_EQ( i + 1, avl.size());
    }

    auto size = avl.size() - 1;
    for( int i = 99; i >= 0; i-- ) {
        avl.remove( i );
        ASSERT_THROW( avl.value( i ), std::out_of_range );
        ASSERT_EQ( size--, avl.size() );
    }
}

TEST( AVLTree_Tests, add_remove_add ) {
    auto avl = eadlib::AVLTree<int, std::string>();
    ASSERT_TRUE( avl.empty() );
    avl.add( 1, "one" );
    ASSERT_EQ( 1, avl.size() );
    ASSERT_NO_THROW( ASSERT_EQ( "one", avl.value( 1 ) ) );
    avl.remove( 1 );
    ASSERT_TRUE( avl.empty() );
    ASSERT_THROW( avl.value( 1 ), std::out_of_range );
    avl.add( 2, "two" );
    ASSERT_EQ( 1, avl.size() );
    ASSERT_NO_THROW( ASSERT_EQ( "two", avl.value( 2 ) ) );
    ASSERT_THROW( avl.value( 1 ), std::out_of_range );
}

TEST( AVLTree_Tests, clear ) {
    auto avl = eadlib::AVLTree<int, std::string>( { { 0, "zero" }, { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" },
                                                    { 5, "five" }, { 6, "six" }, { 7, "seven" }, { 8, "eight" }, { 9, "nine" } } );
    ASSERT_FALSE( avl.empty() );
    avl.clear();
    ASSERT_TRUE( avl.empty() );
    EXPECT_THROW( avl.value( 0 ), std::out_of_range );
}

TEST( AVLTree_Tests, key ) {
    auto avl = eadlib::AVLTree<int, std::string>( { { 0, "zero" }, { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" },
                                                    { 5, "five" }, { 6, "six" }, { 7, "seven" }, { 8, "eight" }, { 9, "nine" } } );
    ASSERT_NO_THROW( ASSERT_EQ( 3, avl.key( "three" ) ) );
    ASSERT_THROW( avl.key( "one hundred" ), std::out_of_range );
}

TEST( AVLTree_Tests, value ) {
    auto avl = eadlib::AVLTree<int, std::string>( { { 0, "zero" }, { 1, "one" }, { 2, "two" }, { 3, "three" }, { 4, "four" },
                                                    { 5, "five" }, { 6, "six" }, { 7, "seven" }, { 8, "eight" }, { 9, "nine" } } );
    ASSERT_NO_THROW( ASSERT_EQ( "three", avl.value( 3 ) ) );
    ASSERT_THROW( avl.value( 100 ), std::out_of_range );
}

TEST( AVLTree_Tests, preorder_iterator ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 10; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
    }
    auto returned_keys = std::list<int>();
    auto returned_vals = std::list<std::string>();
    for( auto it = avl.cbegin_preorder(); it != avl.cend_preorder(); ++it ) {
        returned_keys.emplace_back( it->key() );
        returned_vals.emplace_back( it->value() );
    }

    using testing::ElementsAre;
    using testing::StrEq;
    EXPECT_THAT( returned_keys, ElementsAre( 3, 1, 0, 2, 7, 5, 4, 6, 8, 9 ) );
    EXPECT_THAT( returned_vals, ElementsAre( StrEq( "val_3" ), StrEq( "val_1" ), StrEq( "val_0" ),
                                             StrEq( "val_2" ), StrEq( "val_7" ), StrEq( "val_5" ),
                                             StrEq( "val_4" ), StrEq( "val_6" ), StrEq( "val_8" ),
                                             StrEq( "val_9" ) ) );
}

TEST( AVLTree_Tests, inorder_iterator ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 10; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
    }
    auto returned_keys = std::list<int>();
    auto returned_vals = std::list<std::string>();
    for( auto it = avl.cbegin_inorder(); it != avl.cend_inorder(); ++it ) {
        returned_keys.emplace_back( it->key() );
        returned_vals.emplace_back( it->value() );
    }

    using testing::ElementsAre;
    using testing::StrEq;
    EXPECT_THAT( returned_keys, ElementsAre( 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 ) );
    EXPECT_THAT( returned_vals, ElementsAre( StrEq( "val_0" ), StrEq( "val_1" ), StrEq( "val_2" ),
                                             StrEq( "val_3" ), StrEq( "val_4" ), StrEq( "val_5" ),
                                             StrEq( "val_6" ), StrEq( "val_7" ), StrEq( "val_8" ),
                                             StrEq( "val_9" ) ) );
}

TEST( AVLTree_Tests, postorder_iterator ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 10; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
    }
    auto returned_keys = std::list<int>();
    auto returned_vals = std::list<std::string>();
    for( auto it = avl.cbegin_postorder(); it != avl.cend_postorder(); ++it ) {
        returned_keys.emplace_back( it->key() );
        returned_vals.emplace_back( it->value() );
    }

    using testing::ElementsAre;
    using testing::StrEq;
    EXPECT_THAT( returned_keys, ElementsAre( 0, 2, 1, 4, 6, 5, 9, 8, 7, 3 ) );
    EXPECT_THAT( returned_vals, ElementsAre( StrEq( "val_0" ), StrEq( "val_2" ), StrEq( "val_1" ),
                                             StrEq( "val_4" ), StrEq( "val_6" ), StrEq( "val_5" ),
                                             StrEq( "val_9" ), StrEq( "val_8" ), StrEq( "val_7" ),
                                             StrEq( "val_3" ) ) );
}

TEST( AVLTree_Tests, levelorder_iterator ) {
    eadlib::AVLTree<int, std::string> avl;
    for( int i = 0; i < 10; i++) {
        avl.add( i, std::string("val_") + std::to_string( i ) );
    }
    auto returned_keys = std::list<int>();
    auto returned_vals = std::list<std::string>();
    for( auto it = avl.cbegin_levelorder(); it != avl.cend_levelorder(); ++it ) {
        returned_keys.emplace_back( it->key() );
        returned_vals.emplace_back( it->value() );
    }

    using testing::ElementsAre;
    using testing::StrEq;
    EXPECT_THAT( returned_keys, ElementsAre( 3, 1, 7, 0, 2, 5, 8, 4, 6, 9 ) );
    EXPECT_THAT( returned_vals, ElementsAre( StrEq( "val_3" ), StrEq( "val_1" ), StrEq( "val_7" ),
                                             StrEq( "val_0" ), StrEq( "val_2" ), StrEq( "val_5" ),
                                             StrEq( "val_8" ), StrEq( "val_4" ), StrEq( "val_6" ),
                                             StrEq( "val_9" ) ) );
}
#endif //EADLIB_TESTS_AVLTREE_TEST_H
