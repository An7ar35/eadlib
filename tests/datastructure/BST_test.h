#ifndef EADLIB_TESTS_BST_TEST_H
#define EADLIB_TESTS_BST_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/BST.h"

namespace BST_Test {
    template<typename K, typename V> void print( K &key, V &value ) {
        std::cout << "[" << key << "]=" << value << std::endl;
    }
}


TEST( BST_Tests, Constructor ) {
    eadlib::BST<int,std::string> bst;
    bst.add( 1,"" );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    bst.add( 3,"" );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    bst.add( 2,"" );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    bst.add( 4,"" );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    EXPECT_TRUE( bst.search( 3 ) );
    EXPECT_TRUE( bst.count() == 4 );
    EXPECT_TRUE( bst.remove( 3 ) );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    EXPECT_TRUE( bst.count() == 3 );
    EXPECT_TRUE( bst.remove( 1 ) ); //Goes tits up
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    EXPECT_TRUE( bst.count() == 2 );
    EXPECT_TRUE( bst.remove( 2 ) );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    EXPECT_TRUE( bst.count() == 1 );
    EXPECT_TRUE( bst.remove( 4 ) );
    EXPECT_TRUE( bst.count() == 0 );
//    bst.levelOrder( BST_Test::print<int> ); std::cout << std::endl;
    EXPECT_FALSE( bst.remove( 666 ) );
}

TEST( BST_Tests, getKey ) {
    auto bst = eadlib::BST<int,std::string>( {
                                                 { 1, "one" },
                                                 { 6, "six" },
                                                 { 2, "two" },
                                                 { 7, "seven" },
                                                 { 3, "three" },
                                                 { 8, "eight" },
                                                 { 4, "four" },
                                                 { 9, "nine" },
                                                 { 5, "five" },
                                                 { 10, "five" }
                                             } );
    ASSERT_EQ( 8, bst.getKey( "eight" ) );
    ASSERT_EQ( 5, bst.getKey( "five" ) );
    ASSERT_EQ( 4, bst.getKey( "four" ) );
}

TEST( BST_Tests, getKey_fail ) {
    auto bst = eadlib::BST<int,std::string>( {
                                                 { 1, "one" },
                                                 { 6, "six" },
                                                 { 2, "two" },
                                                 { 7, "seven" },
                                                 { 3, "three" },
                                                 { 8, "eight" },
                                                 { 4, "four" },
                                                 { 9, "nine" },
                                                 { 5, "five" }
                                             } );
    ASSERT_THROW( bst.getKey( "ein" ), std::out_of_range );
}

TEST( BST_Tests, getValue ) {
    auto bst = eadlib::BST<int,std::string>( {
                                                 { 1, "one" },
                                                 { 6, "six" },
                                                 { 2, "two" },
                                                 { 7, "seven" },
                                                 { 3, "three" },
                                                 { 8, "eight" },
                                                 { 4, "four" },
                                                 { 9, "nine" },
                                                 { 5, "five" },
                                                 { 10, "five" }
                                             } );
    ASSERT_EQ( "one", bst.getValue( 1 ) );
    ASSERT_EQ( "five", bst.getValue( 5 ) );
    ASSERT_EQ( "five", bst.getValue( 10 ) );
    ASSERT_EQ( "six", bst.getValue( 6 ) );
}

TEST( BST_Tests, getValue_fail ) {
    auto bst = eadlib::BST<int,std::string>( {
                                                 { 1, "one" },
                                                 { 6, "six" },
                                                 { 2, "two" },
                                                 { 7, "seven" },
                                                 { 3, "three" },
                                                 { 8, "eight" },
                                                 { 4, "four" },
                                                 { 9, "nine" },
                                                 { 5, "five" }
                                             } );
    ASSERT_THROW( bst.getValue( 10 ), std::out_of_range );
}

#endif //EADLIB_TESTS_BST_TEST_H
