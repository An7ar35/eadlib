#ifndef EADLIB_TESTS_BAG_TEST_H
#define EADLIB_TESTS_BAG_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/Bag.h"

TEST( Bag_Tests, Constructor ) {
    eadlib::Bag<int> bag = eadlib::Bag<int>();
    ASSERT_TRUE( bag.getCapacity() == -1 );
}

TEST( Bag_Tests, Constructor_with_value ) {
    eadlib::Bag<int> bag = eadlib::Bag<int>( 1 );
    ASSERT_TRUE( bag.getCapacity() == 1 );
}

TEST( Bag_Tests, put ) {
    eadlib::Bag<int> bag = eadlib::Bag<int>( 1 );
    ASSERT_TRUE( bag.put( int( 10 ) ) );
    ASSERT_FALSE( bag.put( int( 20 ) ) );
    int item = bag.grab();
    ASSERT_TRUE( item == 10 );
}

TEST( Bag_Tests, put_multiple ) {
    eadlib::Bag<int> bag = eadlib::Bag<int>();
    unsigned int n = 10;
    for( unsigned int i = 0; i < n; i++ ) {
        bag.put( int( i ) );
    }
    ASSERT_TRUE( bag.getNumberOfItems() == n );
}

TEST( Bag_Tests, grab ) {
    int n = 100;
    eadlib::Bag<int> bag = eadlib::Bag<int>( n );
    for( unsigned int i = 0; i < n; i++ ) {
        bag.put( int( i ) );
    }
    ASSERT_TRUE( bag.getNumberOfItems() == n && bag.getCapacity() == n );
    EXPECT_NO_THROW( {
        for( int i = 0; i < n; i++ ) {
            bag.grab();
        }
                     } );
    ASSERT_TRUE( bag.getNumberOfItems() == 0 );
}

TEST( Bag_Tests, grab_exception ) {
    eadlib::Bag<int> bag = eadlib::Bag<int>( 1 );
    ASSERT_TRUE( bag.put( int( 10 ) ) );
    EXPECT_NO_THROW( {
        int item = bag.grab();
        ASSERT_TRUE( item == 10 );
                     } );
    ASSERT_THROW( bag.grab(), std::out_of_range );
}

TEST( Bag_Tests, getNumberOfItems ) {
    int n = 100;
    eadlib::Bag<int> bag = eadlib::Bag<int>( n );
}

TEST( Bag_Tests, isFull ) {
    int n = 100;
    eadlib::Bag<int> bag1 = eadlib::Bag<int>();
    for( unsigned int i = 0; i < n; i++ ) {
        bag1.put( int( i ) );
    }
    ASSERT_FALSE( bag1.isFull() );
    eadlib::Bag<int> bag2 = eadlib::Bag<int>( n );
    ASSERT_FALSE( bag2.isFull() );
    for( unsigned int i = 0; i < n; i++ ) {
        bag2.put( int( i ) );
    }
    ASSERT_TRUE( bag2.isFull() );
}

TEST( Bag_Tests, isEmpty ) {
    eadlib::Bag<int> bag1 = eadlib::Bag<int>();
    eadlib::Bag<int> bag2 = eadlib::Bag<int>( 100 );
    ASSERT_TRUE( bag1.isEmpty() );
    ASSERT_TRUE( bag2.isEmpty() );
}

#endif //EADLIB_TESTS_BAG_TEST_H
