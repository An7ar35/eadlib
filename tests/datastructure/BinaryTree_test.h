#ifndef EADLIB_TESTS_BINARYTREE_TEST_H
#define EADLIB_TESTS_BINARYTREE_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/BinaryTree.h"
#include "../../src/logger/Logger.h"

template<typename K, typename V> void print( K &key, V &value ) {
    std::cout << "[" << key << "]=" << value << std::endl;
}

TEST( BinaryTree_Tests, Constructor ) {
    eadlib::BinaryTree<std::string,std::string> tree1( "B","" );
    eadlib::BinaryTree<std::string,std::string> tree2( "C","" );
    eadlib::BinaryTree<std::string,std::string> mid1( "A","", tree1, tree2 );
    eadlib::BinaryTree<std::string,std::string> tree3( "E","" );
    eadlib::BinaryTree<std::string,std::string> tree4( "F","" );
    eadlib::BinaryTree<std::string,std::string> mid2( "D","", tree3, tree4 );
    eadlib::BinaryTree<std::string,std::string> new_tree( "Groot","", mid1, mid2 );
    new_tree.preOrder( print<std::string,std::string> );
    std::cout << std::endl;
    new_tree.inOrder( print<std::string,std::string> );
    std::cout << std::endl;
    new_tree.postOrder( print<std::string,std::string> );
    std::cout << std::endl;
    new_tree.levelOrder( print<std::string,std::string> );
    std::cout << std::endl;
    std::cout << "Number of items in tree  : " << new_tree.count() << std::endl;
    std::cout << "Number of levels in tree : " << new_tree.height() << std::endl;
    EXPECT_TRUE( new_tree.isFull() );
}

TEST( BinaryTree_Tests, Contructor_InitializerList ) {
    eadlib::BinaryTree<std::string,std::string> bt( { {"Groot", ""}, {"A",""}, {"B",""}, {"C",""}, {"D",""}, {"E",""}, {"F",""}, {"G",""}, {"H",""} } );
    bt.levelOrder( print<std::string,std::string> );
    std::cout << "\n" << "Is complete: " << bt.isComplete() << "\n" << "Is Full: " << bt.isFull() << std::endl;
    std::cout << "Levels: " << bt.height() << "\nCount: " << bt.count() << std::endl;
}

TEST( BinaryTree_Tests, isComplete ) {

}

TEST( BinaryTree_Tests, isFull ) {

}
#endif //EADLIB_TESTS_BINARYTREE_TEST_H
