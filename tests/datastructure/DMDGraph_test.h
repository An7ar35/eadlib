#ifndef EADLIB_DMDGRAPH_TEST_H
#define EADLIB_DMDGRAPH_TEST_H


#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "../../src/datastructure/DMDGraph.h"

namespace unit_tests::DMDGraph_Tests {
    eadlib::DMDGraph<int,std::string,std::string> createPentagramGraph() {
        eadlib::DMDGraph<int,std::string,std::string> graph;
        graph.addNode( 0, "zero" );
        graph.addNode( 1, "one" );
        graph.addNode( 2, "two" );
        graph.addNode( 3, "three" );
        graph.addNode( 4, "four" );
        graph.addNode( 5, "five" );
        //Pentagram madness :) \m/
        graph.createDirectedEdge( 0, 1, "Outer" );
        graph.createDirectedEdge( 1, 2, "Outer" );
        graph.createDirectedEdge( 2, 3, "Outer" );
        graph.createDirectedEdge( 3, 4, "Outer" );
        graph.createDirectedEdge( 4, 5, "Outer" );
        graph.createDirectedEdge( 5, 0, "Outer" );
        graph.createDirectedEdge( 0, 2, "Inner1" );
        graph.createDirectedEdge( 2, 0, "Inner1" );
        graph.createDirectedEdge( 2, 4, "Inner1" );
        graph.createDirectedEdge( 4, 2, "Inner1" );
        graph.createDirectedEdge( 4, 0, "Inner1" );
        graph.createDirectedEdge( 0, 4, "Inner1" );
        graph.createDirectedEdge( 1, 3, "Inner2" );
        graph.createDirectedEdge( 3, 1, "Inner2" );
        graph.createDirectedEdge( 3, 5, "Inner2" );
        graph.createDirectedEdge( 5, 3, "Inner2" );
        graph.createDirectedEdge( 5, 1, "Inner2" );
        graph.createDirectedEdge( 1, 5, "Inner2" );
        return graph;
    }
}

TEST( DMDGraph_Tests, copy_constructor ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    for( int i = 1; i <= 10; i++ ) { //will create nodes 0-10
        ASSERT_TRUE( graph.createDirectedEdge( { i, "val_" + std::to_string( i ) }, { i - 1, "val_" + std::to_string( i - 1 ) }, "Friend" ) );
        ASSERT_EQ( i, graph.size() );
    }
    ASSERT_EQ( 11, graph.nodeCount() );
    auto copy = graph;
    ASSERT_EQ( 11, graph.nodeCount() );
    ASSERT_EQ( 10, graph.size() );
    ASSERT_EQ( 11, copy.nodeCount() );
    ASSERT_EQ( 10, copy.size() );
    for( int i = 1; i <= 10; i++ ) {
        ASSERT_TRUE( copy.removeDirectedEdge( i, i - 1, "Friend" ) );
    }
    ASSERT_EQ( 0, copy.size() );
    for( int i = 0; i <= 10; i++ ) {
        ASSERT_TRUE( copy.removeNode( i ) );
    }
    ASSERT_EQ( 0, copy.nodeCount() );

}

TEST( DMDGraph_Tests, move_constructor ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    for( int i = 1; i <= 10; i++ ) { //will create nodes 0-10
        ASSERT_TRUE( graph.createDirectedEdge( { i, "val_" + std::to_string( i ) }, { i - 1, "val_" + std::to_string( i - 1 ) }, "Friend" ) );
        ASSERT_EQ( i, graph.size() );
    }
    ASSERT_EQ( 11, graph.nodeCount() );
    auto moved = std::move( graph );
    ASSERT_EQ( 0, graph.nodeCount() );
    ASSERT_EQ( 0, graph.size() );
    ASSERT_EQ( 11, moved.nodeCount() );
    ASSERT_EQ( 10, moved.size() );
    for( int i = 1; i <= 10; i++ ) {
        ASSERT_TRUE( moved.removeDirectedEdge( i, i - 1, "Friend" ) );
    }
    ASSERT_EQ( 0, moved.size() );
    for( int i = 0; i <= 10; i++ ) {
        ASSERT_TRUE( moved.removeNode( i ) );
    }
    ASSERT_EQ( 0, moved.nodeCount() );
}

TEST( DMDGraph_Tests, addNode ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_TRUE( graph.addNode( 0, "zero" ) );
    ASSERT_FALSE( graph.addNode( 0, "zero" ) );
    ASSERT_EQ( 1, graph.nodeCount() );
    ASSERT_TRUE( graph.addNode( 1, "one" ) );
    ASSERT_EQ( 2, graph.nodeCount() );
}

TEST( DMDGraph_Tests, emplaceNode ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_EQ( "zero", graph.emplaceNode( 0, "zero" ).first->second.value );
    ASSERT_EQ( "zero", graph.emplaceNode( 0, "zero_bis" ).first->second.value );
    ASSERT_EQ( 1, graph.nodeCount() );
    ASSERT_EQ( "one", graph.emplaceNode( 1, "one" ).first->second.value );
    ASSERT_EQ( 2, graph.nodeCount() );
    graph.emplaceNode( 1, "one" ).first->second.value = "new_one";
    ASSERT_EQ( "new_one", graph.at( 1 ) );
}


TEST( DMDGraph_Tests, removeNode ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_TRUE( graph.addNode( 0, "zero" ) );
    ASSERT_TRUE( graph.addNode( 1, "one" ) );
    ASSERT_TRUE( graph.addNode( 2, "two" ) );
    ASSERT_EQ( 3, graph.nodeCount() );
    ASSERT_TRUE( graph.removeNode( 0 ) );
    ASSERT_EQ( 2, graph.nodeCount() );
    ASSERT_TRUE( graph.removeNode( 1 ) );
    ASSERT_EQ( 1, graph.nodeCount() );
    ASSERT_TRUE( graph.removeNode( 2 ) );
    ASSERT_TRUE( graph.empty() );
}

TEST( DMDGraph_Tests, removeNode_with_edges_connected ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    auto expected_node_count = 6;
    auto expected_edge_count = 18;
    ASSERT_EQ( expected_node_count, graph.nodeCount() );
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 0 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    expected_edge_count -= 6;
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 1 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    expected_edge_count -= 5;
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 2 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    expected_edge_count -= 3;
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 3 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    expected_edge_count -= 3;
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 4 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    expected_edge_count -= 1;
    ASSERT_EQ( expected_edge_count, graph.size() );

    ASSERT_TRUE( graph.removeNode( 5 ) );
    ASSERT_EQ( --expected_node_count, graph.nodeCount() );
    ASSERT_EQ( expected_edge_count, graph.size() );
    ASSERT_TRUE( graph.empty() );
}

TEST( DMDGraph_Tests, createDirectedEdge_with_Node_construction ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    graph.addNode( 3, "three" );
    ASSERT_TRUE( graph.createDirectedEdge( { 0, "zero" }, { 1, "one" }, "friend" ) );

    ASSERT_TRUE( graph.find( 0 ) != graph.cend() );
    ASSERT_TRUE( graph.find( 1 ) != graph.cend() );
    ASSERT_NO_THROW( ASSERT_EQ( "zero", graph.at( 0 ) ) );
    ASSERT_NO_THROW( ASSERT_EQ( "one", graph.at( 1 ) ) );


    ASSERT_FALSE( graph.removeDirectedEdge( 0, 3, "friend" ) );
    ASSERT_TRUE( graph.removeDirectedEdge( 0, 1, "friend" ) );
}

TEST( DMDGraph_Tests, removeDirectedEdge ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    graph.createDirectedEdge( 0, 1, "Friend" );
    graph.createDirectedEdge( 0, 1, "Buddy" );
    auto expected_node_count = 6;
    auto expected_edge_count = 18 + 2;
    ASSERT_EQ( expected_node_count, graph.nodeCount() );
    ASSERT_EQ( expected_edge_count, graph.size() );
    //Testing removal of edge from here
    ASSERT_TRUE( graph.removeDirectedEdge( 5, 0 ) );
    ASSERT_EQ( --expected_edge_count, graph.size() );
    ASSERT_TRUE( graph.removeDirectedEdge( 0, 1 ) );
    expected_edge_count -= 3;
    ASSERT_EQ( expected_edge_count, graph.size() );
    ASSERT_FALSE( graph.removeDirectedEdge( 0, 1 ) );
}

TEST( DMDGraph_Tests, removeDirectedEdge_with_relationship_type ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    graph.createDirectedEdge( 0, 1, "Friend" );
    graph.createDirectedEdge( 0, 1, "Buddy" );
    auto expected_node_count = 6;
    auto expected_edge_count = 18 + 2;
    ASSERT_EQ( expected_node_count, graph.nodeCount() );
    ASSERT_EQ( expected_edge_count, graph.size() );
    //Testing removal of edge from here
    ASSERT_TRUE( graph.removeDirectedEdge( 5, 0 ) );
    ASSERT_EQ( --expected_edge_count, graph.size() );
    ASSERT_TRUE( graph.removeDirectedEdge( 0, 1, "Outer" ) );
    ASSERT_EQ( --expected_edge_count, graph.size() );
    ASSERT_TRUE( graph.removeDirectedEdge( 0, 1, "Friend" ) );
    ASSERT_EQ( --expected_edge_count, graph.size() );
    ASSERT_TRUE( graph.removeDirectedEdge( 0, 1, "Buddy" ) );
    ASSERT_EQ( --expected_edge_count, graph.size() );
    ASSERT_FALSE( graph.removeDirectedEdge( 0, 1, "Friend" ) );
}

TEST( DMDGraph_Tests, at ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    ASSERT_NO_THROW( ASSERT_EQ( "zero", graph.at( 0 ) ) );
    ASSERT_THROW( graph.at( 20 ), std::out_of_range );
}

TEST( DMDGraph_Tests, getRelationships ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    ASSERT_NO_THROW(
        auto it = graph.getRelationships( 0 );
        ASSERT_EQ( 2, it.size() );
        for( auto e : it ) {
            ASSERT_TRUE( e.first == "Inner1" || e.first == "Outer" );
        }
    );
    ASSERT_THROW( graph.getRelationships( 20 ), std::out_of_range );
}

TEST( DMDGraph_Tests, getChildList ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    ASSERT_NO_THROW(
        auto cl = graph.getChildList( 0, "Outer" );
        ASSERT_EQ( 1, cl.size() );
        ASSERT_EQ( 1, cl.front() );
    );
    ASSERT_THROW( graph.getChildList( 20, "Outer" ), std::out_of_range );
    ASSERT_THROW( graph.getChildList( 0, "Invalid" ), std::out_of_range );
}

TEST( DMDGraph_Tests, getParentList ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    ASSERT_NO_THROW(
        auto pl = graph.getParentList( 0, "Outer" );
        ASSERT_EQ( 1, pl.size() );
        ASSERT_EQ( 5, pl.front() );
    );
    ASSERT_THROW( graph.getParentList( 20, "Outer" ), std::out_of_range );
    ASSERT_THROW( graph.getParentList( 0, "Invalid" ), std::out_of_range );
}

TEST( DMDGraph_Tests, find ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_TRUE( graph.find( 0 ) == graph.cend() );
    ASSERT_TRUE( graph.addNode( 0, "Zero" ) );
    auto search = graph.find( 0 );
    ASSERT_TRUE( search != graph.cend() );
    ASSERT_EQ( 0, search->first );
    ASSERT_EQ( "Zero", search->second.value );
}

TEST( DMDGraph_Tests, getRelationship ) {
    ASSERT_FALSE( true );
}

TEST( DMDGraph_Tests, isReachable ) {
    ASSERT_FALSE( true );
}

TEST( DMDGraph_Tests, key_exists ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_FALSE( graph.exists( 0 ) );
    ASSERT_TRUE( graph.addNode( 0, "Zero" ) );
    ASSERT_TRUE( graph.exists( 0 ) );
}

TEST( DMDGraph_Tests, edge_exists ) {
    auto graph = unit_tests::DMDGraph_Tests::createPentagramGraph();
    ASSERT_TRUE( graph.exists( 0, 1, "Outer" ) );
    ASSERT_TRUE( graph.exists( 0, 2, "Inner1" ) );
    ASSERT_FALSE( graph.exists( 0, 1, "Inner1" ) );
    ASSERT_FALSE( graph.exists( 20, 0, "Invalid" ) );
    ASSERT_FALSE( graph.exists( 20, 21, "Invalid" ) );
}

TEST( DMDGraph_Tests, empty ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_TRUE( graph.empty() );
    ASSERT_TRUE( graph.addNode( 1, "one" ) );
    ASSERT_FALSE( graph.empty() );
}

TEST( DMDGraph_Tests, nodeCount ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_EQ( 0, graph.nodeCount() );
    for( int i = 1; i <= 10; i++ ) {
        ASSERT_TRUE( graph.addNode( i, "val_" + std::to_string( i ) ) );
        ASSERT_EQ( i, graph.nodeCount() );
    }
}

TEST( DMDGraph_Tests, size ) {
    auto graph = eadlib::DMDGraph<int,std::string,std::string>();
    ASSERT_TRUE( graph.empty() );
    for( int i = 1; i <= 10; i++ ) {
        ASSERT_TRUE( graph.createDirectedEdge( { i, "val_" + std::to_string( i ) }, { i - 1, "val_" + std::to_string( i - 1 ) }, "ParentChild" ) );
        ASSERT_EQ( i, graph.size() );
    }
}

TEST( DMDGraph_Tests, getInDegree ) {
    auto graph = eadlib::DMDGraph<std::string,std::string,std::string>();
    ASSERT_TRUE( graph.addNode( "A", "A" ) );
    ASSERT_TRUE( graph.addNode( "B", "B" ) );
    ASSERT_TRUE( graph.addNode( "C", "C" ) );
    ASSERT_EQ( 0, graph.getInDegree( "A" ) );
    ASSERT_EQ( 0, graph.getInDegree( "B" ) );
    ASSERT_EQ( 0, graph.getInDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "A", "B", "Friend" ) );
    ASSERT_EQ( 0, graph.getInDegree( "A" ) );
    ASSERT_EQ( 1, graph.getInDegree( "B" ) );
    ASSERT_EQ( 0, graph.getInDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "B", "A", "Friend" ) );
    ASSERT_EQ( 1, graph.getInDegree( "A" ) );
    ASSERT_EQ( 1, graph.getInDegree( "B" ) );
    ASSERT_EQ( 0, graph.getInDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "A", "C", "ParentChild" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "B", "C", "Friend" ) );
    ASSERT_EQ( 1, graph.getInDegree( "A" ) );
    ASSERT_EQ( 1, graph.getInDegree( "B" ) );
    ASSERT_EQ( 2, graph.getInDegree( "C" ) );
}

TEST( DMDGraph_Tests, getOutDegree ) {
    auto graph = eadlib::DMDGraph<std::string,std::string,std::string>();
    ASSERT_TRUE( graph.addNode( "A", "A" ) );
    ASSERT_TRUE( graph.addNode( "B", "B" ) );
    ASSERT_TRUE( graph.addNode( "C", "C" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "A" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "B" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "A", "B", "Friend" ) );
    ASSERT_EQ( 1, graph.getOutDegree( "A" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "B" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "A", "C", "ParentChild" ) );
    ASSERT_EQ( 2, graph.getOutDegree( "A" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "B" ) );
    ASSERT_EQ( 0, graph.getOutDegree( "C" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "C", "B", "Friend" ) );
    ASSERT_TRUE( graph.createDirectedEdge( "B", "A", "Friend" ) );
    ASSERT_EQ( 2, graph.getOutDegree( "A" ) );
    ASSERT_EQ( 1, graph.getOutDegree( "B" ) );
    ASSERT_EQ( 1, graph.getOutDegree( "C" ) );
}

TEST( DMDGraph_Tests, shortestRelationshipPath ) {
    auto graph = eadlib::DMDGraph<int, int, std::string>();
    graph.addNode( 1, 1 );
    graph.addNode( 2, 2 );
    graph.addNode( 3, 3 );
    graph.addNode( 4, 4 );
    graph.addNode( 5, 5 );
    graph.addNode( 6, 6 );
    graph.addNode( 7, 7 );
    graph.addNode( 8, 8 );
    graph.addNode( 9, 9 );
    graph.addNode( 10, 10 );
    graph.addNode( 11, 11 );
    graph.addNode( 12, 12 );
    graph.addNode( 13, 13 );
    graph.addNode( 14, 14 );
    graph.addNode( 15, 15 );
    graph.addNode( 16, 16 );
    graph.addNode( 17, 17 );
    graph.addNode( 18, 18 );

    graph.createDirectedEdge( 1, 2, "D1" );
    graph.createDirectedEdge( 1, 3, "D1" );
    graph.createDirectedEdge( 2, 4, "D1" );
    graph.createDirectedEdge( 2, 5, "D1" );
    graph.createDirectedEdge( 3, 6, "D1" );
    graph.createDirectedEdge( 3, 7, "D1" );
    graph.createDirectedEdge( 4, 8, "D1" );
    graph.createDirectedEdge( 4, 9, "D1" );
    graph.createDirectedEdge( 5, 10, "D1" );
    graph.createDirectedEdge( 5, 11, "D1" );
    graph.createDirectedEdge( 6, 12, "D1" );
    graph.createDirectedEdge( 6, 13, "D1" );
    graph.createDirectedEdge( 7, 14, "D1" );
    graph.createDirectedEdge( 7, 15, "D1" );
    graph.createDirectedEdge( 15, 17, "D1" );
    graph.createDirectedEdge( 7, 17, "D1" );
    graph.createDirectedEdge( 17, 16, "D1" );
    graph.createDirectedEdge( 3, 16, "D1" );
    graph.createDirectedEdge( 3, 17, "D1" );

    ASSERT_EQ( 18, graph.nodeCount() );
    ASSERT_EQ( 19, graph.size() );
    auto path = graph.shortestRelationshipPath( 8, 17 );
    for( auto e : *path ) {
        std::cout << e << std::endl;
    }
    ASSERT_EQ( 5, path->size() );
    eadlib::GraphEdgeInfo<int, std::string> p = path->front();
    ASSERT_TRUE( ( p._origin == 8
                 && p._destination == 4
                 && p._direction == eadlib::GraphEdgeInfo<int, std::string>::Direction::PARENT )
    );
    path->pop_front();
    p = path->front();
    ASSERT_TRUE( ( p._origin == 4
                   && p._destination == 2
                   && p._direction == eadlib::GraphEdgeInfo<int, std::string>::Direction::PARENT )
    );
    path->pop_front();
    p = path->front();
    ASSERT_TRUE( ( p._origin == 2
                   && p._destination == 1
                   && p._direction == eadlib::GraphEdgeInfo<int, std::string>::Direction::PARENT )
    );
    path->pop_front();
    p = path->front();
    ASSERT_TRUE( ( p._origin == 1
                   && p._destination == 3
                   && p._direction == eadlib::GraphEdgeInfo<int, std::string>::Direction::CHILD )
    );
    path->pop_front();
    p = path->front();
    ASSERT_TRUE( ( p._origin == 3
                   && p._destination == 17
                   && p._direction == eadlib::GraphEdgeInfo<int, std::string>::Direction::CHILD )
    );
}

#endif //EADLIB_DMDGRAPH_TEST_H
