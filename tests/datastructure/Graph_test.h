#ifndef EADLIB_TESTS_GRAPH_TEST_H
#define EADLIB_TESTS_GRAPH_TEST_H

#include "gtest/gtest.h"
#include <chrono>
#include "../../src/datastructure/Graph.h"

TEST( Graph_Tests, at ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 1, 3 );
    g.createDirectedEdge( 1, 4 );
    g.createDirectedEdge( 4, 1 );
    ASSERT_EQ( 3, g.at( 1 ).childrenList.size() );
    ASSERT_EQ( 1, g.at( 4 ).childrenList.size() );
    ASSERT_EQ( 1, g.at( 1 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 2 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 3 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 4 ).parentsList.size() );
    //Exception
    eadlib::Graph<int> g2 = eadlib::Graph<int>();
    ASSERT_THROW( g2.at( 1 ), std::out_of_range );
}

TEST( Graph_Tests, createDirectedEdge ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_FALSE( g.edgeExists( 1, 1 ) );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 4 ) );
    ASSERT_FALSE( g.edgeExists( 4, 5 ) );
    ASSERT_FALSE( g.edgeExists( 5, 6 ) );
    ASSERT_FALSE( g.edgeExists( 6, 7 ) );
    ASSERT_FALSE( g.edgeExists( 7, 8 ) );
    ASSERT_FALSE( g.edgeExists( 8, 9 ) );
    ASSERT_FALSE( g.edgeExists( 9, 10 ) );
    ASSERT_FALSE( g.edgeExists( 10, 1 ) );
    //Create edges
    ASSERT_TRUE( g.createDirectedEdge( 1, 1 ) );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2 ) );
    ASSERT_TRUE( g.createDirectedEdge( 2, 3 ) );
    ASSERT_TRUE( g.createDirectedEdge( 3, 4 ) );
    ASSERT_TRUE( g.createDirectedEdge( 4, 5 ) );
    ASSERT_TRUE( g.createDirectedEdge( 5, 6 ) );
    ASSERT_TRUE( g.createDirectedEdge( 6, 7 ) );
    ASSERT_TRUE( g.createDirectedEdge( 7, 8 ) );
    ASSERT_TRUE( g.createDirectedEdge( 8, 9 ) );
    ASSERT_TRUE( g.createDirectedEdge( 9, 10 ) );
    ASSERT_TRUE( g.createDirectedEdge( 10, 1 ) );
    ASSERT_FALSE( g.createDirectedEdge( 1, 11 ) );
    //Check if edges are created
    ASSERT_TRUE( g.edgeExists( 1, 1 ) );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.edgeExists( 2, 3 ) );
    ASSERT_TRUE( g.edgeExists( 3, 4 ) );
    ASSERT_TRUE( g.edgeExists( 4, 5 ) );
    ASSERT_TRUE( g.edgeExists( 5, 6 ) );
    ASSERT_TRUE( g.edgeExists( 6, 7 ) );
    ASSERT_TRUE( g.edgeExists( 7, 8 ) );
    ASSERT_TRUE( g.edgeExists( 8, 9 ) );
    ASSERT_TRUE( g.edgeExists( 9, 10 ) );
    ASSERT_TRUE( g.edgeExists( 10, 1 ) );
}

TEST( Graph_Tests, createDirectedEdge_fast ) {
    eadlib::Graph<size_t> g = eadlib::Graph<size_t>();
    for( size_t i = 0; i < 100; i++ ) {
        ASSERT_EQ( i, g.size() );
        g.createDirectedEdge_fast( i, i + 1 );
        ASSERT_EQ( 2 + i, g.nodeCount() );
    }
}

TEST( Graph_Tests, deleteDirectedEdge ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.deleteDirectedEdge( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );

}

TEST( Graph_Tests, addNode ) {
    eadlib::Graph<int> g = eadlib::Graph<int>();
    ASSERT_EQ( 0, g.nodeCount() );
    for( int i = 0; i < 10; i++ ) {
        ASSERT_TRUE( g.addNode( i ) );
        ASSERT_EQ( i + 1, g.nodeCount() );
    }
    ASSERT_FALSE( g.addNode( 1 ) );
}

TEST( Graph_Tests, deleteNode ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 2, 3 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 4, 5 );
    g.createDirectedEdge( 5, 6 );
    g.createDirectedEdge( 6, 7 );
    g.createDirectedEdge( 7, 8 );
    g.createDirectedEdge( 8, 9 );
    g.createDirectedEdge( 9, 10 );
    g.createDirectedEdge( 10, 1 );
    g.createDirectedEdge( 1, 3 );
    g.createDirectedEdge( 3, 1 );
    g.createDirectedEdge( 5, 1 );
    ASSERT_EQ( 10, g.nodeCount() );
    ASSERT_EQ( 13, g.size() );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.edgeExists( 10, 1 ) );
    ASSERT_TRUE( g.edgeExists( 1, 3 ) );
    ASSERT_TRUE( g.edgeExists( 3, 1 ) );
    ASSERT_TRUE( g.edgeExists( 5, 1 ) );
    ASSERT_TRUE( g.deleteNode( 1 ) );
    ASSERT_EQ( 8, g.size() );
    ASSERT_EQ( 9, g.nodeCount() );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 10, 1 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 5, 1 ) );
    ASSERT_FALSE( g.deleteNode( 1 ) );
}

TEST( Graph_Tests, isReachable ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 2, 3 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 4, 5 );
    g.createDirectedEdge( 5, 6 );
    g.createDirectedEdge( 6, 7 );
    g.createDirectedEdge( 7, 8 );
    g.createDirectedEdge( 8, 9 );
    g.createDirectedEdge( 9, 10 );
    ASSERT_TRUE( g.isReachable( 1, 10 ) );
    ASSERT_FALSE( g.isReachable( 10,  1 ) );
}

TEST( Graph_Tests, edgeExists ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3 } );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
    g.createDirectedEdge( 2, 1 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_TRUE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
}

TEST( Graph_Tests, isEmpty ) {
    eadlib::Graph<int> g = eadlib::Graph<int>();
    ASSERT_TRUE( g.isEmpty() );
    g.addNode( 1 );
    ASSERT_FALSE( g.isEmpty() );
    g.deleteNode( 1 );
    ASSERT_TRUE( g.isEmpty() );
}

TEST( Graph_Tests, nodeCount ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    for( unsigned i = 10; i > 0; i-- ) {
        ASSERT_EQ( i, g.nodeCount() );
        g.deleteNode( i );
    }
    ASSERT_EQ( 0, g.nodeCount() );
}

TEST( Graph_Tests, size ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_EQ( 0, g.size() );
    for( int i = 1; i < 9; i++ ) {
        g.createDirectedEdge( i, i + 1 );
        ASSERT_EQ( i, g.size() );
    }
}

TEST( Graph_Tests, getOutDegree ) {
    eadlib::Graph<std::string> g = eadlib::Graph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 1, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 2, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
}

TEST( Graph_Tests, getInDegree ) {
    eadlib::Graph<std::string> g = eadlib::Graph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getInDegree( "A" ) );
    ASSERT_EQ( 0, g.getInDegree( "B" ) );
    ASSERT_EQ( 0, g.getInDegree( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 0, g.getInDegree( "A" ) );
    ASSERT_EQ( 1, g.getInDegree( "B" ) );
    ASSERT_EQ( 0, g.getInDegree( "C" ) );
    g.createDirectedEdge( "B", "A" );
    ASSERT_EQ( 1, g.getInDegree( "A" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 1, g.getInDegree( "A" ) );
    ASSERT_EQ( 1, g.getInDegree( "B" ) );
    ASSERT_EQ( 1, g.getInDegree( "C" ) );
}


TEST( Graph_Tests, printAdjacencyList ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 5 );
    g.createDirectedEdge( 5, 2 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 3, 1 );
    std::string expected = "[10] -> \n"
        "[9] -> \n"
        "[8] -> \n"
        "[7] -> \n"
        "[6] -> \n"
        "[5] -> [2] \n"
        "[1] -> [5] \n"
        "[2] -> \n"
        "[3] -> [4] [1] \n"
        "[4] -> \n";
    std::ostringstream oss;
    g.printAdjacencyList( oss );
    ASSERT_EQ(expected, oss.str() );
}

TEST( Graph_Tests, printGraphNodes ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5 } );
    std::string expected = "5\n"
        "1\n"
        "2\n"
        "3\n"
        "4\n";
    std::ostringstream oss;
    g.printGraphNodes( oss );
    ASSERT_EQ( expected, oss.str() );
}

TEST( Graph_Tests, printStats ) {
    eadlib::Graph<int> g = eadlib::Graph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2 ) );
    ASSERT_TRUE( g.createDirectedEdge( 2, 3 ) );
    ASSERT_TRUE( g.createDirectedEdge( 3, 4 ) );
    ASSERT_TRUE( g.createDirectedEdge( 4, 5 ) );
    ASSERT_TRUE( g.createDirectedEdge( 5, 6 ) );
    ASSERT_TRUE( g.createDirectedEdge( 6, 7 ) );
    ASSERT_TRUE( g.createDirectedEdge( 7, 8 ) );
    ASSERT_TRUE( g.createDirectedEdge( 8, 9 ) );
    ASSERT_TRUE( g.createDirectedEdge( 9, 10 ) );
    ASSERT_TRUE( g.createDirectedEdge( 10, 1 ) );
    std::string expected = "Number of nodes: 10\n"
        "Number of edges: 10\n";
    std::ostringstream oss;
    g.printStats( oss );
    ASSERT_EQ( expected, oss.str() );
}

#endif //EADLIB_TESTS_GRAPH_TEST_H
