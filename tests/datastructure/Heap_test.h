#ifndef EADLIB_TESTS_HEAP_TEST_H
#define EADLIB_TESTS_HEAP_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/Heap.h"
#include "../../src/datastructure/MaxHeap.h"

namespace Heap_Test {
    template<typename T>
        void print( const T &data ) {
            std::cout << data << " ";
        }
}

TEST( Heap_Tests, Constructor ) {
    eadlib::Heap<int> h( { 1,3,4,2,6,5,0 } );
}

TEST( MaxHeap_Test, Constructor ) {
    eadlib::Heap<int> h( { 1,2,3,4,7,8,9,10,14,16 } );
    std::cout << "size: " << h.size() << std::endl;
    std::cout << "top: " << h.top() << std::endl;
    std::cout << h << std::endl;
}

#endif //EADLIB_TESTS_HEAP_TEST_H
