#ifndef EADLIB_TESTS_LINEARLIST_TEST_H
#define EADLIB_TESTS_LINEARLIST_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/LinearList.h"

TEST( LinearList_Tests, Constructor ) {
    eadlib::LinearList<int> ll;
    EXPECT_TRUE( ll.size() == 0 );
    EXPECT_TRUE( ll.capacity() == 2 );
    EXPECT_TRUE( ll.empty() );
    EXPECT_TRUE( ll.str() == "" );
}

TEST( LinearList_Tests, Constructor_Copy ) {
    eadlib::LinearList<int> ll1( {1, 2, 3, 4, 5 } );
    eadlib::LinearList<int> ll2 = ll1;
    EXPECT_TRUE( ll2.size() == ll1.size() );
    EXPECT_TRUE( ll2.capacity() == ll1.capacity() );
    EXPECT_TRUE( ll2.str() == ll1.str() );
}

TEST( LinearList_Tests, Constructor_InitializerList ) {
    eadlib::LinearList<int> ll( { 1, 2, 3, 4, 5 } );
    EXPECT_EQ( ll.str(), "| 1 | 2 | 3 | 4 | 5 | " );
}

TEST( LinearList_Tests, Operator_OutputStream ) { //<<
    eadlib::LinearList<std::string> ll;
    ll.addToEnd( "A" );
    ll.addToEnd( "B" );
    ll.addToEnd( "C" );
    std::stringstream ss;
    ss << ll;
    EXPECT_TRUE( ss.str() == "A B C " );
}

TEST( LinearList_Tests, Operator_accessor ) { //[]
    eadlib::LinearList<int> ll;
    EXPECT_THROW( ll[ 0 ], std::out_of_range );
    for( int i = 0; i < 10; i++ ) {
        ll.addToEnd( i );
    }
    for( int i = 0; i < 10; i++ ) {
        EXPECT_TRUE( ll[ i ] == i );
    }
    EXPECT_THROW( ll[ 10 ], std::out_of_range );
    EXPECT_THROW( ll[ -1 ], std::out_of_range );
}

TEST( LinearList_Tests, Operator_assignment ) { //=
    eadlib::LinearList<int> ll1( {1, 2, 3, 4, 5 } );
    eadlib::LinearList<int> ll2( { 100 } );
    ll2 = ll1;
    EXPECT_TRUE( ll2.size() == ll1.size() );
    EXPECT_TRUE( ll2.capacity() == ll1.capacity() );
    EXPECT_TRUE( ll2.str() == ll1.str() );
}

TEST( LinearList_Tests, addToEnd ) {
    eadlib::LinearList<int> ll;
    for( int i = 0; i < 1000; i++ ) {
        ll.addToEnd( i );
    }
    for( int i = 0; i < 1000; i++ ) {
        EXPECT_EQ( i, ll[ i ] );
    }
}

TEST( LinearList_Tests, addToFront ) {
    eadlib::LinearList<int> ll;
    for( int i = 1000; i >= 0; i-- ) {
        ll.addToFront( i );
    }
    for( int i = 0; i <= 1000; i++ ) {
        EXPECT_EQ( i, ll[ i ] );
    }
}

TEST( LinearList_Tests, addToFront_and_End ) {
    eadlib::LinearList<int> ll;
    auto vec = std::vector<int>( { 5, 6, 4, 7, 3, 8, 2, 9, 1, 10 } );
    for( auto i = 0; i < vec.size(); i++ ) {
        if( i % 2 ) {
            ll.addToEnd( vec.at( i ) );
        } else {
            ll.addToFront( vec.at( i ) );
        }
    }
    EXPECT_EQ( "| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | ", ll.str() );
}

TEST( LinearList_Tests, insert ) {
    eadlib::LinearList<int> ll;
    EXPECT_THROW( ll.insert( 100, 0 ), std::out_of_range );
    for( int i = 0; i < 10; i++ ) {
        ll.addToEnd( i );
    }
    ll.insert( 100, 0 );
    EXPECT_TRUE( ll.str() == "| 0 | 100 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | " );
    ll.insert( 101, 0 );
    EXPECT_TRUE( ll.str() == "| 0 | 101 | 100 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | " );
    ll.insert( 102, 11 );
    EXPECT_TRUE( ll.str() == "| 0 | 101 | 100 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 102 | " );
    ll.insert( 103, 6 );
    EXPECT_TRUE( ll.str() == "| 0 | 101 | 100 | 1 | 2 | 3 | 4 | 103 | 5 | 6 | 7 | 8 | 9 | 102 | " );
}

TEST( LinearList_Tests, remove ) {
    eadlib::LinearList<int> ll;
    EXPECT_THROW( ll.remove( 0 ), std::out_of_range );
    for( int i = 0; i < 10; i++ ) {
        ll.addToEnd( i );
    }
    EXPECT_TRUE( ll.size() == 10 );
    ll.remove( 0 );
    EXPECT_TRUE( ll.str() == "| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | " );
    ll.remove( 8 );
    EXPECT_TRUE( ll.str() == "| 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | " );
    ll.remove( 4 );
    EXPECT_TRUE( ll.str() == "| 1 | 2 | 3 | 4 | 6 | 7 | 8 | " );
    EXPECT_TRUE( ll.size() == 7 );

}

TEST( LinearList_Tests, clear ) {
    //TODO
}

TEST( LinearList_Tests, size ) {
    eadlib::LinearList<int> ll;
    EXPECT_TRUE( ll.size() == 0 );
    for( int i = 0; i < 100; i++ ) {
        ll.addToEnd( i );
        EXPECT_TRUE( ll.size() == i + 1 );
    }
}

TEST( LinearList_Tests, reserve ) {
    eadlib::LinearList<int> ll;
    EXPECT_TRUE( ll.capacity() == 2 );
    ll.reserve( 200 );
    EXPECT_TRUE( ll.capacity() == 200 );
    ll.reserve( 10 );
    EXPECT_TRUE( ll.capacity() == 10 );
}

TEST( LinearList_Tests, capacity ) {
    //TODO
}

TEST( LinearList_Tests, empty ) {
    eadlib::LinearList<int> ll;
    EXPECT_TRUE( ll.empty() );
    ll.addToEnd( 1 );
    EXPECT_FALSE( ll.empty() );
    ll.remove( 0 );
    EXPECT_TRUE( ll.empty() );
}

TEST( LinearList_Tests, str ) {
    eadlib::LinearList<int> ll( { 1, 2, 3, 3, 4, 5 } );
    EXPECT_TRUE( ll.str() == "| 1 | 2 | 3 | 3 | 4 | 5 | " );
}

TEST( LinearList_tests, iterator ) {
    eadlib::LinearList<int> ll( { 1, 2, 3, 4, 5 });
    std::list<int> returned;
    for( auto it = ll.begin(); it != ll.end(); ++it ) {
        if( *it == 3 ) *it = 666;
    }
    for( auto e : ll ) {
        returned.emplace_back( e );
    }
    EXPECT_THAT( returned, testing::ElementsAre( 1, 2, 666, 4, 5 ) );
}

#endif //EADLIB_TESTS_LINEARLIST_TEST_H
