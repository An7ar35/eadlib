#ifndef EADLIB_TESTS_LINKEDLIST_TEST_H
#define EADLIB_TESTS_LINKEDLIST_TEST_H

#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include <chrono>
#include "../../src/datastructure/LinkedList.h"

TEST( LinkedList_Tests, Constructor ) {
    eadlib::LinkedList<int> ll;
    EXPECT_TRUE( ll.str() == "" );
    EXPECT_TRUE( ll.size() == 0 );
}

TEST( LinkedList_Tests, Constructor_InitializerList ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    EXPECT_TRUE( ll.str() == "1 2 3 4 5 " );
    EXPECT_TRUE( ll.size() == 5 );
    eadlib::LinkedList<int> ll_empty( { } );
    EXPECT_TRUE( ll_empty.str() == "" );
    EXPECT_TRUE( ll_empty.size() == 0 );
}

TEST( LinkedList_Tests, Iterator ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    std::list<int> returned;
    for( auto it = ll.begin(); it != ll.end(); ++it ) {
        if( *it == 3 ) *it = 666;
        returned.emplace_back( *it );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 1, 2, 666, 4, 5 ) );
}

TEST( LinkedList_Tests, RangeLoop_Iterator ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    std::list<int> returned;
    for( auto e : ll ) {
        if( e == 3 ) e = 666;
        returned.emplace_back( e );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 1, 2, 666, 4, 5 ) );
}

TEST( LinkedList_Tests, Const_Iterator ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    std::list<int> returned;
    for( auto it = ll.cbegin(); it != ll.cend(); ++it ) {
        returned.emplace_back( *it );
    }
    ASSERT_THAT( returned, testing::ElementsAre( 1, 2, 3, 4, 5 ) );
}

TEST( LinkedList_Tests, addToHead ) {
    eadlib::LinkedList<int> ll;
    EXPECT_TRUE( ll.empty() );
    ll.addToHead( 111 );
    EXPECT_TRUE( ll.removeTail() == 111 );
    EXPECT_TRUE( ll.empty() && ll.size() == 0 );
    ll.addToHead( 1 );
    ll.addToHead( 2 );
    ll.addToHead( 3 ); // 3,2,1
    EXPECT_TRUE( ll.size() == 3 );
    EXPECT_TRUE( ll.removeHead() == 3 ); //2,1
    EXPECT_TRUE( ll.size() == 2 );
    EXPECT_TRUE( ll.removeTail() == 1 ); //2
    EXPECT_TRUE( ll.size() == 1 );
    ll.addToTail( 100 ); //2,100
    ll.addToTail( 101 ); //2,100,101
    EXPECT_TRUE( ll.removeTail() == 101 ); //2,100
    EXPECT_TRUE( ll.removeHead() == 2 ); //100
    EXPECT_TRUE( ll.removeTail() == 100 );
    EXPECT_TRUE( ll.size() == 0 );
}

TEST( LinkedList_Tests, addToTail ) {
    eadlib::LinkedList<int> ll;
    ll.addToTail( 100 );
    EXPECT_TRUE( ll.size() == 1 && !ll.empty() );
    EXPECT_TRUE( ll.str() == "100 " );
    EXPECT_TRUE( ll.removeHead() == 100 );
    EXPECT_TRUE( ll.size() == 0 && ll.empty() );
    ll.addToTail( 101 );
    EXPECT_TRUE( ll.size() == 1 && !ll.empty() );
    EXPECT_TRUE( ll.str() == "101 " );
    EXPECT_TRUE( ll.removeTail() == 101 );
    EXPECT_TRUE( ll.size() == 0 && ll.empty() );
    for( int i = 0; i <  100; i++ ) {
        ll.addToTail( i );
        EXPECT_TRUE( ll.size() == i + 1 );
    }
    for( int i = 99; i >= 0; i-- ) {
        EXPECT_TRUE( ll.removeTail() == i );
    }
    EXPECT_TRUE( ll.size() == 0 && ll.empty() );
}

TEST( LinkedList_Tests, append_LinkedList ) {
    //TODO
}

TEST( LinkedList_Tests, append_InitializerList ) {
    //TODO
}

TEST( LinkedList_Tests, append_Move ) {
    eadlib::LinkedList<int> ll;
    for( int i = 0; i < 10; i++ ) {
        ll.addToTail( i );
    }
    eadlib::LinkedList<int> ll2;
    for( int i = 100; i < 110; i++ ) {
        ll2.addToTail( i );
    }
    ll.append_Move( ll2 );
    ASSERT_TRUE( ll.size() == 20 );
}

TEST( LinkedList_Tests, append_Copy ) {
    eadlib::LinkedList<int> ll;
    for( int i = 0; i < 10; i++ ) {
        ll.addToTail( i );
    }
    eadlib::LinkedList<int> ll2;
    for( int i = 100; i < 110; i++ ) {
        ll2.addToTail( i );
    }
    ll.append_Copy( ll2 );
    ASSERT_TRUE( ll.size() == 20 );
}

TEST( LinkedList_Tests, appendToHead ) {
    //TODO
}

TEST( LinkedList_Tests, appendToTail ) {
    //TODO
}

TEST( LinkedList_Tests, insert_single ) {
    eadlib::LinkedList<int> ll;
    EXPECT_THROW( ll.insert( 666, 1 ), std::out_of_range );
    EXPECT_NO_THROW( ll.insert( 666, 0 ) ); //666
    ll.insert( 1, 0 ); //666,1
    ll.insert( 2, 0 ); //666,2,1
    ll.insert( 111, 1 ); //666,2,111,1
}

TEST( LinkedList_Tests, insert_initializerlist ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    ll.insert( { 100, 101, 102 }, 0 ); //after first ( 1, 100, 101, 102, 2, 3, 4, 5 )
    ll.insert( { 200, 201, 202 }, 4 ); //middle ( 1, 100, 101, 102, 2, 200, 201, 202, 3, 4, 5 )
    ll.insert( { 300, 301, 302 }, 10 ); //end ( 1, 100, 101, 102, 2, 200, 201, 202, 3, 4, 5, 300, 301, 302 )
    EXPECT_TRUE( ll.forwardPrint() == "| 1 | 100 | 101 | 102 | 2 | 200 | 201 | 202 | 3 | 4 | 5 | 300 | 301 | 302 | " );
    EXPECT_TRUE( ll.reversePrint() == "| 302 | 301 | 300 | 5 | 4 | 3 | 202 | 201 | 200 | 2 | 102 | 101 | 100 | 1 | " );
}

TEST( LinkedList_Tests, insert_Move ) {
    eadlib::LinkedList<int> ll1( { 100, 101 } );
    eadlib::LinkedList<int> ll2( { 200 } );
    eadlib::LinkedList<int> ll3( { 300 } );
    eadlib::LinkedList<int> ll0( { 1, 2, 3 } );
    ll0.insert_Move( ll1, 0 ); //1, 100, 101, 2, 3
    ll0.insert_Move( ll2, 2 ); //1, 100, 101, 200, 2, 3
    ll0.insert_Move( ll3, 5 ); //1, 100, 101, 200, 2, 3, 300
    EXPECT_TRUE( ll1.size() == 0 );
    EXPECT_TRUE( ll1.str() == "" );
    EXPECT_TRUE( ll2.size() == 0 );
    EXPECT_TRUE( ll2.str() == "" );
    EXPECT_TRUE( ll3.size() == 0 );
    EXPECT_TRUE( ll3.str() == "" );
    EXPECT_TRUE( ll0.size() == 7 );
    EXPECT_TRUE( ll0.str() == "1 100 101 200 2 3 300 " );
}

TEST( LinkedList_Tests, insert_Copy ) {
    eadlib::LinkedList<int> ll1( { 100, 101 } );
    eadlib::LinkedList<int> ll2( { 200 } );
    eadlib::LinkedList<int> ll3( { 300 } );
    eadlib::LinkedList<int> ll0( { 1, 2, 3 } );
    ll0.insert_Copy( ll1, 0 ); //1, 100, 101, 2, 3
    ll0.insert_Copy( ll2, 2 ); //1, 100, 101, 200, 2, 3
    ll0.insert_Copy( ll3, 5 ); //1, 100, 101, 200, 2, 3, 300
    EXPECT_TRUE( ll1.size() == 2 );
    EXPECT_TRUE( ll1.str() == "100 101 " );
    EXPECT_TRUE( ll2.size() == 1 );
    EXPECT_TRUE( ll2.str() == "200 " );
    EXPECT_TRUE( ll3.size() == 1 );
    EXPECT_TRUE( ll3.str() == "300 " );
    EXPECT_TRUE( ll0.size() == 7 );
    EXPECT_TRUE( ll0.str() == "1 100 101 200 2 3 300 " );
}

TEST( LinkedList_Tests, removeFromHead ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    EXPECT_TRUE( ll.size() == 5 );
    for( int i = 1; i <= 5; i++ ) {
        ASSERT_TRUE( ll.removeHead() == i );
    }
    EXPECT_TRUE( ll.size() == 0 );
}

TEST( LinkedList_Tests, removeTail ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    EXPECT_TRUE( ll.size() == 5 );
    for( int i = 5; i > 0; i-- ) {
        ASSERT_TRUE( ll.removeTail() == i );
    }
    EXPECT_TRUE( ll.size() == 0 );
}

TEST( LinkedList_Tests, popHead ) {
    //TODO
}

TEST( LinkedList_Tests, popTail ) {
    //TODO
}

TEST( LinkedList_Tests, clear ) {
    eadlib::LinkedList<int> ll( { 1, 2, 3, 4, 5 } );
    EXPECT_TRUE( ll.str() == "1 2 3 4 5 " );
    EXPECT_TRUE( ll.size() == 5 );
    ll.clear();
    EXPECT_TRUE( ll.str() == "" );
    EXPECT_TRUE( ll.size() == 0 );
}

TEST( LinkedList_Tests, atHead ) {
    //TODO
}

TEST( LinkedList_Tests, atTail ) {
    //TODO
}

TEST( LinkedList_Tests, size ) {
    eadlib::LinkedList<int> ll;
    for( int i = 0; i < 100; i++ ) {
        EXPECT_TRUE( ll.size() == i );
        ll.addToHead( i );
    }
    EXPECT_TRUE( ll.size() == 100 );
}

TEST( LinkedList_Tests, isEmpty ) {
    eadlib::LinkedList<int> ll;
    EXPECT_TRUE( ll.empty() );
    ll.addToHead( 1 );
    EXPECT_FALSE( ll.empty() );
    ll.clear();
    EXPECT_TRUE( ll.empty() );
}

TEST( LinkedList_Tests, str ) {
    //TODO
}

TEST( LinkedList_Tests, forwardPrint ) {
    //TODO
}

TEST( LinkedList_Tests, reversePrint ) {
    //TODO
}

#endif //EADLIB_TESTS_LINKEDLIST_TEST_H
