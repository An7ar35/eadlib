#ifndef EADLIB_TESTS_MATRIX_TEST_H
#define EADLIB_TESTS_MATRIX_TEST_H

#include "gtest/gtest.h"
#include <chrono>
#include "../../src/datastructure/Matrix.h"

TEST( Matrix_Tests, Access_operator ) { //()
    eadlib::Matrix<int> m( 2, 3, 999 );
    EXPECT_NO_THROW(
        for( int y = 0; y < 2; y++ ) {
            for( int x = 0; x < 3; x++ ) {
                EXPECT_EQ( 999, m( x, y ) );
            }
        }
    );
    EXPECT_THROW( m( 3, 4 ), std::out_of_range );
    //with just 1 column
    eadlib::Matrix<int> m2( 1, 3, 999 );
    EXPECT_NO_THROW(
        for( int y = 0; y < 3; y++ ) {
            EXPECT_EQ( 999, m2( 0, y ) );
        }
    );
    EXPECT_THROW( m2( 0, 4 ), std::out_of_range );
    //with just 1 row
    eadlib::Matrix<int> m3( 3, 1, 999 );
    EXPECT_NO_THROW(
        for( int x = 0; x <  3; x++ ) {
            EXPECT_EQ( 999, m3( x, 0 ) );
        }
    );
    EXPECT_THROW( m3( 3, 1 ), std::out_of_range );
}

TEST( Matrix_Tests, at ) {
    eadlib::Matrix<int> m( 2, 3, 999 );
    EXPECT_NO_THROW(
        for( int y = 0; y < 2; y++ ) {
            for( int x = 0; x < 3; x++ ) {
                EXPECT_EQ( 999, m.at( x, y ) );
            }
        }
    );
    EXPECT_THROW( m.at( 3, 4 ), std::out_of_range );
    //with just 1 column
    eadlib::Matrix<int> m2( 1, 3, 999 );
    EXPECT_NO_THROW(
        for( int y = 0; y < 3; y++ ) {
            EXPECT_EQ( 999, m2.at( 0, y ) );
        }
    );
    EXPECT_THROW( m2.at( 0, 4 ), std::out_of_range );
    //with just 1 row
    eadlib::Matrix<int> m3( 3, 1, 999 );
    EXPECT_NO_THROW(
        for( int x = 0; x <  3; x++ ) {
            EXPECT_EQ( 999, m3.at( x, 0 ) );
        }
    );
    EXPECT_THROW( m3.at( 3, 1 ), std::out_of_range );
}

TEST( Matrix_Tests, sizeX ) {
    eadlib::Matrix<int> m( 3, 4 );
    EXPECT_EQ( 3, m.sizeX() );
}

TEST( Matrix_Tests, sizeY ) {
    eadlib::Matrix<int> m( 3, 4 );
    EXPECT_EQ( 4, m.sizeY() );
}

TEST( Matrix_Tests, toCSVstr ) {
    eadlib::Matrix<std::string> m( 2, 4, "data" );
    m( 0, 1 ) = "salt";
    std::string expected =  "data;data\n"
                            "salt;data\n"
                            "data;data\n"
                            "data;data";
    EXPECT_EQ( expected, m.toCSVstr() );
}

#endif //EADLIB_TESTS_MATRIX_TEST_H
