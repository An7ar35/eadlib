#ifndef EADLIB_OBJECTPOOL_TEST_H
#define EADLIB_OBJECTPOOL_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/ObjectPool.h"

class O {
    friend std::ostream & operator <<( std::ostream &out, const O &object ) {
        out << "object";
        return out;
    }
};

TEST( ObjectPool_Tests, acquire ) {
    auto pool = eadlib::ObjectPool<O>( 5 );
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 5, pool.available() );
    auto a = pool.acquire();
    ASSERT_EQ( 1, pool.used() );
    ASSERT_EQ( 4, pool.available() );
    auto b = pool.acquire();
    ASSERT_EQ( 2, pool.used() );
    ASSERT_EQ( 3, pool.available() );
    auto c = pool.acquire();
    ASSERT_EQ( 3, pool.used() );
    ASSERT_EQ( 2, pool.available() );
    auto d = pool.acquire();
    ASSERT_EQ( 4, pool.used() );
    ASSERT_EQ( 1, pool.available() );
    auto e = pool.acquire();
    ASSERT_EQ( 5, pool.used() );
    ASSERT_EQ( 0, pool.available() );
}

TEST( ObjectPool_Tests, acquire_empty ) {
    auto pool = eadlib::ObjectPool<O>( 1 );
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 1, pool.available() );
    auto a = pool.acquire();
    ASSERT_EQ( 1, pool.used() );
    ASSERT_EQ( 0, pool.available() );
    ASSERT_THROW(pool.acquire(), eadlib::exception::resource_unavailable );
}

TEST( ObjectPool_Tests, release ) {
    auto pool = eadlib::ObjectPool<O>( 1 );
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 1, pool.available() );
    auto a = pool.acquire();
    ASSERT_EQ( 1, pool.used() );
    ASSERT_EQ( 0, pool.available() );
    pool.release( std::move( a ) );
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 1, pool.available() );
}

TEST( ObjectPool_Tests, release_via_ptr_reset ) {
    auto pool = eadlib::ObjectPool<O>( 1 );
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 1, pool.available() );
    auto a = pool.acquire();
    ASSERT_EQ( 1, pool.used() );
    ASSERT_EQ( 0, pool.available() );
    a.reset();
    ASSERT_EQ( 0, pool.used() );
    ASSERT_EQ( 1, pool.available() );
}

TEST( ObjectPool_Tests, empty ) {
    auto pool = eadlib::ObjectPool<O>( 1 );
    ASSERT_FALSE( pool.empty() );
    auto a = pool.acquire();
    ASSERT_TRUE( pool.empty() );
    pool.release( std::move( a ) );
    ASSERT_FALSE( pool.empty() );
}

#endif //EADLIB_OBJECTPOOL_TEST_H
