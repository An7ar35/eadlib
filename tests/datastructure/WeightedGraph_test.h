#ifndef EADLIB_WEIGHTEDGRAPH_TEST_H
#define EADLIB_WEIGHTEDGRAPH_TEST_H

#include "gtest/gtest.h"
#include "../../src/datastructure/WeightedGraph.h"

TEST( WeightedGraph_Tests, at ) {
    auto g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 1, 3 );
    g.createDirectedEdge( 1, 4 );
    g.createDirectedEdge( 4, 1 );
    ASSERT_EQ( 3, g.at( 1 ).childrenList.size() );
    ASSERT_EQ( 1, g.at( 4 ).childrenList.size() );
    ASSERT_EQ( 1, g.at( 1 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 2 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 3 ).parentsList.size() );
    ASSERT_EQ( 1, g.at( 4 ).parentsList.size() );
    //Exception
    eadlib::WeightedGraph<int> g2 = eadlib::WeightedGraph<int>();
    ASSERT_THROW( g2.at( 1 ), std::out_of_range );
}

TEST( WeightedGraph_Tests, createDirectedEdge ) {
    auto g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 4 ) );
    ASSERT_FALSE( g.edgeExists( 4, 5 ) );
    ASSERT_FALSE( g.edgeExists( 5, 6 ) );
    ASSERT_FALSE( g.edgeExists( 6, 7 ) );
    ASSERT_FALSE( g.edgeExists( 7, 8 ) );
    ASSERT_FALSE( g.edgeExists( 8, 9 ) );
    ASSERT_FALSE( g.edgeExists( 9, 10 ) );
    ASSERT_FALSE( g.edgeExists( 10, 1 ) );
    //Create edges
    ASSERT_TRUE( g.createDirectedEdge( 1, 2 ) );
    ASSERT_TRUE( g.createDirectedEdge( 2, 3 ) );
    ASSERT_TRUE( g.createDirectedEdge( 3, 4 ) );
    ASSERT_TRUE( g.createDirectedEdge( 4, 5 ) );
    ASSERT_TRUE( g.createDirectedEdge( 5, 6 ) );
    ASSERT_TRUE( g.createDirectedEdge( 6, 7 ) );
    ASSERT_TRUE( g.createDirectedEdge( 7, 8 ) );
    ASSERT_TRUE( g.createDirectedEdge( 8, 9 ) );
    ASSERT_TRUE( g.createDirectedEdge( 9, 10 ) );
    ASSERT_TRUE( g.createDirectedEdge( 10, 1 ) );
    ASSERT_FALSE( g.createDirectedEdge( 1, 11 ) );
    //Check if edges are created
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.edgeExists( 2, 3 ) );
    ASSERT_TRUE( g.edgeExists( 3, 4 ) );
    ASSERT_TRUE( g.edgeExists( 4, 5 ) );
    ASSERT_TRUE( g.edgeExists( 5, 6 ) );
    ASSERT_TRUE( g.edgeExists( 6, 7 ) );
    ASSERT_TRUE( g.edgeExists( 7, 8 ) );
    ASSERT_TRUE( g.edgeExists( 8, 9 ) );
    ASSERT_TRUE( g.edgeExists( 9, 10 ) );
    ASSERT_TRUE( g.edgeExists( 10, 1 ) );
}

TEST( WeightedGraph_Tests, createDirectedEdge_with_weight ) {
    auto g = eadlib::WeightedGraph<int>( { 1, 2 } );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2 ) );
    ASSERT_EQ( 1, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2, 5 ) );
    ASSERT_EQ( 6, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_THROW( g.createDirectedEdge( 1, 2, std::numeric_limits<size_t>::max() ), std::overflow_error );
}

TEST( WeightedGraph_Tests, creatDirectedEdge_fast ) {
    eadlib::WeightedGraph<size_t> g = eadlib::WeightedGraph<size_t>();
    for( size_t i = 0; i < 100; i++ ) {
        ASSERT_EQ( i, g.size() );
        g.createDirectedEdge_fast( i, i + 1 );
        ASSERT_EQ( 2 + i, g.nodeCount() );
    }
}

TEST( WeightedGraph_Tests, creatDirectedEdge_fast_with_weight ) {
    auto g = eadlib::WeightedGraph<int>();
    ASSERT_EQ( 0, g.size() );
    ASSERT_EQ( 0, g.nodeCount() );
    ASSERT_TRUE( g.createDirectedEdge_fast( 1, 2, 10 ) );
    ASSERT_EQ( 10, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2, 5 ) );
    ASSERT_EQ( 15, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_EQ( 2, g.nodeCount() );
    ASSERT_EQ( 15, g.size() );
    ASSERT_THROW( g.createDirectedEdge( 1, 2, std::numeric_limits<size_t>::max() ), std::overflow_error );
}

TEST( WeightedGraph_Tests, deleteDirectedEdge ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2 } );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.deleteDirectedEdge( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    g.createDirectedEdge( 1, 2, 10 );
    for( int i = 10; i > 0; i-- ) {
        g.deleteDirectedEdge( 1, 2 );
        ASSERT_EQ( i - 1, g.getWeight( 1, 2 ) );
    }
}

TEST( WeightedGraph_Tests, deleteAllDirectedEdges ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2 } );
    g.createDirectedEdge( 1, 2, 10 );
    ASSERT_EQ( 10, g.getWeight( 1, 2 ) );
    ASSERT_TRUE( g.deleteAllDirectedEdges( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 1, 2 ) );
    ASSERT_FALSE( g.deleteAllDirectedEdges( 2, 1 ) );
}

TEST( WeightedGraph_Tests, addNode ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>();
    ASSERT_EQ( 0, g.nodeCount() );
    for( int i = 0; i < 10; i++ ) {
        ASSERT_TRUE( g.addNode( i ) );
        ASSERT_EQ( i + 1, g.nodeCount() );
    }
    ASSERT_FALSE( g.addNode( 1 ) );
}

TEST( WeightedGraph_Tests, deleteNode ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 2, 3 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 4, 5 );
    g.createDirectedEdge( 5, 6 );
    g.createDirectedEdge( 6, 7 );
    g.createDirectedEdge( 7, 8 );
    g.createDirectedEdge( 8, 9 );
    g.createDirectedEdge( 9, 10 );
    g.createDirectedEdge( 10, 1 );
    g.createDirectedEdge( 1, 3 );
    g.createDirectedEdge( 3, 1 );
    g.createDirectedEdge( 5, 1 );
    ASSERT_EQ( 13, g.size() );
    ASSERT_EQ( 10, g.nodeCount() );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_TRUE( g.edgeExists( 10, 1 ) );
    ASSERT_TRUE( g.edgeExists( 1, 3 ) );
    ASSERT_TRUE( g.edgeExists( 3, 1 ) );
    ASSERT_TRUE( g.edgeExists( 5, 1 ) );
    ASSERT_TRUE( g.deleteNode( 1 ) );
    ASSERT_EQ( 8, g.size() );
    ASSERT_EQ( 9, g.nodeCount() );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 10, 1 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 5, 1 ) );
    ASSERT_FALSE( g.deleteNode( 1 ) );
}

TEST( WeightedGraph_Tests, isReachable ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 2 );
    g.createDirectedEdge( 2, 3 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 4, 5 );
    g.createDirectedEdge( 5, 6 );
    g.createDirectedEdge( 6, 7 );
    g.createDirectedEdge( 7, 8 );
    g.createDirectedEdge( 8, 9 );
    g.createDirectedEdge( 9, 10 );
    ASSERT_TRUE( g.isReachable( 1, 10 ) );
    ASSERT_FALSE( g.isReachable( 10,  1 ) );
}

TEST( WeightedGraph_Tests, edgeExists ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3 } );
    ASSERT_FALSE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_FALSE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
    g.createDirectedEdge( 2, 1 );
    ASSERT_TRUE( g.edgeExists( 1, 2 ) );
    ASSERT_FALSE( g.edgeExists( 1, 3 ) );
    ASSERT_TRUE( g.edgeExists( 2, 1 ) );
    ASSERT_FALSE( g.edgeExists( 2, 3 ) );
    ASSERT_FALSE( g.edgeExists( 3, 1 ) );
    ASSERT_FALSE( g.edgeExists( 3, 2 ) );
}

TEST( WeightedGraph_Tests, getWeight ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>();
    g.addNode( 1 );
    g.addNode( 2 );
    g.addNode( 3 );
    ASSERT_EQ( 0, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_EQ( 0, g.getWeight( 1, 3 ) );
    ASSERT_EQ( 0, g.getWeight( 3, 1 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 3 ) );
    ASSERT_EQ( 0, g.getWeight( 3, 2 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_EQ( 1, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    g.createDirectedEdge( 1, 2 );
    ASSERT_EQ( 2, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    g.createDirectedEdge( 1, 3, 10 );
    ASSERT_EQ( 10, g.getWeight( 1, 3 ) );
    ASSERT_EQ( 0, g.getWeight( 3, 1 ) );
    g.deleteDirectedEdge( 1, 3 );
    ASSERT_EQ( 9, g.getWeight( 1, 3 ) );
    //final state check
    ASSERT_EQ( 2, g.getWeight( 1, 2 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 1 ) );
    ASSERT_EQ( 9, g.getWeight( 1, 3 ) );
    ASSERT_EQ( 0, g.getWeight( 3, 1 ) );
    ASSERT_EQ( 0, g.getWeight( 2, 3 ) );
    ASSERT_EQ( 0, g.getWeight( 3, 2 ) );
}

TEST( WeightedGraph_Tests, isEmpty ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>();
    ASSERT_TRUE( g.isEmpty() );
    g.addNode( 1 );
    ASSERT_FALSE( g.isEmpty() );
    g.deleteNode( 1 );
    ASSERT_TRUE( g.isEmpty() );
}

TEST( WeightedGraph_Tests, nodeCount ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    for( unsigned i = 10; i > 0; i-- ) {
        ASSERT_EQ( i, g.nodeCount() );
        g.deleteNode( i );
    }
    ASSERT_EQ( 0, g.nodeCount() );
}

TEST( WeightedGraph_Tests, size ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_EQ( 0, g.size() );
    for( int i = 1; i < 9; i++ ) {
        g.createDirectedEdge( i, i + 1 );
        ASSERT_EQ( i, g.size() );
    }
    auto edges = g.size();
    for( int i = 1; i < 9; i++ ) {
        ASSERT_EQ( edges, g.size() );
        g.deleteDirectedEdge( i, i + 1 );
        edges--;
    }
    ASSERT_EQ( 0, g.size() );
}

TEST( WeightedGraph_Tests, getOutDegree ) {
    eadlib::WeightedGraph<std::string> g = eadlib::WeightedGraph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 1, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 2, g.getOutDegree( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree( "C" ) );
}

TEST( WeightedGraph_Tests, getInDegree ) {
    eadlib::WeightedGraph<std::string> g = eadlib::WeightedGraph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getInDegree( "A" ) );
    ASSERT_EQ( 0, g.getInDegree( "B" ) );
    ASSERT_EQ( 0, g.getInDegree( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 0, g.getInDegree( "A" ) );
    ASSERT_EQ( 1, g.getInDegree( "B" ) );
    ASSERT_EQ( 0, g.getInDegree( "C" ) );
    g.createDirectedEdge( "B", "A" );
    ASSERT_EQ( 1, g.getInDegree( "A" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 1, g.getInDegree( "A" ) );
    ASSERT_EQ( 1, g.getInDegree( "B" ) );
    ASSERT_EQ( 1, g.getInDegree( "C" ) );
}

TEST( WeightedGraph_Tests, getOutDegree_weighted ) {
    eadlib::WeightedGraph<std::string> g = eadlib::WeightedGraph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 1, g.getOutDegree_weighted( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "C" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 2, g.getOutDegree_weighted( "A" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "B" ) );
    ASSERT_EQ( 0, g.getOutDegree_weighted( "C" ) );
    g.createDirectedEdge( "A", "C", 6 );
    g.createDirectedEdge( "A", "B", 3 );
    ASSERT_EQ( 11, g.getOutDegree_weighted( "A" ) );
}

TEST( WeightedGraph_Tests, getInDegree_weighted ) {
    eadlib::WeightedGraph<std::string> g = eadlib::WeightedGraph<std::string>( { "A", "B", "C" } );
    ASSERT_EQ( 0, g.getInDegree_weighted( "A" ) );
    ASSERT_EQ( 0, g.getInDegree_weighted( "B" ) );
    ASSERT_EQ( 0, g.getInDegree_weighted( "C" ) );
    g.createDirectedEdge( "A", "B" );
    ASSERT_EQ( 0, g.getInDegree_weighted( "A" ) );
    ASSERT_EQ( 1, g.getInDegree_weighted( "B" ) );
    ASSERT_EQ( 0, g.getInDegree_weighted( "C" ) );
    g.createDirectedEdge( "B", "A" );
    ASSERT_EQ( 1, g.getInDegree_weighted( "A" ) );
    g.createDirectedEdge( "A", "C" );
    ASSERT_EQ( 1, g.getInDegree_weighted( "A" ) );
    ASSERT_EQ( 1, g.getInDegree_weighted( "B" ) );
    ASSERT_EQ( 1, g.getInDegree_weighted( "C" ) );
    g.createDirectedEdge( "A", "C", 5 );
    ASSERT_EQ( 6, g.getInDegree_weighted( "C" ) );
    g.createDirectedEdge( "B", "C", 10 );
    ASSERT_EQ( 1, g.getInDegree_weighted( "A" ) );
    ASSERT_EQ( 1, g.getInDegree_weighted( "B" ) );
    ASSERT_EQ( 16, g.getInDegree_weighted( "C" ) );
}


TEST( WeightedGraph_Tests, printAdjacencyList ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    g.createDirectedEdge( 1, 5 );
    g.createDirectedEdge( 5, 2 );
    g.createDirectedEdge( 3, 4 );
    g.createDirectedEdge( 3, 1 );
    std::string expected = "[10] -> \n"
        "[9] -> \n"
        "[8] -> \n"
        "[7] -> \n"
        "[6] -> \n"
        "[5] -> [2]x1 \n"
        "[1] -> [5]x1 \n"
        "[2] -> \n"
        "[3] -> [4]x1 [1]x1 \n"
        "[4] -> \n";
    std::ostringstream oss;
    g.printAdjacencyList( oss );
    ASSERT_EQ(expected, oss.str() );
}

TEST( WeightedGraph_Tests, printGraphNodes ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5 } );
    std::string expected = "5\n"
        "1\n"
        "2\n"
        "3\n"
        "4\n";
    std::ostringstream oss;
    g.printGraphNodes( oss );
    ASSERT_EQ( expected, oss.str() );
}

TEST( WeightedGraph_Tests, printStats ) {
    eadlib::WeightedGraph<int> g = eadlib::WeightedGraph<int>( { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 } );
    ASSERT_TRUE( g.createDirectedEdge( 1, 2 ) );
    ASSERT_TRUE( g.createDirectedEdge( 2, 3 ) );
    ASSERT_TRUE( g.createDirectedEdge( 3, 4 ) );
    ASSERT_TRUE( g.createDirectedEdge( 4, 5 ) );
    ASSERT_TRUE( g.createDirectedEdge( 5, 6 ) );
    ASSERT_TRUE( g.createDirectedEdge( 6, 7 ) );
    ASSERT_TRUE( g.createDirectedEdge( 7, 8 ) );
    ASSERT_TRUE( g.createDirectedEdge( 8, 9 ) );
    ASSERT_TRUE( g.createDirectedEdge( 9, 10 ) );
    ASSERT_TRUE( g.createDirectedEdge( 10, 1 ) );
    std::string expected = "Number of nodes: 10\n"
        "Number of edges: 10\n";
    std::ostringstream oss;
    g.printStats( oss );
    ASSERT_EQ( expected, oss.str() );
}

#endif //EADLIB_WEIGHTEDGRAPH_TEST_H
