#ifndef EADLIB_TESTS_COORDINATE2D_TEST_H
#define EADLIB_TESTS_COORDINATE2D_TEST_H

#include "gtest/gtest.h"
#include "../../src/datatype/Coordinate2D.h"

TEST( Coordinate2D_Tests, Constructor ) {
    eadlib::Coordinate2D<int> coord = eadlib::Coordinate2D<int>();
    ASSERT_TRUE( coord.x == 0 && coord.y == 0 );
}

TEST( Coordinate2D_Tests, Constructor_with_values ) {
    eadlib::Coordinate2D<int> coord = eadlib::Coordinate2D<int>( 10, 50 );
    ASSERT_TRUE( coord.x == 10 && coord.y == 50 );
}

TEST( Coordinate2D_Tests, set_values ) {
    eadlib::Coordinate2D<int> coord = eadlib::Coordinate2D<int>();
    coord.set( 100, 999 );
    ASSERT_TRUE( coord.x == 100 && coord.y == 999 );
}

TEST( Coordinate2D_Tests, reset_values ) {
    eadlib::Coordinate2D<int> coord = eadlib::Coordinate2D<int>( 999, 999 );
    ASSERT_TRUE( coord.x == 999 && coord.y == 999 );
    coord.reset();
    ASSERT_TRUE( coord.x == 0 && coord.y == 0 );
}

#endif //EADLIB_TESTS_COORDINATE2D_TEST_H
