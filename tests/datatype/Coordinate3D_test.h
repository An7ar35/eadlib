#ifndef EADLIB_TESTS_COORDINATE3D_TEST_H
#define EADLIB_TESTS_COORDINATE3D_TEST_H

#include "gtest/gtest.h"
#include "../../src/datatype/Coordinate3D.h"

TEST( Coordinate3D_Tests, Constructor ) {
    eadlib::Coordinate3D coord = eadlib::Coordinate3D();
    ASSERT_TRUE( coord.x() == 0 && coord.y() == 0 && coord.z() == 0 );
}

TEST( Coordinate3D_Tests, Constructor_with_values ) {
    eadlib::Coordinate3D coord = eadlib::Coordinate3D( 10.01, 20.02, 30.03 );
    ASSERT_TRUE( coord.x() == 10.01 && coord.y() == 20.02 && coord.z() == 30.03 );
}

TEST( Coordinate3D_Tests, Copy_Constructor ) {
    eadlib::Coordinate3D original = eadlib::Coordinate3D( 10.01, 20.02, 30.03 );
    ASSERT_TRUE( original.x() == 10.01 && original.y() == 20.02 && original.z() == 30.03 );
    eadlib::Coordinate3D copy = eadlib::Coordinate3D( original );
    ASSERT_TRUE( copy.x() == 10.01 && copy.y() == 20.02 && copy.z() == 30.03 );
}

TEST( Coordinate3D_Tests, Move_Constructor ) {
    eadlib::Coordinate3D coord( eadlib::Coordinate3D( 10.01, 20.02, 30.03 ) );
    ASSERT_TRUE( coord.x() == 10.01 && coord.y() == 20.02 && coord.z() == 30.03 );
}

TEST( Coordinate3D_Tests, Operator_Equal ) {
    eadlib::Coordinate3D original = eadlib::Coordinate3D( 10.01, 20.02, 30.03 );
    eadlib::Coordinate3D copy = original;
    ASSERT_TRUE( copy.x() == 10.01 && copy.y() == 20.02 && copy.z() == 30.03 );
}

TEST( Coordinate3D_Tests, Operator_Equality ) {
    eadlib::Coordinate3D coord1 = eadlib::Coordinate3D( 1, 1, 1 );
    eadlib::Coordinate3D coord2 = eadlib::Coordinate3D( 2, 2, 2 );
    eadlib::Coordinate3D coord3 = eadlib::Coordinate3D( 1, 1, 1 );
    ASSERT_TRUE( coord1 == coord3 );
    ASSERT_FALSE( coord1 == coord2 );
}

TEST( Coordinate3D_Tests, Operator_NotEquality ) {
    eadlib::Coordinate3D coord1 = eadlib::Coordinate3D( 1, 1, 1 );
    eadlib::Coordinate3D coord2 = eadlib::Coordinate3D( 2, 2, 2 );
    eadlib::Coordinate3D coord3 = eadlib::Coordinate3D( 1, 1, 1 );
    ASSERT_TRUE( coord1 != coord2 );
    ASSERT_FALSE( coord1 != coord3 );
}

TEST( Coordinate3D_Tests, reset_values ) {
    eadlib::Coordinate3D coord = eadlib::Coordinate3D( 10.01, 20.02, 30.03 );
    ASSERT_TRUE( coord.x() == 10.01 && coord.y() == 20.02 && coord.z() == 30.03 );
    coord.reset();
    ASSERT_TRUE( coord.x() == 0 && coord.y() == 0 && coord.z() == 0 );
}

TEST( Coordinate3D_Tests, isReset ) {
    eadlib::Coordinate3D coord = eadlib::Coordinate3D( 999, 999, 999 );
    ASSERT_TRUE( coord.x() == 999 && coord.y() == 999 && coord.z() == 999 );
    coord.reset();
    ASSERT_TRUE( coord.x() == 0 && coord.y() == 0 && coord.z() == 0 );
}

TEST( Coordinate3D_Tests, swap_with ){
    eadlib::Coordinate3D a = eadlib::Coordinate3D( 100, -100, 0 );
    eadlib::Coordinate3D b = eadlib::Coordinate3D( -50, 50, 99 );
    a.setError( true );
    a.swap( b );
    ASSERT_TRUE( a.x() == -50 && a.y() == 50 && a.z() == 99 && !a.error() );
    ASSERT_TRUE( b.x() == 100 && b.y() == -100 && b.z() == 0 && b.error() );
}

TEST( Coordinate3D_Tests, calcDistance ) {
    eadlib::Coordinate3D c1( 0, 0, 0 );
    eadlib::Coordinate3D c2( 10, 10, 10 );
    eadlib::Coordinate3D c3( -5, -50, -3 );
    ASSERT_NEAR( c1.calcDistance( c2 ), 17.320508075689, 5 );
    ASSERT_NEAR( c2.calcDistance( c3 ), 63.1981012373, 5 );
    ASSERT_NEAR( c1.calcDistance( c3 ), 50.338851794613, 5 );
}

TEST( Coordinate3D_Tests, calcDifference ) {
    eadlib::Coordinate3D c1( 0, 0, 0 );
    eadlib::Coordinate3D c2( 10, 10, 10 );
    eadlib::Coordinate3D c3( -5, -50, -3 );
    ASSERT_TRUE( c1.calcDifference( c2 ) == eadlib::Coordinate3D( -10, -10, -10 ) );
    ASSERT_TRUE( c2.calcDifference( c1 ) == eadlib::Coordinate3D( 10, 10, 10 ) );
    ASSERT_TRUE( c1.calcDifference( c3 ) == eadlib::Coordinate3D( 5, 50, 3 ) );
    ASSERT_TRUE( c2.calcDifference( c3 ) == eadlib::Coordinate3D( 15, 60, 13 ) );
}

TEST( Coordinate3D_Tests, calcDifference_abs ) {
    eadlib::Coordinate3D c1( 0, 0, 0 );
    eadlib::Coordinate3D c2( 10, 10, 10 );
    eadlib::Coordinate3D c3( -5, -50, -3 );
    ASSERT_TRUE( c1.calcDifference_abs( c2 ) == eadlib::Coordinate3D( 10, 10, 10 ) );
    ASSERT_TRUE( c2.calcDifference_abs( c1 ) == eadlib::Coordinate3D( 10, 10, 10 ) );
    ASSERT_TRUE( c1.calcDifference_abs( c3 ) == eadlib::Coordinate3D( 5, 50, 3 ) );
    ASSERT_TRUE( c2.calcDifference_abs( c3 ) == eadlib::Coordinate3D( 15, 60, 13 ) );
}

TEST( Coordinate3D_Tests, add ) {
    eadlib::Coordinate3D a { 0, 0, 0 };
    eadlib::Coordinate3D b { 15, 15, 15 };
    eadlib::Coordinate3D c { -15, -15, -15 };
    a += b;
    ASSERT_TRUE( a == b );
    a = eadlib::Coordinate3D( 0, 0, 0 );
    a += c;
    ASSERT_TRUE( a == c );
}

TEST( Coordinate3D_Tests, substract ) {
    eadlib::Coordinate3D a { 0, 0, 0 };
    eadlib::Coordinate3D b { 15, 15, 15 };
    eadlib::Coordinate3D c { -15, -15, -15 };
    a -= b;
    ASSERT_TRUE( a == c );
    a = eadlib::Coordinate3D( 0, 0, 0 );
    a -= c;
    ASSERT_TRUE( a == b );
}

TEST( Coordinate3D_Tests, rotate ) {
    eadlib::Coordinate3D coord( 1, 2, 5 );
    coord.rotate( eadlib::Coordinate3D::Axis::X, 90 );
    ASSERT_NEAR( coord.x(), 1, 0.0001 );
    ASSERT_NEAR( coord.y(), -5, 0.0001 );
    ASSERT_NEAR( coord.z(), 2, 0.0001 );
    coord = eadlib::Coordinate3D( 1, 2, 5 );
    coord.rotate( eadlib::Coordinate3D::Axis::X, -90 );
    ASSERT_NEAR( coord.x(), 1, 0.0001 );
    ASSERT_NEAR( coord.y(), 5, 0.0001 );
    ASSERT_NEAR( coord.z(), -2, 0.0001 );
    coord.rotate( eadlib::Coordinate3D::Axis::Y, 90 );
    ASSERT_NEAR( coord.x(), 2, 0.0001 );
    ASSERT_NEAR( coord.y(), 5, 0.0001 );
    ASSERT_NEAR( coord.z(), 1, 0.0001 );
    coord = eadlib::Coordinate3D( 1, 2, 5 );
    coord.rotate( eadlib::Coordinate3D::Axis::Y, -90 );
    ASSERT_NEAR( coord.x(), 5, 0.0001 );
    ASSERT_NEAR( coord.y(), 2, 0.0001 );
    ASSERT_NEAR( coord.z(), -1, 0.0001 );
    coord.rotate( eadlib::Coordinate3D::Axis::Z, 90 );
    ASSERT_NEAR( coord.x(), -2, 0.0001 );
    ASSERT_NEAR( coord.y(), 5, 0.0001 );
    ASSERT_NEAR( coord.z(), -1, 0.0001 );
    coord = eadlib::Coordinate3D( 1, 2, 5 );
    coord.rotate( eadlib::Coordinate3D::Axis::Z, -90 );
    ASSERT_NEAR( coord.x(), 2, 0.0001 );
    ASSERT_NEAR( coord.y(), -1, 0.0001 );
    ASSERT_NEAR( coord.z(), 5, 0.0001 );
}

#endif //EADLIB_TESTS_COORDINATE3D_TEST_H
