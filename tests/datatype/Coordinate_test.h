#ifndef EADLIB_COORDINATE_TEST_H
#define EADLIB_COORDINATE_TEST_H

#include "gtest/gtest.h"
#include "../../src/datatype/Coordinate.h"

TEST( Coordinate_Tests, Constructor ) {
    eadlib::Coordinate<char, int> c( { { 'x', 1 }, { 'y', 2 }, { 'z', 3 } } );
    EXPECT_NO_THROW(
        EXPECT_EQ( 1, c.at( 'x' ) );
        EXPECT_EQ( 2, c.at( 'y' ) );
        EXPECT_EQ( 3, c.at( 'z' ) );
    );
    EXPECT_FALSE( c.getFlag() );
}

TEST( Coordinate_Tests, Copy_Constructor ) {
    eadlib::Coordinate<char, int> c( { { 'x', 1 }, { 'y', 2 }, { 'z', 3 } } );
    eadlib::Coordinate<char, int> n( c );
    EXPECT_NO_THROW(
        EXPECT_EQ( 1, n.at( 'x' ) );
        EXPECT_EQ( 2, n.at( 'y' ) );
        EXPECT_EQ( 3, n.at( 'z' ) );
    );
}

TEST( Coordinate_Tests, Assignment_operator ) { //=
    eadlib::Coordinate<char, int> c( { { 'x', 1 }, { 'y', 2 }, { 'z', 3 } } );
    c.setFlag( true );
    eadlib::Coordinate<char, int> n = c;
    EXPECT_NO_THROW(
        EXPECT_EQ( 1, n.at( 'x' ) );
        EXPECT_EQ( 2, n.at( 'y' ) );
        EXPECT_EQ( 3, n.at( 'z' ) );
        EXPECT_TRUE( n.getFlag() );
    );
}

TEST( Coordinate_Tests, Equivalence_operator ) { //==
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    eadlib::Coordinate<char,double> c2(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_EQ( c1, c2 );
}

TEST( Coordinate_Tests, at ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_EQ( 1.1, c1.at( 'x' ) );
    ASSERT_EQ( 3.2, c1.at( 'y' ) );
    ASSERT_EQ( 5.9, c1.at( 'z' ) );
    EXPECT_THROW( c1.at( 'u' ), std::invalid_argument );
}

TEST( Coordinate_Tests, set ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_EQ( 1.1, c1.at( 'x' ) );
    EXPECT_NO_THROW( c1.set( 'x', 6.66 ) );
    EXPECT_EQ( 6.66, c1.at( 'x' ) );
    EXPECT_THROW( c1.set( 'u', 1.11 ), std::invalid_argument );
    EXPECT_THROW( c1.at( 'u' ), std::invalid_argument );
}

TEST( Coordinate_Tests, reset ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_EQ( 1.1, c1.at( 'x' ) );
    ASSERT_EQ( 3.2, c1.at( 'y' ) );
    ASSERT_EQ( 5.9, c1.at( 'z' ) );
    c1.reset( 0.0 );
    ASSERT_EQ( 0, c1.at( 'x' ) );
    ASSERT_EQ( 0, c1.at( 'y' ) );
    ASSERT_EQ( 0, c1.at( 'z' ) );
}

TEST( Coordinate_Tests, isReset ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_EQ( 1.1, c1.at( 'x' ) );
    ASSERT_EQ( 3.2, c1.at( 'y' ) );
    ASSERT_EQ( 5.9, c1.at( 'z' ) );
    EXPECT_FALSE( c1.isReset( 0 ) );
    c1.reset( 0.0 );
    EXPECT_TRUE( c1.isReset( 0 ) );
    EXPECT_EQ( 0, c1.at( 'x' ) );
    EXPECT_EQ( 0, c1.at( 'y' ) );
    EXPECT_EQ( 0, c1.at( 'z' ) );
}

TEST( Coordinate_Tests, setFlag ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    ASSERT_FALSE( c1.getFlag() );
    c1.setFlag( true );
    EXPECT_TRUE( c1.getFlag() );
    c1.setFlag( false );
    EXPECT_FALSE( c1.getFlag() );
}

TEST( Coordinate_Tests, swap ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 2.2 }, { 'z', 3.3 } } );
    eadlib::Coordinate<char,double> c2(  { { 'a', 10 }, { 'b', 20 }, { 'c', 30 } } );
    c2.setFlag( false );
    c1.swap( c2 );
    ASSERT_NO_THROW(
        EXPECT_EQ( 10, c1.at( 'a' ) );
        EXPECT_EQ( 20, c1.at( 'b' ) );
        EXPECT_EQ( 30, c1.at( 'c' ) );
        EXPECT_EQ( 1.1, c2.at( 'x' ) );
        EXPECT_EQ( 2.2, c2.at( 'y' ) );
        EXPECT_EQ( 3.3, c2.at( 'z' ) );
    );
    EXPECT_THROW( c1.at( 'x' ), std::invalid_argument );
    EXPECT_THROW( c1.at( 'y' ), std::invalid_argument );
    EXPECT_THROW( c1.at( 'z' ), std::invalid_argument );
    EXPECT_THROW( c2.at( 'a' ), std::invalid_argument );
    EXPECT_THROW( c2.at( 'b' ), std::invalid_argument );
    EXPECT_THROW( c2.at( 'c' ), std::invalid_argument );
}

TEST( Coordinate_Tests, str ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    auto returned = c1.str();
    EXPECT_TRUE( returned.find("[x=1.1]") != std::string::npos );
    EXPECT_TRUE( returned.find("[y=3.2]") != std::string::npos );
    EXPECT_TRUE( returned.find("[z=5.9]") != std::string::npos );
}

TEST( Coordinate_Tests, str_args ) {
    eadlib::Coordinate<char,double> c1(  { { 'x', 1.1 }, { 'y', 3.2 }, { 'z', 5.9 } } );
    auto returned = c1.str( { 'x', 'y', 'z' } );
    auto expected = "(1.1, 3.2, 5.9)";
    EXPECT_EQ( expected, returned );
}

#endif //EADLIB_COORDINATE_TEST_H
