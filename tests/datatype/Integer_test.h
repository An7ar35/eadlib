#ifndef EADLIB_INTEGER_TEST_H
#define EADLIB_INTEGER_TEST_H

#include "gtest/gtest.h"
#include <limits>
#include "../../src/datatype/Integer.h"

TEST( Integer_Tests, Constructor_bool_true ) {
    auto integer  = eadlib::Integer<64>( true );
    auto expected = std::bitset<64>( 0b01 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_bool_false ) {
    auto integer  = eadlib::Integer<64>( false );
    auto expected = std::bitset<64>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int32_zero ) {
    auto integer  = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = std::bitset<32>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int32_min ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto expected = std::bitset<32>( 0b10000000000000000000000000000000 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int32_max ) {
    auto integer  = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto expected = std::bitset<32>( 0b01111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint32_zero ) {
    auto integer  = eadlib::Integer<32>( uint32_t( 0 ) );
    auto expected = std::bitset<32>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint32_max_fail ) {
    //uint32 max to 32 signed Integer (overflow failure)
    ASSERT_THROW( eadlib::Integer<32>( 4294967295 ), std::overflow_error );
}

TEST( Integer_Tests, Constructor_uint32_max_1 ) {
    //uint32 31bit max to 32bits signed Integer
    auto integer   = eadlib::Integer<32>( uint32_t( 2147483647 ) ); //unsigned 31bit max
    auto expected  = std::bitset<32>( 0b01111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint32_max_2 ) {
    //uint32 32bit max to 33bits signed Integer
    auto integer   = eadlib::Integer<33>( uint32_t( 4294967295 ) );
    auto expected  = std::bitset<33>( 0b011111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int64_zero ) {
    auto integer  = eadlib::Integer<64>( int64_t( 0 ) );
    auto expected = std::bitset<64>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int64_min ) {
    auto integer  = eadlib::Integer<64>( int64_t( 9223372036854775808L ) );
    auto expected = std::bitset<64>( 0b1000000000000000000000000000000000000000000000000000000000000000 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_int64_max ) {
    auto integer  = eadlib::Integer<64>( int64_t( 9223372036854775807L ) );
    auto expected = std::bitset<64>( 0b0111111111111111111111111111111111111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint64_zero ) {
    auto integer  = eadlib::Integer<64>( uint64_t( 0UL ) );
    auto expected = std::bitset<64>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint64_max_fail ) {
    //uint64 max to 64 signed Integer (overflow failure)
    ASSERT_THROW( eadlib::Integer<64>( 18446744073709551615UL ), std::overflow_error );
}

TEST( Integer_Tests, Constructor_uint64_max_1 ) {
    //uint64 63bit max to 64bits signed Integer
    auto integer   = eadlib::Integer<64>( uint64_t( 9223372036854775807UL ) ); //unsigned 63bit max
    auto expected  = std::bitset<64>( 0b0111111111111111111111111111111111111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_uint64_max_2 ) {
    //uint64 64bit max to 65bits signed Integer
    auto integer   = eadlib::Integer<65>( uint64_t( 18446744073709551615UL ) );
    auto expected  = std::bitset<65>( 0b01111111111111111111111111111111111111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_bitset_1 ) {
    auto bitset   = std::bitset<8>( 0b10010110 );
    ASSERT_EQ( std::bitset<8>( 0b10010110 ), eadlib::Integer<8>( bitset ).bitset() );
}

TEST( Integer_Tests, Constructor_bitset_ptr_1 ) {
    auto bitset = std::make_unique<std::bitset<8>>( 0b10010110 );
    ASSERT_EQ( std::bitset<8>( 0b10010110 ), eadlib::Integer<8>( std::move( bitset ) ).bitset() );
}

TEST( Integer_Tests, Constructor_string_fail0 ) {
    ASSERT_THROW( eadlib::Integer<32>( "1o23.1#23" ), std::invalid_argument );
}

TEST( Integer_Tests, Constructor_string_fail1 ) {
    ASSERT_THROW( eadlib::Integer<32>( "" ), std::invalid_argument );
}

TEST( Integer_Tests, Constructor_string_fail2 ) {
    ASSERT_THROW( eadlib::Integer<32>( "2147483648" ), std::overflow_error );
}

TEST( Integer_Tests, Constructor_string_fail3 ) {
    ASSERT_THROW( eadlib::Integer<32>( "-2147483649" ), std::underflow_error );
}

TEST( Integer_Tests, Constructor_string_fail4 ) {
    ASSERT_THROW( eadlib::Integer<32>( "-4,294,967,295" ), std::underflow_error );
}

TEST( Integer_Tests, Constructor_string_fail5 ) {
    ASSERT_THROW( eadlib::Integer<32>( "-18,446,744,065,119,617,025" ), std::underflow_error );
}

TEST( Integer_Tests, Constructor_string_0 ) {
    auto integer  = eadlib::Integer<32>( "000000" );
    auto expected = std::bitset<32>( 0b00 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_1 ) {
    auto integer  = eadlib::Integer<32>( "1 000 005" );
    auto expected = std::bitset<32>( 0b00000000000011110100001001000101 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_2 ) {
    auto integer  = eadlib::Integer<32>( "5,000,001" );
    auto expected = std::bitset<32>( 0b00000000010011000100101101000001 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_3 ) {
    auto integer  = eadlib::Integer<32>( "00000123" );
    auto expected = std::bitset<32>( 0b00000000000000000000000001111011 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_4 ) {
    auto integer  = eadlib::Integer<32>( "2147483647" );
    auto expected = std::bitset<32>( 0b01111111111111111111111111111111 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_5 ) {
    auto integer  = eadlib::Integer<32>( "-0" );
    auto expected = std::bitset<32>( 0b00000000000000000000000000000000 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_6 ) {
    auto integer  = eadlib::Integer<32>( "- 500 125" );
    auto expected = std::bitset<32>( 0b11111111111110000101111001100011 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_7 ) {
    auto integer  = eadlib::Integer<32>( "-2147483648" );
    auto expected = std::bitset<32>( 0b10000000000000000000000000000000 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, Constructor_string_8 ) {
    auto integer  = eadlib::Integer<32>( "-2147483647" );
    auto expected = std::bitset<32>( 0b10000000000000000000000000000001 );
    ASSERT_EQ( expected, integer.bitset() );
}

TEST( Integer_Tests, create_1 ) { //same size
    auto bitset   = std::bitset<8>( 0b10100011 ); //-93
    auto returned = eadlib::Integer<8>::create( bitset );
    ASSERT_EQ( std::bitset<8>( 0b10100011 ), returned.bitset() );
}

TEST( Integer_Tests, create_2 ) { //smaller bitset
    auto bitset   = std::bitset<8>( 0b10100011 ); //-93
    auto returned = eadlib::Integer<16>::create( bitset );
    ASSERT_EQ( std::bitset<16>( 0b1111111110100011 ), returned.bitset() );
}

TEST( Integer_Tests, create_3 ) { //larger bitset
    auto bitset   = std::bitset<16>( 0b1111111110100011 ); //-93
    auto returned = eadlib::Integer<8>::create( bitset );
    ASSERT_EQ( std::bitset<8>( 0b10100011 ), returned.bitset() );
}

TEST( Integer_Tests, create_fail1 ) { //larger bitset fail (neg)
    auto bitset   = std::bitset<16>( 0b1111111000100011 ); //-477
    ASSERT_THROW( eadlib::Integer<8>::create( bitset ), std::underflow_error );
}

TEST( Integer_Tests, create_fail2 ) { //larger bitset fail (pos)
    auto bitset   = std::bitset<16>( 0b0001111000100011 ); //7715
    ASSERT_THROW( eadlib::Integer<8>::create( bitset ), std::overflow_error );
}

TEST( Integer_Tests, convert_1 ) { //same size
    auto integer  = eadlib::Integer<8>( "-93" );
    auto returned = eadlib::Integer<8>::convert( integer );
    ASSERT_EQ( std::bitset<8>( 0b10100011 ), returned.bitset() );
}

TEST( Integer_Tests, convert_2 ) { //smaller Integer size
    auto integer  = eadlib::Integer<8>( "-93" );
    auto returned = eadlib::Integer<16>::convert( integer );
    ASSERT_EQ( std::bitset<16>( 0b1111111110100011 ), returned.bitset() );
}

TEST( Integer_Tests, convert_3 ) { //larger Integer size
    auto integer  = eadlib::Integer<16>( "-93" );
    auto returned = eadlib::Integer<8>::convert( integer );
    ASSERT_EQ( std::bitset<8>( 0b10100011 ), returned.bitset() );
}

TEST( Integer_Tests, convert_fail1 ) { //larger Integer fail (neg)
    auto integer = eadlib::Integer<16>( "-477" );
    ASSERT_THROW( eadlib::Integer<8>::convert( integer ), std::underflow_error );
}

TEST( Integer_Tests, convert_fail2 ) { //larger Integer fail (pos)
    auto integer = eadlib::Integer<16>( "7715" );
    ASSERT_THROW( eadlib::Integer<8>::convert( integer ), std::overflow_error );
}

TEST( Integer_Tests, str_base2 ) {
    auto integer  = eadlib::Integer<64>( 34589 );
    auto expected = "0000000000000000000000000000000000000000000000001000011100011101";
    auto returned = integer.str_base2();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, str_base10 ) {
    auto integer  = eadlib::Integer<64>( 34589 );
    auto expected = "34589";
    auto returned = integer.str_base10();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, str_base16 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 34589 ) );
    auto expected = "871D";
    auto returned = integer.str_base16();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, printBase2 ) {
    auto integer  = eadlib::Integer<64>( 34589 );
    auto expected = "0000000000000000000000000000000000000000000000001000011100011101";
    std::stringstream returned;
    integer.printBase2( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_zero ) {
    auto integer  = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = "0";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_pos_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = "1";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_pos_2 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 34589 ) );
    auto expected = "34589";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_neg_0 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -0 ) );
    auto expected = "0";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_neg_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = "-1";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_neg_2 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -34589 ) );
    auto expected = "-34589";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_min ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto expected = "-2147483648";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase10_int32_max ) {
    auto integer  = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto expected = "2147483647";
    std::stringstream returned;
    integer.printBase10( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_zero ) {
    auto integer  = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = "0";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_pos_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = "1";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_pos_2 ) {
    auto integer = eadlib::Integer<32>( int32_t( 34589 ) );
    auto expected = "871D";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_neg_zero ) {
    auto integer  = eadlib::Integer<32>( int32_t( -0 ) );
    auto expected = "0";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_neg_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = "FFFFFFFF";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_neg_2 ) {
    auto integer = eadlib::Integer<32>( int32_t( -34589 ) );
    auto expected = "FFFF78E3";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_min ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto expected = "80000000";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int32_max ) {
    auto integer  = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto expected = "7FFFFFFF";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int23 ) {
    auto integer  = eadlib::Integer<23>( std::bitset<23>( 0b00111010101010111000111 ) ); //1922503
    auto expected = "1D55C7";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int22 ) {
    auto integer  = eadlib::Integer<22>( std::bitset<22>( 0b0111001010101110101011 ) ); //1878955
    auto expected = "1CABAB";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, printBase16_int21 ) {
    auto integer  = eadlib::Integer<23>( std::bitset<23>( 0b011010101010111000111 ) ); //873927
    auto expected = "D55C7";
    std::stringstream returned;
    integer.printBase16( returned );
    ASSERT_EQ( expected, returned.str() );
}

TEST( Integer_Tests, inputBase2 ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "1101001"; //-23
    integer.inputBase2( ss );
    ASSERT_EQ( std::bitset<8>( 0b11101001 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase2_zero ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "00000000000000";
    integer.inputBase2( ss );
    ASSERT_EQ( std::bitset<8>( 0b0 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase2_min ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "10000000";
    integer.inputBase2( ss );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase2_max ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "01111111";
    integer.inputBase2( ss );
    ASSERT_EQ( std::bitset<8>( 0b01111111 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase2_fail1 ) { //invalid argument: empty
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "";
    ASSERT_THROW( integer.inputBase2( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase2_fail2 ) { //invalid argument: malformed string
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "00110o5";
    ASSERT_THROW( integer.inputBase2( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase2_fail4 ) { //overflow (too large)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "0 1111 1111";
    ASSERT_THROW( integer.inputBase2( ss ), std::overflow_error );
}

TEST( Integer_Tests, inputBase2_fail5 ) { //underflow (too small)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "101 0111 1111";
    ASSERT_THROW( integer.inputBase2( ss ), std::underflow_error );
}

TEST( Integer_Tests, inputBase10 ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "-23";
    integer.inputBase10( ss );
    ASSERT_EQ( std::bitset<8>( 0b11101001 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase10_zero ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "0";
    integer.inputBase10( ss );
    ASSERT_EQ( std::bitset<8>( 0b0 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase10_min ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "-128";
    integer.inputBase10( ss );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase10_max ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "127";
    integer.inputBase10( ss );
    ASSERT_EQ( std::bitset<8>( 0b01111111 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase10_fail1 ) { //invalid argument: empty
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "";
    ASSERT_THROW( integer.inputBase10( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase10_fail2 ) { //invalid argument: malformed string
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "0012o";
    ASSERT_THROW( integer.inputBase10( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase10_fail4 ) { //overflow (too large)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "128";
    ASSERT_THROW( integer.inputBase10( ss ), std::overflow_error );
}

TEST( Integer_Tests, inputBase10_fail5 ) { //underflow (too small)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "-129";
    ASSERT_THROW( integer.inputBase10( ss ), std::underflow_error );
}

TEST( Integer_Tests, inputBase16 ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "FE9";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<8>( 0b11101001 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_zero ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "0";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_min ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "F80";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_max ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "7F";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<8>( 0b01111111 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_padded1 ) {
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "FF8F";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<8>( 0b10001111 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_padded2 ) {
    auto integer = eadlib::Integer<9>();
    std::stringstream ss;
    ss << "FB5";
    integer.inputBase16( ss );
    ASSERT_EQ( std::bitset<9>( 0b110110101 ), integer.bitset() );
}

TEST( Integer_Tests, inputBase16_fail1 ) { //invalid argument: empty
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "";
    ASSERT_THROW( integer.inputBase16( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase16_fail2 ) { //invalid argument: malformed string
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "7G5A";
    ASSERT_THROW( integer.inputBase16( ss ), std::invalid_argument );
}

TEST( Integer_Tests, inputBase16_fail4 ) { //overflow (too large)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "100";
    ASSERT_THROW( integer.inputBase16( ss ), std::overflow_error );
}

TEST( Integer_Tests, inputBase16_fail5 ) { //underflow (too small)
    auto integer = eadlib::Integer<8>();
    std::stringstream ss;
    ss << "F7F";
    ASSERT_THROW( integer.inputBase16( ss ), std::underflow_error );
}

TEST( Integer_Tests, compareTo_0 ) { //same sizes -1/0/+1
    ASSERT_EQ(  0, eadlib::Integer<16>( int16_t( 0 ) ).compareTo( eadlib::Integer<16>( int16_t( 0 ) ) ) );
    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( 0 ) ).compareTo( eadlib::Integer<16>( int16_t( 1 ) ) ) );
    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( 1 ) ).compareTo( eadlib::Integer<16>( int16_t( 0 ) ) ) );
    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( -1 ) ).compareTo( eadlib::Integer<16>( int16_t( 0 ) ) ) );
    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( 0 ) ).compareTo( eadlib::Integer<16>( int16_t( -1 ) ) ) );
    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( -1 ) ).compareTo( eadlib::Integer<16>( int16_t( 1 ) ) ) );
    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( 1 ) ).compareTo( eadlib::Integer<16>( int16_t( -1 ) ) ) );
}

TEST( Integer_Tests, compareTo_1 ) { //same sizes std
    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( 12 ) ).compareTo( eadlib::Integer<16>( int16_t( 128 ) ) ) );
    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( 128 ) ).compareTo( eadlib::Integer<16>( int16_t( 12 ) ) ) );
    ASSERT_EQ(  0, eadlib::Integer<16>( int16_t( 128 ) ).compareTo( eadlib::Integer<16>( int16_t( 128 ) ) ) );

    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( -12 ) ).compareTo( eadlib::Integer<16>( int16_t( -128 ) ) ) );
    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( -128 ) ).compareTo( eadlib::Integer<16>( int16_t( -12 ) ) ) );
    ASSERT_EQ(  0, eadlib::Integer<16>( int16_t( -12 ) ).compareTo( eadlib::Integer<16>( int16_t( -12 ) ) ) );
    ASSERT_EQ(  0, eadlib::Integer<16>( int16_t( -128 ) ).compareTo( eadlib::Integer<16>( int16_t( -128 ) ) ) );

    ASSERT_EQ( -1, eadlib::Integer<16>( int16_t( -128 ) ).compareTo( eadlib::Integer<16>( int16_t( 128 ) ) ) );
    ASSERT_EQ(  1, eadlib::Integer<16>( int16_t( 128 ) ).compareTo( eadlib::Integer<16>( int16_t( -128 ) ) ) );
}

TEST( Integer_Tests, compareTo_2 ) { //same sizes crossing bit
    auto a = eadlib::Integer<8>( char( -4 ) );
    auto b = eadlib::Integer<8>( char( -2 ) );
    auto c = eadlib::Integer<8>( char(  2 ) );
    auto d = eadlib::Integer<8>( char(  4 ) );
    ASSERT_EQ( -1, a.compareTo( b ) );
    ASSERT_EQ(  1, b.compareTo( a ) );
    ASSERT_EQ( -1, c.compareTo( d ) );
    ASSERT_EQ(  1, d.compareTo( c ) );
    ASSERT_EQ( -1, a.compareTo( d ) );
    ASSERT_EQ( -1, b.compareTo( c ) );
    ASSERT_EQ(  1, d.compareTo( a ) );
    ASSERT_EQ(  1, c.compareTo( b ) );
}

TEST( Integer_Tests, compareTo_3 ) {
    auto a = eadlib::Integer<16>();
    auto b = eadlib::Integer<8>();
    ASSERT_EQ( 0, a.compareTo( b ) );
    ASSERT_EQ( 0, b.compareTo( a ) );
}

TEST( Integer_Tests, compareTo_4 ) {
    auto a = eadlib::Integer<16>( int16_t( 1 ) );
    auto b = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( 1, a.compareTo( b ) );
    ASSERT_EQ( -1, b.compareTo( a ) );
}

TEST( Integer_Tests, compareTo_5 ) {
    auto a = eadlib::Integer<16>( int16_t( 1 ) );
    auto b = eadlib::Integer<32>( int32_t( -1 ) );
    ASSERT_EQ( 1, a.compareTo( b ) );
    ASSERT_EQ( -1, b.compareTo( a ) );
}

TEST( Integer_Tests, compareTo_6 ) {
    auto a = eadlib::Integer<16>( int16_t( 32767 ) );
    auto b = eadlib::Integer<32>( int32_t( 65535 ) );
    ASSERT_EQ( -1, a.compareTo( b ) );
    ASSERT_EQ( 1, b.compareTo( a ) );
}

TEST( Integer_Tests, compareTo_7 ) {
    auto a = eadlib::Integer<16>( int16_t( -32768 ) );
    auto b = eadlib::Integer<32>( int32_t( -65536 ) );
    ASSERT_EQ( 1, a.compareTo( b ) );
    ASSERT_EQ( -1, b.compareTo( a ) );
}

TEST( Integer_Tests, compareTo_8 ) {
    auto a = eadlib::Integer<16>( int16_t( -32768 ) );
    auto b = eadlib::Integer<32>( int32_t( 65535 ) );
    ASSERT_EQ( -1, a.compareTo( b ) );
    ASSERT_EQ( 1, b.compareTo( a ) );
}

TEST( Integer_Tests, equality_operator_0 ) { //==
    auto a = eadlib::Integer<32>();
    auto b = eadlib::Integer<32>();
    ASSERT_TRUE( a == b );
}

TEST( Integer_Tests, equality_operator_1 ) { //==
    auto a = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto b = eadlib::Integer<32>( int32_t( 2147483647 ) );
    ASSERT_TRUE( a == b );
}

TEST( Integer_Tests, equality_operator_2 ) { //==
    auto a = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto b = eadlib::Integer<32>( int32_t( -2147483648 ) );
    ASSERT_TRUE( a == b );
}

TEST( Integer_Tests, equality_operator_3 ) { //==
    auto a = eadlib::Integer<32>( 123456 );
    auto b = eadlib::Integer<32>( 123457 );
    ASSERT_FALSE( a == b );
}

TEST( Integer_Tests, notEqual_operator_0 ) { //!=
    auto a = eadlib::Integer<32>( 654321 );
    auto b = eadlib::Integer<32>( 654231 );
    ASSERT_TRUE( a != b );
}

TEST( Integer_Tests, notEqual_operator_1 ) { //!=
    auto a = eadlib::Integer<32>( 654231 );
    auto b = eadlib::Integer<32>( 654231 );
    ASSERT_FALSE( a != b );
}

TEST( Integer_Tests, lessThan_operator_0 ) { //<
    auto a = eadlib::Integer<32>();
    auto b = eadlib::Integer<32>();
    ASSERT_FALSE( a < b );
}

TEST( Integer_Tests, lessThan_operator_1 ) { //<
    auto a = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto b = eadlib::Integer<32>( int32_t( 2147483647 ) );
    ASSERT_TRUE( a < b );
    ASSERT_FALSE( b < a );
}

TEST( Integer_Tests, lessThan_operator_2 ) { //<
    auto a = eadlib::Integer<32>( int32_t( -12346 ) );
    auto b = eadlib::Integer<32>( int32_t( -12345 ) );
    ASSERT_TRUE( a < b );
    ASSERT_FALSE( b < a );
}

TEST( Integer_Tests, lessThan_operator_3 ) { //<
    auto a = eadlib::Integer<32>( int32_t( -12345 ) );
    auto b = eadlib::Integer<32>( int32_t( 12345 ) );
    ASSERT_TRUE( a < b );
    ASSERT_FALSE( b < a );
}

TEST( Integer_Tests, lessThan_operator_4 ) { //<
    auto a = eadlib::Integer<32>( int32_t( 12345 ) );
    auto b = eadlib::Integer<32>( int32_t( 12346 ) );
    ASSERT_TRUE( a < b );
    ASSERT_FALSE( b < a );
}

TEST( Integer_Tests, lessThan_operator_5 ) { //<
    auto a = eadlib::Integer<32>( int32_t( -3 ) );
    auto b = eadlib::Integer<32>( int32_t( -2 ) );
    ASSERT_TRUE( a < b );
    ASSERT_FALSE( b < a );
}

TEST( Integer_Tests, moreThan_operator_0 ) { //>
    auto a = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto b = eadlib::Integer<32>( int32_t( 2147483647 ) );
    ASSERT_FALSE( a > b );
    ASSERT_TRUE( b > a );
}

TEST( Integer_Tests, moreThan_operator_1 ) { //>
    auto a = eadlib::Integer<32>( 123456 );
    auto b = eadlib::Integer<32>( 123457 );
    ASSERT_FALSE( a > b );
    ASSERT_TRUE( b > a );
}

TEST( Integer_Tests, moreThan_operator_2 ) { //>
    auto a = eadlib::Integer<32>( -123456 );
    auto b = eadlib::Integer<32>( -123457 );
    ASSERT_TRUE( a > b );
    ASSERT_FALSE( b > a );
}

TEST( Integer_Tests, lessThanEqual_operator_0 ) { //<=
    auto a1 = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto a2 = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto b1 = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto b2 = eadlib::Integer<32>( int32_t( 2147483647 ) );
    ASSERT_TRUE( a1 <= b1 );
    ASSERT_TRUE( a1 <= a2 );
    ASSERT_FALSE( b1 <= a1 );
    ASSERT_TRUE( b1 <= b2 );
}

TEST( Integer_Tests, lessThanEqual_operator_1 ) { //<=
    auto a1 = eadlib::Integer<32>( int32_t( 25 ) );
    auto a2 = eadlib::Integer<32>( int32_t( 25 ) );
    auto b1 = eadlib::Integer<32>( int32_t( 61 ) );
    auto b2 = eadlib::Integer<32>( int32_t( 61 ) );
    ASSERT_TRUE( a1 <= b1 );
    ASSERT_TRUE( a1 <= a2 );
    ASSERT_FALSE( b1 <= a1 );
    ASSERT_TRUE( b1 <= b2 );
}

TEST( Integer_Tests, lessThanEqual_operator_2 ) { //<=
    auto a1 = eadlib::Integer<32>( int32_t( -25 ) );
    auto a2 = eadlib::Integer<32>( int32_t( -25 ) );
    auto b1 = eadlib::Integer<32>( int32_t( -61 ) );
    auto b2 = eadlib::Integer<32>( int32_t( -61 ) );
    ASSERT_TRUE( b1 <= a1 );
    ASSERT_TRUE( b1 <= b2 );
    ASSERT_FALSE( a1 <= b1 );
    ASSERT_TRUE( a1 <= a2 );
}

TEST( Integer_Tests, moreThanEqual_operator_0 ) { //>=
    auto a1 = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto a2 = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto b1 = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto b2 = eadlib::Integer<32>( int32_t( 2147483647 ) );
    ASSERT_TRUE( b1 >= a1 );
    ASSERT_TRUE( b1 >= b2 );
    ASSERT_FALSE( a1 >= b1 );
    ASSERT_TRUE( a1 >= a2 );
}

TEST( Integer_Tests, moreThanEqual_operator_1 ) { //>=
    auto a1 = eadlib::Integer<32>( int32_t( 254 ) );
    auto a2 = eadlib::Integer<32>( int32_t( 254 ) );
    auto b1 = eadlib::Integer<32>( int32_t( 257 ) );
    auto b2 = eadlib::Integer<32>( int32_t( 257 ) );
    ASSERT_TRUE( b1 >= a1 );
    ASSERT_TRUE( b1 >= b2 );
    ASSERT_FALSE( a1 >= b1 );
    ASSERT_TRUE( a1 >= a2 );
}

TEST( Integer_Tests, moreThanEqual_operator_2 ) { //>=
    auto a1 = eadlib::Integer<32>( int32_t( -254 ) );
    auto a2 = eadlib::Integer<32>( int32_t( -254 ) );
    auto b1 = eadlib::Integer<32>( int32_t( -257 ) );
    auto b2 = eadlib::Integer<32>( int32_t( -257 ) );
    ASSERT_TRUE( a1 >= b1 );
    ASSERT_TRUE( a1 >= a2 );
    ASSERT_FALSE( b1 >= a1 );
    ASSERT_TRUE( b1 >= b2 );
}

TEST( Integer_Tests, addition_operator_0 ) { //zero
    auto a = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( eadlib::Integer<32>( 0 ), a + a );
}

TEST( Integer_Tests, addition_operator_1 ) { //min positive
    auto a = eadlib::Integer<32>( int32_t( 0 ) );
    auto b = eadlib::Integer<32>( int32_t( 1 ) );
    ASSERT_EQ( eadlib::Integer<32>( 1 ), a + b );
    ASSERT_EQ( eadlib::Integer<32>( 2 ), b + b );
}

TEST( Integer_Tests, addition_operator_2 ) { // min negative
    auto a         = eadlib::Integer<32>( int32_t( 0 ) );
    auto b         = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected1 = eadlib::Integer<32>( -1 );
    auto expected2 = eadlib::Integer<32>( -2 );
    ASSERT_EQ( expected1, a + b );
    ASSERT_EQ( expected2, b + b );
}

TEST( Integer_Tests, addition_operator_3 ) { //min mixed
    auto a        = eadlib::Integer<32>( int32_t( 1 ) );
    auto b        = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_4 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected = eadlib::Integer<32>( 20603 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_5 ) {
    auto a        = eadlib::Integer<32>( int32_t( -8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -12345 ) );
    auto expected = eadlib::Integer<32>( -20603 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_6 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -8258 ) );
    auto expected = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_7 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -12345 ) );
    auto expected = eadlib::Integer<32>( -4087 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_8 ) {
    auto a = eadlib::Integer<32>( int32_t( -8258 ) );
    auto b = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected = eadlib::Integer<32>( 4087 );
    ASSERT_EQ( expected, a + b );
}

TEST( Integer_Tests, addition_operator_9 ) {
    auto a  = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b  = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected  = eadlib::Integer<32>( 28861 );
    ASSERT_EQ( expected, a + a + b );
}

TEST( Integer_Tests, addition_operator_fail0 ) { //underflow
    auto a   = eadlib::Integer<32>( int32_t( -1 ) );
    auto min = eadlib::Integer<32>::min();
    ASSERT_THROW( min + min, std::underflow_error );
    ASSERT_THROW( a + min, std::underflow_error );
}

TEST( Integer_Tests, addition_operator_fail1 ) { //overflow
    auto a   = eadlib::Integer<32>( int32_t( 1 ) );
    auto max = eadlib::Integer<32>::max();
    ASSERT_THROW( max + max, std::overflow_error );
    ASSERT_THROW( a + max, std::overflow_error );
}

TEST( Integer_Tests, subtraction_operator_0 ) {
    auto a = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( eadlib::Integer<32>( 0 ), a + a );
}

TEST( Integer_Tests, subtraction_operator_1 ) {
    auto a         = eadlib::Integer<32>( int32_t( 0 ) );
    auto b         = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected1 = eadlib::Integer<32>( 1 );
    auto expected2 = eadlib::Integer<32>( -1 );
    auto expected3 = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected1, b - a );
    ASSERT_EQ( expected2, a - b );
    ASSERT_EQ( expected3, b - b );
}

TEST( Integer_Tests, subtraction_operator_2 ) {
    auto a         = eadlib::Integer<32>( int32_t( 0 ) );
    auto b         = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected1 = eadlib::Integer<32>( 1 );
    auto expected2 = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected1, a - b );
    ASSERT_EQ( expected2, b - b );
}

TEST( Integer_Tests, subtraction_operator_3 ) {
    auto a        = eadlib::Integer<32>( int32_t( 1 ) );
    auto b        = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = eadlib::Integer<32>( 2 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_4 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected = eadlib::Integer<32>( -4087 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_5 ) {
    auto a        = eadlib::Integer<32>( int32_t( -8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -12345 ) );
    auto expected = eadlib::Integer<32>( 4087 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_6 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -8258 ) );
    auto expected = eadlib::Integer<32>( 16516 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_7 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( -12345 ) );
    auto expected =  eadlib::Integer<32>( 20603 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_8 ) {
    auto a        = eadlib::Integer<32>( int32_t( -8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected = eadlib::Integer<32>( -20603 );
    ASSERT_EQ( expected, a - b );
}

TEST( Integer_Tests, subtraction_operator_9 ) { //min-min (i.e.: (-n)-(-n) or (-n)+n edge case
    auto min      = eadlib::Integer<32>::min();
    auto expected = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected, min - min );
}

TEST( Integer_Tests, subtraction_operator_10 ) {
    auto max      = eadlib::Integer<32>::max();
    auto expected = eadlib::Integer<32>( 0 );
    ASSERT_EQ( expected, max - max );
}

TEST( Integer_Tests, subtraction_operator_11 ) {
    auto a        = eadlib::Integer<32>( int32_t( 8258 ) );
    auto b        = eadlib::Integer<32>( int32_t( 12345 ) );
    auto expected = eadlib::Integer<32>( -4171 );
    ASSERT_EQ( expected, b - a - a );
}

TEST( Integer_Tests, subtraction_operator_fail0 ) { //underflow
    auto a   = eadlib::Integer<32>( int32_t( 1 ) );
    auto min = eadlib::Integer<32>::min();
    auto max = eadlib::Integer<32>::max();
    ASSERT_THROW( min - a, std::underflow_error );
    ASSERT_THROW( min - max, std::underflow_error );
}

TEST( Integer_Tests, subtraction_operator_fail1 ) { //overflow
    auto a = eadlib::Integer<32>( int32_t( -1 ) );
    auto min = eadlib::Integer<32>::min();
    auto max = eadlib::Integer<32>::max();
    ASSERT_THROW( max - a, std::overflow_error );
    ASSERT_THROW( max - min, std::overflow_error );
}

TEST( Integer_Tests, subtraction_operator_fail2 ) { //overflow
    auto a = eadlib::Integer<32>( int32_t( -1 ) );
    auto min = eadlib::Integer<32>::min();
    auto max = eadlib::Integer<32>::max();
    ASSERT_THROW( max - a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_0 ) {
    auto a        = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( expected, a * a );
}

TEST( Integer_Tests, multiplication_operator_1 ) {
    auto a        = eadlib::Integer<32>( int32_t( 0 ) );
    auto b        = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_2 ) {
    auto a        = eadlib::Integer<32>( int32_t( 1 ) );
    auto b        = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = eadlib::Integer<32>( int32_t( 1 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_3 ) {
    auto a        = eadlib::Integer<8>( char( 5 ) );
    auto b        = eadlib::Integer<8>( char( 3 ) );
    auto expected = eadlib::Integer<8>( char( 15 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_4 ) {
    auto a        = eadlib::Integer<8>( char( 5 ) );
    auto b        = eadlib::Integer<8>( char( -3 ) );
    auto expected = eadlib::Integer<8>( char( -15 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_5 ) {
    auto a        = eadlib::Integer<8>( char( 59 ) );
    auto b        = eadlib::Integer<8>( char( 2 ) );
    auto expected = eadlib::Integer<8>( char( 118 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_6 ) {
    auto a        = eadlib::Integer<32>( int32_t( 666 ) );
    auto b        = eadlib::Integer<32>( int32_t( 1235 ) );
    auto expected = eadlib::Integer<32>( int32_t( 822510 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_7 ) {
    auto a        = eadlib::Integer<32>( int32_t( 666 ) );
    auto b        = eadlib::Integer<32>( int32_t( -1235 ) );
    auto expected = eadlib::Integer<32>( int32_t( -822510 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_8 ) {
    auto a        = eadlib::Integer<32>( int32_t( -666 ) );
    auto b        = eadlib::Integer<32>( int32_t( -1235 ) );
    auto expected = eadlib::Integer<32>( int32_t( 822510 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_9 ) {
    auto a        = eadlib::Integer<32>::min();
    auto b        = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = eadlib::Integer<32>::min();
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_10 ) {
    auto a        = eadlib::Integer<32>::min();
    auto b        = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = eadlib::Integer<32>( int32_t( 0 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_11 ) {
    auto a        = eadlib::Integer<32>::max();
    auto b        = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = eadlib::Integer<32>( int32_t( -2147483647 ) );
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_12 ) {
    auto a        = eadlib::Integer<32>( int32_t( -8 ) );
    auto b        = eadlib::Integer<32>( int32_t( 268435456 ) );
    auto expected = eadlib::Integer<32>::min();
    ASSERT_EQ( expected, a * b );
    ASSERT_EQ( expected, b * a );
}

TEST( Integer_Tests, multiplication_operator_13 ) {
    auto a        = eadlib::Integer<32>( char( 5 ) );
    auto b        = eadlib::Integer<32>( char( -3 ) );
    auto chained  = eadlib::Integer<32>( -75 );
    ASSERT_EQ( chained, a * b * a );
}

TEST( Integer_Tests, multiplication_operator_fail1 ) {
    auto a        = eadlib::Integer<32>::min();
    auto b        = eadlib::Integer<32>( int32_t( -1 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail2 ) {
    auto a        = eadlib::Integer<32>::min();
    auto b        = eadlib::Integer<32>( int32_t( -2 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail3 ) {
    auto a        = eadlib::Integer<32>::max();
    auto b        = eadlib::Integer<32>( int32_t( 2 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail4 ) {
    auto a        = eadlib::Integer<32>::max();
    auto b        = eadlib::Integer<32>( int32_t( -2 ) );
    ASSERT_THROW( a * b, std::underflow_error );
    ASSERT_THROW( b * a, std::underflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail5 ) {
    auto a        = eadlib::Integer<8>( char( 63 ) );
    auto b        = eadlib::Integer<8>( char( -3 ) );
    auto expected = eadlib::Integer<8>( char( -189 ) );
    ASSERT_THROW( a * b, std::underflow_error );
    ASSERT_THROW( b * a, std::underflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail6 ) {
    auto a        = eadlib::Integer<8>( char( 12 ) );
    auto b        = eadlib::Integer<8>( char( -18 ) );
    auto expected = eadlib::Integer<8>( char( -216 ) );
    ASSERT_THROW( a * b, std::underflow_error );
    ASSERT_THROW( b * a, std::underflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail7 ) {
    auto a        = eadlib::Integer<8>( char( 12 ) );
    auto b        = eadlib::Integer<8>( char( 18 ) );
    auto expected = eadlib::Integer<8>( char( 216 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail8 ) {
    auto a        = eadlib::Integer<8>( char( 40 ) );
    auto b        = eadlib::Integer<8>( char( 28 ) );
    auto expected = eadlib::Integer<8>( char( 1120 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, multiplication_operator_fail9 ) {
    auto a        = eadlib::Integer<8>( char( 59 ) );
    auto b        = eadlib::Integer<8>( char( 6 ) );
    auto expected = eadlib::Integer<8>( char( 354 ) );
    ASSERT_THROW( a * b, std::overflow_error );
    ASSERT_THROW( b * a, std::overflow_error );
}

TEST( Integer_Tests, division_operator0 ) {
    auto a = eadlib::Integer<8>( char( 100 ) );
    auto b = eadlib::Integer<8>( char( 7 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 14 ) ), a / b );
    ASSERT_EQ( eadlib::Integer<8>( char( 0 ) ), b / a );
}

TEST( Integer_Tests, division_operator1 ) {
    auto a = eadlib::Integer<8>( char( 20 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 5 ) ), a / b );
    ASSERT_EQ( eadlib::Integer<8>( char( 0 ) ), b / a );
}

TEST( Integer_Tests, division_operator2 ) {
    auto a = eadlib::Integer<8>( char( 0 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 0 ) ), a / b );
}

TEST( Integer_Tests, division_operator3 ) {
    auto a = eadlib::Integer<8>( char( 25 ) );
    auto b = eadlib::Integer<8>( char( -4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( -6 ) ), a / b );
}

TEST( Integer_Tests, division_operator4 ) {
    auto a = eadlib::Integer<8>( char( -25 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( -6 ) ), a / b );
}

TEST( Integer_Tests, division_operator5 ) {
    auto a = eadlib::Integer<256>( "5688845545668520212546588241345511255667" );
    auto b = eadlib::Integer<256>( "2" );
    ASSERT_EQ( eadlib::Integer<256>( "2844422772834260106273294120672755627833" ), a / b );
}

TEST( Integer_Tests, division_operator6 )  {
    auto a = eadlib::Integer<8>( char( 127 ) );
    auto b = eadlib::Integer<8>( char( 2 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 63 ) ), a / b );
}

TEST( Integer_Tests, division_operator7 ) {
    auto a = eadlib::Integer<8>( char( 120 ) );
    auto b = eadlib::Integer<8>( true );
    ASSERT_EQ( eadlib::Integer<8>( char( 120 ) ), a / b );
}

TEST( Integer_Tests, division_operator8 ) { //165 digits / 2 digits
    auto a = eadlib::Integer<600>( "453231884221445646547132121138523771544832148951111531314454987412315868812313211833584211232584498869511321654834531123168358451158456448822312115635574335451341353" );
    auto b = eadlib::Integer<600>( "30" );
    ASSERT_EQ( eadlib::Integer<600>( "15107729474048188218237737371284125718161071631703717710481832913743862293743773727786140374419483295650377388494484370772278615038615214960743737187852477848378045" ), a / b );
}

TEST( Integer_Tests, division_operator_fail0 ) {
    auto a = eadlib::Integer<8>( char( 20 ) );
    auto b = eadlib::Integer<8>( char( 0 ) );
    ASSERT_THROW( a / b, std::runtime_error );
}

TEST( Integer_Tests, modulo_operator0 ) {
    auto a = eadlib::Integer<8>( char( 100 ) );
    auto b = eadlib::Integer<8>( char( 7 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 2 ) ), a % b );
    ASSERT_EQ( eadlib::Integer<8>( char( 7 ) ), b % a );
}

TEST( Integer_Tests, modulo_operator1 ) {
    auto a = eadlib::Integer<8>( char( 20 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 0 ) ), a % b );
    ASSERT_EQ( eadlib::Integer<8>( char( 4 ) ), b % a );
}

TEST( Integer_Tests, modulo_operator2 ) {
    auto a = eadlib::Integer<8>( char( 0 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 0 ) ), a % b );
}

TEST( Integer_Tests, modulo_operator3 ) {
    auto a = eadlib::Integer<8>( char( 25 ) );
    auto b = eadlib::Integer<8>( char( -4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( 1 ) ), a % b );
}

TEST( Integer_Tests, modulo_operator4 ) {
    auto a = eadlib::Integer<8>( char( -25 ) );
    auto b = eadlib::Integer<8>( char( 4 ) );
    ASSERT_EQ( eadlib::Integer<8>( char( -1 ) ), a % b );
}

TEST( Integer_Tests, modulo_operator_fail0 ) {
    auto a = eadlib::Integer<8>( char( 20 ) );
    auto b = eadlib::Integer<8>( char( 0 ) );
    ASSERT_THROW( a % b, std::runtime_error );
}

TEST( Integer_Tests, bitwiseNOT_operator_1 ) {
    auto a = eadlib::Integer<16>();                //0000 0000 0000 0000
    auto b = eadlib::Integer<16>( int16_t( -1 ) ); //1111 1111 1111 1111
    ASSERT_EQ( a, ~b );
    ASSERT_EQ( b, ~a );
}

TEST( Integer_Tests, bitwiseNOT_operator_2 ) {
    auto a = eadlib::Integer<16>( int16_t( -21846 ) ); //1010 1010 1010 1010
    auto b = eadlib::Integer<16>( int16_t( 21845) );   //0101 0101 0101 0101
    ASSERT_EQ( a, ~b );
    ASSERT_EQ( b, ~a );
}

TEST( Integer_Tests, bitwiseAND_operator_1 ) {
    auto a        = eadlib::Integer<16>( int16_t( -21846 ) );//1010 1010 1010 1010
    auto b        = eadlib::Integer<16>( int16_t( 21845) );  //0101 0101 0101 0101
    auto expected = eadlib::Integer<16>( int16_t( 0 ) );     //0000 0000 0000 0000
    ASSERT_EQ( expected, a & b );
    ASSERT_EQ( expected, b & a );
}

TEST( Integer_Tests, bitwiseAND_operator_2 ) {
    auto a = eadlib::Integer<16>( int16_t( -256 ) );     //1111 1111 0000 0000
    auto b = eadlib::Integer<16>( int16_t( 255 ) );      //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( 0 ) ); //0000 0000 0000 0000
    ASSERT_EQ( expected, a & b );
    ASSERT_EQ( expected, b & a );
}

TEST( Integer_Tests, bitwiseAND_operator_3 ) {
    auto a        = eadlib::Integer<16>( int16_t( -1 ) );  //1111 1111 1111 1111
    auto b        = eadlib::Integer<16>( int16_t( 255) );  //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( 255 ) ); //0000 0000 1111 1111
    ASSERT_EQ( expected, a & b );
    ASSERT_EQ( expected, b & a );
}

TEST( Integer_Tests, bitwiseOR_operator_1 ) {
    auto a        = eadlib::Integer<16>( int16_t( -21846 ) ); //1010 1010 1010 1010
    auto b        = eadlib::Integer<16>( int16_t( 21845) );   //0101 0101 0101 0101
    auto expected = eadlib::Integer<16>( int16_t( -1 ) );     //1111 1111 1111 1111
    ASSERT_EQ( expected, a | b );
    ASSERT_EQ( expected, b | a );
}

TEST( Integer_Tests, bitwiseOR_operator_2 ) {
    auto a        = eadlib::Integer<16>( int16_t( -256 ) ); //1111 1111 0000 0000
    auto b        = eadlib::Integer<16>( int16_t( 255 ) );  //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( -1 ) );   //1111 1111 1111 1111
    ASSERT_EQ( expected, a | b );
    ASSERT_EQ( expected, b | a );
}

TEST( Integer_Tests, bitwiseOR_operator_3 ) {
    auto a        = eadlib::Integer<16>( int16_t( -3841 ) ); //1111 0000 1111 1111
    auto b        = eadlib::Integer<16>( int16_t( 255) );    //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( -3841 ) ); //1111 0000 1111 1111
    ASSERT_EQ( expected, a | b );
    ASSERT_EQ( expected, b | a );
}

TEST( Integer_Tests, bitwiseXOR_operator_1 ) {
    auto a        = eadlib::Integer<16>( int16_t( -21846 ) ); //1010 1010 1010 1010
    auto b        = eadlib::Integer<16>( int16_t( 21845) );   //0101 0101 0101 0101
    auto expected = eadlib::Integer<16>( int16_t( -1 ) );     //1111 1111 1111 1111
    ASSERT_EQ( expected, a ^ b );
    ASSERT_EQ( expected, b ^ a );
}

TEST( Integer_Tests, bitwiseXOR_operator_2 ) {
    auto a        = eadlib::Integer<16>( int16_t( -256 ) ); //1111 1111 0000 0000
    auto b        = eadlib::Integer<16>( int16_t( 255 ) );  //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( -1 ) );   //1111 1111 1111 1111
    ASSERT_EQ( expected, a ^ b );
    ASSERT_EQ( expected, b ^ a );
}

TEST( Integer_Tests, bitwiseXOR_operator_3 ) {
    auto a        = eadlib::Integer<16>( int16_t( -1 ) );   //1111 1111 1111 1111
    auto b        = eadlib::Integer<16>( int16_t( 255) );   //0000 0000 1111 1111
    auto expected = eadlib::Integer<16>( int16_t( -256 ) ); //1111 1111 0000 0000
    ASSERT_EQ( expected, a ^ b );
    ASSERT_EQ( expected, b ^ a );
}

TEST( Integer_Tests, bitshift_left_operator_1 ) {
    auto bitset   = std::bitset<8>( 0b00110010 );
    auto integer  = eadlib::Integer<8>( bitset );
    auto returned = integer << 1;
    bitset  <<= 1;
    ASSERT_EQ( bitset, returned.bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00110010 ), integer.bitset() ); //check original is intact
}

TEST( Integer_Tests, bitshift_right_operator_1 ) {
    auto bitset   = std::bitset<8>( 0b10110010 );
    auto integer  = eadlib::Integer<8>( bitset );
    auto returned = integer >> 1;
    bitset  >>= 1;
    ASSERT_EQ( bitset, returned.bitset() );
    ASSERT_EQ( std::bitset<8>( 0b10110010 ), integer.bitset() ); //check original is intact
}

TEST( Integer_Tests, addition_assigment_operator_0 ) { //No need to test more as uses operator+( Integer ) in implementation
    auto integer  = eadlib::Integer<32>( int32_t( 10588 ) );
    auto expected = eadlib::Integer<32>( int32_t( 62727 ) );
    integer += eadlib::Integer<32>( int32_t( 52139 ) );
    ASSERT_EQ( expected, integer );
}

TEST( Integer_Tests, addition_assigment_operator_fail0 ) {
    auto integer  = eadlib::Integer<32>::max();
    auto val      = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = integer;
    ASSERT_THROW( integer += val, std::overflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, addition_assigment_operator_fail1 ) {
    auto integer  = eadlib::Integer<32>::min();
    auto val      = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = integer;
    ASSERT_THROW( integer += val, std::underflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, subtraction_assigment_operator_0 ) { //No need to test more as uses operator-( Integer ) in implementation
    auto integer  = eadlib::Integer<32>( int32_t( 10588 ) );
    auto expected = eadlib::Integer<32>( int32_t( -41551 ) );
    integer -= eadlib::Integer<32>( int32_t( 52139 ) );
    ASSERT_EQ( expected, integer );
}

TEST( Integer_Tests, subtraction_assigment_operator_fail0 ) {
    auto integer  = eadlib::Integer<32>::min();
    auto val      = eadlib::Integer<32>( int32_t( 1 ) );
    auto expected = integer;
    ASSERT_THROW( integer -= val, std::underflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, subtraction_assigment_operator_fail1 ) {
    auto integer  = eadlib::Integer<32>::max();
    auto val      = eadlib::Integer<32>( int32_t( -1 ) );
    auto expected = integer;
    ASSERT_THROW( integer -= val, std::overflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, multiplication_assigment_operator_0 ) { //No need to test more as uses operator*( Integer ) in implementation
    auto integer  = eadlib::Integer<32>( int32_t( 10588 ) );
    auto expected = eadlib::Integer<32>( int32_t( -439941988 ) );
    integer *= eadlib::Integer<32>( int32_t( -41551 ) );
    ASSERT_EQ( expected, integer );
}

TEST( Integer_Tests, multiplication_assigment_operator_fail0 ) {
    auto integer  = eadlib::Integer<32>::min();
    auto val      = eadlib::Integer<32>( int32_t( 2 ) );
    auto expected = integer;
    ASSERT_THROW( integer *= val, std::underflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, multiplication_assigment_operator_fail1 ) {
    auto integer  = eadlib::Integer<32>::max();
    auto val      = eadlib::Integer<32>( int32_t( 2 ) );
    auto expected = integer;
    ASSERT_THROW( integer *= val, std::overflow_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, division_assignment_operator0 ) { //No need to test more as uses operator/( Integer ) in implementation
    auto integer  = eadlib::Integer<32>( int32_t( 25585 ) );
    auto expected = eadlib::Integer<32>( int32_t( -46 ) );
    integer /= eadlib::Integer<32>( int32_t( -548 ) );
    ASSERT_EQ( expected, integer );
}

TEST( Integer_Tests, division_assignment_operator_fail0 ) {
    auto integer  = eadlib::Integer<32>::max();
    auto val      = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = integer;
    ASSERT_THROW( integer /= val, std::runtime_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, modulo_assignment_operator0 ) { //No need to test more as uses operator%( Integer ) in implementation
    auto integer  = eadlib::Integer<32>( int32_t( 25585 ) );
    auto expected = eadlib::Integer<32>( int32_t( 377 ) );
    integer %= eadlib::Integer<32>( int32_t( -548 ) );
    ASSERT_EQ( expected, integer );
}

TEST( Integer_Tests, modulo_assignment_operator_fail0 ) {
    auto integer  = eadlib::Integer<32>::max();
    auto val      = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = integer;
    ASSERT_THROW( integer %= val, std::runtime_error );
    ASSERT_EQ( expected, integer ); //un-damaged
}

TEST( Integer_Tests, bitwiseAND_assignment_operator_1 ) { //No need to test more as uses operator&( Integer ) in implementation
    auto a        = eadlib::Integer<16>( int16_t( -24336 ) ); //1010 0000 1111 0000
    auto b        = eadlib::Integer<16>( int16_t( -20736 ) ); //1010 1111 0000 0000
    auto expected = eadlib::Integer<16>( int16_t( -24576 ) ); //1010 0000 0000 0000
    ASSERT_EQ( expected, a & b );
    ASSERT_EQ( expected, b & a );
}

TEST( Integer_Tests, bitwiseOR_assignment_operator_1 ) { //No need to test more as uses operator|( Integer ) in implementation
    auto a        = eadlib::Integer<16>( int16_t( -24336 ) ); //1010 0000 1111 0000
    auto b        = eadlib::Integer<16>( int16_t( -20736 ) ); //1010 1111 0000 0000
    auto expected = eadlib::Integer<16>( int16_t( -20496 ) ); //1010 1111 1111 0000
    ASSERT_EQ( expected, a | b );
    ASSERT_EQ( expected, b | a );
}

TEST( Integer_Tests, bitwiseXOR_assignment_operator_1 ) { //No need to test more as uses operator^( Integer ) in implementation
    auto a        = eadlib::Integer<16>( int16_t( -24336 ) ); //1010 0000 1111 0000
    auto b        = eadlib::Integer<16>( int16_t( -20736 ) ); //1010 1111 0000 0000
    auto expected = eadlib::Integer<16>( int16_t( 4080 ) );   //0000 1111 1111 0000
    ASSERT_EQ( expected, a ^ b );
    ASSERT_EQ( expected, b ^ a );
}

TEST( Integer_Tests, bitshift_left_assignment_operator_1 ) {
    auto bitset  = std::bitset<8>( 0b00110010 );
    auto integer = eadlib::Integer<8>( bitset );
    integer <<= 1;
    bitset  <<= 1;
    ASSERT_EQ( bitset, integer.bitset() );
}

TEST( Integer_Tests, bitshift_right_assignment_operator_1 ) {
    auto bitset  = std::bitset<8>( 0b10110010 );
    auto integer = eadlib::Integer<8>( bitset );
    integer >>= 1;
    bitset  >>= 1;
    ASSERT_EQ( bitset, integer.bitset() );
}

TEST( Integer_Tests, pre_increment_operator_0 ) {
    auto integer = eadlib::Integer<32>( int32_t( -100 ) );
    for( int32_t i = -99; i <= 100; i++ ) {
        ++integer;
        auto expected = eadlib::Integer<32>( i );
        ASSERT_EQ( expected, integer );
    }
}

TEST( Integer_Tests, pre_increment_operator_fail0 ) {
    auto integer = eadlib::Integer<32>::max();
    ASSERT_THROW( ++integer, std::overflow_error );
    ASSERT_EQ( eadlib::Integer<32>::min(), integer );
}

TEST( Integer_Tests, post_increment_operator_0 ) {
    auto integer = eadlib::Integer<32>( int32_t( -100 ) );
    for( int32_t i = -99; i <= 100; i++ ) {
        auto returned  = integer++;
        auto expected1 = eadlib::Integer<32>( i - 1 );
        auto expected2 = eadlib::Integer<32>( i );
        ASSERT_EQ( expected1, returned );
        ASSERT_EQ( expected2, integer );
    }
}

TEST( Integer_Tests, post_increment_operator_fail0 ) {
    auto integer = eadlib::Integer<32>::max();
    ASSERT_THROW( integer++, std::overflow_error );
    ASSERT_EQ( eadlib::Integer<32>::min(), integer );
}

TEST( Integer_Tests, pre_decrement_operator_0 ) {
    auto integer = eadlib::Integer<32>( int32_t( 100 ) );
    for( int32_t i = 99; i >= -100; i-- ) {
        --integer;
        auto expected = eadlib::Integer<32>( i );
        ASSERT_EQ( expected, integer );
    }
}

TEST( Integer_Tests, pre_decrement_operator_fail0 ) {
    auto integer = eadlib::Integer<32>::min();
    ASSERT_THROW( --integer, std::underflow_error );
    ASSERT_EQ( eadlib::Integer<32>::max(), integer );
}

TEST( Integer_Tests, post_decrement_operator_0 ) {
    auto integer = eadlib::Integer<32>( int32_t( 100 ) );
    for( int32_t i = 99; i >= -100; i-- ) {
        auto returned  = integer--;
        auto expected1 = eadlib::Integer<32>( i + 1 );
        auto expected2 = eadlib::Integer<32>( i );
        ASSERT_EQ( expected1, returned );
        ASSERT_EQ( expected2, integer );
    }
}

TEST( Integer_Tests, post_decrement_operator_fail0 ) {
    auto integer = eadlib::Integer<32>::min();
    ASSERT_THROW( integer--, std::underflow_error );
    ASSERT_EQ( eadlib::Integer<32>::max(), integer );
}

//Lite testing as both division and modulo are already tested separately with their operators
TEST( Integer_Tests, divisionModulo0 ) {
    auto a = eadlib::Integer<32>( int32_t( 666 ) );
    auto b = eadlib::Integer<32>( int32_t( 25 ) );
    auto result1 = a.divide( b );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 26 ) ), result1.first );  //quotient
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 16 ) ), result1.second ); //remainder
    auto result2 = b.divide( a );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 0 ) ), result2.first );  //quotient
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 25 ) ), result2.second ); //remainder
}

TEST( Integer_Tests, divisionModulo_fail0 ) {
    auto a = eadlib::Integer<32>( int32_t( 500 ) );
    auto b = eadlib::Integer<32>();
    ASSERT_THROW( a / b, std::runtime_error );
}

TEST( Integer_Tests, pow_Integer0 ) {
    auto integer = eadlib::Integer<32>(); //0
    auto result = integer.pow( eadlib::Integer<32>( int32_t( 8 ) ) ); //0^8
    ASSERT_EQ( eadlib::Integer<32>(), result );
}

TEST( Integer_Tests, pow_Integer1 ) {
    auto integer = eadlib::Integer<32>( int32_t( 1 ) );
    auto result = integer.pow( eadlib::Integer<32>( int32_t( 8 ) ) ); //1^8
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 1 ) ), result );
}

TEST( Integer_Tests, pow_Integer2 ) { //Just about fits into Size
    auto integer = eadlib::Integer<10>( "2" );
    auto result  = integer.pow( eadlib::Integer<10>( "8" ) ); //2^8
    ASSERT_EQ( eadlib::Integer<10>( "256" ), result );
}

TEST( Integer_Tests, pow_Integer3 ) {
    auto integer = eadlib::Integer<32>( int32_t( 7 ) );
    auto result  = integer.pow( eadlib::Integer<32>( int32_t( 11 ) ) ); //7^11
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 1977326743 ) ), result );
}

TEST( Integer_Tests, pow_Integer4 ) {
    auto integer = eadlib::Integer<32>( int32_t( -7 ) );
    auto result  = integer.pow( eadlib::Integer<32>( int32_t( 11 ) ) ); //-7^11
    ASSERT_EQ( eadlib::Integer<32>( int32_t( -1977326743 ) ), result );
}

TEST( Integer_Tests, pow_Integer5 ) {
    auto integer = eadlib::Integer<1280>( int32_t( 3 ) );
    auto result  = integer.pow( eadlib::Integer<1280>( int32_t( 678 ) ) ); //3^678
    ASSERT_EQ( eadlib::Integer<1280>( "307758955909249394517870183098026963674896177301252679948607788113207931343058028252222084540857300163606062102834509948268007008296183137468284611330109667185661485171036357085278783175400218567695450166784587392063318756286320799640300377688739287614570603793732040753475860174362284830169767693747741599975883489952403289" ), result );
}

TEST( Integer_Tests, pow_Integer_fail0 ) { //underflow
    auto integer = eadlib::Integer<32>( int32_t( -2 ) );
    ASSERT_THROW( integer.pow( eadlib::Integer<32>( int32_t( 32 ) ) ), std::underflow_error );
}

TEST( Integer_Tests, pow_Integer_fail1 ) { //underflow
    auto integer = eadlib::Integer<32>( int32_t( -3 ) );
    ASSERT_THROW( integer.pow( eadlib::Integer<32>( int32_t( 20 ) ) ), std::underflow_error );
}

TEST( Integer_Tests, pow_Integer_fail3 ) { //overflow
    auto integer = eadlib::Integer<32>( int32_t( 2 ) );
    ASSERT_THROW( integer.pow( eadlib::Integer<32>( int32_t( 31 ) ) ), std::overflow_error );
}

TEST( Integer_Tests, pow_int0 ) {
    auto integer = eadlib::Integer<32>(); //0
    auto result  = integer.pow( 8 ); //0^8
    ASSERT_EQ( eadlib::Integer<32>(), result );
}

TEST( Integer_Tests, pow_int1 ) {
    auto integer = eadlib::Integer<32>( int32_t( 1 ) );
    auto result  = integer.pow( 8 ); //1^8
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 1 ) ), result );
}

TEST( Integer_Tests, pow_int2 ) { //Just about fits into Size
    auto integer = eadlib::Integer<10>( "2" );
    auto result  = integer.pow( 8 );
    ASSERT_EQ( eadlib::Integer<10>( "256" ), result );
}

TEST( Integer_Tests, pow_int3 ) {
    auto integer = eadlib::Integer<32>( int32_t( 7 ) );
    auto result  = integer.pow( 11 ); //7^11
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 1977326743 ) ), result );
}

TEST( Integer_Tests, pow_int4 ) {
    auto integer = eadlib::Integer<32>( int32_t( -7 ) );
    auto result  = integer.pow( 11 ); //-7^11
    ASSERT_EQ( eadlib::Integer<32>( int32_t( -1977326743 ) ), result );
}

TEST( Integer_Tests, pow_int_fail0 ) { //underflow
    auto integer = eadlib::Integer<32>( int32_t( -2 ) );
    ASSERT_THROW( integer.pow( 32 ), std::underflow_error );
}

TEST( Integer_Tests, pow_int_fail1 ) { //underflow
    auto integer = eadlib::Integer<32>( int32_t( -3 ) );
    ASSERT_THROW( integer.pow( 20 ), std::underflow_error );
}

TEST( Integer_Tests, pow_int_fail2 ) { //overflow
    auto integer = eadlib::Integer<32>( int32_t( 2 ) );
    ASSERT_THROW( integer.pow( 31 ), std::overflow_error );
}

TEST( Integer_Tests, modpow0 ) {
    auto base = eadlib::Integer<32>( int32_t( 2003 ) );
    auto exp  = eadlib::Integer<32>( int32_t( 17 ) );
    auto mod  = eadlib::Integer<32>( int32_t( 3713 ) );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 232 ) ), base.modpow( exp, mod ) );
}

TEST( Integer_Tests, modpow1 ) {
    auto base = eadlib::Integer<32>::max();
    auto exp  = eadlib::Integer<32>::max();
    auto mod  = eadlib::Integer<32>( int32_t( 5 ) );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 3 ) ), base.modpow( exp, mod ) );
}

TEST( Integer_Tests, modpow_fail1 ) {
    auto base = eadlib::Integer<32>::max();
    auto exp  = eadlib::Integer<32>::max();
    auto mod  = eadlib::Integer<32>( int32_t( 1073741823 ) );
    ASSERT_THROW( base.modpow( exp, mod ), std::overflow_error );
}

TEST( Integer_Tests, sqrt0 ) {
    auto integer = eadlib::Integer<32>();
    ASSERT_EQ( eadlib::Integer<32>(), integer.sqrt() );
}

TEST( Integer_Tests, sqrt1 ) {
    auto integer = eadlib::Integer<32>( int32_t( 1 ) );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 1 ) ), integer.sqrt() );
}

TEST( Integer_Tests, sqrt2 ) {
    auto integer = eadlib::Integer<32>( int32_t( 25 ) );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 5 ) ), integer.sqrt() );
}

TEST( Integer_Tests, sqrt3 ) {
    auto integer = eadlib::Integer<32>( int32_t( -25 ) );
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 5 ) ), integer.sqrt() );
}

TEST( Integer_Tests, sqrt4 ) {
    auto integer = eadlib::Integer<32>( std::numeric_limits<int32_t>::max() ); //2147483647
    ASSERT_EQ( eadlib::Integer<32>( int32_t( 46340 ) ), integer.sqrt() );
}

TEST( Integer_Tests, sqrt5 ) {
    auto integer = eadlib::Integer<256>( "5688845545668520212546588241345511255666" );
    ASSERT_EQ( eadlib::Integer<256>( "75424435998345524337" ), integer.sqrt() );
}

TEST( Integer_Tests, ars_1 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b00000000 ) );
    auto expected = eadlib::Integer<8>(); //0
    for( size_t i = 0; i < 10; ++i ) {
        ASSERT_EQ( expected, integer.ars( i ) );
    }
}

TEST( Integer_Tests, ars_2 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b10000000 ) );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.ars( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11000000 ), integer.ars( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11100000 ), integer.ars( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11110000 ), integer.ars( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111000 ), integer.ars( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111100 ), integer.ars( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111110 ), integer.ars( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111111 ), integer.ars( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111111 ), integer.ars( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.bitset() );
}

TEST( Integer_Tests, ars_3 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b10001100 ) );
    ASSERT_EQ( std::bitset<8>( 0b10001100 ), integer.ars( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11000110 ), integer.ars( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11100011 ), integer.ars( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11110001 ), integer.ars( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111000 ), integer.ars( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111100 ), integer.ars( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111110 ), integer.ars( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111111 ), integer.ars( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b11111111 ), integer.ars( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b10001100 ), integer.bitset() );
}

TEST( Integer_Tests, ars_4 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b01000100 ) );
    ASSERT_EQ( std::bitset<8>( 0b01000100 ), integer.ars( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00100010 ), integer.ars( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00010001 ), integer.ars( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00001000 ), integer.ars( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.ars( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000010 ), integer.ars( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000001 ), integer.ars( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.ars( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.ars( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b01000100 ), integer.bitset() );
}

TEST( Integer_Tests, lrs_1 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b00000000 ) );
    auto expected = eadlib::Integer<8>(); //0
    for( size_t i = 0; i < 10; ++i ) {
        ASSERT_EQ( expected, integer.lrs( i ) );
    }
}

TEST( Integer_Tests, lrs_2 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b10000000 ) );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.lrs( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b01000000 ), integer.lrs( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00100000 ), integer.lrs( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00010000 ), integer.lrs( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00001000 ), integer.lrs( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.lrs( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000010 ), integer.lrs( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000001 ), integer.lrs( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.lrs( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b10000000 ), integer.bitset() );
}

TEST( Integer_Tests, lrs_3 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b10001100 ) );
    ASSERT_EQ( std::bitset<8>( 0b10001100 ), integer.lrs( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b01000110 ), integer.lrs( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00100011 ), integer.lrs( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00010001 ), integer.lrs( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00001000 ), integer.lrs( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.lrs( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000010 ), integer.lrs( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000001 ), integer.lrs( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.lrs( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b10001100 ), integer.bitset() );
}

TEST( Integer_Tests, lrs_4 ) {
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b01000100 ) );
    ASSERT_EQ( std::bitset<8>( 0b01000100 ), integer.lrs( 0 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00100010 ), integer.lrs( 1 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00010001 ), integer.lrs( 2 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00001000 ), integer.lrs( 3 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.lrs( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000010 ), integer.lrs( 5 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000001 ), integer.lrs( 6 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.lrs( 7 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000000 ), integer.lrs( 8 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b01000100 ), integer.bitset() );
}

TEST( Integer_Tests, arsSelf_1 ) { //just checking assignemnt has happened
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b01000100 ) );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.arsSelf( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.bitset() );
}

TEST( Integer_Tests, lrsSelf_1 ) { //just checking assignemnt has happened
    auto integer = eadlib::Integer<8>( std::bitset<8>( 0b01000100 ) );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.lrsSelf( 4 ).bitset() );
    ASSERT_EQ( std::bitset<8>( 0b00000100 ), integer.bitset() );
}

TEST( Integer_Tests, negate_0 ) {
    auto integer  = eadlib::Integer<32>( 0 );
    auto returned = integer.negate();
    ASSERT_EQ( std::bitset<32>( 0b00 ), returned.bitset() );
}

TEST( Integer_Tests, negate_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto returned = integer.negate();
    auto expected = "-2147483647";
    ASSERT_EQ( expected, returned.str_base10() );
}

TEST( Integer_Tests, negate_2 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto returned = integer.negate();
    auto expected = "-2147483648";
    ASSERT_EQ( expected, returned.str_base10() );
}

TEST( Integer_Tests, negate_3 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483647 ) );
    auto returned = integer.negate();
    auto expected = "2147483647";
    ASSERT_EQ( expected, returned.str_base10() );
}

TEST( Integer_Tests, abs_0 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 0 ) );
    auto expected = eadlib::Integer<32>( abs( int32_t( 0 ) ) );
    auto returned = integer.abs();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, abs_1 ) {
    auto integer  = eadlib::Integer<32>( int32_t( 2147483647 ) );
    auto returned = integer.abs();
    ASSERT_EQ( integer, returned );
}

TEST( Integer_Tests, abs_2 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483647 ) );
    auto expected = eadlib::Integer<32>( abs( int32_t( -2147483647 ) ) );
    auto returned = integer.abs();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, abs_3 ) {
    auto integer  = eadlib::Integer<32>( int32_t( -2147483648 ) );
    auto expected = eadlib::Integer<32>( abs( int32_t( -2147483648 ) ) );
    auto returned = integer.abs();
    ASSERT_EQ( expected, returned );
}

TEST( Integer_Tests, isNegative_0 ) {
    auto integer = eadlib::Integer<32>( -1 );
    ASSERT_TRUE( integer.isNegative() );
}

TEST( Integer_Tests, isNegative_1 ) {
    auto integer = eadlib::Integer<32>( 1 );
    ASSERT_FALSE( integer.isNegative() );
}

TEST( Integer_Tests, isNegative_2 ) {
    auto integer = eadlib::Integer<32>( 0 );
    ASSERT_FALSE( integer.isNegative() );
}

TEST( Integer_Tests, isZero_0 ) {
    ASSERT_TRUE( eadlib::Integer<8>().isZero() );
}

TEST( Integer_Tests, isZero_1 ) {
    ASSERT_FALSE( eadlib::Integer<8>( char( 1 ) ).isZero() );
    ASSERT_FALSE( eadlib::Integer<8>( char( -1 ) ).isZero() );
}

TEST( Integer_Tests, bitSize ) {
    auto integer1 = eadlib::Integer<64>( 34589 );
    auto integer2 = eadlib::Integer<64>( 1 );
    auto integer3 = eadlib::Integer<64>( 0 );
    ASSERT_EQ( 64, integer1.bitSize() );
    ASSERT_EQ( 64, integer2.bitSize() );
    ASSERT_EQ( 64, integer3.bitSize() );
}

TEST( Integer_Tests, minReqBits ) {
    ASSERT_EQ( 32, ( eadlib::Integer<32>::min() ).minReqBits() );
    ASSERT_EQ( 17, ( eadlib::Integer<32>( -34589 ) ).minReqBits() );
    ASSERT_EQ( 3,  ( eadlib::Integer<32>( -2 ) ).minReqBits() );
    ASSERT_EQ( 2,  ( eadlib::Integer<32>( -1 ) ).minReqBits() );
    ASSERT_EQ( 2,  ( eadlib::Integer<32>( 0 ) ).minReqBits() );
    ASSERT_EQ( 2,  ( eadlib::Integer<32>( 1 ) ).minReqBits() );
    ASSERT_EQ( 3,  ( eadlib::Integer<32>( 2 ) ).minReqBits() );
    ASSERT_EQ( 17, ( eadlib::Integer<32>( 34589 ) ).minReqBits() );
    ASSERT_EQ( 32, ( eadlib::Integer<32>::max() ).minReqBits() );
}

TEST( Integer_Tests, min_32bits ) {
    ASSERT_EQ( 
        std::bitset<32>( 0b10000000000000000000000000000000 ), 
        eadlib::Integer<32>::min().bitset() 
    );
}

TEST( Integer_Tests, min_64bits ) {
    ASSERT_EQ( 
        std::bitset<64>( 0b1000000000000000000000000000000000000000000000000000000000000000 ), 
        eadlib::Integer<64>::min().bitset() 
    );
}

TEST( Integer_Tests, min_128bits ) {
    auto integer = eadlib::Integer<128>::min();
    ASSERT_TRUE( integer.bitset().test( 127 ) );
    ASSERT_EQ( 1, integer.bitset().count() );
    //0b10000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
}

TEST( Integer_Tests, max_32bits ) {
    ASSERT_EQ( 
        std::bitset<32>( 0b01111111111111111111111111111111 ), 
        eadlib::Integer<32>::max().bitset() 
    );
}

TEST( Integer_Tests, max_64bits ) {
    ASSERT_EQ( 
        std::bitset<64>( 0b0111111111111111111111111111111111111111111111111111111111111111 ), 
        eadlib::Integer<64>::max().bitset() 
    );
}

TEST( Integer_Tests, max_128bits ) {
    auto integer = eadlib::Integer<128>::max();
    ASSERT_FALSE( integer.bitset().test( 127 ) );
    ASSERT_EQ( 127, integer.bitset().count() );
    //0b01111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111
}

TEST( Integer_Tests, numeric_limits_min) {
    ASSERT_EQ( 
        std::bitset<8>( 0b10000000 ), //-128 
        eadlib::Integer<8>::min().bitset() 
    );
}

TEST( Integer_Tests, numeric_limits_max ) {
    ASSERT_EQ( 
        std::bitset<8>( 0b01111111 ), //127 
        eadlib::Integer<8>::max().bitset() 
    );
}

TEST( Integer_Tests, numeric_limits_lowest ) {
        ASSERT_EQ( 
        std::bitset<9>( 0b100000000 ), //-256 
        eadlib::Integer<9>::min().bitset() 
    );
}

TEST( Integer_Tests, numeric_limits_is_integer ) {
    ASSERT_TRUE( std::numeric_limits<eadlib::Integer<32>>::is_integer );
}

TEST( Integer_Tests, numeric_limits_is_signed ) {
    ASSERT_TRUE( std::numeric_limits<eadlib::Integer<32>>::is_signed );
}

TEST( Integer_Tests, numeric_limits_has_infinity ) {
    ASSERT_FALSE( std::numeric_limits<eadlib::Integer<32>>::has_infinity );
}

TEST( Integer_Tests, numeric_limits_has_quiet_NaN ) {
    ASSERT_FALSE( std::numeric_limits<eadlib::Integer<32>>::has_quiet_NaN );
}

#endif //EADLIB_INTEGER_TEST_H
