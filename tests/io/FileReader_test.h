#ifndef EADLIB_FILEREADER_TEST_H
#define EADLIB_FILEREADER_TEST_H

#include "gtest/gtest.h"
#include "../../src/io/FileReader.h"

//TODO battery of tests with a dynamically created and written file

TEST( FileReader_Tests, test ) {
    //TODO
    auto reader = eadlib::io::FileReader( "log_config.cfg" );
    ASSERT_TRUE( reader.open() );
    std::cout << "Size: " << reader.size() << std::endl;
    size_t buffer_size = 20;
    std::cout << "Full buffer reads: " << reader.size() / buffer_size << std::endl;
    std::cout << "Remainder size: " << reader.size() % buffer_size << std::endl;
    std::vector<char> buffer;
    size_t counter { 0 };
    while( !reader.isDone() ) {
        std::cout << "Read: " << reader.read( buffer, buffer_size ) << " chars." << std::endl;
        counter++;
    };

    std::cout << "Read Counter: " << counter << "\nLast buffer: ";
    for( auto e : buffer ) {
        std::cout << e;
    }
    std::cout << std::endl;
    ///
    std::cout << "RESET" << std::endl;
    reader.reset();
    std::cout << "Reading line. Size: " << reader.readLine( buffer ) << ". Line = " << std::endl;
    for( auto e : buffer ) {
        std::cout << e;
    }
    std::cout << std::endl;
    reader.close();
    ASSERT_EQ( -1, reader.size() );
}

TEST( FileReader_Tests, readLine ) {
    auto reader = eadlib::io::FileReader( "log_config.cfg" );
    ASSERT_TRUE( reader.open() );
    std::vector<char> buffer;
    while( !reader.isDone() ) {
        std::cout << "readLine: " << reader.readLine( buffer ) << " chars. Buffer: \n\t";
        for( auto i : buffer ) {
            std::cout  << i;
        }
        std::cout << std::endl;
    }
    reader.close();
}

TEST( FileReader_Tests, readLine_2 ) { //Partly eaten lines
    auto reader = eadlib::io::FileReader( "log_config.cfg" );
    ASSERT_TRUE( reader.open() );
    std::vector<char> buffer;
    size_t buffer_size = 10;
    EXPECT_EQ( buffer_size, reader.read( buffer, buffer_size ) );
    std::streamsize stream_size = reader.readLine( buffer ) + 1; //+1 for the '\n' char at the end which is omitted
    std::cout << "readLine: " << stream_size << " chars. Buffer: \n\t";
    for( auto i : buffer ) {
        std::cout  << i;
    }
    std::cout << std::endl;
    EXPECT_EQ( buffer_size + stream_size, reader.getPosition() );
    reader.close();
}

#endif //EADLIB_FILEREADER_TEST_H
