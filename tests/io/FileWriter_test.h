#ifndef EADLIB_FILEWRITER_TEST_H
#define EADLIB_FILEWRITER_TEST_H

#include "gtest/gtest.h"
#include "../../src/io/FileWriter.h"

//TODO

TEST( FileWriter_Tests, test ) {
    auto writer = eadlib::io::FileWriter( "FileWriter_test.text" );
    ASSERT_TRUE( writer.open() );
    EXPECT_TRUE( writer.write<char>( 'A' ) );
}

#endif //EADLIB_FILEWRITER_TEST_H
