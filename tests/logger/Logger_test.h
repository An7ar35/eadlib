#ifndef EADLIB_TESTS_LOGGER_TEST_H
#define EADLIB_TESTS_LOGGER_TEST_H

#include "gtest/gtest.h"
#include "../../src/logger/Logger.h"
#include "../threader.h"
#include <iostream>

void logChannel1() {
    LOG_FATAL( "Channel ", 1 );
}

void logChannel2() {
    LOG_DEBUG( "Channel ", 2 );
}

void logChannel3() {
    LOG_WARNING( "Channel ", 3 );
}

TEST( Logger_Tests, x ) {
    threader( logChannel1, "ch1", 1000 );
    threader( logChannel2, "ch2", 1000 );
    threader( logChannel3, "ch3", 1000 );

}

#endif //EADLIB_TESTS_LOGGER_TEST_H
