#include "gtest/gtest.h"

//RC

//ALGORITHM
#include "algorithm/sort_test.h"
#include "algorithm/Tarjan_test.h"
//CLI
#include "cli/Parser_test.h"
#include "cli/braille/BrailleBitMapper_test.h"
#include "cli/braille/BraillePlot_test.h"
#include "cli/braille/braille_test.h"
#include "cli/braille/graph/graph_test.h"
#include "cli/braille/graph/labelling/labelling_test.h"
#include "cli/braille/raster/raster_test.h"
//DATASTRUCTURES
#include "datastructure/Bag_test.h"
#include "datastructure/LinkedList_test.h"
#include "datastructure/LinearList_test.h"
#include "datastructure/BinaryTree_test.h"
#include "datastructure/BST_test.h"
#include "datastructure/AVLTree_test.h"
#include "datastructure/Heap_test.h"
#include "datastructure/Matrix_test.h"
#include "datastructure/Graph_test.h"
#include "datastructure/WeightedGraph_test.h"
#include "wrapper/SQLite/TableDB_test.h"
#include "wrapper/SQLite/TableDBCell_test.h"
#include "datastructure/DMDGraph_test.h"
#include "datastructure/ObjectPool_test.h"
//DATATYPE
#include "datatype/Coordinate_test.h"
#include "datatype/Coordinate2D_test.h"
#include "datatype/Coordinate3D_test.h"
//WRAPPER\SQLITE
//#include "wrappers/SQLite/sqlite_wrapper_test.h"
//LOGGER
#include "logger/Logger_test.h"
//TOOLBOX\COORDINATES
#include "toolbox/coordinates/coordinates_test.h"
//MATH
#include "math/geo/trigonometry_test.h"
#include "math/geo/spatial_test.h"
#include "math/geo/planar_test.h"
#include "math/Fraction_test.h"
#include "math/shape/Circle_test.h"
#include "math/Matrix_test.h"
#include "math/math_test.h"
#include "datatype/Integer_test.h"
//STRING
#include "string/string_test.h"
//TOOL
#include "tool/Convert_test.h"
#include "tool/Progress_test.h"
#include "tool/enum_test.h"
//IO
#include "io/FileReader_test.h"
#include "io/FileWriter_test.h"

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    //::testing::GTEST_FLAG(filter) = "Coordinate_Tests*";
    return RUN_ALL_TESTS();
}