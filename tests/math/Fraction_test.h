#ifndef EADLIB_TESTS_FRACTION_TEST_H
#define EADLIB_TESTS_FRACTION_TEST_H

#include "gtest/gtest.h"
#include "../../src/math/Fraction.h"
#include "../../src/math/math.h"

const int MAX_INT_VALUE { std::numeric_limits<int>::max() };
const int MIN_INT_VALUE { std::numeric_limits<int>::min() };

TEST( Fraction_Tests, Constructor_with_1_integer ) {
    EXPECT_TRUE( "2/1" == eadlib::math::Fraction( 2 ).str() );
    EXPECT_TRUE( "-2/1" == eadlib::math::Fraction( -2 ).str() );
    EXPECT_TRUE( "0/1" == eadlib::math::Fraction( 0 ).str() );
    EXPECT_TRUE( "0/1" == eadlib::math::Fraction( -0 ).str() );
}

TEST( Fraction_Tests, Constructor_with_double ) {
    EXPECT_NEAR( 12.3456, eadlib::math::Fraction( 12.3456 ).asDouble(), 0.0001 );
    EXPECT_TRUE( eadlib::math::Fraction( 0.75 ) == eadlib::math::Fraction( 3, 4 ) );
}

TEST( Fraction_Tests, Constructor_with_2_integers ) {
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction( 5, 10 ).str() );
    EXPECT_TRUE( "-1/2" == eadlib::math::Fraction( -5, 10 ).str() );
    EXPECT_TRUE( "-1/2" == eadlib::math::Fraction(5, -10).str() );
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction(-5, -10).str() );
    EXPECT_TRUE( "0/1" == eadlib::math::Fraction(0, -1).str() );
    EXPECT_TRUE( "0/1" == eadlib::math::Fraction(-0, 1).str() );
}

TEST( Fraction_Tests, Constructor_with_String ) {
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction( "25/50" ).str() );
    EXPECT_TRUE( "-1/2" == eadlib::math::Fraction( "-25/50" ).str() );
    EXPECT_TRUE( "-1/2" == eadlib::math::Fraction( "25/-50" ).str() );
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction( "-25/-50" ).str() );
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction( " 25 / 50 " ).str() );
    EXPECT_TRUE( "1/2" == eadlib::math::Fraction( "- 25 / - 50" ).str() );
    EXPECT_TRUE( "-1/2" == eadlib::math::Fraction( "25 / -50" ).str() );
    EXPECT_THROW( eadlib::math::Fraction( "25\\50" ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "     " ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "25\\5o" ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( " 2 5 / 0 " ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "1/0" ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "1.234/5" ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "5/1.1234" ), std::invalid_argument );
    EXPECT_THROW( eadlib::math::Fraction( "1.2/1.2" ), std::invalid_argument );
    //INTEGER TYPE RANGE: -2147483648 to 2147483647
    EXPECT_THROW( eadlib::math::Fraction( "1/2147483648" ), std::overflow_error );
    EXPECT_THROW( eadlib::math::Fraction( "2147483648/1" ), std::overflow_error );
    EXPECT_THROW( eadlib::math::Fraction( "1/-2147483649" ), std::overflow_error );
    EXPECT_THROW( eadlib::math::Fraction( "-2147483649/1" ), std::overflow_error );
}

TEST( Fraction_Tests, Operator_ostream ) {
    std::ostringstream out;
    eadlib::math::Fraction fraction( 666, 999 );
    out << fraction;
    EXPECT_EQ( "2/3", out.str() );
}

TEST( Fraction_Tests, Operator_Add ) {
    eadlib::math::Fraction result1 = eadlib::math::Fraction( 5, 10 ) + eadlib::math::Fraction( 20, 40 );
    EXPECT_TRUE( "1/1" == result1.str() );
    EXPECT_THROW( eadlib::math::Fraction( 1, 2147483647 ) + eadlib::math::Fraction( 1, 1 ), std::overflow_error );
}


TEST( Fraction_Tests, Operator_Subtract ) {
    eadlib::math::Fraction result1 = eadlib::math::Fraction( 20, 40 ) - eadlib::math::Fraction( 5, 20 ) ;
    EXPECT_TRUE( "1/4" == result1.str() );
    EXPECT_THROW( eadlib::math::Fraction( 5, 999999999 ) - eadlib::math::Fraction( 5, 2 ), std::overflow_error );
}

TEST( Fraction_Tests, Operator_Multiply ) {
    eadlib::math::Fraction result1 = eadlib::math::Fraction( 1, 2 ) * eadlib::math::Fraction( 60, 30 ) ;
    EXPECT_TRUE( "1/1" == result1.str() );
    EXPECT_THROW( eadlib::math::Fraction( 2, 999999999 ) * eadlib::math::Fraction( 2, 999999999 ), std::overflow_error );
}

TEST( Fraction_Tests, Operator_Divide ) {
    eadlib::math::Fraction result1 = eadlib::math::Fraction( 1, 2 ) / eadlib::math::Fraction( 60, 30 );
    EXPECT_TRUE( "1/4" == result1.str() );
    EXPECT_THROW( eadlib::math::Fraction( 2, 999999999 ) / eadlib::math::Fraction( 999999999, 2 ), std::overflow_error );
}

TEST( Fraction_Tests, Operator_Less ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 1 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 4 );
    EXPECT_TRUE( fraction3 < fraction2 );
    EXPECT_TRUE( fraction3 < fraction1 );
    EXPECT_FALSE( fraction1 < fraction2 );
    EXPECT_FALSE( fraction1 < fraction3 );
}

TEST( Fraction_Tests, Operator_Greater ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 1 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 4 );
    EXPECT_TRUE( fraction1 > fraction2 );
    EXPECT_TRUE( fraction1 > fraction3 );
    EXPECT_FALSE( fraction3 > fraction2 );
    EXPECT_FALSE( fraction3 > fraction1 );
}

TEST( Fraction_Tests, Operator_LessEqual ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 1 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 4 );
    eadlib::math::Fraction fraction4 = eadlib::math::Fraction( 1, 1 );
    EXPECT_TRUE( fraction2 <= fraction1 );
    EXPECT_TRUE( fraction3 <= fraction2 && fraction3 <= fraction1 );
    EXPECT_TRUE( fraction4 <= fraction1 );
    EXPECT_FALSE( fraction1 < fraction2 && fraction1 < fraction3 );
}

TEST( Fraction_Tests, Operator_GreaterEqual ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 1 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 4 );
    eadlib::math::Fraction fraction4 = eadlib::math::Fraction( 1, 1 );
    EXPECT_TRUE( fraction1 >= fraction2 && fraction1 >= fraction3 && fraction1 >= fraction4 );
    EXPECT_FALSE( fraction2 >= fraction1 );
    EXPECT_FALSE( fraction3 >= fraction2 && fraction3 >= fraction1 );
}

TEST( Fraction_Tests, Operator_NotEquivalent ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 3 );
    EXPECT_FALSE( fraction1 != fraction2 );
    EXPECT_TRUE( fraction1 != fraction3 );
}

TEST( Fraction_Tests, Operator_Equivalent ) {
    eadlib::math::Fraction fraction1 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction2 = eadlib::math::Fraction( 1, 2 );
    eadlib::math::Fraction fraction3 = eadlib::math::Fraction( 1, 3 );
    EXPECT_TRUE( fraction1 == fraction2 );
    EXPECT_FALSE( fraction1 == fraction3 );
}

TEST( Fraction_Tests, Abs ) {
    eadlib::math::Fraction fraction1abs = eadlib::math::Fraction( 2, 5 ).abs();
    EXPECT_TRUE( "2/5" == fraction1abs.str() );
    eadlib::math::Fraction fraction2abs = eadlib::math::Fraction( -2, 5 ).abs();
    EXPECT_TRUE( "2/5" == fraction2abs.str() );
}

TEST( Fraction_Tests, Negate ) {
    eadlib::math::Fraction fraction1n = eadlib::math::Fraction( 1, 1 ).negate();
    EXPECT_TRUE( "-1/1" == fraction1n.str() );
    eadlib::math::Fraction fraction2n = eadlib::math::Fraction( -1, -1 ).negate();
    EXPECT_TRUE( "-1/1" == fraction2n.str() );
    eadlib::math::Fraction fraction3n = eadlib::math::Fraction( 1, -1 ).negate();
    EXPECT_TRUE( "1/1" == fraction3n.str() );
    eadlib::math::Fraction fraction4n = eadlib::math::Fraction( -1, 1 ).negate();
    EXPECT_TRUE( "1/1" == fraction4n.str() );
}

TEST( Fraction_Tests, Inverse ) {
    eadlib::math::Fraction fraction1inv = eadlib::math::Fraction( 2, 5 ).inverse();
    EXPECT_TRUE( "5/2" == fraction1inv.str() );
    eadlib::math::Fraction fraction2inv = eadlib::math::Fraction( -2, 5 ).inverse();
    EXPECT_TRUE( "-5/2" == fraction2inv.str() );
}

TEST( Fraction_Tests, str ) {
    EXPECT_TRUE( eadlib::math::Fraction( 9999999, 9999999 ).str() == "1/1" );
    EXPECT_TRUE( eadlib::math::Fraction( 1, 2 ).str() == "1/2" );
}

TEST( Fraction_Tests, asDouble ) {
    EXPECT_TRUE( eadlib::math::Fraction( 1, 1 ).asDouble() == 1 );
    EXPECT_TRUE( eadlib::math::Fraction( 1, 2 ).asDouble() == 0.5 );
}

#endif //EADLIB_TESTS_FRACTION_TEST_H
