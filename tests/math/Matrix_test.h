#ifndef EADLIB_TESTS_MATRIX_TEST_H
#define EADLIB_TESTS_MATRIX_TEST_H

#include "gtest/gtest.h"
#include "../../../../src/toolbox/math/objects/Matrix.h"
#include "../../../../src/toolbox/math/objects/Matrix.cpp"

TEST( Matrix_Tests, size_y ) {
    eadlib::math::Matrix<int> m( 5, 6 );
    EXPECT_TRUE( m.size_y() == 5 );
}

TEST( Matrix_Tests, size_x ) {
    eadlib::math::Matrix<int> m( 5, 6 );
    EXPECT_TRUE( m.size_x() == 6 );
}

TEST( Matrix_Tests, Operator_brackets ) {
    eadlib::math::Matrix<int> m( 5, 6 );

}


#endif //EADLIB_TESTS_MATRIX_TEST_H
