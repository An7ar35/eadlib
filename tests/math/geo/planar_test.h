#ifndef EADLIB_TESTS_PLANAR_TEST_H
#define EADLIB_TESTS_PLANAR_TEST_H

#include "gtest/gtest.h"
#include "../../../src/math/geo/planar.h"

TEST( Planar_Tests, calcCircleIntersections ) {
    eadlib::math::shape::Circle a( 10 );
    eadlib::math::shape::Circle b( 10 );
    eadlib::Coordinate2D<double> A { 0, 0 };
    eadlib::Coordinate2D<double> B { 0, 5 };
    eadlib::Coordinate2D<double> intersect1 { };
    eadlib::Coordinate2D<double> intersect2 { };
    EXPECT_TRUE( eadlib::math::geo::planar::calcCircleIntersections( a, b, A, B, intersect1, intersect2 ) == 2 );
    EXPECT_NEAR( intersect1.x, 9.682, 0.001 );
    EXPECT_NEAR( intersect1.y, 2.5, 0 );
    EXPECT_NEAR( intersect2.x, -9.682, 0.001 );
    EXPECT_NEAR( intersect2.y, 2.5, 0 );
    eadlib::math::shape::Circle c( 2 );
    eadlib::Coordinate2D<double> C { 20, 0 };
    EXPECT_TRUE( eadlib::math::geo::planar::calcCircleIntersections( a, c, A, C, intersect1, intersect2 ) == 0 );
}

#endif //EADLIB_TESTS_PLANAR_TEST_H
