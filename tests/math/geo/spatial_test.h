#ifndef EADLIB_TESTS_SPATIAL_TEST_H
#define EADLIB_TESTS_SPATIAL_TEST_H

#include "gtest/gtest.h"
#include "../../../src/testing/equalish.h"
#include "../../../src/math/geo/spatial.h"

#include "../../../src/logger/Logger.h"
#include "../../../src/datatype/Coordinate3D.h"


TEST( Spatial_Tests, calcAngleTo ) {
    //XY plane
    eadlib::Coordinate3D coord( 5, 5, 0 );
    long double angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X,eadlib::math::geo::Plane3D::XY, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y, eadlib::math::geo::Plane3D::XY, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Z, eadlib::math::geo::Plane3D::XY, coord );
    ASSERT_TRUE( angle == 0 );
    coord = eadlib::Coordinate3D( 1, 2, 0 );
    long double angle1 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X, eadlib::math::geo::Plane3D::XY, coord );
    ASSERT_NEAR( angle1, 63.4349, 2 );
    long double angle2 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y, eadlib::math::geo::Plane3D::XY, coord );
    ASSERT_NEAR( angle2, 26.5651, 2 );
    ASSERT_NEAR( ( angle1 + angle2 ), 90, 0.01 );
    //XZ Plane3D
    coord = eadlib::Coordinate3D( 5, 0, 5 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X, eadlib::math::geo::Plane3D::XZ, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Z, eadlib::math::geo::Plane3D::XZ, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y, eadlib::math::geo::Plane3D::XZ, coord );
    ASSERT_TRUE( angle == 0 );
    coord = eadlib::Coordinate3D( 1, 0, 2 );
    angle1 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X, eadlib::math::geo::Plane3D::XZ, coord );
    ASSERT_NEAR( angle1, 63.4349, 2 );
    angle2 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Z, eadlib::math::geo::Plane3D::XZ, coord );
    ASSERT_NEAR( angle2, 26.5651, 2 );
    ASSERT_NEAR( ( angle1 + angle2 ), 90, 0.01 );
    //YZ Plane3D
    coord = eadlib::Coordinate3D( 0, 5, 5 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y, eadlib::math::geo::Plane3D::YZ, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Z, eadlib::math::geo::Plane3D::YZ, coord );
    ASSERT_NEAR( angle, 45, 0.01 );
    angle = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::X, eadlib::math::geo::Plane3D::YZ, coord );
    ASSERT_TRUE( angle == 0 );
    coord = eadlib::Coordinate3D( 0, 1, 2 );
    angle1 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Y, eadlib::math::geo::Plane3D::YZ, coord );
    ASSERT_NEAR( angle1, 63.4349, 2 );
    angle2 = eadlib::math::geo::spatial::calcAngleTo( eadlib::Coordinate3D::Axis::Z, eadlib::math::geo::Plane3D::YZ, coord );
    ASSERT_NEAR( angle2, 26.5651, 2 );
    ASSERT_NEAR( ( angle1 + angle2 ), 90, 0.01 );
}

TEST( Spatial_Tests, calcBounds ) {
    eadlib::Coordinate3D a = eadlib::Coordinate3D( -2, 2, 2 );
    eadlib::Coordinate3D b = eadlib::Coordinate3D( 5, -6, -3 );
    eadlib::Coordinate3D u { };
    eadlib::Coordinate3D l { };
    eadlib::math::geo::spatial::calcBounds( a, b, l, u );
    ASSERT_TRUE( l.x() == -2 && l.y() == -6 && l.z() == -3 ); //upper bound
    ASSERT_TRUE( u.x() == 5 && u.y() == 2 && u.z() == 2 ); //lower bound
}

TEST( Spatial_Tests, orderPoints ) {
    eadlib::Coordinate3D a { 10, 0, 0 };
    eadlib::Coordinate3D b { -10, 0, 0 };
    eadlib::Coordinate3D c { 100, 0, 0 };
    double ax = 11;
    double bx = 22;
    double cx = 33;
    eadlib::math::geo::spatial::orderPoints( a, b, c, ax, bx, cx );
    //order should now be b,c,a
    ASSERT_TRUE( a.x() == -10 );
    ASSERT_TRUE( b.x() == 100 );
    ASSERT_TRUE( c.x() == 10 );
    ASSERT_TRUE( ax == 22 );
    ASSERT_TRUE( bx == 33 );
    ASSERT_TRUE( cx == 11 );
}

 TEST( Spatial_Tests, trilaterate_1 ) {
    eadlib::Coordinate3D X1, X2;
    EXPECT_TRUE( eadlib::math::geo::spatial::trilaterate( 13.0773, 20.0062, 8.11983, 19.6534, 15.984, 22.4274, X1, X2 ) );
    eadlib::Coordinate3D X3, X4;
    EXPECT_TRUE( eadlib::math::geo::spatial::trilaterate( 15, 10.77, 14.87, 16.52, 16.11, 13.9, X3, X4 ) );
    //1.
    ASSERT_NEAR( X1.x(), 11.5385, 0.001 );
    ASSERT_NEAR( X1.y(), -15.6373, 0.001 );
    ASSERT_NEAR( X1.z(), 2.93148, 0.001 );
    //1.2
    ASSERT_NEAR( X2.x(), 11.5385, 0.001 );
    ASSERT_NEAR( X2.y(), -15.6373, 0.001 );
    ASSERT_NEAR( X2.z(), -2.93148, 0.001 );
    //2.1
    ASSERT_NEAR( X3.x(), 7.94594, 0.001 );
    ASSERT_NEAR( X3.y(), 6.60871, 0.001 );
    ASSERT_NEAR( X3.z(), 12.8879, 0.001 );
    //2.2
    ASSERT_NEAR( X4.x(), 7.94594, 0.001 );
    ASSERT_NEAR( X4.y(), 6.60871, 0.001 );
    ASSERT_NEAR( X4.z(), -12.8879, 0.001 );
}

TEST( Spatial_Tests, trilaterate_2 ) {
    eadlib::Coordinate3D A = eadlib::Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
    eadlib::Coordinate3D B = eadlib::Coordinate3D( -22.375, 34.8438, 4 ); //20
    eadlib::Coordinate3D C = eadlib::Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
    eadlib::Coordinate3D X1, X2;
    EXPECT_TRUE( eadlib::math::geo::spatial::trilaterate( A, B, C, 19.6534, 15.984, 22.4274, X1, X2 ) );
    eadlib::Coordinate3D X { -11.5625, 43.8125, 11.625 }; //3
    bool a {
        eadlib::testing::numeric::equalish<double>( X1.x(), X.x(), 2 ) &&
        eadlib::testing::numeric::equalish<double>( X1.y(), X.y(), 2 ) &&
        eadlib::testing::numeric::equalish<double>( X1.z(), X.z(), 2 )
    };
    bool b {
        eadlib::testing::numeric::equalish<double>( X2.x(), X.x(), 2 ) &&
        eadlib::testing::numeric::equalish<double>( X2.y(), X.y(), 2 ) &&
        eadlib::testing::numeric::equalish<double>( X2.z(), X.z(), 2 )
    };
    ASSERT_TRUE( a || b );
}

#endif //EADLIB_TESTS_SPATIAL_TEST_H
