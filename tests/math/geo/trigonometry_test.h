#ifndef EADLIB_TESTS_TRIGONOMETRY_TEST_H
#define EADLIB_TESTS_TRIGONOMETRY_TEST_H

#include "gtest/gtest.h"
#include "../../threader.h"
#include "../../../src/math/geo/trigonometry.h"

TEST( Trigonometry_Tests, radian2degree ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::rad2deg( 0.4 ), 22.918311805, 4 );
}

TEST( Trigonometry_Tests, degree2radian ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::deg2rad( 45 ), 0.7853981634, 4 );
}

TEST( Trigonometry_Tests, cotan ) {
    //TODO eadlib::math::geo::trigonometry::cotan(..) unit test
}

TEST( Trigonometry_Tests, calcNorm ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::norm( 5, 8 ), 9.43398, 0.0001 );
}

TEST( Trigonometry_Tests, calcAngle_SSS ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::angleSSS( 8, 6, 7 ), 75.5225, 4 );
}

TEST( Trigonometry_Tests, calcSide_AAS ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::sideAAS( 35, 62, 7 ), 4.5473, 4 );
}
TEST( Trigonometry_Tests, calcSide_SAS ) {
    ASSERT_NEAR( eadlib::math::geo::trigonometry::sideSAS( 5, 49, 7 ), 5.2987, 4 );
}

TEST( Trigonometry_Tests, pythagoras ) {
    ASSERT_TRUE( eadlib::math::geo::trigonometry::pythagoras( 3, 5 ) == 4 );
}

TEST( Trigonometry_Tests, pythagoras_hyp ) {
    ASSERT_TRUE( eadlib::math::geo::trigonometry::hypotenuse( 4, 3 ) == 5 );
}

TEST( Trigonometry_Tests, getAreaOf_Triangle ) {
    ASSERT_TRUE( eadlib::math::geo::trigonometry::triangleArea( 24, 30, 18 ) == 216 );
}


#endif //EADLIB_TESTS_TRIGONOMETRY_TEST_H
