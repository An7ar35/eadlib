#ifndef EADLIB_MATH_TEST_H
#define EADLIB_MATH_TEST_H

#include "gtest/gtest.h"
#include "../../src/math/math.h"

TEST( math_Tests, fibonacci ) {
    EXPECT_TRUE( eadlib::math::fibonacci( 0 ) == 0 );
    EXPECT_TRUE( eadlib::math::fibonacci( 1 ) == 1 );
    EXPECT_TRUE( eadlib::math::fibonacci( 2 ) == 1 );
    EXPECT_TRUE( eadlib::math::fibonacci( 3 ) == 2 );
    EXPECT_TRUE( eadlib::math::fibonacci( 4 ) == 3 );
    EXPECT_TRUE( eadlib::math::fibonacci( 5 ) == 5 );
    EXPECT_TRUE( eadlib::math::fibonacci( 6 ) == 8 );
    EXPECT_TRUE( eadlib::math::fibonacci( 7 ) == 13 );
    EXPECT_TRUE( eadlib::math::fibonacci( 8 ) == 21 );
    EXPECT_TRUE( eadlib::math::fibonacci( 9 ) == 34 );
    EXPECT_TRUE( eadlib::math::fibonacci( 20 ) == 6765 );
    EXPECT_TRUE( eadlib::math::fibonacci( 50 ) == 12586269025 );
    long long i = 354224848179261915075;
    std::cout << "a: 354224848179261915075\n" << "b: " << i << std::endl;
    EXPECT_TRUE( eadlib::math::fibonacci( 100 ) == 354224848179261915075 );
    EXPECT_TRUE( eadlib::math::fibonacci( 1000 ) == 354224848179261915075 );
}

#endif //EADLIB_MATH_TEST_H
