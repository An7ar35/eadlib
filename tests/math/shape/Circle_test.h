#ifndef EADLIB_TESTS_CIRCLE_TEST_H
#define EADLIB_TESTS_CIRCLE_TEST_H

#include "gtest/gtest.h"
#include "../../../src/math/shape/Circle.h"

TEST( Circle_Tests, getRadius ) {
    EXPECT_TRUE( eadlib::math::shape::Circle( 123.456 ).getRadius() == 123.456 );
}

#endif //EADLIB_TESTS_CIRCLE_TEST_H
