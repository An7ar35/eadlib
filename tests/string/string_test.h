#ifndef EADLIB_STRING_TEST_H
#define EADLIB_STRING_TEST_H

#include "gtest/gtest.h"
#include "../../src/string/string.h"

TEST( string_Tests, split ) {
    auto r0 = eadlib::string::split( "123.456", '.' );
    ASSERT_EQ( r0.first, "123" );
    ASSERT_EQ( r0.second, "456" );

    auto r1 = eadlib::string::split( "123456.", '.' );
    ASSERT_EQ( r1.first, "123456" );
    ASSERT_EQ( r1.second, "" );

    auto r2 = eadlib::string::split( ".123456", '.' );
    ASSERT_EQ( r2.first, "" );
    ASSERT_EQ( r2.second, "123456" );

    auto r3 = eadlib::string::split( "123456", '.' );
    ASSERT_EQ( r3.first, "123456" );
    ASSERT_EQ( r3.second, "" );

    auto r4 = eadlib::string::split( ".", '.' );
    ASSERT_EQ( r4.first, "" );
    ASSERT_EQ( r4.second, "" );

    auto r5 = eadlib::string::split( "", '.' );
    ASSERT_EQ( r5.first, "" );
    ASSERT_EQ( r5.second, "" );
}

TEST( string_Tests, length_frac ) {
    ASSERT_EQ( 0, eadlib::string::length_frac<double>( 1., 5 ) );
    ASSERT_EQ( 2, eadlib::string::length_frac<double>( 1.01, 10 ) );
    ASSERT_EQ( 1, eadlib::string::length_frac<double>( 0.2, 6 ) );
    ASSERT_EQ( 1, eadlib::string::length_frac<double>( 0.200000003f, 6 ) );
    ASSERT_EQ( 6, eadlib::string::length_frac<double>( 1.000999, 10 ) );
    ASSERT_EQ( 3, eadlib::string::length_frac<double>( 1.000999, 4 ) ); // will round to .001
    ASSERT_EQ( 5, eadlib::string::length_frac<double>( 1.000909, 5 ) ); // will round to .00091
    ASSERT_EQ( 20, eadlib::string::length_frac<double>( 1e-20, 25 ) );
    ASSERT_EQ( 300, eadlib::string::length_frac<double>( 1e-300, 310 ) ); //.0000.....1
    ASSERT_EQ( 0, eadlib::string::length_frac<double>( 1e+300, 310 ) ); //1000....00.0
}

TEST( string_Tests, length_int ) {
    ASSERT_EQ( 1, eadlib::string::length_int<int>( 0 ) );
    ASSERT_EQ( 2, eadlib::string::length_int<int>( -1 ) );
    ASSERT_EQ( 1, eadlib::string::length_int<int>( 1 ) );

    ASSERT_EQ( 1, eadlib::string::length_int<float>( 0. ) );
    ASSERT_EQ( 1, eadlib::string::length_int<float>( 0.00000 ) );
    ASSERT_EQ( 2, eadlib::string::length_int<float>( -1. ) );
    ASSERT_EQ( 1, eadlib::string::length_int<float>( 1. ) );

    ASSERT_EQ( 1, eadlib::string::length_int<float>( 0.123 ) );
    ASSERT_EQ( 2, eadlib::string::length_int<float>( -1.123 ) );
    ASSERT_EQ( 1, eadlib::string::length_int<float>( 1.123 ) );

    ASSERT_EQ( 11, eadlib::string::length_int<long>( 11111111111 ) );
    ASSERT_EQ( 12, eadlib::string::length_int<long>( -11111111111 ) );
    ASSERT_EQ( 19, eadlib::string::length_int( 9223372036854775807 ) );
    ASSERT_EQ( 20, eadlib::string::length_int<long>( -9223372036854775807 ) );
}

TEST( string_Tests, length_number ) {
    ASSERT_EQ( 1, eadlib::string::length<int>( 0 ) );
    ASSERT_EQ( 2, eadlib::string::length<int>( -1 ) );
    ASSERT_EQ( 1, eadlib::string::length<int>( 1 ) );

    ASSERT_EQ( 1, eadlib::string::length<float>( 0. ) );
    ASSERT_EQ( 2, eadlib::string::length<float>( -1. ) );
    ASSERT_EQ( 1, eadlib::string::length<float>( 1. ) );

    ASSERT_EQ( 11, eadlib::string::length<long>( 11111111111 ) );
    ASSERT_EQ( 12, eadlib::string::length<long>( -11111111111 ) );
    ASSERT_EQ( 19, eadlib::string::length( 9223372036854775807 ) );
    ASSERT_EQ( 20, eadlib::string::length<long>( -9223372036854775807 ) );

    ASSERT_EQ( 1, eadlib::string::length<double>( 0. ) );
    ASSERT_EQ( 4, eadlib::string::length<double>( 1e-2 ) ); //0.01
    ASSERT_EQ( 3, eadlib::string::length<double>( 1e+2 ) ); //100

    /* These fail as the building inaccuracy grows in the mantissa */
//    ASSERT_EQ( 303, eadlib::string::length<double>( 1e-300 ) );
//    ASSERT_EQ( 302, eadlib::string::length<double>( 1e+300 ) );
}

#endif //EADLIB_STRING_TEST_H
