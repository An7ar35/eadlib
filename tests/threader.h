#ifndef EADLIB_TESTS_THREADER_H
#define EADLIB_TESTS_THREADER_H
#include <iostream>
#include <ctime>
#include <thread>

template<typename T> void threader(  T(*function)(), std::string fname, int n_threads ) {
    std::clock_t start;
    start = std::clock();
    std::thread threads[n_threads];
    for( int i = 0; i < n_threads; i++ ) {
        threads[ i ] = std::thread( function );
        threads[ i ].join();
    }
    std::cout << "             >Function '" << fname << "' x " << n_threads << " threads took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
}

void threader( void(*function)(), std::string fname, int n_threads ) {
    std::clock_t start;
    start = std::clock();
    std::thread threads[n_threads];
    for( int i = 0; i < n_threads; i++ ) {
        threads[ i ] = std::thread( function );
        threads[ i ].join();
    }
    std::cout << "             >Function '" << fname << "' x " << n_threads << " threads took: " << (std::clock() - start) / (double)(CLOCKS_PER_SEC / 1000) << " ms" << std::endl;
}
#endif //EADLIB_TESTS_THREADER_H
