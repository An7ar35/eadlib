#ifndef EADLIB_CONVERT_TEST_H
#define EADLIB_CONVERT_TEST_H

#include "gtest/gtest.h"
#include "../../src/tool/Convert.h"

TEST( Convert_Tests, convert ) {
    auto converter = eadlib::tool::Convert();
    std::string s = "100o00";
    ASSERT_THROW( converter.string_to_type<unsigned>( s ), std::bad_cast );
}

#endif //EADLIB_CONVERT_TEST_H
