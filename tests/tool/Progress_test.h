#ifndef EADLIB_PROGRESS_TEST_H
#define EADLIB_PROGRESS_TEST_H

#include <functional>
#include <gtest/gtest.h>
#include "../../src/tool/Progress.h"
#include "../../src/cli/graphic/ProgressBar.h"

namespace unit_tests::Progress_Tests {
    struct Bar : public eadlib::interface::IObserver {
        Bar() {};
        void update( const double &val ) {
            std::cout << std::fixed;
            std::cout << "\rProgress = " << std::setprecision( 2 ) << val * 100;
        }
    };

    struct Worker {
        void work( eadlib::tool::Progress &progress ) {
            progress.begin( 1000 );
            for( int i = 0; i < 1000; i++ ) {
                progress++;
                usleep( 4000 );
            }
        }
    };
}


TEST( Progress_Tests, first ) {
    auto progress = eadlib::tool::Progress();
    auto observer = eadlib::cli::ProgressBar( 70, 2 );
    progress.attachObserver( observer );

    auto worker = unit_tests::Progress_Tests::Worker();
    worker.work( progress );


    //auto worker_thread = std::thread( &unit_tests::Progress_Tests::Worker::work, progress );
    //auto observer_thread = std::thread( &unit_tests::Progress_Tests::Bar::update, nullptr );

}

#endif //EADLIB_PROGRESS_TEST_H
