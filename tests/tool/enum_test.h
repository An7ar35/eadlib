#ifndef EADLIB_ENUM_CAST_TEST_H
#define EADLIB_ENUM_CAST_TEST_H

#include "gtest/gtest.h"
#include "../../src/datatype/enum.h"

namespace testing::enum_test {
    enum class TestEnums {
        one = 1,
        two = 2,
        three = 3
    };
}

TEST( enum_Tests, enum_int ) {
    using testing::enum_test::TestEnums;
    using eadlib::enum_int;
    ASSERT_EQ( 1, enum_int<TestEnums>( TestEnums::one ) );
    ASSERT_EQ( 2, enum_int<TestEnums>( TestEnums::two ) );
    ASSERT_EQ( 3, enum_int<TestEnums>( TestEnums::three ) );
}

TEST( enum_Tests, to_string ) {
    using testing::enum_test::TestEnums;
    using eadlib::to_string;
    ASSERT_EQ( "1", to_string<TestEnums>( TestEnums::one ) );
    ASSERT_EQ( "2", to_string<TestEnums>( TestEnums::two ) );
    ASSERT_EQ( "3", to_string<TestEnums>( TestEnums::three ) );
}

#endif //EADLIB_ENUM_CAST_TEST_H
