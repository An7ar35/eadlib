#ifndef EADLIB_TESTS_COORDINATES_TEST_H
#define EADLIB_TESTS_COORDINATES_TEST_H

#include "gtest/gtest.h"
#include "../../../src/toolbox/coordinates/coordinates.h"
#include "../../../src/toolbox/coordinates/coordinates.cpp"

TEST( Coordinates_Tests, calc1D ) {
    eadlib::Coordinate3D valid1 = eadlib::Coordinate3D( 0, 0, 0 );
    eadlib::Coordinate3D valid2 = eadlib::Coordinate3D( 0, 0, 0 );
    eadlib::Coordinate3D invalid( 5, 5, 5 );
    eadlib::toolbox::coordinates::calc1D( invalid, 20, valid2 );
    EXPECT_FALSE( eadlib::toolbox::coordinates::calc1D( invalid, 20, valid2 ) );
    EXPECT_TRUE( eadlib::toolbox::coordinates::calc1D( valid1, 20, valid2 ) );
    EXPECT_TRUE( valid2 == eadlib::Coordinate3D( 20, 0, 0 ) );
}

TEST( Coordinates_Tests, calc2D ) {
    eadlib::Coordinate3D A = eadlib::Coordinate3D( 0, 0, 0 );
    eadlib::Coordinate3D B = eadlib::Coordinate3D( 10, 0, 0 );
    eadlib::Coordinate3D X1 = eadlib::Coordinate3D();
    eadlib::Coordinate3D X2 = eadlib::Coordinate3D();
    EXPECT_TRUE( eadlib::toolbox::coordinates::calc2D( A, B, 2, 2, X1, X2 ) == 0 );
    EXPECT_TRUE( eadlib::toolbox::coordinates::calc2D( A, B, 10, 10, X1, X2 ) == 2 );
    EXPECT_NEAR( X1.x(), 5, 0.001 );
    EXPECT_NEAR( X1.y(), -8.66, 0.001 );
    EXPECT_TRUE( X1.z() == 0 && X2.z() == 0 );
    EXPECT_NEAR( X2.x(), 5, 0.001 );
    EXPECT_NEAR( X2.y(), 8.66, 0.001 );
}

TEST( Coordinates_Tests, calc3D ) {
    eadlib::Coordinate3D A = eadlib::Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
    eadlib::Coordinate3D B = eadlib::Coordinate3D( -22.375, 34.8438, 4 ); //20
    eadlib::Coordinate3D C = eadlib::Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
    eadlib::Coordinate3D X1 { };
    eadlib::Coordinate3D X2 { };
    EXPECT_TRUE( eadlib::toolbox::coordinates::calc3D( A, B, C, 13.0773, 20.0062, 19.6534, 8.11983, 15.984, 22.4274, X1, X2 ) );
    eadlib::Coordinate3D Xreal { -11.5625, 43.8125, 11.625 }; //3
    EXPECT_NEAR( X1.x(), Xreal.x(), 0.01 );
    EXPECT_NEAR( X1.y(), Xreal.y(), 0.01 );
    EXPECT_NEAR( X1.z(), Xreal.z(), 0.01 );
}

TEST( Coordinates_Tests, calcCoordinates ) {
    eadlib::Coordinate3D A = eadlib::Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
    eadlib::Coordinate3D B = eadlib::Coordinate3D( -22.375, 34.8438, 4 ); //20
    eadlib::Coordinate3D C = eadlib::Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
    eadlib::Coordinate3D D = eadlib::Coordinate3D( -17.3125, 49.5312, -1.6875 ); //40
    eadlib::Coordinate3D X { };
    EXPECT_TRUE( eadlib::toolbox::coordinates::calcCoordinates( A, B, C, D, 19.6534, 15.984, 22.4274, 15.5881, X ) );
    eadlib::Coordinate3D Xreal { -11.5625, 43.8125, 11.625 }; //3
    EXPECT_NEAR( X.x(), Xreal.x(), 0.01 );
    EXPECT_NEAR( X.y(), Xreal.y(), 0.01 );
    EXPECT_NEAR( X.z(), Xreal.z(), 0.01 );
}

#endif //EADLIB_TESTS_COORDINATES_TEST_H
