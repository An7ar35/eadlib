#ifndef EADLIB_TABLEDBCELL_TEST_H
#define EADLIB_TABLEDBCELL_TEST_H

#include "gtest/gtest.h"
#include "../../../src/wrapper/SQLite/TableDBCell.h"

//#include "../../../src/testing/exec_time.h"

TEST( TableDBCell_Tests, test ) {
    using eadlib::TableDBCell;
    auto a = TableDBCell( "string" );
    auto b = TableDBCell( 2.13 );
    ASSERT_EQ( TableDBCell::DataType::STRING, a.getType() );
    ASSERT_EQ( "string", a.getString() );
    ASSERT_EQ( TableDBCell::DataType::DOUBLE, b.getType() );
    ASSERT_EQ( 2.13, b.getDouble() );
    a.swap( b );
    ASSERT_EQ( TableDBCell::DataType::STRING, b.getType() );
    ASSERT_EQ( "string", b.getString() );
    ASSERT_EQ( TableDBCell::DataType::DOUBLE, a.getType() );
    ASSERT_EQ( 2.13, a.getDouble() );
}

//TEST( Item_Tests, Operator_Less ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item string_c = eadlib::Item( "caa" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_FALSE( null_a < null_b );
//    ASSERT_FALSE( null_b < null_a );
//    //INT
//    ASSERT_TRUE( int_a < int_b );
//    ASSERT_FALSE( int_b < int_a );
//    ASSERT_FALSE( int_a < int_a );
//    //DOUBLE
//    ASSERT_TRUE( double_a < double_b );
//    ASSERT_FALSE( double_b < double_a );
//    ASSERT_FALSE( double_a < double_a );
//    //STRING
//    ASSERT_TRUE( string_a < string_b );
//    ASSERT_FALSE( string_b < string_a );
//    ASSERT_FALSE( string_a < string_a );
//    //BOOL
//    ASSERT_FALSE( bool_a < bool_b );
//    ASSERT_TRUE( bool_b < bool_a );
//    ASSERT_FALSE( bool_a < bool_a );
//}
//
//TEST( Item_Tests, Operator_Greater ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_FALSE( null_a > null_b );
//    ASSERT_FALSE( null_b > null_a );
//    //INT
//    ASSERT_FALSE( int_a > int_b );
//    ASSERT_TRUE( int_b > int_a );
//    ASSERT_FALSE( int_a > int_a );
//    //DOUBLE
//    ASSERT_FALSE( double_a > double_b );
//    ASSERT_TRUE( double_b > double_a );
//    ASSERT_FALSE( double_a > double_a );
//    //STRING
//    ASSERT_FALSE( string_a > string_b );
//    ASSERT_TRUE( string_b > string_a );
//    ASSERT_FALSE( string_a > string_a );
//    //BOOL
//    ASSERT_TRUE( bool_a > bool_b );
//    ASSERT_FALSE( bool_b > bool_a );
//    ASSERT_FALSE( bool_b > bool_b );
//}
//
//TEST( Item_Tests, Operator_Less_equal ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_TRUE( null_a <= null_b );
//    ASSERT_TRUE( null_b <= null_a );
//    ASSERT_TRUE( null_a <= null_a );
//    //INT
//    ASSERT_TRUE( int_a <= int_b );
//    ASSERT_FALSE( int_b <= int_a );
//    ASSERT_TRUE( int_a <= int_a );
//    //DOUBLE
//    ASSERT_TRUE( double_a <= double_b );
//    ASSERT_FALSE( double_b <= double_a );
//    ASSERT_TRUE( double_a <= double_a );
//    //STRING
//    ASSERT_TRUE( string_a <= string_b );
//    ASSERT_FALSE( string_b <= string_a );
//    ASSERT_TRUE( string_a <= string_a );
//    //BOOL
//    ASSERT_FALSE( bool_a <= bool_b );
//    ASSERT_TRUE( bool_b <= bool_a );
//    ASSERT_TRUE( bool_b <= bool_b );
//}
//
//TEST( Item_Tests, Operator_Greater_equal ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_TRUE( null_a >= null_b );
//    ASSERT_TRUE( null_b >= null_a );
//    ASSERT_TRUE( null_b >= null_a );
//    //INT
//    ASSERT_FALSE( int_a >= int_b );
//    ASSERT_TRUE( int_b >= int_a );
//    ASSERT_TRUE( int_a >= int_a );
//    //DOUBLE
//    ASSERT_FALSE( double_a >= double_b );
//    ASSERT_TRUE( double_b >= double_a );
//    ASSERT_TRUE( double_a >= double_a );
//    //STRING
//    ASSERT_FALSE( string_a >= string_b );
//    ASSERT_TRUE( string_b >= string_a );
//    ASSERT_TRUE( string_a >= string_a );
//    //BOOL
//    ASSERT_TRUE( bool_a >= bool_b );
//    ASSERT_FALSE( bool_b >= bool_a );
//    ASSERT_TRUE( bool_b >= bool_b );
//}
//
//TEST( Item_Tests, Operator_Equality ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item int_c = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item double_c = eadlib::Item( 1.000001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item string_c = eadlib::Item( "aaa" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_TRUE( null_a == null_b );
//    ASSERT_TRUE( null_b == null_a );
//    ASSERT_TRUE( null_a == null_a );
//    //INT
//    ASSERT_FALSE( int_a == int_b );
//    ASSERT_FALSE( int_b == int_a );
//    ASSERT_TRUE( int_a == int_a );
//    ASSERT_TRUE( int_a == int_c );
//    //DOUBLE
//    ASSERT_FALSE( double_a == double_b );
//    ASSERT_FALSE( double_b == double_a );
//    ASSERT_TRUE( double_a == double_a );
//    ASSERT_TRUE( double_a == double_c );
//    //STRING
//    ASSERT_FALSE( string_a == string_b );
//    ASSERT_FALSE( string_b == string_a );
//    ASSERT_TRUE( string_a == string_a );
//    ASSERT_TRUE( string_a == string_c );
//    //BOOL
//    ASSERT_FALSE( bool_a == bool_b );
//    ASSERT_TRUE( bool_a == bool_a );
//}
//
//TEST( Item_Tests, Operator_NotEquality ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = eadlib::Item( 2 );
//    eadlib::Item int_c = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 1.00001 );
//    eadlib::Item double_c = eadlib::Item( 1.000001 );
//    eadlib::Item string_a = eadlib::Item( "aaa" );
//    eadlib::Item string_b = eadlib::Item( "aab" );
//    eadlib::Item string_c = eadlib::Item( "aaa" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    ASSERT_FALSE( null_a != null_b );
//    ASSERT_FALSE( null_b != null_a );
//    ASSERT_FALSE( null_a != null_a );
//    //INT
//    ASSERT_TRUE( int_a != int_b );
//    ASSERT_TRUE( int_b != int_a );
//    ASSERT_FALSE( int_a != int_a );
//    ASSERT_FALSE( int_a != int_c );
//    //DOUBLE
//    ASSERT_TRUE( double_a != double_b );
//    ASSERT_TRUE( double_b != double_a );
//    ASSERT_FALSE( double_a != double_a );
//    ASSERT_FALSE( double_a != double_c );
//    //STRING
//    ASSERT_TRUE( string_a != string_b );
//    ASSERT_TRUE( string_b != string_a );
//    ASSERT_FALSE( string_a != string_a );
//    ASSERT_FALSE( string_a != string_c );
//    //BOOL
//    ASSERT_TRUE( bool_a != bool_b );
//    ASSERT_FALSE( bool_a != bool_a );
//}
//
//TEST( Item_Tests, Constructor ) {
//    eadlib::Item item = eadlib::Item();
//    std::string s = item.getString();
//    ASSERT_TRUE( s == "" );
//    ASSERT_TRUE( item.getType() == eadlib::Item::DataType::NONE );
//}
//
//TEST( Item_Tests, Constructor_Integer ) {
//    eadlib::Item item = eadlib::Item( 5 );
//    int i = item.getInt();
//    ASSERT_TRUE( i == 5 );
//    ASSERT_TRUE( item.getType() == eadlib::Item::DataType::INT );
//}
//
//TEST( Item_Tests, Constructor_Double ) {
//    eadlib::Item item = eadlib::Item( 666.666 );
//    double d = item.getDouble();
//    ASSERT_TRUE( d == 666.666 );
//    ASSERT_TRUE( item.getType() == eadlib::Item::DataType::DOUBLE );
//}
//
//TEST( Item_Tests, Constructor_String ) {
//    eadlib::Item item = eadlib::Item( "Hello world!" );
//    std::string s = item.getString();
//    ASSERT_TRUE( s == "Hello world!" );
//    ASSERT_TRUE( item.getType() == eadlib::Item::DataType::STRING );
//}
//
//TEST( Item_Tests, Constructor_Boolean ) {
//    eadlib::Item item = eadlib::Item( true );
//    bool b = item.getBool();
//    ASSERT_TRUE( b == true );
//    ASSERT_TRUE( item.getType() == eadlib::Item::DataType::BOOL );
//}
//
//TEST( Item_Tests, Operator_Stream_out ) {
//    std::stringstream ss;
//    eadlib::Item item = eadlib::Item( "this is a stream test.. testing.. uhm.. yeah." );
//    ss << item;
//    std::string s = ss.str();
//    ASSERT_TRUE( s == "this is a stream test.. testing.. uhm.. yeah." );
//}
//
//TEST( Item_Tests, Operator_Equal ) {
//    //NULL
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item null_b = null_a;
//    ASSERT_TRUE( null_b.getType() == eadlib::Item::DataType::NONE );
//    //ITEM
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item int_b = int_a;
//    int integer_value = int_b.getInt();
//    ASSERT_TRUE( int_b.getType() == eadlib::Item::DataType::INT );
//    ASSERT_TRUE( integer_value == 1 );
//    //DOUBLE
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = double_a;
//    double double_value = double_b.getDouble();
//    ASSERT_TRUE( double_b.getType() == eadlib::Item::DataType::DOUBLE );
//    ASSERT_TRUE( double_value == 1.000001 );
//    //STRING
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item string_b = string_a;
//    std::string string_item= string_b.getString();
//    ASSERT_TRUE( string_b.getType() == eadlib::Item::DataType::STRING );
//    ASSERT_TRUE( string_item == "cool" );
//    //BOOL
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = bool_a;
//    bool bool_value = bool_b.getBool();
//    ASSERT_TRUE( bool_b.getType() == eadlib::Item::DataType::BOOL );
//    ASSERT_TRUE( bool_value == true );
//}
//
//TEST( Item_Tests, getInt ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( -0.99 );
//    eadlib::Item double_c = eadlib::Item( 9.49 );
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    int null2int_value = null_a.getInt();
//    ASSERT_TRUE( null2int_value == 0 );
//    //INT
//    int int2int_value = int_a.getInt();
//    ASSERT_TRUE( int2int_value == 1 );
//    //DOUBLE
//    int double2int_value = double_a.getInt();
//    ASSERT_TRUE( double2int_value == 1 );
//    double2int_value = double_b.getInt();
//    ASSERT_TRUE( double2int_value == -1 );
//    double2int_value = double_c.getInt();
//    ASSERT_TRUE( double2int_value == 9 );
//    //STRING
//    int string2int_value = string_a.getInt();
//    ASSERT_TRUE( string2int_value == 4 );
//    //BOOL
//    bool bool2int_value = bool_a.getInt();
//    ASSERT_TRUE( bool2int_value == 1 );
//    bool2int_value = bool_b.getInt();
//    ASSERT_TRUE( bool2int_value == 0 );
//}
//
//TEST( Item_Tests, getDouble ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    double null2double_value = null_a.getDouble();
//    ASSERT_TRUE( null2double_value == 0 );
//    //INT
//    double int2double_value = int_a.getDouble();
//    ASSERT_TRUE( int2double_value == 1 );
//    //DOUBLE
//    double double2double_value = double_a.getDouble();
//    ASSERT_TRUE( double2double_value == 1.000001 );
//    //STRING
//    double string2double_value = string_a.getDouble();
//    ASSERT_TRUE( string2double_value == 4 );
//    //BOOL
//    double bool2double_value = bool_a.getDouble();
//    ASSERT_TRUE( bool2double_value == 1 );
//    bool2double_value = bool_b.getDouble();
//    ASSERT_TRUE( bool2double_value == 0 );
//}
//
//TEST( Item_Tests, getString ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    std::string null2string_value = null_a.getString();
//    ASSERT_TRUE( null2string_value == "" );
//    //INT
//    std::string int2string_value = int_a.getString();
//    ASSERT_TRUE( int2string_value == "1" );
//    //DOUBLE
//    std::string double2string_value = double_a.getString();
//    ASSERT_TRUE( double2string_value == "1.000001" );
//    //STRING
//    std::string string2string_value = string_a.getString();
//    ASSERT_TRUE( string2string_value == "cool" );
//    //BOOL
//    std::string bool2string_value = bool_a.getString();
//    ASSERT_TRUE( bool2string_value == "TRUE" );
//    bool2string_value = bool_b.getString();
//    ASSERT_TRUE( bool2string_value == "FALSE" );
//}
//
//TEST( Item_Tests, getBool ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 5 );
//    eadlib::Item int_b = eadlib::Item( 0 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item double_b = eadlib::Item( 0 );
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item string_b = eadlib::Item( "" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    eadlib::Item bool_b = eadlib::Item( false );
//    //NULL
//    bool null2bool_value = null_a.getBool();
//    ASSERT_TRUE( null2bool_value == false );
//    //INT
//    bool int2bool_value = int_a.getBool();
//    ASSERT_TRUE( int2bool_value == true );
//    int2bool_value = int_b.getBool();
//    ASSERT_TRUE( int2bool_value == false );
//    //DOUBLE
//    bool double2bool_value = double_a.getBool();
//    ASSERT_TRUE( double2bool_value == true );
//    double2bool_value = double_b.getBool();
//    ASSERT_TRUE( double2bool_value == false );
//    //STRING
//    bool string2bool_value = string_a.getBool();
//    ASSERT_TRUE( string2bool_value == true );
//    string2bool_value = string_b.getBool();
//    ASSERT_TRUE( string2bool_value == false );
//    //BOOL
//    bool bool2bool_value = bool_a.getBool();
//    ASSERT_TRUE( bool2bool_value == true );
//    bool2bool_value = bool_b.getBool();
//    ASSERT_TRUE( bool2bool_value == false );
//}
//TEST( Item_Tests, getType ) {
//    eadlib::Item null_a = eadlib::Item();
//    eadlib::Item int_a = eadlib::Item( 1 );
//    eadlib::Item double_a = eadlib::Item( 1.000001 );
//    eadlib::Item string_a = eadlib::Item( "cool" );
//    eadlib::Item bool_a = eadlib::Item( true );
//    //NULL
//    ASSERT_TRUE( null_a.getType() == eadlib::Item::DataType::NONE );
//    //INT
//    ASSERT_TRUE( int_a.getType() == eadlib::Item::DataType::INT );
//    //DOUBLE
//    ASSERT_TRUE( double_a.getType() == eadlib::Item::DataType::DOUBLE );
//    //STRING
//    ASSERT_TRUE( string_a.getType() == eadlib::Item::DataType::STRING );
//    //BOOL
//    ASSERT_TRUE( bool_a.getType() == eadlib::Item::DataType::BOOL );
//}
//
//TEST( Item_Tests, swap ) {
//    eadlib::Item item1 = eadlib::Item();
//    eadlib::Item item2 = eadlib::Item( true );
//    eadlib::Item item3 = eadlib::Item( 10 );
//    eadlib::Item item4 = eadlib::Item( 1.123456 );
//    eadlib::Item item5 = eadlib::Item( "some string" );
//    item1.swap( item2 );
//    item2.swap( item3 );
//    item3.swap( item4 );
//    item4.swap( item5 );
//    EXPECT_TRUE( item5.getType() == eadlib::Item::DataType::NONE );
//    EXPECT_TRUE( item4.getType() == eadlib::Item::DataType::STRING && item4.getString() == "some string" );
//    EXPECT_TRUE( item3.getType() == eadlib::Item::DataType::DOUBLE && item3.getDouble() == 1.123456 );
//    EXPECT_TRUE( item2.getType() == eadlib::Item::DataType::INT && item2.getInt() == 10 );
//    EXPECT_TRUE( item1.getType() == eadlib::Item::DataType::BOOL && item1.getBool() );
//}
//
//void stressTest() {
//    std::string s = "Some string stuff";
//    eadlib::Item item = eadlib::Item( s );
//}
//
//void stressTest_baseline() {
//    std::string string = "Some string stuff";
//    std::string s = std::string( string );
//}
//
//void stressTest_baseline2() {
//    int integer = 10;
//    int i = int( integer );
//}
//
//TEST( Item_Tests, stress_assign ) {
//    /*
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 10000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 10000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 10000 );
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 100000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 100000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 100000 );
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 1000000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 1000000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 1000000 );
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 10000000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 10000000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 10000000 );
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 100000000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 100000000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 100000000 );
//    eadlib::testing::exec_time( stressTest_baseline2, "int construct", 1000000000 );
//    eadlib::testing::exec_time( stressTest_baseline, "string construct", 1000000000 );
//    eadlib::testing::exec_time( stressTest, "item construct", 1000000000 );
//     */
//
//}

#endif //EADLIB_TABLEDBCELL_TEST_H
