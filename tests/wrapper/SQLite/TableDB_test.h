#ifndef EADLIB_TABLEDB_TEST_H
#define EADLIB_TABLEDB_TEST_H

#include "gtest/gtest.h"
#include "../../../src/wrapper/SQLite/SQLite.h"
#include "../../../src/wrapper/SQLite/TableDB.h"

TEST( TableDB_Tests, at ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
	//Item & operator()( int column, int row )
    auto table = TableDB();
	auto db = SQLite();
    db.open( "testdata.db" );
    db.pull( "SELECT * FROM TableTest", table );
	db.close();
	//Row 1
	TableDBCell item_a = table.at( 0, 0 );
    TableDBCell item_b = table.at( 1, 0 );
    TableDBCell item_c = table.at( 2, 0 );
    auto container1 = item_a.getInt();
    auto container2 = item_b.getInt();
	auto container3 = item_c.getString();
    ASSERT_EQ( 1, container1 );
    ASSERT_EQ( 10, container2 );
    ASSERT_EQ( "Ten", container3 );
	//Row 2
	item_a = table.at( 0, 1 );
	item_b = table.at( 1, 1 );
	item_c = table.at( 2, 1 );
    container1 = item_a.getInt();
    container2 = item_b.getInt();
    container3 = item_c.getString();
    ASSERT_EQ( 2, container1 );
    ASSERT_EQ( 20, container2 );
    ASSERT_EQ( "Twenty", container3 );
	//Row 3
	item_a = table.at( 0, 2 );
	item_b = table.at( 1, 2 );
	item_c = table.at( 2, 2 );
    container1 = item_a.getInt();
    container2 = item_b.getInt();
    container3 = item_c.getString();
    ASSERT_EQ( 3, container1 );
    ASSERT_EQ( 30, container2 );
    ASSERT_EQ( "Thirty", container3 );
	//Row 4
	item_a = table.at( 0, 3 );
	item_b = table.at( 1, 3 );
	item_c = table.at( 2, 3 );
    container1 = item_a.getInt();
    container2 = item_b.getInt();
    container3 = item_c.getString();
    ASSERT_EQ( 4, container1 );
    ASSERT_EQ( 40, container2 );
    ASSERT_EQ( "Forty", container3 );
	//Row 5
	item_a = table.at( 0, 4 );
	item_b = table.at( 1, 4 );
	item_c = table.at( 2, 4 );
    container1 = item_a.getInt();
    container2 = item_b.getInt();
    container3 = item_c.getString();
    ASSERT_EQ( 5, container1 );
    ASSERT_EQ( 50, container2 );
    ASSERT_EQ( "Fifty", container3 );
}

TEST( TableDB_Tests, reset ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
    auto table = TableDB();
    auto test = SQLite();
    test.open( "testdata.db" );
    test.pull( "SELECT * FROM TableTest", table );
    test.close();
    table.reset();
    ASSERT_THROW( table.at( 0, 0 ), std::out_of_range );
    ASSERT_EQ( 0, table.getColCount() );
    ASSERT_EQ( 0, table.getRowCount() );
}

TEST( TableDB_Tests, clear ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
    auto table = TableDB();
    auto test = SQLite();
    test.open( "testdata.db" );
    test.pull( "SELECT * FROM TableTest", table );
    test.close();
    ASSERT_EQ( 3, table.getColCount() );
    ASSERT_EQ( 5, table.getRowCount() );
    table.clear();
    ASSERT_EQ( 3, table.getColCount() );
    ASSERT_EQ( "ID", table.getHeading( 0 ) );
    ASSERT_EQ( "data1", table.getHeading( 1 ) );
    ASSERT_EQ( "data2", table.getHeading( 2 ) );
    ASSERT_EQ( 0, table.getRowCount() );
    ASSERT_THROW( table.at( 0, 0 ), std::out_of_range );
}

TEST( TableDB_Tests, createColumn ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
    auto table = TableDB();
    ASSERT_EQ( 0, table.getColCount() );
    ASSERT_TRUE( table.createColumn( "integer", TableDBCell::DataType::INT ) );
    ASSERT_EQ( 1, table.getColCount() );
    ASSERT_TRUE( table.createColumn( "double", TableDBCell::DataType::DOUBLE ) );
    ASSERT_EQ( 2, table.getColCount() );
    ASSERT_TRUE( table.createColumn( "string", TableDBCell::DataType::STRING ) );
    ASSERT_EQ( 3, table.getColCount() );
    ASSERT_TRUE( table.createColumn( "bool", TableDBCell::DataType::BOOL ) );
    ASSERT_EQ( 4, table.getColCount() );
    table.lockStructure();
    ASSERT_FALSE( table.createColumn( "post_lock", TableDBCell::DataType::INT ) );
    ASSERT_EQ( 4, table.getColCount() );
}

TEST( TableDB_Tests, lockStructure ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
    auto table = TableDB();
    ASSERT_TRUE( table.createColumn( "integer", TableDBCell::DataType::INT ) );
    ASSERT_EQ( 1, table.getColCount() );
    ASSERT_FALSE( table.add( static_cast<long long int>( 100 ) ) );
    table.lockStructure();
    ASSERT_FALSE( table.createColumn( "new", TableDBCell::DataType::INT ) );
    ASSERT_EQ( 1, table.getColCount() );
    ASSERT_TRUE( table.add( static_cast<long long int>( 200 ) ) );
}

TEST( TableDB_Tests, add_NULL ) {
    using eadlib::TableDB;
    using eadlib::TableDBCell;
    using eadlib::wrapper::SQLite;
    auto table = TableDB();
    table.createColumn( "dataA", TableDBCell::DataType::INT );
    table.createColumn( "dataB", TableDBCell::DataType::INT );
    table.createColumn( "dataC", TableDBCell::DataType::INT );
    ASSERT_FALSE( table.add() );
    table.lockStructure();
    //Write/Read tests
    for( int i = 1; i <= 300; i++ ) {
        ASSERT_TRUE( table.add() );
    }
    for( int i = 0, count = 1; i < 100; i++ ) { //rows
        for( int j = 0; j < 3; j++, count++ ) { //columns
            auto cell = table.at( j, i );
            auto val = cell.getString();
            ASSERT_EQ( "", val );
        }
    }
}

//TEST( TableDB_Tests, add_INT ) {
//    Table table = Table();
//    table.createColumn( 1, "dataA" );
//    table.createColumn( 1, "dataB" );
//    table.createColumn( 1, "dataC" );
//    bool returned = table.add( 666 );
//    ASSERTM( "Testing add(int) returned 1", returned == false );
//    table.lockTableStructure();
//    //Write/Read tests
//    int container { };
//    for( int i = 1; i <= 300; i++ ) {
//        returned = table.add( i );
//        ASSERTM( "Testing add(int) write successes", returned == true );
//    }
//    for( int i = 0, count = 1; i < 100; i++ ) { //rows
//        for( int j = 0; j < 3; j++, count++ ) { //columns
//            Item item = table( j, i );
//            item.getValue( container );
//            ASSERTM( "Testing add(int) read successes", container == count );
//        }
//    }
//}
//
//TEST( TableDB_Tests, add_DOUBLE ) {
//    Table table = Table();
//    table.createColumn( 2, "dataA" );
//    table.createColumn( 2, "dataB" );
//    table.createColumn( 2, "dataC" );
//    bool returned = table.add( 666.666 );
//    ASSERTM( "Testing add(double) returned 1", returned == false );
//    table.lockTableStructure();
//    //Write/Read tests
//    double container { };
//    double value { 1.123456 };
//    for( int i = 1; i <= 300; i++ ) {
//        returned = table.add( value );
//        ASSERTM( "Testing add(double) write successes", returned == true );
//        value += 1;
//    }
//    double count { 1.123456 };
//    for( int i = 0; i < 100; i++ ) { //rows
//        for( int j = 0; j < 3; j++ ) { //columns
//            Item item = table( j, i );
//            item.getValue( container );
//            ASSERTM( "Testing add(double) read successes", container == count );
//            count += 1;
//        }
//    }
//}
//
//TEST( TableDB_Tests, add_STRING ) {
//    Table table = Table();
//    table.createColumn( 3, "dataA" );
//    table.createColumn( 3, "dataB" );
//    table.createColumn( 3, "dataC" );
//    bool returned = table.add( "some data" );
//    ASSERTM( "Testing add(string) returned 1", returned == false );
//    table.lockTableStructure();
//    //Write/Read tests
//    std::string container { };
//    for( int i = 1; i <= 300; i++ ) {
//        returned = table.add( "some data" );
//        ASSERTM( "Testing add(string) write successes", returned == true );
//    }
//    for( int i = 0; i < 100; i++ ) { //rows
//        for( int j = 0; j < 3; j++ ) { //columns
//            Item item = table( j, i );
//            item.getValue( container );
//            ASSERTM( "Testing add(int) read successes", container == "some data" );
//        }
//    }
//}
//
//TEST( TableDB_Tests, getHeading ) {
//void getHeading__test() {
//    Table table = Table();
//    ASSERTM( "Testing getHeading() 1", table.getHeading( 0 ) == "INVALID COL" );
//    table.createColumn( 1, "integer" );
//    ASSERTM( "Testing getHeading() 2", table.getHeading( 0 ) == "integer" );
//    table.createColumn( 2, "double" );
//    ASSERTM( "Testing getHeading() 3", table.getHeading( 1 ) == "double" );
//    ASSERTM( "Testing getHeading() 4", table.getHeading( 2 ) == "INVALID COL" );
//}
//
//TEST( TableDB_Tests, getType ) {
//    Table table = Table();
//    table.createColumn( 1, "Int_Col" );
//    table.createColumn( 2, "Double_Col" );
//    table.createColumn( 3, "String_Col" );
//    table.createColumn( 1000, "Bool_Col" );
//    ASSERTM( "Testing getType() 1 )", table.getType( 0 ) == dataType::INTEGER );
//    ASSERTM( "Testing getType() 2 )", table.getType( 1 ) == dataType::DOUBLE );
//    ASSERTM( "Testing getType() 3 )", table.getType( 2 ) == dataType::STRING );
//    ASSERTM( "Testing getType() 5 )", table.getType( 3 ) == dataType::BOOLEAN );
//    ASSERTM( "Testing getType() 6 )", table.getType( 4 ) == dataType::NONE );
//}
//
//
//TEST( TableDB_Tests, getColumnCount ) {
//    Table table = Table();
//    ASSERTM( "Testing getColumnCount() 1", table.getColumnCount() == 0 );
//    table.createColumn( 1, "integer" );
//    ASSERTM( "Testing getColumnCount() 2", table.getColumnCount() == 1 );
//    table.createColumn( 2, "double" );
//    ASSERTM( "Testing getColumnCount() 3", table.getColumnCount() == 2 );
//    table.createColumn( 3, "string" );
//    ASSERTM( "Testing getColumnCount() 4", table.getColumnCount() == 3 );
//}
//
//TEST( TableDB_Tests, getRowCount ) {
//    Table table = Table();
//    table.createColumn( 1, "data" );
//    table.lockTableStructure();
//    ASSERTM( "Testing getRowCount() 0 rows", table.getRowCount() == 0 );
//    for( int i = 1; i < 1000; i++ ) {
//        table.add( i );
//        ASSERTM( "Testing getRowCount() 1-1000 rows", table.getRowCount() == i );
//    }
//    Item last = table( 0, ( table.getRowCount() - 1 ) );
//    int container { };
//    last.getValue( container );
//    ASSERTM( "Testing getRowCount()'s last value added", container == 999 );
//}
//
//TEST( TableDB_Tests, findColumn ) {
//    Table table = Table();
//    table.createColumn( 1, "Int_Col" );
//    table.createColumn( 2, "Double_Col" );
//    table.createColumn( 3, "String_Col" );
//    table.createColumn( 1000, "Bool_Col" );
//    ASSERTM( "Testing whereIsThisColumn(..) 1", table.whereIsThisColumn( "Int_Col" ) == 0 );
//    ASSERTM( "Testing whereIsThisColumn(..) 2", table.whereIsThisColumn( "Double_Col" ) == 1 );
//    ASSERTM( "Testing whereIsThisColumn(..) 3", table.whereIsThisColumn( "String_Col" ) == 2 );
//    ASSERTM( "Testing whereIsThisColumn(..) 4", table.whereIsThisColumn( "Bool_Col" ) == 3 );
//    ASSERTM( "Testing whereIsThisColumn(..) 5", table.whereIsThisColumn( "Unknown" ) == -1 );
//}
//
//TEST( TableDB_Tests, stress_test ) {
//    Table table = Table();
//    table.createColumn( 1, "col1" );
//    table.createColumn( 1, "col2" );
//    table.createColumn( 1, "col3" );
//    table.createColumn( 1, "col4" );
//    table.createColumn( 1, "col5" );
//    table.createColumn( 1, "col6" );
//    table.createColumn( 1, "col7" );
//    table.createColumn( 1, "col8" );
//    table.createColumn( 1, "col9" );
//    table.createColumn( 1, "col10" );
//    table.lockTableStructure();
//    int count { 10000000 };
//    std::chrono::high_resolution_clock::time_point t1 =
//            std::chrono::high_resolution_clock::now();
//    for( int i = 0; i < count; i++ ) {
//        table.add( i );
//    }
//    std::chrono::high_resolution_clock::time_point t2 =
//            std::chrono::high_resolution_clock::now();
//    auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
//    Item last = table( 9, 999999 );
//    int container { };
//    last.getValue( container );
//    ASSERTM( "Stress test with 1 million items in Table", container == 9999999 );
//    std::cout << "  -> Adding " << count << " Items took: " << duration << " microseconds ("
//            << ( duration / 1000000 ) << " secs)" << std::endl;
//}


#endif //EADLIB_TABLEDB_TEST_H
